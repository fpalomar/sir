<?php
	require 'config.php';
	require 'aws/aws-autoloader.php';
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	
	if(isset($argv[1], $argv[2], $argv[3])) {
		$file = $argv[1];
		$category = $argv[2];
		$fileType = $argv[3];
		$pathParts = pathinfo($file);
		$route = $fileType."/".$category."/".$pathParts['basename'];
		
		$s3Client = S3Client::factory(array(
			'key'    => AWS_KEY,
			'secret' => AWS_SECRET,
		));
		
		try {
			$result = $s3Client->deleteObject(array(
				'Bucket' 	  => AWS_BUCKET,
				'Key'    	  => $route
			));
		}
		catch (S3Exception $e) {
		}
	}