		<script>
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo URL; ?>public/themes/ace/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<!-- Inline scripts -->
		<script>
			$(function() {
				// Forgot password / login box
				$('.toolbar a[data-target]').click(function(e) {
					e.preventDefault();
					var target = $(this).data('target');
					$('.widget-box.visible').removeClass('visible'); // Hide others
					$(target).addClass('visible');	// Show target
				});
				// Remember me
				if (localStorage.c && localStorage.c !== '') {
                    $('#remember-me').attr('checked', 'checked');
                    $('[name="login"]').val(localStorage.l);
                    $('[name="pass"]').val(localStorage.p);
                }
				else {
                    $('#remember-me').removeAttr('checked');
                    $('[name="login"]').val('');
                    $('[name="pass"]').val('');
                }
                $('#remember-me').click(function() {
                    if ($('#remember-me').is(':checked')) {
                        // Save login and password
                        localStorage.l = $('[name="login"]').val();
                        localStorage.p = $('[name="pass"]').val();
                        localStorage.c = $('#remember-me').val();
                    }
					else {
                        localStorage.l = '';
                        localStorage.p = '';
                        localStorage.c = '';
                    }
                });
				// Validation
				var LoginValCount = 0;
				var PassValCount = 0;
				$('#login').formValidation({
					framework: 'bootstrap',
					icon: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'						
					},
					fields: {
						login: {
							validators: {
								notEmpty: {
									message: 'error',
									onError: function(e, data) {
										if(LoginValCount === 0) {
											$.noty.setText(notifications('error', 100).options.id, '\u00a1Oh no! Debes escribir tu correo');
											LoginValCount++;
										}
									},
									onSuccess: function(e, data) {
										LoginValCount = 0;
										$.noty.close(100);
									}
								}
							}
						},
						pass: {
							validators: {
								notEmpty: {
									message: 'error',
									onError: function(e, data) {
										if(PassValCount === 0) {
											$.noty.setText(notifications('error', 200).options.id, '\u00a1Oh no! La contrase\u00f1a no puede estar vac\u00eda');
											PassValCount++;
										}
									},
									onSuccess: function(e, data) {
										PassValCount = 0;
										$.noty.close(200);
									}
								}
							}
						}
					}
				}).on('success.form.fv', function(e) {
					// Animate submit click
					e.preventDefault();
					$.gritter.removeAll();
					$('.container').addClass('fadeOut');
					setTimeout(function() {
						$('#login')[0].submit();
					}, 1000);
				}).on('err.field.fv', function(e, data) {
					// $(e.target)  --> The field element
					// data.fv      --> The FormValidation instance
					// data.field   --> The field name
					// data.element --> The field element
					
					// Hide the messages
					data.element
						.data('fv.messages')
						.find('.help-block[data-fv-for="' + data.field + '"]').hide();
				}).on('success.field.fv', function(e, data) {
					if (data.fv.getSubmitButton()) {
						data.fv.disableSubmitButtons(false);
					}
				});
				// Notifications
				function notifications(type, id) {
					if(typeof id !== 'undefined')	
						var n = noty({
							id			: id,
							text        : type,
							type        : type,
							dismissQueue: true,
							killer		: false,
							layout      : 'bottom',
							maxVisible  : 1
						});
					else
						var n = noty({
							text        : type,
							type        : type,
							dismissQueue: true,
							killer		: false,
							layout      : 'bottom',
							maxVisible  : 1
						});
					return n;
				}
				// Get URL parameter
				$.extend({
				  getUrlVars: function(){
					var vars = [], hash;
					var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
					for(var i = 0; i < hashes.length; i++)
					{
					  hash = hashes[i].split('=');
					  vars.push(hash[0]);
					  vars[hash[0]] = hash[1];
					}
					return vars;
				  },
				  getUrlVar: function(name){
					return $.getUrlVars()[name];
				  }
				});
				var e = $.getUrlVar('e');
				if(typeof e !== 'undefined') {
					$.gritter.add({
						// (string | mandatory) the heading of the notification
						title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
						// (string | mandatory) the text inside the notification
						text: '<i class="ace-icon fa fa fa-database"></i> ' + decodeURIComponent(e) + '<br />Por favor, intenta de nuevo.',
						sticky: true,
						class_name: 'gritter-error'
					});
					// Shake login box
					$('.container').removeClass('hide').addClass('animated fadeIn');
					setTimeout(function () {
						$('.container').removeClass('fadeIn').addClass('shake');}, 400
					);
				}
				else {
					// Animate Header
					setTimeout(function () {
						$('.container').removeClass('hide').show().addClass('animated fadeIn');}, 25
					);
				}
			});
		</script>
	</body>
</html>
