<h1>Programa</h1>
<table width="100%" id="listado" border="0">
	<tr>
		<td colspan="8"><hr></td>
	</tr>
	<tr>
		<td colspan="8">
			<div id="Busqueda">
				<form name="buscar" action="<?php echo URL; ?>program/search"
					method="post">
					<input type="text" name="programaTitulo" id="programaTitulo" placeholder="T�tulo"> 
					<input type="text" name="programaDescrip" id="programaDescrip" placeholder="Descripci�n"> 
					<input type="text" name="programaFechaIngreso" id="programaFechaIngreso" placeholder="Fecha de Ingreso"> 
					<input type="button" name="enviar" value="Buscar" id="botones" onclick="javascript:search(this.form);">
					<input type="button" name="Limpiar" value="Limpiar" id="botones" onclick="javascript:clearPrograma(this.form);">
				</form>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="8"><hr></td>
	</tr>
	<tr id="programTitles">
		<td id="buscador" width="5%">&nbsp;</td>
		<td id="buscador" width="18%"><p>T&iacute;tulo</p></td>
		<td id="buscador" width="30%"><p>Descripci&oacute;n</p></td>
		<td id="buscador" width="18%"><p>Fecha</p></td>
		<td id="buscador" width="5%"><p>Editar</p></td> 
		<!--<td id="buscador" width = 2%><p>Eliminar</p></td>-->
		<td id="buscador" width="6%"><p>Publicar</p></td>
		<td id="buscador" width="8%"><p>Despublicar</p></td>
		<td id="buscador" width="6%"><p>Eliminar</p></td>
	</tr>
	
	</table>
	    <form method="post" action="<?php echo URL; ?>program/generateXml" name="varios" id="varios"> 	
	
   		<table width="100%" id="resultados" border="0">
		 <?php foreach ($this->programList as $key => $value) {
		        if(($key%2) == 1){
		            echo '<tr bgcolor="#F2F2F2">';
		        }else{
		            echo '<tr>';
		        }
				if($value['programaStatus'] == 0)
					$statusImage = URL.'public/images/published.png';
				else
					$statusImage = URL.'public/images/saved.png';
				echo '<td width="5%">' . '<img src="'. $statusImage .  '" alt="estatus"></td>';	
		        echo '<td width="18%">' . $value['programaTitulo'] . '</td>';
		        echo '<td width="30%">' . $value['programaDescrip'] . '</td>';
		        echo '<td width="18%">' . $value['programaFechaIngreso'] . '</td>';
		        echo '<td width="6%"><a href = "' . URL . 'program/edit/' . $value['idprograma'] . '"><center><img src="'.URL.'public/images/editar.png" alt="Editar" width="16" heigth="16"></center></a></td>';
		       //echo '<td width="5%"><a class = "delete" href="' . URL . 'program/delete/' . $value['idprograma'] . '"><center><img src="'.URL.'public/images/eliminar.png" alt="Eliminar" width="16" heigth="16"><center></a></td>';
		        echo '<td width="7%"><center><input type="checkbox" name="editar" value="'.$value['idprograma'].'" id="ediarV"></center></td>';
				echo '<td width="8%"><center><input type="checkbox" name="despublicar" value="'.$value['idprograma'].'" id="despublicarV"></center></td>';
		        echo '<td width="7%"><center><input type="checkbox" name="borrar" value="'.$value['idprograma'].'" id="eliminarV"></center></td>';
		        
		        echo '</tr>';
		    }?>  
    	</table>
		<table align="right" id="programButtons">
			<tr>
				<?php if (Session::get('rol') == 'admin'):?>
				<td><input type="button" onclick="javascript:generateXML('nuevo','');" value="Publicar contenido" name="Generar XML" id="botones"/></td>
				<?php endif; ?>
				<td><input name="generar XML" type="button" value="Publicar selecc." id="botones" class ="editar"></td>
				<td><input name="generar XML" type="button" value="Despublicar selecc." id="botones" class ="despublicar"></td>
				<td><input name="generar XML" type="button" value="Borrar selecc." id="botones" class = "delete"></td>
   			</tr>
   		</table>
    </form>  
    		
   		<table width="100%"  border="0">	
   		<tr>
    		<td colspan="4"><hr></td>
		</tr>
	<tr id="programPaginate">
		<td colspan="4">
			<?php 
   				 $v = $this->nPaginar;
				 $separator = "...";
				 echo '<div id="paginador">';
   				 for($i = 1 ; $i<=$v; $i++){
   				 	if($i == $this->wiam) {
						echo '<div id="pagina"><strong>'.$i.'</strong></div>';
					}
					else if($i == 1 || $i == $v || ($i >= $this->wiam - RPP && $i <= $this->wiam + RPP)) {
						if($i == $this->wiam - RPP && $i > 1 + 1)
							echo $separator;
						echo '<div id="pagina"><a href="'. URL.'program/show/'.$i.'">'.$i.'</a></div>';
						if($i == $this->wiam + RPP && $i < $v - 1)
							echo $separator;
					}
   				 }
   				 echo '</div>';
			?>	
		</td>
	</tr>
   		</table>

<div id="addElement" style="font: 100% verdana,arial,sans-serif">
	<span> 
		<a id="addDisplayLink" onclick="javascript:toggle('formulario',2);" style="font-size: 20px;">+</a>
	</span>
	<span style="font-size: 15px; font-weight: bolder;"><a id="addDisplayText" onclick="javascript:toggle('formulario',2);">A&ntilde;adir Programa</a></span>
</div>		

<div id="formulario" style="display:none">
	<img id="opener" src="<?php echo URL;?>/public/images/ayuda.png" alt="A" title="Ayuda (?)">
	<img id="opener2" src="<?php echo URL;?>/public/images/tip.png" alt="T" title="Tip (!)" style="margin-top:400px;">
	
	<form method="post" action="<?php echo URL; ?>program/create" id="form">
		<div class="dropzone" id="dropzone">
			<div id="thumbnail"></div>
			<span id="textThumb">Thumbnail<span>o</span></span>
		</div>
		<label for="programaTitulo">T&iacute;tulo</label> 
		<input id="programaTitulo" type="text" name="titulo" onblur="namecheck(this.value)"><span id="namecheck"></span>
		<label for="programaBranch">Categor&iacute;a</label>
		<select id="programaBranch"  name="branches">
			<option selected>-- Seleccione una categor&iacute;a --</option>
			<?php
				foreach($this->branchList as $key => $value) {
					echo '<option name="'.$value['nombre'].'" value="'.$value['idBranch'].'">'.$value['nombre'].'</option>';
				}
			?>
		</select>
		<label for="programaHorario">Horario</label>
		<p id="programaHorario"><input id="programaHoraInicio" type="text" name="horaInicio" class="time start" style="margin-right: 10px;"> a <input id="programaHoraFin" type="text" name="horaFin" class="time end"></p>
		<label for="programaUrl">Url del Programa</label> 
		<input id="programaUrl" type="text" name="url"> 
		<label for="programaDescrip">Descripci&oacute;n</label>
		<textarea id="programaDescrip" name="descripcion"></textarea>
		<label for="programatThumbmail" id="UrlThumb">Url del Thumbnail</label> 
		<input id="programatThumbmail" type="text" name="thumbmail">
		<label for="programaKeywords">Keywords</label>
        <input id ="programaKeywords" type="text" name="keywords">		
		<label for="programaUrlCapitulosCompletos">Url capitulos completos</label> 
		<input id="programaUrlCapitulosCompletos" type="text" name="urlCapitulos"> 
		<label for="programaUrlNoticias">Url noticias</label> 
		<input id="programaUrlNoticias" type="text" name="urlNoticias"> 
		<label for="programaUrlFotos">Url fotos</label> 
		<input id="programaUrlFotos" type="text" name="urlFotos"> 
		<label for="programaUrlVideos">Url Videos</label>
		<input id="programaUrlVideos" type="text" name="urlVideos"> 
		<label for="programaUrlFacebook">Url Facebook</label>
		<input id="programaUrlFacebook" type="text" name="urlFacebook">
		<label for="programaUrlTwitter">Url Twitter</label> 
		<input id="programaUrlTwitter" type="text" name="urlTwitter"> 
		<label for="programaUrlYoutube">Url Youtube</label> 
		<input id="programaUrlYoutube" type="text" name="urlYoutube">
		<label for="programaUrlInstagram">Url Instagram</label> 
		<input id="programaUrlInstagram" type="text" name="urlInstagram">		
		<label for="programaUrlPinterest">Url Pinterest</label> 
		<input id="programaUrlPinterest" type="text" name="urlPinterest">
		<label for="programaUrlGplus">Url Google plus</label> 
		<input id="programaUrlGplus" type="text" name="urlGplus"> 
		<label for="programaContent">Contenido</label>
		<textarea id="programaContent" name="content"></textarea>
		<label style="display:none;" for="programaConsultRelacionada">Consulta Relacionada</label>
		<textarea style="display:none;" id="programaConsultRelacionada" name="sinonimo"></textarea>

		<input type="hidden" id="programaFechaIngreso"
			name="programaFechaIngreso"
			value="<?php echo date("Y-m-d H:i:s"); ?>">

		<div id="acciones">
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrar('existente', '-1');">-</a> <a
					onclick="javascript:mostrar('existente', '1');">+</a>
				</span> Persona Relacionada existente
			</p>
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrar('nueva', '-1');">-</a> <a
					onclick="javascript:mostrar('nueva', '1');">+</a>
				</span> Nueva Persona Relacionada
			</p>
		</div>

		<select name="personaRelacionada" id="personaRelacionada" size="5">
            <?php
            foreach ($this->personaLista as $key => $value) {
                echo '<option value="' . $value['idPersona'] . '">' . $value['personaNom'] . '</option>';
            }
            ?>
        </select>
       	<div id="orden">
	        <input name="del" value="<<" type="button" onclick="moverElementos(this.form,1);" id="pRelacionada2"/>
			<input name="add" value=">>" type="button" onclick="moverElementos(this.form,0);" id="pRelacionada3"/>
		</div>
		<select name="personaRelacionada2" size="5" id="personaRelacionada2" size="5">
			
		</select>
		

		<div id="relacionNueva"></div>

		<div id="acomodarG">
			
			
			<p id="mensaje"></p>
			<input type="button" onclick="javascript:send(this.form, 'mensaje','personaRelacionada','a');" value="Guardar" name="Guardar" id="botones" />
		</div>

	</form>
</div>

<div id="help_dialog" title="Ayuda (?)"><img src="<?php echo URL;?>/public/images/help.png"></div>
<div id="tip_dialog" title="Tip (!)"><img src="<?php echo URL;?>/public/images/informacion.png"></div>


<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.timepicker.css';?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.noty.css';?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL.'public/css/jquery.qtip.css';?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL.'public/css/jquery.nailthumb.css';?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'/public/css/dropzone.css';?>"/>
<script src="<?php echo URL.'public/js/jquery.timepicker.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.datepair.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.noty.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/dropzone.js';?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.slickhover.js';?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo URL.'public/js/jquery.qtip.js'; ?>"></script>
<script type="text/javascript" src="<?php echo URL.'public/js/jquery.nailthumb.js'; ?>"></script>		
<script>
var aviability = true;
function namecheck(name) {
	$('#namecheck').html('<img src="' + URL + 'public/images/loading.gif" />');
	$.post(URL + "program/availability", {n: name} , function(data) {
		$('#namecheck').show();
		$('.qtip').each(function(){
			$(this).data('qtip').destroy();
		});
		var objects = jQuery.parseJSON(data);
		if(objects && objects.length !== 0) {
			aviability = false;
			$('#namecheck').delay(1000).fadeOut(400, function() {
				var link = "";
				var image = "";
				if(objects[0].edit)
					link = '<tr><td colspan="2" align="center"><a href="' + objects[0].edit + '">Editar</a></td></tr>';
				if(objects[0].thumb)
					image = '<td><div class="thumb-container name-image"><img src="' + objects[0].thumb + '" alt="' + objects[0].name + '" width="75" height="75"></div></td>';
				$('#namecheck').html('<img id="nameerror" src="' + URL + 'public/images/error.png" />').fadeIn(400);
				$("input[id=programaTitulo]").css("border", "solid 1px #FA5858");
				$('#nameerror').qtip({
					content: '<table border="0"><tr>' + image + '<td align="center"><h2>' + objects[0].name + '</h2>ya existe en la base de datos</td></tr>' + link + '</table>',
					style: {
						classes: 'qtip qtip-red qtip-shadow qtip-rounded'
					},
					show: {
						ready: true
					},
					hide: {
						event: false
					},
					events: {
						show: function () {
							$('.thumb-container').nailthumb({
								width: 75,
								height: 75
							});
						}
					}
				});
			});
		}	
		else {	
			aviability = true;
			$("input[id=programaTitulo]").css("border", "");
			$('#namecheck').delay(1000).fadeOut(400, function() {
				$('#namecheck').html('')
			});
		}
	}, 'json');
}
$(function() {
	
	$('#programaHorario .time').timepicker({
        'showDuration': true,
		'timeFormat': 'g:i a'
    });
	$('#programaHorario').datepair();

	$( "#help_dialog" ).dialog({ autoOpen: false, width: 999 });
	$( "#tip_dialog" ).dialog({ autoOpen: false, width: 939 });
	$( "#opener" ).click(function() {
		$( "#help_dialog" ).dialog( "open" );
	});
	$( "#opener2" ).click(function() {
		$( "#tip_dialog" ).dialog( "open" );
	});
	
    $('.delete').click(function(e) {
    	var checkboxes = $("form input:checkbox");
    	var cont = 0;
    	var vv = new Array();
	    for (var x=0; x < checkboxes.length; x++) {
			if (checkboxes[x].checked == true && checkboxes[x].name == 'borrar') {
				cont++;
				vv.push(checkboxes[x].value);
			}
	    }
	    if(cont == 0){
	    	alert("Seleccione los elementos a eliminar");
	    }else{
        
	        var c = confirm("\u00BFEst\u00E1 seguro de eliminar completamente?");
	        if (c == false)
	            return false;
	        else if(c == true){
	            eliminarV(vv);
	        }
	    }
    });
    
	$('.despublicar').click(function(e) {
    	var checkboxes = $("form input:checkbox");
    	var cont = 0;
    	var vv = new Array();
	    for (var x=0; x < checkboxes.length; x++) {
			if (checkboxes[x].checked == true && checkboxes[x].name == 'despublicar') {
				cont++;
				vv.push(checkboxes[x].value);
			}
	    }
	    if(cont == 0){
	    	alert("Seleccione los elementos para despublicar");
	    }else{
				guardarV(vv);
	        
	    }
    });
	
    $('.editar').click(function(e) {
    	var checkboxes = $("form input:checkbox");
    	var cont = 0;
    	var vv = new Array();
	    for (var x=0; x < checkboxes.length; x++) {
			if (checkboxes[x].checked == true && checkboxes[x].name == 'editar') {
				cont++;
				vv.push(checkboxes[x].value);
			}
	    }
	    if(cont == 0){
	    	alert("Seleccione los elementos para generar el XML");
	    }else{
	            generarXMLVarios(vv);
	        
	    }
    });
	var hover = function() {
		$("#thumbnail").slickhover({
			icon: URL + "public/images/slickhover/plus-black.png",
			animateIn: true,
			opacity: 0.1,
			speed: 600
		});
	}
	var removeHover = function() {
		$("#thumbnail").off();
		$(".iconslickhoverWrapper").remove();
		var slickhoverContent = $(".slickhoverWrapper").contents();
		$(".slickhoverWrapper").replaceWith(slickhoverContent);
		$("#thumbnail").css("opacity", "");
	}
	Dropzone.autoDiscover = false;
	$("#thumbnail").dropzone({ 
		url: URL + "<?php echo IMG_PROGRAM_UPLOAD_HANDLER; ?>",
		maxFilesize: "<?php echo IMG_MAX_UPLOAD_SIZE; ?>",
		addRemoveLinks: true,
		acceptedFiles: "image/*",
		dictInvalidFileType: "No es un formato de imagen v\u00E1lido",
		dictFileTooBig: "La imagen es demasiado grande. <?php echo IMG_MAX_UPLOAD_SIZE; ?> MB m\u00E1ximo",
		dictResponseError: "Hubo un error en el servidor",
		dictRemoveFile: "Remover",
		accept: function(file, done) {
			console.log("accept");
			removeHover();
			done();
		},
		success: function(file, response) {
			console.log("success");
			removeHover();
			response = JSON.parse(response);
			if(response.response == "error") {
				var node, _i, _len, _ref, _results;
				var message = response.serversays;
				file.previewElement.classList.add("dz-error");
				_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
				_results = [];
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				  node = _ref[_i];
				  _results.push(node.textContent = message);
				}
				return _results;
			}
			else {
				$("#programatThumbmail").val(response.image_url).prop("readonly", true).css({'background-color' :  '#C0C0C0'});
				return file.previewElement.classList.add("dz-success");
			}
		},
		error: function (file, message) {
			console.log("error");
			removeHover();
			var node, _i, _len, _ref, _results;
			if (file.previewElement) {
			  file.previewElement.classList.add("dz-error");
			  if (typeof message !== "String" && message.error) {
				message = message.error;
			  }
			  _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
			  _results = [];
			  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				node = _ref[_i];
				_results.push(node.textContent = message);
			  }
			  return _results;
			}
		},
		removedfile: function(file) {
			console.log("removed");
			hover();
			$("#programatThumbmail").val("").prop("readonly", false).css({'background-color' :  ''});
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		},
		init: function () {
			console.log("init");
			hover();
		}
	});
});
</script>