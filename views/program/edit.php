<h1>Programa: Editar</h1>
<div id="formulario">
		
		<img id="opener" src="<?php echo URL;?>/public/images/ayuda.png" alt="A" title="Ayuda (?)">
		<img id="opener2" src="<?php echo URL;?>/public/images/tip.png" alt="T" title="Tip (!)">
		<form method="post" action="<?php echo URL; ?>program/editSave/<?php echo $this->program[0]['idprograma'];?>"/>
		<div class="dropzone dropzoneEdit" id="dropzone">
			<div id="thumbnail"></div>
			<span id="textThumb">Thumbnail<span>o</span></span>
		</div>
		<label for="programaTitulo">T&iacute;tulo</label>
		<?php
			$readonly = '';
			if($this->program[0]['programaStatus'] == 0)
				$readonly = ' readonly="readonly" ';
		?>		
		<input id ="programaTitulo" type="text" name="titulo" value="<?php echo  htmlspecialchars($this->program[0]['programaTitulo']);?>"<?php echo $readonly; ?>/>
		<label for="programaBranch">Categor&iacute;a</label>
		<select id="programaBranch" name="branches">
			<option>-- Seleccione una categor&iacute;a --</option>
			<?php
				foreach($this->branchList as $key => $value) {
					if($this->program[0]['programaBranch'] == $value['idBranch'])
						$selected = " selected";
					else
						$selected = "";
					echo '<option name="'.$value['nombre'].'" value="'.$value['idBranch'].'"'.$selected.'>'.$value['nombre'].'</option>';
				}
			?>
		</select>
		<label for="programaHorario">Horario</label>
		<p id="programaHorario"><input id="programaHoraInicio" type="text" name="horaInicio" class="time start" style="margin-right: 10px;" value="<?php echo $this->program[0]['programaHoraInicio'];?>"> a <input id="programaHoraFin" type="text" name="horaFin" class="time end" value="<?php echo $this->program[0]['programaHoraFin']; ?>"></p>		
		<label for="programaUrl">Url del Programa</label> 
		<input id ="programaUrl" type="text" name="url" value="<?php echo $this-> program[0]['programaUrl'];?>"/>
		<label for="programaDescrip">Descripci&oacute;n</label>
		<textarea id="programaDescrip" name="descripcion" ><?php echo $this->program[0]['programaDescrip'];?></textarea>
		<label for="programatThumbmail" id="UrlThumb">Url del Thumbnail</label> 
		<input id ="programatThumbmail" type="text" name="thumbmail" value="<?php echo $this->program[0]['programatThumbmail'];?>">
		<label for="programaKeywords">Keywords</label>
        <input id ="programaKeywords" type="text" name="keywords" value="<?php echo $this->program[0]['programaKeywords'];?>">		
		<label for="programaUrlCapitulosCompletos">Url capitulos completos</label> 
		<input id ="programaUrlCapitulosCompletos" type="text" name="urlCapitulos" value="<?php echo $this->program[0]['programaUrlCapitulosCompletos'];?>"> 
		<label for="programaUrlNoticias">Url noticias</label> 
		<input id ="programaUrlNoticias" type="text" name="urlNoticias" value="<?php echo $this->program[0]['programaUrlNoticias'];?>"> 
		<label for="programaUrlFotos">Url fotos</label> 
		<input id ="programaUrlFotos" type="text" name="urlFotos"  value="<?php echo $this->program[0]['programaUrlFotos'];?>"> 
		<label for="programaUrlVideos">Url Videos</label>
		<input id ="programaUrlVideos" type="text" name="urlVideos" value="<?php echo $this->program[0]['programaUrlVideos'];?>"> 
		<label for="programaUrlFacebook">Url Facebook</label>
		<input id ="programaUrlFacebook" type="text" name="urlFacebook" value="<?php echo $this->program[0]['programaUrlFacebook'];?>">
		<label for="programaUrlTwitter">Url Twitter</label> 
		<input id ="programaUrlTwitter" type="text" name="urlTwitter" value="<?php echo $this->program[0]['programaUrlTwitter'];?>"> 
		<label for="programaUrlYoutube">Url Youtube</label> 
		<input id ="programaUrlYoutube" type="text" name="urlYoutube"  value="<?php echo $this->program[0]['programaUrlYoutube'];?>">
		<label for="programaUrlInstagram">Url Instagram</label> 
		<input id ="programaUrlInstagram" type="text" name="urlInstagram" value="<?php echo $this->program[0]['programaUrlInstagram'];?>">
		<label for="programaUrlPinterest">Url Pinterest</label> 
		<input id ="programaUrlPinterest" type="text" name="urlPinterest" value="<?php echo $this->program[0]['programaUrlPinterest'];?>">
		<label for="programaUrlGplus">Url Google plus</label> 
		<input id ="programaUrlGplus" type="text" name="urlGplus" value="<?php echo $this->program[0]['programaUrlGplus']?>"> 
		<label for="programaContent">Contenido</label>
		<textarea id="programaContent" name="content"><?php echo $this->program[0]['programaContent'];?></textarea>
		<label style="display:none;" for="programaConsultRelacionada">Consulta Relacionada</label>
		<textarea style="display:none;" id="programaConsultRelacionada" name="sinonimo"><?php echo $this->program[0]["programaConsultRelacionada"] ?></textarea>

		<input type="hidden" id="programaFechaIngreso" name="programaFechaIngreso" value="<?php echo date("Y-m-d H:i:s"); ?>">

		 <div id="acciones">
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrar('existente', '-1');">-</a> <a
					onclick="javascript:mostrar('existente', '1');">+</a>
				</span> Persona Relacionada existente
			</p>
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrar('nueva', '-1');">-</a> <a
					onclick="javascript:mostrar('nueva', '1');">+</a>
				</span> Nueva Persona Relacionada
			</p>
		</div>
		<select name="personaRelacionada" id="personaRelacionada" size="5">
            <?php
            foreach ($this->personaLista as $key => $value) {
                echo '<option value="' . $value['idPersona'] . '">' . $value['personaNom'] . '</option>';
            }
            ?>
        </select>
       	<div id="orden">
	        <input name="del" value="<<" type="button" onclick="moverElementos(this.form,1);" id="pRelacionada2"/>
			<input name="add" value=">>" type="button" onclick="moverElementos(this.form,0);" id="pRelacionada3"/>
		</div>
		<select name="personaRelacionada2" size="5" id="personaRelacionada2" size="5">
		 <?php
            foreach ($this->personasSeleccionadas as $key => $value) {
                echo '<option value="' . $value['idPersona'] . '">' . $value['personaNom'] . '</option>';
            }
            ?>
		</select>    

		<div id="relacionNueva"></div> 
		<div id="acomodarG">
			<p id="mensaje"></p>
			<input type="button" onclick="javascript:send(this.form, 'mensaje','personaRelacionada','e');" value="Guardar" name="Guardar" id="botones" /> 
			<input type="button" onclick="javascript:generateXML('editar',<?php echo $this->program[0]['idprograma']?>);" value="GenerarXML" name="GenerarXML" id="botones" /> 
		</div>

	</form>
	
</div>

<div id="help_dialog" title="Ayuda (?)"><img src="<?php echo URL;?>/public/images/help.png"></div>
<div id="tip_dialog" title="Tip (!)"><img src="<?php echo URL;?>/public/images/informacion.png"></div>

<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.timepicker.css';?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.noty.css';?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'/public/css/dropzone.css';?>"/>
<script src="<?php echo URL.'public/js/jquery.timepicker.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.datepair.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.noty.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/dropzone.js';?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.slickhover.js';?>" type="text/javascript"></script>		
<script>
$(function() {

	$( "#help_dialog" ).dialog({ autoOpen: false, width: 999 });
	$( "#tip_dialog" ).dialog({ autoOpen: false, width: 939 });
	$( "#opener" ).click(function() {
		$( "#help_dialog" ).dialog( "open" );
	});
	$( "#opener2" ).click(function() {
		$( "#tip_dialog" ).dialog( "open" );
	});

	$('#programaHorario .time').timepicker({
        'showDuration': true,
		'timeFormat': 'g:i a'
    });
	$('#programaHorario').datepair();
	
	var hover = function() {
		$("#thumbnail").slickhover({
			icon: URL + "public/images/slickhover/plus-black.png",
			animateIn: true,
			opacity: 0.1,
			speed: 600
		});
	}
	var removeHover = function() {
		$("#thumbnail").off();
		$(".iconslickhoverWrapper").remove();
		var slickhoverContent = $(".slickhoverWrapper").contents();
		$(".slickhoverWrapper").replaceWith(slickhoverContent);
		$("#thumbnail").css("opacity", "");
	}
	Dropzone.autoDiscover = false;
	$("#thumbnail").dropzone({ 
		url: URL + "<?php echo IMG_PROGRAM_UPLOAD_HANDLER; ?>",
		maxFilesize: "<?php echo IMG_MAX_UPLOAD_SIZE; ?>",
		addRemoveLinks: true,
		acceptedFiles: "image/*",
		dictInvalidFileType: "No es un formato de imagen v\u00E1lido",
		dictFileTooBig: "La imagen es demasiado grande. <?php echo IMG_MAX_UPLOAD_SIZE; ?> MB m\u00E1ximo",
		dictResponseError: "Hubo un error en el servidor",
		dictRemoveFile: "Remover",
		accept: function(file, done) {
			console.log("accept");
			removeHover();
			done();
		},
		success: function(file, response) {
			console.log("success");
			removeHover();
			response = JSON.parse(response);
			if(response.response == "error") {
				var node, _i, _len, _ref, _results;
				var message = response.serversays;
				file.previewElement.classList.add("dz-error");
				_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
				_results = [];
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				  node = _ref[_i];
				  _results.push(node.textContent = message);
				}
				return _results;
			}
			else {
				$("#programatThumbmail").val(response.image_url).prop("readonly", true).css({'background-color' :  '#C0C0C0'});
				return file.previewElement.classList.add("dz-success");
			}
		},
		error: function (file, message) {
			console.log("error");
			removeHover();
			var node, _i, _len, _ref, _results;
			if (file.previewElement) {
			  file.previewElement.classList.add("dz-error");
			  if (typeof message !== "String" && message.error) {
				message = message.error;
			  }
			  _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
			  _results = [];
			  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				node = _ref[_i];
				_results.push(node.textContent = message);
			  }
			  return _results;
			}
		},
		removedfile: function(file) {
			console.log("removed");
			hover();
			$("#programatThumbmail").val("").prop("readonly", false).css({'background-color' :  ''});
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		},
		init: function () {
			console.log("init");
			<?php
				if($this->program[0]['programatThumbmail'] != "") {
			?>removeHover();
			$("#programatThumbmail").prop("readonly", true).css({'background-color' :  '#C0C0C0'});
			<?php
					if($this->isImage !== false) {
						$imgObjects = json_decode($this->getImageInfo);
			?>var mockFile = { name: "<?php echo $imgObjects -> {'name'}; ?>", size: <?php echo $imgObjects -> {'length'}; ?>, type: "<?php echo $imgObjects -> {'type'}; ?>"}; 
			this.emit("addedfile", mockFile);
			this.options.thumbnail.call(this, mockFile, "<?php echo $this->program[0]['programatThumbmail']; ?>");
			<?php
					}
					else {
			?>var mockFile = { name: "<?php echo basename($this->program[0]['programatThumbmail']);?>", size: 0}; 
			this.emit("addedfile", mockFile);
			this.emit("error", mockFile, "No existe la imagen o no est\u00e1 disponible en este momento");
			$('#dropzone').addClass('dz-error');
			this.options.thumbnail.call(this, "", "<?php echo $this->program[0]['programatThumbmail']; ?>");
			<?php
					}
				}
				else {
			?>hover();
			<?php
				}
			?>
		}
	});
});
</script>