<style type="text/css">
	a#order {
		color: inherit;
	}
	a#sort, a.resetreturn {
		color: inherit;
		text-decoration: none;
	}
	table {
		font: 100% verdana,arial,sans-serif;
	}
	form input[type="text"] {
		border: 1px solid gray;
		border-radius: 5px;
		margin: 5px 0 0 10px;
		padding: 1px;
		width: 150px;
	}
	.error {
		border: 1px solid #FA5858 !important;
	}
</style>
<h1>Logs</h1>
<table width="100%" border="0">
<?php
	if (Session::get('rol') !== 'admin')
		echo '<tr><td style="text-align: center">Usuario sin permisos</td></tr>';
	else {
		$array = json_decode($this->showType, true);
		if (in_array('error', $array, true) || in_array('warning', $array, true)) {
			echo '<tr><td style="text-align: center">' . $array["message"] . '</td></tr>';
		}
		else {
			if($this->tos == "DESC" || $this->tos == "d") {
				$symbol = "&#x25BC;";
				$link = "u:a/";
			}
			else {
				$symbol = "&#x25B2;";
				$link = "u:d/";
			}
?>
	<tr>
		<td colspan="7">
			<hr>
		</td>
	</tr>
	<tr>
		<td colspan="7">
			<div id="Busqueda">
				<form name="buscar" id="search" action="<?php echo URL; ?>log/show/<?php echo $this->type; ?>/s:1/" method="post">
					<input type="text" name="u" id="u" placeholder="Usuario"> 
					<input type="text" name="a" id="a" placeholder="Acci&oacute;n">
					<input type="text" name="m" id="m" placeholder="Modificaci&oacute;n">
					<input type="text" name="f" id="f" placeholder="Fecha"> 
					<input type="submit" name="enviar" value="Buscar" id="botones">
					<input type="button" name="Limpiar" value="Limpiar" id="botones" class="clear">
				</form>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="7">
			<hr>
		</td>
	</tr>
	<tr>
		<td colspan="7" align="right">
			<form>
				<select name="URL" onchange="window.location.href= this.form.URL.options[this.form.URL.selectedIndex].value" style="width: auto;">
					<option value>Elementos</option>
					<option value="<?php echo URL . $this->wol; ?>">10</option>
					<option <?php if ($this->lpp == 20 ) echo 'selected'; ?> value="<?php echo URL . $this->wol . 'l:20/'; ?>">20</option>
					<option <?php if ($this->lpp == 50 ) echo 'selected'; ?> value="<?php echo URL . $this->wol . 'l:50/'; ?>">50</option>
					<option <?php if ($this->lpp == 100 ) echo 'selected'; ?> value="<?php echo URL . $this->wol . 'l:100/';?>">100</option>
					<option <?php if ($this->lpp == 250 ) echo 'selected'; ?> value="<?php echo URL . $this->wol . 'l:250/';?>">250</option>
					<option <?php if ($this->lpp == 500 ) echo 'selected'; ?> value="<?php echo URL . $this->wol . 'l:500/';?>">500</option>
					<option <?php if ($this->lpp == 1000 ) echo 'selected'; ?> value="<?php echo URL . $this->wol . 'l:1000/';?>">1000</option>
				</select>
			</form>
		</td>
	</tr>
	<tr>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:i/';?>" id="order">ID</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "i") echo $symbol; ?></a></p></td>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:u/';?>" id="order">Usuario</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "u") echo $symbol; ?></a></p></td>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:s/';?>" id="order">Secci&oacute;n</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "s") echo $symbol; ?></a></p></td>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:a/';?>" id="order">Acci&oacute;n</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "a") echo $symbol; ?></a></p></td>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:m/';?>" id="order">Modificaci&oacute;n</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "m") echo $symbol; ?></a></p></td>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:r/';?>" id="order">Respuesta</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "r") echo $symbol; ?></a></p></td>
		<td id="buscador"><p><a href="<?php echo URL . $this->woo . 'o:f/';?>" id="order">Fecha</a> <a href="<?php echo URL . $this->wos . $link;?>" id="sort"><?php if($this->too == "fecha" || $this->too == "f") echo $symbol; ?></a></p></td>
	</tr>
<?php
			foreach($array as $key => $value) {
				if(($key%2) == 1)
					echo '<tr bgcolor="#dadada">';
				else
					echo '<tr>';
				echo '<td>' . $value['id'] . '</td>';
				echo '<td>' . $value['usuario'] . '</td>';
				echo '<td>' . $value['seccion'] . '</td>';
				echo '<td>' . $value['accion'] . '</td>';
				echo '<td>' . $value['modificacion'] . '</td>';
				echo '<td>' . $value['respuesta'] . '</td>';
				echo '<td>' . $value['fecha'] . '</td>';
				echo '</tr>';
			}
?>
	<tr>
		<td colspan="7" id="personPaginate">
<?php
			$v = $this->pages;
			$separator = "...";
			echo '<div id="paginador">';
			for($i = 1 ; $i <= $v; $i++){
				if($i == $this->wpaio)
					echo '<div id="pagina"><strong>' . $i . '</strong></div>';
				
				else if($i == 1 || $i == $v || ($i >= $this->wpaio - $this->lpp && $i <= $this->wpaio + $this->lpp)) {
					if($i == $this->wpaio - $this->lpp && $i > 1 + 1)
						echo $separator;
					echo '<div id="pagina"><a href="'. URL . $this->wop .'p:'.$i.'/">'.$i.'</a></div>';
					if($i == $this->wpaio + $this->lpp && $i < $v - 1)
						echo $separator;
				}
			}
			echo '</div>';
?>		
		</td>
	</tr>
	<tr>
		<td colspan="7" align="center">
			<a title="Home" href="<?php echo URL . "log/"; ?>" class="resetreturn">&#127968;</a> | <a title="Reset" href="<?php echo URL . "log/show/" . $this->type . "/"; ?>" class="resetreturn">&#8634;</a>
		</td>
	</tr>
	<script type="text/javascript">
		$(function() {
			$('a.resetreturn').click(function(){
				$.ajax({
					url: URL + 'log/clearSearch'
				});
			});
		});
	</script>
<?php 
		}
	}
?>
</table>

<link type="text/css" rel="stylesheet" href="<?php echo URL.'public/css/jquery.qtip.css';?>" />
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.noty.css';?>" />
<script type="text/javascript" src="<?php echo URL.'public/js/jquery.noty.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo URL.'public/js/jquery.qtip.js'; ?>"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var elements = {
			position: {
				my: 'bottom left',
				at: 'top right'
			},
			style: {
				classes: 'qtip-shadow qtip-rounded'
			}
		};
		var message = {
			content: {
				text: 'Para b&uacute;squeda m&uacute;ltiple usar coma (,). Ej.: <i>B&uacute;squeda 1</i><b>,</b> <i>B&uacute;squeda 2</i><b>,</b> <i>B&uacute;squeda 3</i>'
			}
		}
		$('#u, #a, #m').qtip({
			content: message.content,
			position: elements.position,
			style: elements.style
		});
		$('#f').qtip({
			content: message.content.text + "<br />" + 'Para rango de fechas usar pipe (|). Ej.: 2014-12-30<strong>|</strong>2014-12-20',
			position: elements.position,
			style: elements.style
		});
		$(".clear").click(function() {
			$('#search').trigger("reset");
			if ($(":input").hasClass("error")) {
				$.noty.closeAll()
				$(":input").removeClass("error");
			}
		});
		$('#search').submit(function() {
			required = ["u", "a", "m", "f"];
			emptyerror = "Por favor, llena alguno de los campos para hacer una b&uacute;squeda";
			for (i = 0; i < required.length; i++) {
				var input = $('#' + required[i]);
				if ((input.val() == "")) {
					input.addClass("error");
				}
				else {
					$.noty.closeAll()
					input.removeClass("error");
				}
			}
			if ($("#u").val() == "" && $("#a").val() == "" && $("#m").val() == "" && $("#f").val() == "") {
				noty({text: emptyerror, layout: 'bottom', type: 'error', maxVisible: 1, dismissQueue: true, killer: true});
				return false;
			}
			else {
				$.noty.closeAll()
				return true;
			}
		});
		$(":input").focus(function(){		
			if ($(this).hasClass("error")) {
				$.noty.closeAll()
				$(this).val("");
				$(this).removeClass("error");
			}
		});
	});
</script>