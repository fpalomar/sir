<style type="text/css">
	table {
		font: 100% verdana,arial,sans-serif;
	}
</style>
<h1>Logs</h1>
<table align="center" border="0">
<?php
	if (Session::get('rol') !== 'admin')
		echo '<tr><td style="text-align: center">Usuario sin permisos</td></tr>';
	else {
?>
	<tr>
		<td id="buscador" colspan="2" align="center"><p>Tipo</p></td>
	</tr>
<?php
		foreach($this->logType as $key => $value) {
			if($key % 2 == 1)
				echo '<tr bgcolor="#F2F2F2">';
			else
				echo '<tr>';
			$number = $key + 1;
			echo '<td>' . $number . '</td>';
			echo '<td><a href="' . URL . 'log/show/' . strtolower($value) . '/">' . $value . '</a></td>';
		}
	}
?>
</table>