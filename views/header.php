<!doctype html>
<html>
<head>
    <title><?=(isset($this->title)) ? $this->title : 'admin'; ?></title>
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css" />    
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/themes/sunny/jquery-ui.css" />
    <script type="text/javascript" src="<?php echo URL; ?>public/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo URL; ?>public/js/custom.js"></script>
    <?php 
    if (isset($this->js)) 
    {
        foreach ($this->js as $js)
        {
            echo '<script type="text/javascript" src="'.URL.'views/'.$js.'"></script>';
        }
    }
    ?>
</head>
<body>

<?php Session::init(); ?>
    
<div id="header">
    <div id="buscador">
        <form name="buscador" action="">
            <!--<input name="buscador" type="text">-->
        </form>
    </div>
    <div id="logo">
       <!-- <a href="<?php echo URL; ?>index"><img src="public/images/logo_tvsa.gif"></a>-->
    </div>
    
    <div id="botonesI">
        <?php if (Session::get('loggedIn') == false):?>
            <a href="<?php echo URL; ?>index">Index</a>
            |
            <a href="<?php echo URL; ?>help">Help</a>
            
        <?php endif; ?>    
        <?php if (Session::get('loggedIn') == true):?>
           
            <!--<a href="<?php //echo URL; ?>dashboard">Dashboard</a>
            |-->
            <a href="<?php echo URL; ?>program">Programas</a>
            |
            <a href="<?php echo URL; ?>personas">Personas</a>
            <!--|
            <a href="<?php echo URL; ?>equipos">Equipos</a-->
            <?php if (Session::get('rol') == 'admin'):?>
            |
			<a href="<?php echo URL; ?>user">Usuarios</a>
            <?php endif; ?>
            <?php if (Session::get('rol') == 'admin'):?>
            |
			<a href="<?php echo URL; ?>branch">Ramas</a>
            <?php endif; ?>
			<?php if (Session::get('rol') == 'admin'):?>
            |
			<a href="<?php echo URL; ?>log">Logs</a>
            <?php endif; ?>
            |
            <a href="<?php echo URL; ?>dashboard/logout">Salir</a>    
        <?php else: ?>
            |
            <a href="<?php echo URL; ?>login">Login</a>
        <?php endif; ?>
    </div>
</div>
    
<div id="content">
    
    