							<div class="page-header">
								<h1>
									Personas
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
											Lista de personas
									</small>
								</h1>
							</div><!-- /.page-header -->
							<div class="row persons-list">
								<div class="col-xs-12">
									<div class="clearfix">
										<div class="pull-right tableTools-container">
											<div class="dt-buttons btn-overlap btn-group">
												<a id="publishAll" class="dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="" href="<?php echo URL; ?>persona/publishall/">
													<span>
														<i class="fa fa-cloud-upload bigger-110 green"></i>
														<span class="hidden">Publicar selección</span>
													</span>
												</a>
												<a id="unpublishAll" class="dt-button buttons-copy buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="" href="<?php echo URL; ?>persona/unpublishall/">
													<span>
														<i class="fa fa-cloud-download bigger-110 orange"></i>
														<span class="hidden">Despublicar selección</span>
													</span>
												</a>
												<a id="deleteAll" class="dt-button buttons-csv buttons-html5 btn btn-white btn-primary btn-bold" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="" href="<?php echo URL; ?>persona/deleteall/">
													<span>
														<i class="fa fa-trash bigger-110 red"></i>
														<span class="hidden">Borrar selección</span>
													</span>
												</a>
											</div>
										</div>
									</div>
									<div class="table-header"></div>
									<div class="table-responsive">
										<table id="persons-table" class="table table-striped table-bordered table-hover dt-responsive nowrap">
											<thead>
												<tr>
													<th class="no-orderable">
														<label id="selectAllLabel" class="pos-rel">
															<input id="selectAll" type="checkbox" class="ace">
															<span class="lbl"></span>
														</label>
													</th>
													<th>Nombre</th>
													<th>Biografía</th>
													<th>
														<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
														Última modificación
													</th>
													<th>Estatus</th>
													<th class="no-orderable">Acciones</th>
												</tr>
											</thead>
										</table>
									</div>
								</div><!-- /.col-xs-12 -->
								<div id="loading-screen" class="hide">
									<i class="ace-icon fa fa-refresh fa-spin blue bigger-250"></i>
								</div>
							</div><!-- /.row -->
							<div class="row">
								<div class="col-xs-12">
									<button class="btn btn-info btn-block" id="addPerson">
										<i class="ace-icon fa fa-plus align-middle bigger-125"></i>
										Añadir nueva persona
									</button>
								</div>
							</div>
							<div class="row persons-new" style="/*display: none;*/">
								<div class="col-xs-12">
									<h3 class="header smaller lighter blue">Nueva persona</h3>
								</div>
								<form class="form-horizontal" id="person" role="form" method="post" action="<?php echo URL; ?>persona/create">
									<div class="col-sm-offset-1 col-sm-10">
										<h4 class="header blue bolder smaller">General</h4>
										<div class="form-group"><!-- Field type -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-type">Tipo</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="col-xs-6 no-padding-right no-padding-left radio inline">
													<label>
														<input id="form-field-type" class="ace input-lg person" type="radio" name="type" value="person">
														<span class="lbl">Persona</span>
													</label>
												</div>
												<div class="col-xs-6 no-padding-right no-padding-left radio inline">
													<label>
														<input id="form-field-type" class="ace input-lg character" type="radio" name="type" value="character">
														<span class="lbl">Personaje</span>
													</label>
												</div>
											</div>
										</div><!-- /Field type-->
										<div class="form-group"><!-- Field branch -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-branch">Canal</label>
											<div id="field-branch" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<select class="form-control branches" id="form-field-branch" name="branches">
													<option value="" selected>Selecciona el canal al que pertenece</option>
<?php
$branch = json_decode($this->ramaList, true);
foreach($branch as $key => $value) {
	echo '													<option name="'.$value['ramaNombre'].'" value="'.$value['ramaId'].'">'.$value['ramaNombre'].'</option>'.PHP_EOL;
}
?>
												</select>
											</div>
										</div><!-- /Field branch -->
										<div class="person-group complete-name" style="display: none;"><!-- Person group -->
											<div class="form-group"><!-- Field person name -->
												<div class="total-group col-lg-8 col-xs-12 no-padding">
													<div class="margin-bottom-15">
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-person-name">Nombre</label>
														<div id="field-person-name" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<input type="text" name="person-name" placeholder="Nombre completo" class="col-xs-12 col-sm-12" id="form-field-person-name">
														</div>
													</div><!-- /Field person name -->
													<div class="margin-bottom-15"><!-- Field person surname -->
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-person-surname">Apellido Paterno</label>
														<div id="field-person-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<input type="text" name="person-surname" placeholder="Apellido Paterno" class="col-xs-12 col-sm-12" id="form-field-person-surname">
														</div>
													</div><!-- /Field person surname -->
													<div class="margin-bottom-15"><!-- Field person maidenname -->
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-person-maidenname">Apellido Materno</label>
														<div id="field-person-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<input type="text" name="person-maidenname" placeholder="Apellido Materno" class="col-xs-12 col-sm-12" id="form-field-person-maidenname">
														</div>
													</div><!-- /Field person surname -->
													<!-- Create a hidden field which is combined by 3 fields above -->
													<input type="hidden" name="person-complete-name">
												</div>
											</div>
										</div><!-- /Person group -->
										<div class="character-group" style="display: none;"><!-- Character group -->
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-character-person">Persona</label>
												<div id="field-character-person" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<select class="character-person" data-placeholder="Persona que interpreta al personaje" name="character-person" id="form-field-character-person">
														<option value=""></option>
<?php
	$list = json_decode($this->personaSingleList, true);
	foreach ($list as $key => $value) {
		echo '													<option value="' . $value['personaId'] . '" title="' . $value['personaNombre'] . '">' . $value['personaNombre'] . '</option>'.PHP_EOL;
	}
?>
													</select>
												</div>
											</div>
											<!-- The template for adding new field -->
											<div class="hide" id="new-character-person-template">
												<div class="form-group"><!-- Field person name -->
													<div class="total-group col-lg-8 col-xs-12 no-padding">
														<div class="margin-bottom-15">
															<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-new-character-person-name">Nombre</label>
															<div id="field-new-character-person-name" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
																<input type="text" name="new-character-person-name" placeholder="Nombre completo del interprete" class="col-xs-12 col-sm-12" id="form-field-new-character-person-name" disabled="disabled">
															</div>
														</div><!-- /Field person name -->
														<div class="margin-bottom-15"><!-- Field person surname -->
															<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-new-character-person-surname">Apellido Paterno</label>
															<div id="field-new-character-person-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
																<input type="text" name="new-character-person-surname" placeholder="Apellido Paterno del interprete" class="col-xs-12 col-sm-12" id="form-field-new-character-person-surname" disabled="disabled">
															</div>
														</div><!-- /Field person surname -->
														<div class="margin-bottom-15"><!-- Field person maidenname -->
															<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-new-character-person-maidenname">Apellido Materno</label>
															<div id="field-new-character-person-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
																<input type="text" name="new-character-person-maidenname" placeholder="Apellido Materno del interprete" class="col-xs-12 col-sm-12" id="form-field-new-character-person-maidenname" disabled="disabled">
															</div>
														</div><!-- /Field person surname -->
														<div class="margin-bottom-15"><!-- Field bio url -->
															<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-new-character-person-bio-url">Dirección</label>
															<div id="field-new-character-person-bio-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
																<input type="text" name="new-character-person-bio-url" placeholder="Ej.: http://www.televisa.com/interprete/biografia/" class="col-xs-12 col-sm-12" id="form-field-new-character-person-bio-url" disabled="disabled">
															</div>
														</div><!-- /Field bio url -->
														<!-- Create a hidden field which is combined by 3 fields above -->
														<input type="hidden" name="new-character-person-complete-name" disabled="disabled">
													</div>
												</div>
											</div><!-- /Person group -->
											<div class="form-group"><!-- Field character name -->
												<div class="total-group col-lg-8 col-xs-12 no-padding">
													<div class="margin-bottom-15">
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-character-name">Nombre</label>
														<div id="field-character-name" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<input type="text" name="character-name" placeholder="Nombre completo del personaje" class="col-xs-12 col-sm-12" id="form-field-character-name">
														</div>
													</div><!-- /Field character name -->
													<div class="margin-bottom-15"><!-- Field character surname -->
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-character-surname">Apellido Paterno</label>
														<div id="field-character-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<input type="text" name="character-surname" placeholder="Apellido Paterno del personaje" class="col-xs-12 col-sm-12" id="form-field-character-surname">
														</div>
													</div><!-- /Field character surname -->
													<div class="margin-bottom-15"><!-- Field character maidenname -->
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-character-maidenname">Apellido Materno</label>
														<div id="field-character-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<input type="text" name="character-maidenname" placeholder="Apellido Materno del personaje" class="col-xs-12 col-sm-12" id="form-field-character-maidenname">
														</div>
													</div><!-- /Field character surname -->
													<div class="margin-bottom-15"><!-- Field character maidenname -->
														<label class="col-xs-12 col-sm-6 col-md-4 col-lg-6 control-label no-padding-right" for="form-field-character-maidenname">¿Personaje actual?</label>
														<div class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
															<div class="col-xs-6 no-padding-right no-padding-left radio inline">
																<label>
																	<input id="form-field-type" class="ace input-lg person" type="radio" name="is-actual-character" value="1">
																	<span class="lbl">Si</span>
																</label>
															</div>
															<div class="col-xs-6 no-padding-right no-padding-left radio inline">
																<label>
																	<input id="form-field-type" class="ace input-lg character" type="radio" name="is-actual-character" value="2">
																	<span class="lbl">No</span>
																</label>
															</div>
														</div>
													</div><!-- /Field character surname -->
													<!-- Create a hidden field which is combined by 3 fields above -->
													<input type="hidden" name="character-complete-name">
												</div>
											</div>
										</div><!-- Character group -->
										<div class="form-group"><!-- Gender type -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-gender">Sexo</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="col-xs-6 no-padding-right no-padding-left radio inline">
													<label>
														<input id="form-field-gender" class="ace input-lg" type="radio" name="gender" value="1">
														<span class="lbl">Femenino</span>
													</label>
												</div>
												<div class="col-xs-6 no-padding-right no-padding-left radio inline">
													<label>
														<input id="form-field-gender" class="ace input-lg" type="radio" name="gender" value="2">
														<span class="lbl">Masculino</span>
													</label>
												</div>
											</div>
										</div><!-- /Field type-->
										<div class="form-group"><!-- Field nicknames -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-nicknames">Apodos</label>
											<div id="field-nicknames" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="width-100">
													<input type="text" placeholder="Apodos separados por coma" id="form-field-nicknames" name="nicknames">
												</div>
											</div>
										</div><!-- /Field hobbies -->
										<div class="form-group"><!-- Field birthday -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-birthday">Fecha de nacimiento</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div id="field-birthday" class="input-group date">
													<input type="text" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy" id="form-field-birthday" class="col-xs-12 col-sm-12" name="birthday">
													<span class="input-group-addon">
														<i class="ace-icon fa fa-calendar"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field birthday -->
										<div class="form-group"><!-- Field deathday -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-deathday">Fecha de fallecimiento</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div id="field-deathday" class="input-group date">
													<input type="text" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy" id="form-field-deathday" class="col-xs-12 col-sm-12" name="deathday">
													<span class="input-group-addon">
														<i class="ace-icon fa fa-calendar"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field deathday -->
										<div class="form-group"><!-- Field birth place -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-birthplace">Lugar de nacimiento</label>
											<div id="field-birthplace" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="birthplace" placeholder="Lugar de nacimiento" class="col-xs-12 col-sm-12" id="form-field-birthplace">
											</div>
										</div><!-- /Field birth place -->
										<div class="form-group"><!-- Field nacionality -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-nacionality">Nacionalidad</label>
											<div id="field-nacionality" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="nacionality" placeholder="Lugar de nacimiento" class="col-xs-12 col-sm-12" id="form-field-nacionality">
											</div>
										</div><!-- /Field nacionality -->
										<div class="form-group"><!-- Field hobbies -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-hobbies">Hobbies</label>
											<div id="field-hobbies" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="width-100">
													<input type="text" placeholder="Hobbies separados por coma" id="form-field-hobbies" name="hobbies">
												</div>
											</div>
										</div><!-- /Field hobbies -->
										<h4 class="header blue bolder smaller">Imagen</h4>
										<div class="form-group"><!-- Field image url -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-image-url">Dirección</label>
											<div id="field-image-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="image-url" placeholder="Ej.: http://www.televisa.com/personaje/imagen.jpg" class="col-xs-12 col-sm-12" id="form-field-image-url" id="personaUrlThumb">
											</div>
										</div><!-- /Field image url -->
										<div class="form-group"><!-- Dropzone image -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right">&nbsp;</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div id="dropzone" class="dropzone well dz-clickable">
												</div>
											</div>
										</div><!-- /Dropzone image -->
										<h4 class="header blue bolder smaller">Biografía</h4>
										<div class="form-group"><!-- Field bio url -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-bio-url">Dirección</label>
											<div id="field-bio-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="bio-url" placeholder="Ej.: http://www.televisa.com/persona/biografia/" class="col-xs-12 col-sm-12" id="form-field-bio-url">
											</div>
										</div><!-- /Field bio url -->
										<div class="form-group"><!-- Field bio resume -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-bio-resume">Resumen</label>
											<div id="field-bio-resume" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<textarea placeholder="Resumen de la biograf&iacute;a" class="autosize-transition form-control" id="bio-resume" name="bio-resume"></textarea>
											</div>
										</div><!-- /Field bio resume -->
										<div class="form-group"><!-- Field bio complete -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-bio-complete">Contenido</label>
											<div id="field-bio-complete" class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
												<textarea placeholder="Contenido completo de la biograf&iacute;a" class="autosize-transition form-control" id="bio-complete" name="bio-complete"></textarea>
											</div>
										</div><!-- /Field bio complete -->
										<div class="form-group"><!-- Field curious fact -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-curious-fact">Dato Curioso</label>
											<div id="field-curious-fact" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
												<input type="text" name="curious-fact[]" placeholder="Dato Curioso" class="col-xs-12 col-sm-12" id="form-field-curious-fact">
											</div>
											<div class="col-xs-1 no-padding">
												<button type="button" class="btn btn-sm btn-default addFact tt" title="Agregar otro dato curioso" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
											</div>
										</div><!-- /Field curious fact -->
										<div class="hide" id="curious-fact-template"><!-- The template for adding new field -->
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-curious-fact">Dato Curioso</label>
												<div id="field-new-curious-fact" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="curious-fact[]" placeholder="Dato Curioso" class="col-xs-12 col-sm-12" id="form-field-curious-fact">
												</div>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default removeFact tt" title="Quitar dato curioso" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
												</div>
											</div>
										</div><!-- /Template -->
										<div class="person-group" style="display: none;">
											<h4 class="header blue bolder smaller">Egresados y profesores del CEA</h4>
											<div class="form-group"><!-- Field CEA type -->
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-cea-type">Tipo</label>
												<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<div class="col-xs-6 no-padding-right no-padding-left checkbox type">
														<label>
															<input name="cea-type[]" class="ace" type="checkbox" id="form-field-cea-type" value="egresado">
															<span class="lbl">Egresado</span>
														</label>
													</div>
													<div class="col-xs-6 no-padding-right no-padding-left checkbox type">
														<label>
															<input name="cea-type[]" class="ace" type="checkbox" id="form-field-cea-type" value="profesor">
															<span class="lbl">Profesor</span>
														</label>
													</div>
												</div>
											</div><!-- /Field CEA type -->
										</div>
										<h4 class="header blue bolder smaller">Programas Actuales</h4>
										<div class="form-group"><!-- Field actual program -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-actual-program">Programas actuales</label>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<select multiple="multiple" size="10" name="duallistbox_actualprogram[]" id="actual-program">
<?php
$list = json_decode($this->programaList, true);
foreach ($list as $key => $value) {
	echo '													<option value="' . $value['programaId'] . '" title="' . $value['programaTitulo'] . '">' . $value['programaTitulo'] . '</option>'.PHP_EOL;
}
?>
												</select>
											</div>
										</div>
										<div class="form-group"><!-- Field new actual program -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-program-name">Nuevo programa actual</label>
											<div id="field-new-actual-program-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
												<input type="text" name="new-actual-program-name[]" placeholder="Nombre del nuevo programa actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-program-name">
											</div>
											<div class="col-xs-1 no-padding">
												<button type="button" class="btn btn-sm btn-default addNewActualProgram tt" title="Agregar otro programa actual" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-program-url">Dirección</label>
											<div id="field-new-actual-program-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="new-actual-program-url[]" placeholder="Dirección del nuevo programa actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-program-url">
											</div>
										</div>
										<!-- The template for adding new field -->
										<div class="hide" id="new-actual-program-template">
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-program-name">Nuevo programa relacionado</label>
												<div id="field-new-actual-program-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="new-actual-program-name[]" placeholder="Nombre del nuevo programa relacionado" class="col-xs-12 col-sm-12" id="form-field-new-actual-program-name">
												</div>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default removeNewActualProgram tt" title="Quitar programa actual" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-program-url">Dirección</label>
												<div id="field-new-actual-program-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" placeholder="Dirección del nuevo programa actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-program-url">
												</div>
											</div>
										</div>
										<h4 class="header blue bolder smaller">Programas Relacionados</h4>
										<div class="form-group"><!-- Field related program -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-related-program">Programas relacionados</label>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<select multiple="multiple" size="10" name="duallistbox_relatedprogram[]" id="related-program">
<?php
$list = json_decode($this->programaList, true);
foreach ($list as $key => $value) {
	echo '													<option value="' . $value['programaId'] . '" title="' . $value['programaTitulo'] . '">' . $value['programaTitulo'] . '</option>'.PHP_EOL;
}
?>
												</select>
											</div>
										</div><!-- /Field related program -->
										<div class="form-group"><!-- Field new related program name -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-program-name">Nuevo programa relacionado</label>
											<div id="field-new-related-program-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
												<input type="text" name="new-related-program-name[]" placeholder="Nombre del nuevo programa relacionado" class="col-xs-12 col-sm-12" id="form-field-new-related-program-name">
											</div>
											<div class="col-xs-1 no-padding">
												<button type="button" class="btn btn-sm btn-default addNewRelatedProgram tt" title="Agregar otro programa relacionado" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
											</div>
										</div><!-- /Field new related program name -->
										<div class="form-group"><!-- Field new related program url -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-program-url">Dirección</label>
											<div id="field-new-related-program-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="new-related-program-url[]" placeholder="Dirección del nuevo programa relacionado" class="col-xs-12 col-sm-12" id="form-field-new-related-program-url">
											</div>
										</div><!-- /Field new related program url -->
										<div class="hide" id="new-related-program-template"><!-- The template for adding new field -->
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-program-name">Nuevo programa relacionado</label>
												<div id="field-new-related-program-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="new-related-program-name[]" placeholder="Nombre del nuevo programa relacionado" class="col-xs-12 col-sm-12" id="form-field-new-related-program-name">
												</div>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default removeNewRelatedProgram tt" title="Quitar programa relacionado" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-program-url">Dirección</label>
												<div id="field-new-related-program-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" placeholder="Dirección del nuevo programa relacionado" class="col-xs-12 col-sm-12" id="form-field-new-related-program-url">
												</div>
											</div>
										</div><!-- /Template -->
										<div class="person-group" style="display: none;">
											<h4 class="header blue bolder smaller">Personajes Actuales</h4>
											<div class="form-group"><!-- Field actual character -->
												<label class="col-xs-12 col-sm-12 control-label center" for="form-field-actual-character">Personajes Actuales</label>
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<select multiple="multiple" size="10" name="duallistbox_actualcharacter[]" id="actual-character">
<?php
	$list = json_decode($this->personajeList, true);
	foreach ($list as $key => $value) {
		echo '													<option value="' . $value['personaId'] . '|' . $value['programaId'] . '" title="' . $value['personaNombre'] . ' - ' . $value['programaTitulo'] . '">' . $value['personaNombre'] . ' - ' . $value['programaTitulo'] . '</option>'.PHP_EOL;
	}
?>
													</select>
												</div>
											</div>
											<h4 class="header blue bolder smaller">Personajes Relacionados</h4>
											<div class="form-group"><!-- Field related character -->
												<label class="col-xs-12 col-sm-12 control-label center" for="form-field-related-character">Personajes relacionados</label>
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
													<select multiple="multiple" size="10" name="duallistbox_relatedcharacter[]" id="related-character">
<?php
	$list = json_decode($this->personajeList, true);
	foreach ($list as $key => $value) {
		echo '													<option value="' . $value['personaId'] . '|' . $value['programaId'] . '" title="' . $value['personaNombre'] . ' - ' . $value['programaTitulo'] . '">' . $value['personaNombre'] . ' - ' . $value['programaTitulo'] . '</option>'.PHP_EOL;
	}
?>
													</select>
												</div>
											</div><!-- /Field related character -->
											<h4 class="header blue bolder smaller">Premios y reconocimientos</h4>
											<div class="award-result-row">
												<div class="form-group"><!-- Field award name -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-name">Premio</label>
													<div id="field-new-award-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
														<input type="text" name="award-name[0]" placeholder="Nombre del premio o reconocimiento" class="col-xs-12 col-sm-12" id="form-field-award-name">
													</div>
													<div class="col-xs-1 no-padding">
														<button type="button" class="btn btn-sm btn-default addAward tt" title="Agregar otro premio o reconocimiento" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
													</div>
												</div><!-- /Field award -->
												<div class="form-group"><!-- Field category -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-category">Categoría</label>
													<div id="field-award-category" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<input type="text" name="award-category[0]" placeholder="Categoría del premio o reconocimiento" class="col-xs-12 col-sm-12" id="form-field-award-category">
													</div>
												</div><!-- Field category -->
												<div class="form-group"><!-- Field year -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-year">Año</label>
													<div id="field-award-year" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<div class="width-100">
															<input type="text" placeholder="Si ganó en multiples años, sepáralos por coma" name="award-year[0]" class="award-year">
														</div>
													</div>
												</div><!-- /Field year -->
												<div class="form-group field-result"><!-- Field result -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-result">Resultado</label>
													<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<div class="col-xs-6 no-padding-right no-padding-left radio inline award-winner">
															<label>
																<input id="form-field-award-result" class="ace input-lg" type="radio" name="award-result[0]" value="1">
																<span class="lbl">Ganador</span>
															</label>
														</div>
														<div class="col-xs-6 no-padding-right no-padding-left radio inline award-nominated">
															<label>
																<input id="form-field-award-result" class="ace input-lg" type="radio" name="award-result[0]" value="2">
																<span class="lbl">Nominado</span>
															</label>
														</div>
													</div>
												</div><!-- /Field result -->
											</div>
											<div class="hide" id="award-template"><!-- The template for adding new field -->
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-name">Premio</label>
													<div id="field-new-award-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
														<input type="text" name="award-name[]" placeholder="Nombre del premio o reconocimiento" class="col-xs-12 col-sm-12" id="form-field-award-name">
													</div>
													<div class="col-xs-1 no-padding">
														<button type="button" class="btn btn-sm btn-default removeAward tt" title="Quitar premio o reconocimiento" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="form-group"><!-- Field category -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-category">Categoría</label>
													<div id="field-award-category" class="col-xs-12 col-sm-4 col-md-7 col-lg-4">
														<input type="text" name="award-category[]" placeholder="Categoría del premio o reconocimiento" class="col-xs-12 col-sm-12" id="form-field-award-category">
													</div>
												</div><!-- Field category -->
												<div class="form-group"><!-- Field year -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-year">Año</label>
													<div id="field-award-year" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<div class="width-100">
															<input type="text" placeholder="Si ganó en multiples años, sepáralos por coma" name="award-year[]">
														</div>
													</div>
												</div><!-- /Field year -->
												<div class="form-group field-result"><!-- Field result -->
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-award-result">Resultado</label>
													<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<div class="col-xs-6 no-padding-right no-padding-left radio inline">
															<label>
																<input id="form-field-award-result" class="ace input-lg" type="radio" name="award-result[]" value="1">
																<span class="lbl">Ganador</span>
															</label>
														</div>
														<div class="col-xs-6 no-padding-right no-padding-left radio inline">
															<label>
																<input id="form-field-award-result" class="ace input-lg" type="radio" name="award-result[]" value="2">
																<span class="lbl">Nominado</span>
															</label>
														</div>
													</div>
												</div><!-- /Field result -->
											</div><!-- /Template -->
										</div><!-- /.person-group -->
										<h4 class="header blue bolder smaller">Información complementaria</h4>
										<div class="form-group"><!-- Field keywords -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-keywords">Keywords</label>
											<div id="field-keywords" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="width-100">
													<input type="text" placeholder="Keywords separados por coma" value="" id="form-field-keywords" name="keywords">
												</div>
											</div>
										</div><!-- /Field keywords -->
										<div class="form-group"><!-- Field news -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-news">Noticias</label>
											<div id="field-news" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="news" placeholder="Ej.: http://www.televisa.com/persona/noticias/" class="col-xs-12 col-sm-12" id="form-field-news">
											</div>
										</div><!-- /Field news -->
										<div class="form-group"><!-- Field photos -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-photos">Fotos</label>
											<div id="field-photos" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="photos" placeholder="Ej.: http://www.televisa.com/persona/imagenes/" class="col-xs-12 col-sm-12" id="form-field-photos">
											</div>
										</div><!-- /Field photos -->
										<div class="form-group"><!-- Field videos -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-videos">Videos</label>
											<div id="field-videos" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="videos" placeholder="Ej.: http://www.televisa.com/persona/videos/" class="col-xs-12 col-sm-12" id="form-field-videos">
											</div>
										</div><!-- /Field videos -->
										<h4 class="header blue bolder smaller">Redes sociales</h4>
										<div class="form-group"><!-- Field facebook -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-facebook">Facebook</label>
											<div id="field-facebook" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="facebook" placeholder="Ej.: https://www.facebook.com/persona" class="col-xs-12 col-sm-12" id="form-field-facebook">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-facebook facebook"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field facebook -->
										<div class="form-group"><!-- Field twitter -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-twitter">Twitter</label>
											<div id="field-twitter" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="twitter" placeholder="Ej.: https://twitter.com/persona" class="col-xs-12 col-sm-12" id="form-field-twitter">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-twitter twitter"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field twitter -->
										<div class="form-group"><!-- Field youtube -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-youtube">YouTube</label>
											<div id="field-youtube" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="youtube" placeholder="Ej.: https://www.youtube.com/channel/p3r50n4" class="col-xs-12 col-sm-12" id="form-field-youtube">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-youtube youtube"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field youtube -->
										<div class="form-group"><!-- Field instagram -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-instagram">Instagram</label>
											<div id="field-instagram" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="instagram" placeholder="Ej.: https://www.instagram.com/persona" class="col-xs-12 col-sm-12" id="form-field-instagram">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-instagram instagram"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field instagram -->
										<div class="form-group"><!-- Field pinterest -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-pinterest">Pinterest</label>
											<div id="field-pinterest" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="pinterest" placeholder="Ej.: https://pinterest.com/personaje" class="col-xs-12 col-sm-12" id="form-field-pinterest">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-pinterest pinterest"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field pinterest -->
										<div class="form-group"><!-- Field google plus -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-gplus">Google Plus</label>
											<div id="field-gplus" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="gplus" placeholder="Ej.: https://plus.google.com/1234567890" class="col-xs-12 col-sm-12" id="form-field-gplus">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-google-plus gplus"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field google plus -->
										<div class="clearfix form-actions">
											<!-- The default publishing status -->
											<input type="hidden" name="status" value="unpublished">
											<div class="text-center col-xs-12 col-sm-12">
												<button type="submit" name="save" id="save" class="btn btn-info">
													<i class="ace-icon fa fa-floppy-o bigger-110"></i>
													Guardar
												</button>
												&nbsp;&nbsp;&nbsp;
												<button type="submit" name="publish" id="publish" class="btn btn-success">
													<i class="ace-icon fa fa-cloud-upload bigger-110"></i>
													Guardar y publicar
												</button>
												&nbsp;&nbsp;&nbsp;
												<button type="reset" class="btn">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div><!-- /.form-actions -->
									</div><!-- /.col-sm-offset-1 -->
								</form>
							</div><!-- /.persons-new -->
							<script>
								$(function() {
									var Home = $('body').attr('data-home-url');
									var CheckURL;
									// DataTable Start
									var oldStart = 0; // Pagination scroll to top
									var personsTable = $('#persons-table').on('xhr.dt', function (e, settings, json, xhr) {
										// Database error
										if (json.dberror === 1 || json.dberror === 2 || json.dberror === 3) {
											// Custom error report
											$('.persons-list > .col-xs-12').hide();
											$('.persons-list').append('<div class="alert alert-block alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i></button><p><strong><i class="ace-icon fa fa-times"></i> &iexcl;Oh no!</strong> Tenemos un problema <i class="ace-icon fa fa-frown-o"></i>:</p><p><ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>Error en la consulta a la base de datos (Error ' + json.dberror +')</li></ul></p></div>');
											return true;
										}
									}).DataTable({
										sateSave: true,
										processing: true,
										serverSide: true,
										responsive: true,
										ajax: {
											url: Home + 'persona/personaDataTable/',
											type: 'post',
											dataSrc: function (json){
												// Database error
												if(json.dberror)
													return [];
												else
													return json.data;
											}
										},
										order: [[3, 'desc']],
										columns: [
											{
												width: '2%',
												'class': 'center',
												data: null,
												render: function (data, type, full, meta) {
												return '<label class="pos-rel"><input type="checkbox" id="' + data.personaId + '" class="ace case" name="person-select"><span class="lbl"></span></label>';
												}
											},
											{
												width: '10%',
												data: null,
												render: function(data, type, full, meta) {
													return $.trim(data.personaNombre);
												}
											},
											{
												width: '30%',
												data: 'personaResumenBio'
											},
											{
												width: '10%',
												'class': 'center',
												data: 'personaFechaModificacion'
											},
											{
												width: '5%',
												'class': 'center',
												data: null,
												render: function (data, type, full, meta) {
													if(data.personaStatus === '0')
														return '<span class="label label-sm label-success">Publicado</span>';
													else if(data.personaStatus === '1') // More statuses?
														return'<span class="label label-sm label-info">Guardado</span>';
													else
														return'<span class="label label-sm label-warning">Desconocido</span>';
												}
											},
											{
												width: '10%',
												'class': 'center',
												data: null,
												render: function (data, type, full, meta) {
													return '<div class="hidden-sm hidden-xs action-buttons"><a data-original-title="Editar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/edit/' + data.personaId + '" class="blue tooltip-info tt"><i class="ace-icon fa fa-pencil bigger-130"></i></a><a data-original-title="Publicar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/publish/' + data.personaId + '" class="green tooltip-success tt publishThis"><i class="ace-icon fa fa-cloud-upload bigger-130"></i></a><a data-original-title="Despublicar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/unpublish/' + data.personaId + '" class="orange tooltip-warning tt unpublishThis"><i class="ace-icon fa fa-cloud-download bigger-130"></i></a><a data-original-title="Borrar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/delete/' + data.personaId + '" class="red tooltip-error tt deleteThis"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div><div class="hidden-md hidden-lg"><div class="inline pos-rel dropup"><button data-position="auto" data-toggle="dropdown" class="btn btn-minier btn-yellow dropdown-toggle" aria-expanded="false"><i class="ace-icon fa fa-caret-down icon-only bigger-120"></i></button><ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close" style=""><li><a data-original-title="Editar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/edit/' + data.personaId + '" class="tooltip-info tt"><span class="blue"><i class="ace-icon fa fa-search-plus bigger-120"></i></span></a></li><li><a data-original-title="Publicar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/publish/' + data.personaId + '" class="tooltip-success tt publishThis"><span class="green"><i class="ace-icon fa fa-cloud-upload bigger-120"></i></span></a></li><li><a data-original-title="Despublicar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/unpublish/' + data.personaId + '" class="tooltip-warning tt unpublishThis"><span class="orange"><i class="ace-icon fa fa-cloud-download bigger-120"></i></span></a></li><li><a data-original-title="Borrar" data-placement="top" data-rel="tooltip" href="' + Home + 'persona/delete/' + data.personaId + '" class="tooltip-error tt deleteThis"><span class="red"><i class="ace-icon fa fa-trash-o bigger-120"></i></span></a></li></ul></div></div>';
												}
											}
										],
										columnDefs: [
											{
												orderable: false,
												targets: 'no-orderable'
											}
										],
										autoWidth: false,
										language: {
											'lengthMenu': '_MENU_ personas por p\u00e1gina',
											'zeroRecords': 'No encontramos la persona que buscas',
											'info': 'P\u00e1gina _PAGE_ de _PAGES_',
											'infoEmpty': 'No hay registros de personas a\u00fan (;-;)',
											'infoFiltered': '(filtrado de _MAX_ personas totales)',
											'search': 'Buscar',
											'paginate': {
												'previous': 'Anterior',
												'next': 'Siguiente'
											},
											'processing': '<div role="alert" class="tree-loader center" style=""><div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue bigger-250"></i></div></div>'
										},
										select: {
											style: 'multi'
										},
										fnDrawCallback: function (o) {
											if (o._iDisplayStart != oldStart) {
												var targetOffset = $('#persons-table_wrapper').offset().top;
												$('html, body').animate({scrollTop: targetOffset}, 500);
												oldStart = o._iDisplayStart;
											}
										}
									});
									// General buttons actions
									// Publish selected
									$('#publishAll').click(function(event) {
										event.preventDefault();
										var checked = [];
										$.each($('input[name="person-select"]:checked'), function() {
											checked.push($(this).attr('id'));
										});
										if(checked.length === 0)
											bootbox.alert('<span class="bigger-110">Necesitas seleccionar al menos un elemento de la lista para publicar</span>');
										else {
											bootbox.confirm({
											message: '<span class="bigger-110">\u00bfEst\u00e1s seguro que deseas publicar los elementos seleccionados?</span>',
												buttons: {
													confirm: {
														label: '<i class="ace-icon fa fa-check"></i> Si',
														className: 'btn-sm btn-success'
													},
													cancel: {
														label: 'No',
														className: 'btn-sm btn-danger'
													}
												},
												callback: function (result) {
													if(result)
														console.log(checked.join(', '));
												}
											});
											
										}
									});
									// Unpublish selected
									$('#unpublishAll').click(function(event) {
										event.preventDefault();
										var checked = [];
										$.each($('input[name="person-select"]:checked'), function() {
											checked.push($(this).attr('id'));
										});
										if(checked.length === 0)
											bootbox.alert('<span class="bigger-110">Necesitas seleccionar al menos un elemento de la lista para despublicar</span>');
										else {
											bootbox.confirm({
											message: '<span class="bigger-110">\u00bfEst\u00e1s seguro que deseas despublicar los elementos seleccionados?</span>',
												buttons: {
													confirm: {
														label: '<i class="ace-icon fa fa-check"></i> Si',
														className: 'btn-sm btn-success'
													},
													cancel: {
														label: 'No',
														className: 'btn-sm btn-danger'
													}
												},
												callback: function (result) {
													if(result)
														console.log(checked.join(', '));
												}
											});
											
										}
									});
									// Delete selected
									$('#deleteAll').click(function(event) {
										event.preventDefault();
										var checked = [];
										$.each($('input[name="person-select"]:checked'), function() {
											checked.push($(this).attr('id'));
										});
										if(checked.length === 0)
											bootbox.alert('<span class="bigger-110">Necesitas seleccionar al menos un elemento de la lista para eliminar</span>');
										else {
											bootbox.confirm({
											message: '<span class="bigger-110">\u00bfEst\u00e1s seguro que deseas eliminar los elementos seleccionados?<br \><br />Esta acci\u00f3n no se puede deshacer</span>',
												buttons: {
													confirm: {
														label: '<i class="ace-icon fa fa-check"></i> Si',
														className: 'btn-sm btn-success'
													},
													cancel: {
														label: 'No',
														className: 'btn-sm btn-danger'
													}
												},
												callback: function (result) {
													if(result) {
														console.log(checked.join(', '));
														personsTable.draw();
													}
												}
											});
											
										}
									});
									// Individual buttons actions
									// Publish
									personsTable.on('click', '.publishThis', function(event) {
										event.preventDefault();
										var id = $(this).attr('href').substring($(this).attr('href').lastIndexOf('/') + 1);
										var publishUrl = $(this).attr('href');
										bootbox.confirm({
										message: '<span class="bigger-110">\u00bfEst\u00e1s seguro que deseas publicar este elemento?</span>',
											buttons: {
												confirm: {
													label: '<i class="ace-icon fa fa-check"></i> Si',
													className: 'btn-sm btn-success'
												},
												cancel: {
													label: 'No',
													className: 'btn-sm btn-danger'
												}
											},
											callback: function (result) {
												if(result) {
													$('#loading-screen').toggleClass('hide block');
													$.ajax({
														type: 	  'POST',
														dataType: 'json',
														url: 	  publishUrl,
														data:     {'id': id}
													}).done(function(data) {
														console.log(data);
														if(data.response === 'success') {
															$.gritter.add({
																// (string | mandatory) the heading of the notification
																title: '\u00a1Listo!',
																// (string | mandatory) the text inside the notification
																text: '\u00a1Se public\u00f3 con \u00e9xito!',
																time: 2000,
																class_name: 'gritter-success'
															});
															personsTable.draw();
														}
														else {
															if(data.serversays.hasOwnProperty('error')) {
																$.gritter.add({
																	// (string | mandatory) the heading of the notification
																	title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
																	// (string | mandatory) the text inside the notification
																	text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
																	sticky: true,
																	class_name: 'gritter-error'
																});
															}
															else {
																$.gritter.add({
																	// (string | mandatory) the heading of the notification
																	title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh vaya! Hubo un error al publicar<br />',
																	// (string | mandatory) the text inside the notification
																	text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays + '</li></ul><b>Por favor, ay\u00fadanos a corregirlo, report\u00e1ndolo.</b>',
																	sticky: true,
																	class_name: 'gritter-error gritter-light'
																});
															}
														}
														$('#loading-screen').toggleClass('hide block');
													})
												}
											}
										});
									});
									// Unpublish
									personsTable.on('click', '.unpublishThis', function(event) {
										event.preventDefault();
										var id = $(this).attr('href').substring($(this).attr('href').lastIndexOf('/') + 1);
										var unpublishUrl = $(this).attr('href');
										bootbox.confirm({
										message: '<span class="bigger-110">\u00bfEst\u00e1s seguro que deseas despublicar este elemento?</span>',
											buttons: {
												confirm: {
													label: '<i class="ace-icon fa fa-check"></i> Si',
													className: 'btn-sm btn-success'
												},
												cancel: {
													label: 'No',
													className: 'btn-sm btn-danger'
												}
											},
											callback: function (result) {
												if(result) {
													$('#loading-screen').toggleClass('hide block');
													$.ajax({
														type: 	  'POST',
														dataType: 'json',
														url: 	  unpublishUrl,
														data:     {'id': id}
													}).done(function(data) {
														console.log(data);
														if(data.response === 'success') {
															$.gritter.add({
																// (string | mandatory) the heading of the notification
																title: '\u00a1Listo!',
																// (string | mandatory) the text inside the notification
																text: '\u00a1Se despublic\u00f3 con \u00e9xito!',
																time: 2000,
																class_name: 'gritter-success'
															});
															personsTable.draw();
														}
														else {
															if(data.serversays.hasOwnProperty('error')) {
																$.gritter.add({
																	// (string | mandatory) the heading of the notification
																	title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
																	// (string | mandatory) the text inside the notification
																	text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
																	sticky: true,
																	class_name: 'gritter-error'
																});
															}
															else {
																$.gritter.add({
																	// (string | mandatory) the heading of the notification
																	title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh vaya! Hubo un error al despublicar<br />',
																	// (string | mandatory) the text inside the notification
																	text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays + '</li></ul><b>Por favor, ay\u00fadanos a corregirlo, report\u00e1ndolo.</b>',
																	sticky: true,
																	class_name: 'gritter-error gritter-light'
																});
															}
														}
														$('#loading-screen').toggleClass('hide block');
													})
												}
											}
										});
									});
									// Action buttons tooltips
									$('body').tooltip({
										selector: '.tt'
									});
									// Select/deselect all checkboxes
									$('#selectAll').click(function() {
										var isChecked = $('#selectAll').is(':checked');
										if(isChecked) {
											$('.case').each(function() {
												$(this).prop('checked', true);
											});
										}
										else {
											$('.case').each(function(){
												$(this).prop('checked', false);
											});
										}
									});
									$('body').on('click', '.case', function () {
										if ($(this).is(":checked")) {
											var isAllChecked = 0;
											$('.case').each(function() {
												if(!this.checked)
													isAllChecked = 1;
											})              
											if(isAllChecked === 0)
												$('#selectAll').prop('checked', true);
										}
										else
											$('#selectAll').prop('checked', false);
									});
									// Select all tooltip
									$('#selectAllLabel').tooltip({
										title: 'Seleccionar todos',
										placement: 'top'
									});
									// Global actions tooltips
									setTimeout(function() {
										$($('.tableTools-container')).find('a.dt-button').each(function() {
											var div = $(this).find(' > div').first();
											if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
											else $(this).tooltip({container: 'body', title: $(this).text()});
										});
									}, 500);
									// DataTable End
									// Add person button
									var addPerson = $('#addPerson').click(function() {
										$('.persons-new').slideToggle('slow', function() {
											if($(this).is(':visible')) {
												$('#addPerson').find('i').replaceWith('');
												$('#addPerson').text(' Cerrar');
												$('#addPerson').prepend('<i class="ace-icon fa fa-minus align-middle bigger-125"></i>');
											}
											else {
												$('#addPerson').find('i').replaceWith('');
												$('#addPerson').text(' A\u00f1adir nueva persona');
												$('#addPerson').prepend('<i class="ace-icon fa fa-plus align-middle bigger-125"></i>');
											}
										});
										var targetOffset = $('#addPerson').offset().top;
										$('html, body').animate({scrollTop: targetOffset}, 500);
										return false;
									});
									// Form
									var form = $('#person'),
										Action = form.attr('action'),
									// Choose Person o Character
										chosenPerson = false,
										chosenProgram = false,
									// Use Chosen or Direct Input 
										chosenPlugin = false,
										directInput = false;
									$('input:radio[name="type"]').click(function () {
										if($('input:radio[name="type"]:checked').val() === 'person') {
											// Show person group
											$('.person-group').fadeIn();
											// Hide character group
											$('.character-group').hide();
										}
										else if($('input:radio[name="type"]:checked').val() === 'character') {
											// Hide person group
											$('.person-group').hide();
											// Show character group
											$('.character-group').fadeIn();
										}
									}),
									// Choose character person
									$('.character-person').chosen({
										width: '100%',
										allow_single_deselect: true,
										no_results_text: '\u00a1Ops! No encontramos a la persona que lo interpreta. <a href="#" id="character-person-no-results">\u00bfDeseas agregarla?</a>'
									}).on('change', function(evt, params) {
										// Remove new person click handler
										var $row = form.find('.new-person');
										// Remove element containing the field
										$row.slideUp(1000, function() {
											$(this).remove();
										});
										chosenPlugin = true;
										directInput = false;
									}),
									// Add the new person fields
									form.on('click', '#character-person-no-results', function(event) {
										event.preventDefault();
										// Add new person click handler
										var $template = $('#new-character-person-template'),
											$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('new-person').insertBefore($template).hide().slideDown(1000);
										// Remove disabled attribute from inputs
										$clone.each(function() {
											$(this).find(':input').removeAttr('disabled');
										});
										// Close the chosen program window
										$('.character-person').trigger('chosen:updated');
										// Set the value of select to none
										$('.character-person').val('');
										// Update chosen program value
										$('.character-person').trigger('chosen:updated');
										chosenPlugin = false;
										directInput = true;
									}),
									// Nicknames functionality
										nicknames = $('#form-field-nicknames').tag({
										placeholder: $('#form-field-nicknames').attr('placeholder')
									}),
									// Birthday functionality
										birthday = $('#field-birthday').datepicker({
										autoclose: true,
										language: 'es',
										clearBtn: true,
										format: 'dd-mm-yyyy',
										endDate: '0d'
									}).children('[name="birthday"]').mask('99-99-9999'),
									// Day of death functionality
										deathday = $('#field-deathday').datepicker({
										autoclose: true,
										language: 'es',
										clearBtn: true,
										format: 'dd-mm-yyyy',
										endDate: '0d'
									}).children('[name="deathday"]').mask('99-99-9999'),
									// Hobbies functionality
										hobbies = $('#form-field-hobbies').tag({
										placeholder: $('#form-field-hobbies').attr('placeholder')
									});
									// Image functionality
									Dropzone.autoDiscover = false;
									var dropzone = $('#dropzone').dropzone({
										url: Home + '<?php echo IMG_PERSON_UPLOAD_HANDLER; ?>',
										maxFilesize: '<?php echo IMG_MAX_UPLOAD_SIZE; ?>',
										addRemoveLinks: true,
										acceptedFiles: 'image/*',
										dictFileTooBig: 'La imagen es demasiado grande. <?php echo IMG_MAX_UPLOAD_SIZE; ?> MB m\u00E1ximo',
										dictResponseError: 'Hubo un error en el servidor',
										dictRemoveFile: 'Remover',
										dictDefaultMessage: '<span class="smaller-80 bolder">Si no tienes la direcci&oacuten de la imagen, puedes subir una<br /> \<span class="smaller-70 grey">Arrastra el archivo o haz click aqu\u00ed</span><br />\<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
										accept: function(file, done) {
											done();
										},
										success: function(file, response) {
											response = JSON.parse(response);
											if(response.response == "error") {
												var node, _i, _len, _ref, _results;
												var message = response.serversays;
												file.previewElement.classList.add("dz-error");
												_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
												_results = [];
												for (_i = 0, _len = _ref.length; _i < _len; _i++) {
												  node = _ref[_i];
												  _results.push(node.textContent = message);
												}
												return _results;
											}
											else {
												$("#form-field-image-url").val(response.image_url).prop("readonly", true).css({'background-color' :  '#C0C0C0'});
												return file.previewElement.classList.add("dz-success");
											}
										},
										error: function (file, message) {
											var node, _i, _len, _ref, _results;
											if (file.previewElement) {
											  file.previewElement.classList.add("dz-error");
											  if (typeof message !== "String" && message.error) {
												message = message.error;
											  }
											  _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
											  _results = [];
											  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
												node = _ref[_i];
												_results.push(node.textContent = message);
											  }
											  return _results;
											}
										},
										removedfile: function(file) {
											$('#form-field-image-url').val('').prop('readonly', false).css({'background-color' :  ''});
											var _ref;
											return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
										},
										init: function () {
										}
									}),
									// Summernote
										summernote = $('#bio-complete').summernote({
											height: 200,
											tabsize: 2,
											lang: 'es-ES',
											placeholder: $('#bio-complete').attr('placeholder'),
											toolbar: [
												['style', ['style']],
												['font', ['bold', 'italic', 'underline', 'clear']],
												['color', ['color']],
												['para', ['ul', 'ol', 'paragraph']],
												['height', ['height']],
												['table', ['table']],
												['insert', ['link', 'picture', 'hr', 'video']],
												['view', ['fullscreen', 'codeview']],
												['help', ['help']]
											 ]
										  }),
									// Dualist actual program
										actualProgram = $('select[name="duallistbox_actualprogram[]"]').bootstrapDualListbox({
											filterTextClear: 'Mostrar todos',filterPlaceHolder: 'Filtrar',
											moveSelectedLabel: 'Mover seleccionados',
											moveAllLabel: 'Mover todos',
											removeSelectedLabel: 'Remover seleccionados',
											removeAllLabel: 'Remover todos',
											infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
											infoText: 'Mostrando {0}',
											infoTextEmpty: 'Vac\u00eda'
										});
									// Dualist related program
										relatedProgram = $('select[name="duallistbox_relatedprogram[]"]').bootstrapDualListbox({
											filterTextClear: 'Mostrar todos',filterPlaceHolder: 'Filtrar',
											moveSelectedLabel: 'Mover seleccionados',
											moveAllLabel: 'Mover todos',
											removeSelectedLabel: 'Remover seleccionados',
											removeAllLabel: 'Remover todos',
											infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
											infoText: 'Mostrando {0}',
											infoTextEmpty: 'Vac\u00eda'
										}),
									// Add new actual program
										addNewActualProgram = $('.addNewActualProgram').click(function() {
											// Add new related program click handler
											var $template = $('#new-actual-program-template'),
												$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('actual-program').insertBefore($template).hide().slideDown(1000);
										}),
									// Remove new actual program
										removeNewActualProgram = form.on('click', '.removeNewActualProgram', function() {
											// Remove new actual program click handler
											var $row = $(this).parents('.actual-program');
											// Remove element containing the field
											$row.slideUp(1000, function() {
												$(this).remove();
											});
										}),
									// Add new related program
										addNewRelatedProgram = $('.addNewRelatedProgram').click(function() {
											// Add new related program click handler
											var $template = $('#new-related-program-template'),
												$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('related-program').insertBefore($template).hide().slideDown(1000);
										}),
									// Remove new related program
										removeNewRelatedProgram = form.on('click', '.removeNewRelatedProgram', function() {
											// Remove new related program click handler
											var $row = $(this).parents('.related-program');
											// Remove element containing the field
											$row.slideUp(1000, function() {
												$(this).remove();
											});
										}),
									// Dualist actual character
										actualCharacter = $('select[name="duallistbox_actualcharacter[]"]').bootstrapDualListbox({
											filterTextClear: 'Mostrar todos',
											filterPlaceHolder: 'Filtrar',
											moveSelectedLabel: 'Mover seleccionados',
											moveAllLabel: 'Mover todos',
											removeSelectedLabel: 'Remover seleccionados',
											removeAllLabel: 'Remover todos',
											infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
											infoText: 'Mostrando {0}',
											infoTextEmpty: 'Vac\u00eda',
											selectorMinimalHeight: 270
										}),
									// Dualist related character
										relatedCharacter = $('select[name="duallistbox_relatedcharacter[]"]').bootstrapDualListbox({
											filterTextClear: 'Mostrar todos',filterPlaceHolder: 'Filtrar',
											moveSelectedLabel: 'Mover seleccionados',
											moveAllLabel: 'Mover todos',
											removeSelectedLabel: 'Remover seleccionados',
											removeAllLabel: 'Remover todos',
											infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
											infoText: 'Mostrando {0}',
											infoTextEmpty: 'Vac\u00eda',
											selectorMinimalHeight: 270
										}),
										relatedContainer = relatedProgram.bootstrapDualListbox('getContainer'),
										relatedContainer.find('.btn').addClass('btn-white btn-info btn-bold'),
									// Add new actual character
										addNewActualCharacter = $('.addNewActualCharacter').click(function() {
											// Add new related character click handler
											var $template = $('#new-actual-character-template'),
												$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('actual-character').insertBefore($template).hide().slideDown(1000);
										}),
									// Remove new actual character
										removeNewActualCharacter = form.on('click', '.removeNewActualCharacter', function() {
											// Remove new actual character click handler
											var $row = $(this).parents('.actual-character');
											// Remove element containing the field
											$row.slideUp(1000, function() {
												$(this).remove();
											});
										}),
									// Add new related character
										addNewRelatedCharacter = $('.addNewRelatedCharacter').click(function() {
											// Add new related character click handler
											var $template = $('#new-related-character-template'),
												$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('related-character').insertBefore($template).hide().slideDown(1000);
										}),
									// Remove new related character
										removeNewRelatedCharacter = form.on('click', '.removeNewRelatedCharacter', function() {
											// Remove new related character click handler
											var $row = $(this).parents('.related-character');
											// Remove element containing the field
											$row.slideUp(1000, function() {
												$(this).remove();
											});
										}),
									// Keywords functionality
										keywords = $('#form-field-keywords').tag({
											placeholder: $('#form-field-keywords').attr('placeholder')
										}),
									// Award years functionality
										awardYear = $('.award-year').tag({
												placeholder: $('.award-year').attr('placeholder'),
												onChange: function(elem, elem_tags) {
													console.log('d');
												}
										}).parent().find('input:last').mask('9999,'),
									$('#field-award-year div.width-100 div.tags').change(function(event) {
											$(this).find('.tag').each(function() {
												$('.tag:contains(\'____\')').remove();
											})
										});
									// Update award row
									var updateAward = function() {
										var $row = $('.award-result-row')
										$row.each(function(index) {
											var $input = $(this).find('input');
											$input.each(function () {
												var $newInputName = $(this).prop('name').substring(0, $(this).prop('name').indexOf('['));
												$(this).prop('name', $newInputName + '[' + index + ']');
											});
										});
									},
									// Add award
										addAward = $('.addAward').click(function() {
											// Add new award click handler
											var $template = $('#award-template'),
												$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('award').insertBefore($template).hide().slideDown(1000);
												// Add award years functionality
												$clone.find('[name="award-year[]"]').tag({
													placeholder: $('.award-year').attr('placeholder')
												});
												// Award row index change
												$clone.first('div').addClass('award-result-row');
												updateAward();
										}),
									// Remove award
										removeAward = form.on('click', '.removeAward', function() {
											// Remove award click handler
											var $row = $(this).parents('.award');
											// Remove element containing the field
											$row.slideUp(1000, function() {
												$(this).remove();
												updateAward();
											});
										}),
									// Add Fact
										addFact = $('.addFact').click(function() {
											// Add new fact click handler
											var $template = $('#curious-fact-template'),
												$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('fact').insertBefore($template).hide().slideDown(1000);
										}),
									// Remove fact
										removeFact = form.on('click', '.removeFact', function() {
											// Remove fact click handler
											var $row = $(this).parents('.fact');
											// Remove element containing the field
											$row.slideUp(1000, function() {
												$(this).remove();
											});
										}),
									// Validation Start
									// Validations vars
										TypeValCount = 0,
										BranchValCount = 0,
										NameValCount = 0,
										RegexpValCount = 0,
										NameCheckCount = 0,
										CharacterValCount = 0,
										CharacterCheckCount = 0,
										ChosenPersonValCount = 0,
										NewNameCheckValCount = 0,
										BirthdayValCount = 0,
										DeathdayValCount = 0,
										ImageURLValCount = 0,
										ImageCheckURLValCount = 0,
										BioURLValCount = 0,
										BioCompleteValCount = 0,
										NewProgramNameValCount = 0,
										NewProgramURLValCount = 0,
										NewRelatedProgramNameValCount = 0,
										NewRelatedProgramURLValCount = 0,
										KeywordsValCount = 0,
										NewsValCount = 0,
										PhotosValCount = 0,
										VideosValCount = 0,
										FacebookValCount = 0,
										TwitterValCount = 0,
										YouTubeValCount = 0,
										InstagramValCount = 0,
										PinterestValCount = 0,
										PlusValCount = 0,
										validation = form.formValidation({
											framework: 'bootstrap',
											excluded: ':disabled',
											icon: {
												valid: 'glyphicon glyphicon-ok',
												invalid: 'glyphicon glyphicon-remove',
												validating: 'glyphicon glyphicon-refresh'
											},
											fields: {
												type: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(TypeValCount === 0) {
																	$.noty.setText(notifications('error', 100).options.id, '\u00A1Oh no! Necesitas seleccionar si es persona o personaje');
																	TypeValCount++;
																}
															},
															onSuccess: function(e, data) {
																TypeValCount = 0;
																$.noty.close(100);
															}
														}
													}
												},
												branches: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(BranchValCount === 0) {
																	$.noty.setText(notifications('error', 200).options.id, '\u00A1Oh no! Necesitas seleccionar el canal al que pertenece');
																	BranchValCount++;
																}
															},
															onSuccess: function(e, data) {
																BranchValCount = 0;
																$.noty.close(200);
															}
														}
													}
												},
												'person-complete-name': {
													enabled: false,
													excluded: false,
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(NameValCount === 0) {
																	$.noty.setText(notifications('error', 300).options.id, '\u00A1Oh no! El nombre de la persona est\u00E1 vac\u00edo');
																	NameValCount++;
																}
															},
															onSuccess: function(e, data) {
																NameValCount = 0;
																$.noty.close(300);
															}
														},
														regexp: {
															message: 'error',
															regexp: /^([ \u00c0-\u01ffa-zA-Z0-9'\-])+$/,
															onError: function(e, data) {
																if(RegexpValCount === 0) {
																	$.noty.setText(notifications('error', 301).options.id, '\u00A1Oh no! El nombre de la persona s\u00f3lo puede tener letras y n\u00fameros');
																	RegexpValCount++;
																}							
															},
															onSuccess: function(e, data) {
																RegexpValCount = 0;
																$.noty.close(301);
															}
														},
														remote: {
															enabled: false,
															url: Home + 'persona/checkperson/',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar a la persona en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																	});
																else {
																	if(NameCheckCount === 0) {
																		$.noty.setText(notifications('error', 302).options.id, '\u00A1Oh no! El nombre de esa persona ya est\u00E1 registrado');
																		NameCheckCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																NameCheckCount = 0;
																$.noty.close(302);
															}
														}
													}
												},
												'character-person': {
													enabled: false,
													validators: {
														callback: {
															message: 'error',
															callback: function(value, validator, $field) {
																var cpVal = validator.getFieldElements('character-person').val();
																if(cpVal === null || cpVal.length === 0) {
																	if(ChosenPersonValCount === 0) {
																		$.noty.setText(notifications('error', 310).options.id, '\u00A1Oh no! Necesitas seleccionar el nombre de la persona interpreta al personaje');
																		ChosenPersonValCount++;
																	}
																	return false;
																}
																else {
																	ChosenPersonValCount = 0;
																	$.noty.close(310);
																	return true;
																}	
															}
														}
													},
									                onSuccess: function(e, data) {
									                    // data.fv is the plugin instance
									                    // Revalidate the character-complete-name if it's not valid
									                    if (!data.fv.isValidField('character-complete-name')) {
									                        data.fv.revalidateField('character-complete-name');
									                    }
									                }
												},
												'character-complete-name': {
													enabled: false,
													excluded: false,
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(CharacterValCount === 0) {
																	$.noty.setText(notifications('error', 320).options.id, '\u00A1Oh no! El nombre del personaje est\u00E1 vac\u00edo');
																	CharacterValCount++;
																}
															},
															onSuccess: function(e, data) {
																CharacterValCount = 0;
																$.noty.close(320);
															}
														},
														regexp: {
															message: 'error',
															regexp: /^([ \u00c0-\u01ffa-zA-Z0-9-|'\-])+$/,
															onError: function(e, data) {
																if(RegexpValCount === 0) {
																	$.noty.setText(notifications('error', 330).options.id, '\u00A1Oh no! El nombre del personaje s\u00f3lo puede tener letras y n\u00fameros');
																	RegexpValCount++;
																}							
															},
															onSuccess: function(e, data) {
																RegexpValCount = 0;
																$.noty.close(330);
															}
														},
														remote: {
															enabled: false,
															url: Home + 'persona/checkcharacter/',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar al personaje en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																	});
																else {
																	if(CharacterCheckCount === 0) {
																		$.noty.setText(notifications('error', 340).options.id, '\u00A1Oh no! El nombre de ese personaje ya est\u00E1 registrado');
																		CharacterCheckCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																CharacterCheckCount = 0;
																$.noty.close(340);
															}
														}
													},
									                onSuccess: function(e, data) {
									                    // data.fv is the plugin instance
									                    // Revalidate the character-complete-name if it's not valid
									                    if (!data.fv.isValidField('character-person')) {
									                        data.fv.revalidateField('character-person');
									                    }
									                    // Revalidate the character-complete-name if it's not valid
									                    if (!data.fv.isValidField('new-character-person-complete-name')) {
									                        data.fv.revalidateField('new-character-person-complete-name');
									                    }
									                }
												},
												'new-character-person-complete-name': {
													enabled: false,
													excluded: false,
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(NewNameCheckValCount === 0) {
																	$.noty.setText(notifications('error', 350).options.id, '\u00A1Oh no! El nombre de la persona que interpreta al personaje est\u00E1 vac\u00edo');
																	NewNameCheckValCount++;
																}
															},
															onSuccess: function(e, data) {
																NewNameCheckValCount = 0;
																$.noty.close(350);
															}
														}
													},
									                onSuccess: function(e, data) {
									                    // data.fv is the plugin instance
									                    // Revalidate the character-complete-name if it's not valid
									                    if (!data.fv.isValidField('character-complete-name')) {
									                        data.fv.revalidateField('character-complete-name');
									                    }
									                }

												},
												birthday: {
													enabled: false,
													validators: {
														date: {
															format: 'DD-MM-YYYY',
															message: 'error',
															onError: function(e, data) {
																if(BirthdayValCount === 0) {
																	$.noty.setText(notifications('error', 400).options.id, '\u00A1Oh no! La fecha de nacimiento est\u00e1 mal escrita');
																	BirthdayValCount++;
																}							
															},
															onSuccess: function(e, data) {
																BirthdayValCount = 0;
																$.noty.close(400);
															}
														}
													}
												},
												deathday: {
													enabled: false,
													validators: {
														date: {
															format: 'DD-MM-YYYY',
															message: 'error',
															onError: function(e, data) {
																if(DeathdayValCount === 0) {
																	$.noty.setText(notifications('error', 500).options.id, '\u00A1Oh no! La fecha de fallecimiento est\u00e1 mal escrita');
																	DeathdayValCount++;
																}							
															},
															onSuccess: function(e, data) {
																DeathdayValCount = 0;
																$.noty.close(500);
															}
														}
													}
												},
												'image-url': {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(ImageURLValCount === 0) {
																	$.noty.setText(notifications('error', 600).options.id, '\u00A1Oh no! La direcci\u00f3n de la  imagen est\u00E1 vac\u00eda');
																	ImageURLValCount++;
																}
															},
															onSuccess: function(e, data) {
																ImageURLValCount = 0;
																$.noty.close(600);
															}
														},
														regexp: {
															enabled: false,
															message: 'error',
															regexp: /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[\/?#]\S*)?(?:jpe?g|gif|png)$/i,
															onError: function(e, data) {
																if(ImageCheckURLValCount === 0) {
																	$.noty.setText(notifications('error', 601).options.id, '\u00A1Oh no! La direcci\u00f3n de la imagen parece estar mal escrita (s\u00f3lo acepta terminaciones .jpg, .png o .gif)');
																	ImageCheckURLValCount++;
																}							
															},
															onSuccess: function(e, data) {
																ImageCheckURLValCount = 0;
																$.noty.close(601);
															}
														}
													}
												},
												'bio-url': {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(BioURLValCount === 0) {
																	$.noty.setText(notifications('error', 700).options.id, '\u00A1Oh no! La direcci\u00f3n de la biograf\u00eda no es una direcci\u00f3n v\u00e1lida');
																	BioURLValCount++;
																}
															},
															onSuccess: function(e, data) {
																BioURLValCount = 0;
																$.noty.close(700);
															}
														}
													}
												},
												'bio-complete': {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(BioCompleteValCount === 0) {
																	$.noty.setText(notifications('error', 800).options.id, '\u00A1Oh no! La biograf\u00eda est\u00E1 vac\u00eda');
																	BioCompleteValCount++;
																}
															},
															onSuccess: function(e, data) {
																BioCompleteValCount = 0;
																$.noty.close(800);
															}
														}
													}
												},
												'new-actual-program-name[]': {
													enabled: false,
													validators: {
														remote: {
															url: Home + 'programa/checkprogram/',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar al programa en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																});
																else {
																	if(NewProgramNameValCount === 0) {
																		$.noty.setText(notifications('error', 900).options.id, '\u00A1Oh no! El nombre de ese nuevo programa ya est\u00E1 registrado');
																		NewProgramNameValCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																NewProgramNameValCount = 0;
																$.noty.close(900);
															}
														}
													}
												},
												'new-actual-program-url[]': {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(NewProgramURLValCount === 0) {
																	$.noty.setText(notifications('error', 1000).options.id, '\u00A1Oh no! La direcci\u00f3n del nuevo programa actual no es v\u00e1lida');
																	NewProgramURLValCount++;
																}
															},
															onSuccess: function(e, data) {
																NewProgramURLValCount = 0;
																$.noty.close(1000);
															}
														}
													}
												},
												'new-related-program-name[]': {
													enabled: false,
													validators: {
														remote: {
															url: Home + 'programa/checkprogram/',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar al programa en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																});
																else {
																	if(NewRelatedProgramNameValCount === 0) {
																		$.noty.setText(notifications('error', 1100).options.id, '\u00A1Oh no! El nombre de ese nuevo programa relacionado ya est\u00E1 registrado');
																		NewRelatedProgramNameValCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																NewRelatedProgramNameValCount = 0;
																$.noty.close(1100);
															}
														}
													}
												},
												'new-related-program-url[]': {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(NewRelatedProgramURLValCount === 0) {
																	$.noty.setText(notifications('error', 1200).options.id, '\u00A1Oh no! La direcci\u00f3n del nuevo programa relacionado no es v\u00e1lida');
																	NewRelatedProgramURLValCount++;
																}
															},
															onSuccess: function(e, data) {
																NewRelatedProgramURLValCount = 0;
																$.noty.close(1200);
															}
														}
													}
												},
												keywords: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(KeywordsValCount === 0) {
																	$.noty.setText(notifications('error', 1300).options.id, '\u00A1Oh no! Los keywords no pueden quedar vac\u00edos');
																	KeywordsValCount++;
																}
																$('#field-keywords div.width-100 div.tags').css({'border-color': '#f2a696', 'box-shadow': 'none', 'color': '#d68273'});
															},
															onSuccess: function(e, data) {
																KeywordsValCount = 0;
																$.noty.close(1300);
																$('#field-keywords div.width-100 div.tags').css({'border-color': '#9cc573', 'box-shadow': 'none', 'color': '#8bad4c'});
															}
														}
													}
												},
												news: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(NewsValCount === 0) {
																	$.noty.setText(notifications('error', 1400).options.id, '\u00A1Oh no! La direcci\u00f3n de las noticias no es v\u00e1lida');
																	NewsValCount++;
																}
															},
															onSuccess: function(e, data) {
																NewsValCount = 0;
																$.noty.close(1400);
															}
														}
													}
												},
												photos: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(PhotosValCount === 0) {
																	$.noty.setText(notifications('error', 1500).options.id, '\u00A1Oh no! La direcci\u00f3n de las fotos no es v\u00e1lida');
																	PhotosValCount++;
																}
															},
															onSuccess: function(e, data) {
																PhotosValCount = 0;
																$.noty.close(1500);
															}
														}
													}
												},
												videos: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(VideosValCount === 0) {
																	$.noty.setText(notifications('error', 1600).options.id, '\u00A1Oh no! La direcci\u00f3n de las videos no es v\u00e1lida');
																	VideosValCount++;
																}
															},
															onSuccess: function(e, data) {
																VideosValCount = 0;
																$.noty.close(1600);
															}
														}
													}
												},
												facebook: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(FacebookValCount === 0) {
																	$.noty.setText(notifications('error', 1700).options.id, '\u00A1Oh no! La direcci\u00f3n de Facebook no es v\u00e1lida');
																	FacebookValCount++;
																}
															},
															onSuccess: function(e, data) {
																FacebookValCount = 0;
																$.noty.close(1700);
															}
														}
													}
												},
												twitter: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(TwitterValCount === 0) {
																	$.noty.setText(notifications('error', 1800).options.id, '\u00A1Oh no! La direcci\u00f3n de Twitter no es v\u00e1lida');
																	TwitterValCount++;
																}
															},
															onSuccess: function(e, data) {
																TwitterValCount = 0;
																$.noty.close(1800);
															}
														}
													}
												},
												youtube: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(YouTubeValCount === 0) {
																	$.noty.setText(notifications('error', 1900).options.id, '\u00A1Oh no! La direcci\u00f3n de YouTube no es v\u00e1lida');
																	YouTubeValCount++;
																}
															},
															onSuccess: function(e, data) {
																YouTubeValCount = 0;
																$.noty.close(1900);
															}
														}
													}
												},
												instagram: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(InstagramValCount === 0) {
																	$.noty.setText(notifications('error', 2000).options.id, '\u00A1Oh no! La direcci\u00f3n de Instagram no es v\u00e1lida');
																	InstagramValCount++;
																}
															},
															onSuccess: function(e, data) {
																InstagramValCount = 0;
																$.noty.close(2000);
															}
														}
													}
												},
												pinterest: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(PinterestValCount === 0) {
																	$.noty.setText(notifications('error', 2100).options.id, '\u00A1Oh no! La direcci\u00f3n de Pinterest no es v\u00e1lida');
																	PinterestValCount++;
																}
															},
															onSuccess: function(e, data) {
																PinterestValCount = 0;
																$.noty.close(2100);
															}
														}
													}
												},
												gplus: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(PlusValCount === 0) {
																	$.noty.setText(notifications('error', 2200).options.id, '\u00A1Oh no! La direcci\u00f3n de Google Plus no es v\u00e1lida');
																	PlusValCount++;
																}
															},
															onSuccess: function(e, data) {
																PlusValCount = 0;
																$.noty.close(2200);
															}
														}
													}
												}
											}
										}).on('err.field.fv', function(e, data) {
											// $(e.target)  --> The field element
											// data.fv      --> The FormValidation instance
											// data.field   --> The field name
											// data.element --> The field element
											if (data.fv.getSubmitButton()) {
												data.fv.disableSubmitButtons(false);
											}
											// Hide the messages
											data.element
												.data('fv.messages')
												.find('.help-block[data-fv-for="' + data.field + '"]').hide();
										}).on('success.field.fv', function(e, data) {
											if (data.fv.getSubmitButton()) {
												data.fv.disableSubmitButtons(false);
											}
										}).on('click', 'input:radio[name="type"]', function() {
											if($('input:radio[name="type"]:checked').val() === 'person') {
												// Disables the choose person field validator
												form.formValidation('enableFieldValidators', 'character-person', false);
												// Clean Inputs
												cleanCharacterInputs();
												// Close all notifications
												$.noty.closeAll();
												// Enables the person name field validator
												form.formValidation('enableFieldValidators', 'person-complete-name', true)
												.on('keydown', 'input[name="person-name"], input[name="person-surname"], input[name="person-maidenname"]', function() {
													form.formValidation('enableFieldValidators', 'person-complete-name', false, 'remote');
												}).on('blur', 'input[name="person-name"], input[name="person-surname"], input[name="person-maidenname"]', function() {
													var n = form.find('[name="person-name"]').val(),
														s = form.find('[name="person-surname"]').val(),
														m = form.find('[name="person-maidenname"]').val();
														// Set the complete name field value
														form.find('[name="person-complete-name"]').val([n, s, m].join(' '));
													form.formValidation('enableFieldValidators', 'person-complete-name', true, 'remote');
													// Revalidate it
													form.formValidation('revalidateField', 'person-complete-name');
												});
											}
											else if($('input:radio[name="type"]:checked').val() === 'character') {
												// Disable the person name field validator
												form.formValidation('enableFieldValidators', 'person-complete-name', false);
												// Clean Inputs
												cleanPersonInputs();
												// Close all notifications
												$.noty.closeAll();
												// Enables the choose person field validator
												form.formValidation('enableFieldValidators', 'character-person', true);
												// Enables the character name field validator
												form.formValidation('enableFieldValidators', 'character-complete-name', true)
												.on('keydown', 'input[name="character-name"], input[name="character-surname"], input[name="character-maidenname"]', function() {
													form.formValidation('enableFieldValidators', 'character-complete-name', false, 'remote');
												}).on('blur', 'input[name="character-name"], input[name="character-surname"], input[name="character-maidenname"]', function() {
													var n 	 = form.find('[name="character-name"]').val(),
														s    = form.find('[name="character-surname"]').val(),
														m    = form.find('[name="character-maidenname"]').val(),
														iorn;
														// Validation via dropdown menu + character name
														if(chosenPlugin) {
															// Enables the choose person field validator
															form.formValidation('enableFieldValidators', 'character-person', true);
															// Disables the direct input field validator
															form.formValidation('enableFieldValidators', 'new-character-person-complete-name', false);
															iorn = form.find('#form-field-character-person').val(); // The Id of the person
														}
														else if(directInput) {
															// Disables the choose person field validator
															form.formValidation('enableFieldValidators', 'character-person', false);
															// Enables the direct input field validator
															form.formValidation('enableFieldValidators', 'new-character-person-complete-name', true);
															var nn = form.find('[name="new-character-person-name"]').val(),
																ns = form.find('[name="new-character-person-surname"]').val(),
																nm = form.find('[name="new-character-person-maidenname"]').val();
															form.find('[name="new-character-person-complete-name"]').val([nn, ns, nm].join(' ')); // The new name of the person
															iorn = form.find('[name="new-character-person-complete-name"]').val();
														}
														// Set the complete name field value with the id or the new person name + complete name of the character
														var completeName = [n, s, m].join(' '),
															validationName = [iorn, completeName].join('|');
														form.find('[name="character-complete-name"]').val(validationName);
													form.formValidation('enableFieldValidators', 'character-complete-name', true, 'remote');
													// Revalidate it
													form.formValidation('revalidateField', 'character-complete-name');
												});
											}
										}).on('change', 'input[name="birthday"]', function(e) {
											form.formValidation('enableFieldValidators', 'birthday', true);
											form.formValidation('revalidateField', 'birthday');
										}).on('change', 'input[name="deathday"]', function(e) {
											form.formValidation('enableFieldValidators', 'deathday', true);
											form.formValidation('revalidateField', 'deathday');
										}).on('keydown', '[name="image-url"]', function() {
											form.formValidation('enableFieldValidators', 'image-url', false, 'regexp');
										}).on('blur', '[name="image-url"]', function() {
											form.formValidation('enableFieldValidators', 'image-url', true, 'regexp');
											form.formValidation('revalidateField', 'image-url');
										}).on('keydown', '[name="bio-url"]', function() {
											form.formValidation('enableFieldValidators', 'bio-url', false);
										}).on('blur', '[name="bio-url"]', function() {
											form.formValidation('enableFieldValidators', 'bio-url', true);
											form.formValidation('revalidateField', 'bio-url');
										}).on('keydown', '[name="new-actual-program-name[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-actual-program-name[]', false);
										}).on('blur', '[name="new-actual-program-name[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-actual-program-name[]', true);
											form.formValidation('revalidateField', 'new-actual-program-name[]');
										}).on('keydown', '[name="new-actual-program-url[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-actual-program-url[]', false);
										}).on('blur', '[name="new-actual-program-url[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-actual-program-url[]', true);
											form.formValidation('revalidateField', 'new-actual-program-url[]');
										}).on('click', '.addNewActualProgram', function() {
											var $actualProgramName = form.find('.actual-program').find('[name="new-actual-program-name[]"]'),
												$actualProgramURL = form.find('.actual-program').find('#form-field-new-actual-program-url').attr('name', 'new-actual-program-url[]');
											// Add new field
											form.formValidation('addField', $actualProgramName);
											form.formValidation('addField', $actualProgramURL);
										}).on('click', '.removeNewActualProgram', function() {
											var	$actualProgramName = $(this).parent().parent().parent().find('[name="new-actual-program-name[]"]'),
												$actualProgramURL = $(this).parent().parent().parent().find('[name="new-actual-program-url[]"]');
											// Remove field
											form.formValidation('removeField', $actualProgramName);
											form.formValidation('removeField', $actualProgramURL);
										}).on('keydown', '[name="new-related-program-name[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-related-program-name[]', false);
										}).on('blur', '[name="new-related-program-name[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-related-program-name[]', true);
											form.formValidation('revalidateField', 'new-related-program-name[]');
										}).on('keydown', '[name="new-related-program-url[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-related-program-url[]', false);
										}).on('blur', '[name="new-related-program-url[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-related-program-url[]', true);
											form.formValidation('revalidateField', 'new-related-program-url[]');
										}).on('click', '.addNewRelatedProgram', function() {
											var $relatedProgramName = form.find('.related-program').find('[name="new-related-program-name[]"]'),
												$relatedProgramURL = form.find('.related-program').find('#form-field-new-related-program-url').attr('name', 'new-related-program-url[]');
											// Add new field
											form.formValidation('addField', $relatedProgramName);
											form.formValidation('addField', $relatedProgramURL);
										}).on('click', '.removeNewRelatedProgram', function() {
											var	$relatedProgramName = $(this).parent().parent().parent().find('[name="new-related-program-name[]"]'),
												$relatedProgramURL = $(this).parent().parent().parent().find('[name="new-related-program-url[]"]');
											// Remove field
											form.formValidation('removeField', $relatedProgramName);
											form.formValidation('removeField', $relatedProgramURL);
										}).on('keydown', '#field-keywords input', function() {
											form.formValidation('enableFieldValidators', 'keywords', false);
										}).on('blur', '#field-keywords input', function() {
											form.formValidation('enableFieldValidators', 'keywords', true);
											form.formValidation('revalidateField', 'keywords');
										}).on('keydown', '[name="news"]', function() {
											form.formValidation('enableFieldValidators', 'news', false);
										}).on('blur', '[name="news"]', function() {
											form.formValidation('enableFieldValidators', 'news', true);
											form.formValidation('revalidateField', 'news');
										}).on('keydown', '[name="photos"]', function() {
											form.formValidation('enableFieldValidators', 'photos', false);
										}).on('blur', '[name="photos"]', function() {
											form.formValidation('enableFieldValidators', 'photos', true);
											form.formValidation('revalidateField', 'photos');
										}).on('keydown', '[name="videos"]', function() {
											form.formValidation('enableFieldValidators', 'videos', false);
										}).on('blur', '[name="videos"]', function() {
											form.formValidation('enableFieldValidators', 'videos', true);
											form.formValidation('revalidateField', 'videos');
										}).on('keydown', '[name="facebook"]', function() {
											form.formValidation('enableFieldValidators', 'facebook', false);
										}).on('blur', '[name="facebook"]', function() {
											form.formValidation('enableFieldValidators', 'facebook', true);
											form.formValidation('revalidateField', 'facebook');
										}).on('keydown', '[name="twitter"]', function() {
											form.formValidation('enableFieldValidators', 'twitter', false);
										}).on('blur', '[name="twitter"]', function() {
											form.formValidation('enableFieldValidators', 'twitter', true);
											form.formValidation('revalidateField', 'twitter');
										}).on('keydown', '[name="youtube"]', function() {
											form.formValidation('enableFieldValidators', 'youtube', false);
										}).on('blur', '[name="youtube"]', function() {
											form.formValidation('enableFieldValidators', 'youtube', true);
											form.formValidation('revalidateField', 'youtube');
										}).on('keydown', '[name="instagram"]', function() {
											form.formValidation('enableFieldValidators', 'instagram', false);
										}).on('blur', '[name="instagram"]', function() {
											form.formValidation('enableFieldValidators', 'instagram', true);
											form.formValidation('revalidateField', 'instagram');
										}).on('keydown', '[name="pinterest"]', function() {
											form.formValidation('enableFieldValidators', 'pinterest', false);
										}).on('blur', '[name="pinterest"]', function() {
											form.formValidation('enableFieldValidators', 'pinterest', true);
											form.formValidation('revalidateField', 'pinterest');
										}).on('keydown', '[name="gplus"]', function() {
											form.formValidation('enableFieldValidators', 'gplus', false);
										}).on('blur', '[name="gplus"]', function() {
											form.formValidation('enableFieldValidators', 'gplus', true);
											form.formValidation('revalidateField', 'gplus');
										}).on('success.form.fv', function(e) {
											var $form = $(e.target), // Form instance
												$button = $form.data('formValidation').getSubmitButton(), // Get the clicked button
												$statusField = $form.find('[name="status"]'); //update the "status" field before submitting the form
											switch ($button.attr('id')) {
												case 'save':
													$statusField.val('save');
													//bootbox.alert('La informaci');
													break;

												case 'publish':
													$statusField.val('publish');
													//bootbox.alert('The article will be saved as a draft');
													break;

												case 'saveButton':
												default:
													$statusField.val('unpublished');
													//bootbox.alert('The article will be saved');
													break;
											}
											// Form submit
											form.submit(function(event) {
												event.preventDefault();
												var data = {
													'type'		   	  	  	   : $('[name="type"]').val(),
													'branch'	   	  	  	   : $('[name="branches"]').val(),
													'name'		   	  	  	   : $('[name="person-complete-name"]').val(),
													'gender'	   	  	  	   : $('[name="gender"]').val(),
													'nicknames'	   	  	  	   : $('[name="nicknames"]').val(),
													'birthday'	   	  	  	   : $('[name="birthday"]').val(),
													'deathday'	   	  	  	   : $('[name="deathday"]').val(),
													'birthplace'   	  	  	   : $('[name="birthplace"]').val(),
													'nacionality'  	  	  	   : $('[name="nacionality"]').val(),
													'hobbies'	   	  	  	   : $('[name="hobbies"]').val(),
													'image'		   	  	  	   : $('[name="image-url"]').val(),
													'bio-url'	   	  	  	   : $('[name="bio-url"]').val(),
													'bio-resume'   	  	  	   : $('[name="bio-resume"]').val(),
													'bio-complete' 	  	  	   : $('[name="bio-complete"]').val(),
													'curious-fact'			   : $('[name="curious-fact[]"]').val(),
													'cea-type'		  	  	   : $('[name="cea-type[]"]').val(),
													'actual-programs'	  	   : $('[name="duallistbox_actualprogram[]"]').val(),
													'new-actual-program-name'  : $('[name="new-actual-program-name[]"]').val(),
													'new-actual-program-url'   : $('[name="new-actual-program-url[]"]').val(),
													'related-programs'		   : $('[name="duallistbox_relatedprogram[]"]').val(),
													'new-related-program-name' : $('[name="new-related-program-name[]"]').val(),
													'new-related-program-url'  : $('[name="new-related-program-url[]"]').val(),
													'actual-characters'	  	   : $('[name="duallistbox_actualcharacter[]"]').val(),
													'related-characters'	   : $('[name="duallistbox_relatedcharacter[]"]').val(),
													'award-names'			   : $('[name="award-name[]"]').val(),
													'award-categories'		   : $('[name="award-category[]"]').val(),
													'award-years'			   : $('[name="award-year[]"]').val(),
													'award-results'			   : $('[name="award-result[]"]').val(),
													'keywords'				   : $('[name="keywords"]').val(),
													'news'					   : $('[name="news"]').val(),
													'photos'				   : $('[name="photos"]').val(),
													'videos'				   : $('[name="videos"]').val(),
													'facebook'				   : $('[name="facebook"]').val(),
													'twitter'				   : $('[name="twitter"]').val(),
													'youtube'				   : $('[name="youtube"]').val(),
													'instagram'				   : $('[name="instagram"]').val(),
													'pinterest'				   : $('[name="pinterest"]').val(),
													'gplus'					   : $('[name="gplus"]').val(),
													'status'				   : $statusField
												};
												data = $(this).serialize();
												$.ajax({
													type: 'POST',
													dataType: 'json',
													url: Action,
													data: data,
												}).done(function(data) {
													console.log(data);
													if(data.response === 'success') {
														$.gritter.add({
															// (string | mandatory) the heading of the notification
															title: '\u00a1Listo!',
															// (string | mandatory) the text inside the notification
															text: '\u00a1Se cre\u00f3 con \u00e9xito!',
															time: 2000,
															class_name: 'gritter-success'
														});
														/*window.setTimeout(function() {
															window.location.href = Home + 'user/';
														}, 2800);*/
													}
													else {
														if(data.serversays.error[0].errorcode == 0) {
															$.gritter.add({
																// (string | mandatory) the heading of the notification
																title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
																// (string | mandatory) the text inside the notification
																text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
																sticky: true,
																class_name: 'gritter-error'
															});
														}
														else {
															var html = '<ul class="fa-ul">';
															$.each(data.serversays.error, function(key){ html +=  '<li><i class="ace-icon fa fa-hand-o-right"></i> ' + data.serversays.error[key].errormessage + '</li>';});
															html += '</ul>';
															$.gritter.add({
																// (string | mandatory) the heading of the notification
																title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh vaya! ' + data.serversays.title + '<br />',
																// (string | mandatory) the text inside the notification
																text: html + data.serversays.texts,
																sticky: true,
																class_name: 'gritter-error gritter-light'
															});
														}
													}
												}).fail(function() {
													$.gritter.add({
														// (string | mandatory) the heading of the notification
														title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! Algo raro pasa aqu\u00ed<br />',
														// (string | mandatory) the text inside the notification
														text: 'La informaci\u00f3n no la puedo enviar.<br />Por favor, ay\u00fadanos a corregirlo report\u00E1ndolo.',
														sticky: true,
														class_name: 'gritter-error'
													});
												});
												return false;
											});
										});
									// Validation End
									// Cleans the inputs value for person
									function cleanPersonInputs() {
										$('.person-group').find('#form-field-person-name').val('');
										$('.person-group').find('#form-field-person-surname').val('');
										$('.person-group').find('#form-field-person-maidenname').val('');
										$('.person-group').find('[name="person-complete-name"]').val('');
									}
									// Cleans the inputs value for character
									function cleanCharacterInputs() {
										// Close the chosen person window
										$('.character-person').trigger('chosen:updated');
										// Set the value of select to none
										$('.character-person').val('');
										// Update chosen person value
										$('.character-person').trigger('chosen:updated');
										$('.character-group').find('#form-field-character-name').val('');
										$('.character-group').find('#form-field-character-surname').val('');
										$('.character-group').find('#form-field-character-maidenname').val('');
									}
									// Tooltip placement on right or left
									function tooltip_placement(context, source) {
										var $source = $(source);
										var $parent = $source.closest('table')
										var off1 = $parent.offset();
										var w1 = $parent.width();
										var off2 = $source.offset();
										if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
										return 'left';
									}
									// Notifications
									function notifications(type, id) {
										if(typeof id !== 'undefined')	
											var n = noty({
												id			: id,
												text        : type,
												type        : type,
												dismissQueue: true,
												killer		: false,
												layout      : 'bottom',
												maxVisible  : 1
											});
										else
											var n = noty({
												text        : type,
												type        : type,
												dismissQueue: true,
												killer		: false,
												layout      : 'bottom',
												maxVisible  : 1
											});
										return n;
									}
								});
							</script>