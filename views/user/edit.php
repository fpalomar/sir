	<div class="page-header">
		<h1>
			Control de usuarios
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
					Editar usuario
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="ace-settings-container" id="ace-settings-container">
		<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
			<i class="ace-icon fa fa-cog bigger-130"></i>
		</div>
		<div class="ace-settings-box clearfix" id="ace-settings-box">
			<div class="pull-left">
				<div class="widget-header widget-header-blue widget-header-flat">
					<h4 class="widget-title lighter"><i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer blue"></i>Admin Tools</h4>
				</div>
				<div class="ace-settings-item">
					<input type="checkbox" class="ace ace-switch ace-switch-4" id="skip-validation">
					<label class="lbl" for="ace-settings-breadcrumbs">
						&nbsp;Validaciones
					</label>
				</div>
				<div class="ace-settings-item">
					<a href="<?php echo URL ?>user/clear/" class="btn-alt"><i class="ace-icon fa fa-trash-o"></i></a>
					<label class="lbl" for="ace-settings-breadcrumbs">
						Mensajes
					</label>
				</div>
			</div><!-- /.pull-left -->
		</div><!-- /.ace-settings-box -->
	</div><!-- /.ace-settings-container -->
<?php
	$errorFlag = false;
	$formFlag = true;
	if(isset($_SESSION["edituser"])) {
		$errorFlag = true;
		$errorHandler = json_decode($_SESSION["edituser"]);
		$title = $errorHandler->serversays->title;
		$text = $errorHandler->serversays->texts;
		$error = $errorHandler->serversays->error;
		if($error[0]->errorcode == 0) {
			$message = '<li><i class="fa-li fa fa-database"></i>' . $error[0]->errormessage . '</li>';
			$excl = "&iexcl;Oh no!";
			$faIcon = "fa-times";
			$faFace = "fa-frown-o";
			$alertType = "danger"; 
		}
		else {
			$message = '';
			foreach($error as $key => $value)
				$message.= '<li><i class="fa-li fa fa-hand-o-right"></i>' . $value->errormessage . '</li>';
			$excl = "&iexcl;Oh vaya!";
			$faIcon = "fa-times";
			$faFace = "fa-frown-o";
			$alertType = "danger"; 
		}
		$id = $errorHandler->values->id;
		$login = $errorHandler->values->login;
		$pass = $errorHandler->values->pass;
		$rol = $errorHandler->values->rol;
		if(isset($errorHandler->values->ramas))
			$ramas = implode("-", $errorHandler->values->ramas);
	}
	else if(in_array(false, $this->user, true)) {
		$errorFlag = true;
		$formFlag = false;
		$errorHandler = json_decode($this->error);
		$messages = $errorHandler->messages;
		$title = $errorHandler->texts->dberror->title;
		$text = $errorHandler->texts->dberror->texts;
		$message = '<li><i class="fa-li fa fa-database"></i>' . $messages->{0} . ' (' . $this->user[1][1] . ')</li>';
		$excl = "&iexcl;Oh no!";
		$faIcon = "fa-times";
		$faFace = "fa-frown-o";
		$alertType = "danger"; 
	}
	else if(in_array(true, $this->user, true)) {
		if(empty($this->user[1])) {
			$errorFlag = true;
			$formFlag = false;
			$errorHandler = json_decode($this->error);
			$messages = $errorHandler->messages;
			$title = $errorHandler->texts->dbnull->title;
			$text = $errorHandler->texts->dbnull->texts;
			$message = '<li><i class="fa-li fa fa-database"></i>' . $messages->{10} . '</li>';
			$excl = "&iexcl;Oh no!";
			$faIcon = "fa-exclamation-triangle";
			$faFace = "fa-meh-o";
			$alertType = "warning";
		}
		else {
			$id = $this->user[1][0]['idusuariosAdm'];
			$login = $this->user[1][0]['login'];
			$rol = $this->user[1][0]['rol'];
			$ramas = $this->user[1][0]['permissions'];
		}
	}
	if($errorFlag) {
?>
	<div class="alert alert-block alert-<?php echo $alertType; ?>">
		<button data-dismiss="alert" class="close" type="button">
			<i class="ace-icon fa fa-times"></i>
		</button>
		<p>
			<strong>
				<i class="ace-icon fa <?php echo $faIcon; ?>"></i>
				<?php echo $excl; ?>
			</strong><?php echo $title; ?> <i class="ace-icon fa <?php echo $faFace; ?>"></i>:
		</p>
		<p>
			<ul class="fa-ul">
				<?php echo $message.PHP_EOL; ?>
			</ul>
		</p>
		<p>
			<?php echo $text.PHP_EOL; ?>
		</p>
	</div>
<?php
	}
	if($formFlag) {
?>
	<form class="form-horizontal" id="users" role="form" method="post" action="<?php echo URL; ?>user/editSave/<?php echo $id; ?>/">
		<div class="form-group">
			<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-1">Usuario</label>
			<div class="col-xs-10 col-sm-4">
				<input type="text" value="<?php echo $login; ?>" name="login" placeholder="Usuario" class="col-xs-12 col-sm-12" readonly="readonly" />
			</div>
			<div class="col-xs-2 col-sm-1" style="padding-left: 0px;">
				<a id="btn-user-locked" class="btn btn-default btn-inverse btn-sm" data-rel="tooltip" title="Desbloquear" data-placement="right">
					<i class="ace-icon fa fa-lock icon-only"></i>
				</a>
				<a id="btn-user-unlocked" class="btn btn-default btn-grey btn-sm" style="display: none;" data-rel="tooltip" title="Bloquear" data-placement="right">
					<i class="ace-icon fa fa-unlock icon-only"></i>
				</a>
			</div>
		</div>
		<div class="form-group">
			<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-2">Contrase&ntilde;a</label>
			<div class="col-xs-10 col-sm-4">
				<input type="text" name="pass" placeholder="Contrase&ntilde;a" class="col-xs-12 col-sm-12" />
			</div>
			<div class="col-xs-2 col-sm-1" style="padding: 5px;">
				<span class="help-button" data-rel="popover" data-trigger="hover" data-placement="left" data-content="Deja en blanco para no cambiar la contrase&ntilde;a" title=";-)">?</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label no-padding-right" for="form-field-3">Rol</label>
			<div class="col-sm-4">
				<select name="rol" class="col-xs-12 col-sm-12">
					<option value="default"<?php if($rol == 'default') echo ' selected'; ?>>Default</option>
					<option value="admin"<?php if($rol == 'admin') echo ' selected'; ?>>Admin</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label no-padding-right" for="form-field-4">Permisos</label>
			<div class="col-sm-4">
				<div class="checkbox">
					<label>
						<input type="checkbox" class="ace" id="selectall" name="ramas[]" value="0"<?php if(isset($ramas)) {if($ramas == 0) echo ' checked';}?>>
						<span class="lbl"> <b>Todos</b></span>
					</label>
				</div>
			<?php
				foreach($this->branchList as $key => $value) {
					$n = $key + 1;
					$checkbox = '';
					if(isset($ramas)) {
						if($ramas == 0)
							$checkbox = ' checked';
						else {
							if(strpos($ramas, (string)$n) === false)
								$checkbox = '';
							else
								$checkbox = ' checked';
						}
					}
					echo '<div class="checkbox">'.PHP_EOL;
					echo '	<label>'.PHP_EOL;
					echo '		<input type="checkbox" class="ace case" name="ramas[]" value="'.$value['idBranch'] .'"' . $checkbox . '>'.PHP_EOL;
					echo '		<span class="lbl"> ' . $value['nombre'] . '</span>'.PHP_EOL;
					echo '	</label>'.PHP_EOL;
					echo '</div>'.PHP_EOL;
				}
			?>
			</div>
		</div>
		<div class="clearfix form-actions">
			<div class="text-center col-xs-12 col-sm-12">
				<button class="btn btn-info" type="submit">
					<i class="ace-icon fa fa-check bigger-110"></i>
					Editar
				</button>
				&nbsp; &nbsp; &nbsp;
				<button class="btn" type="reset">
					<i class="ace-icon fa fa-undo bigger-110"></i>
					Reset
				</button>
			</div>
		</div>
	</form>
	<?php
	}
	?>
	<script type="text/javascript">
		$(function() {
			var user = $('.breadcrumb li:last-child').text();
			var pen = $('.breadcrumb li:last').prev("li");
			var linkPen = $('a', pen).attr("href");
			var newLinkPen =  linkPen + user + '/';
			var LoginValCont = 0;
			var PassValCont = 0;
			var RegexpValCont = 0;
			var RamasValCont = 0;
			var UserCheckCont = 0;
			var Home = $('body').attr('data-home-url');
			var Action = $('#users').attr('action');
			var Key = 0;
			$('a', pen).prop("href", newLinkPen);
			$('#btn-user-locked').click(function() {
				$('#btn-user-locked').hide();
				$('#btn-user-unlocked').show();
				$('[name="login"]').removeAttr('readonly');
			});
			$('#btn-user-unlocked').click(function() {
				$('#btn-user-locked').show();
				$('#btn-user-unlocked').hide();
				$('[name="login"]').attr('readonly', 'readonly');
			});
			$('#users').formValidation({
				framework: 'bootstrap',
				icon: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					login: {
						validators: {
							notEmpty: {
								message: 'error',
								onError: function(e, data) {
									if(LoginValCont === 0) {
										$.noty.setText(notifications('error', 100).options.id, '\u00A1Oh no! El nombre de usuario est\u00E1 vac\u00edo');
										LoginValCont++;
									}
								},
								onSuccess: function(e, data) {
									LoginValCont = 0;
									$.noty.close(100);
								}
							},
							regexp: {
								message: 'error',
								regexp: /^[a-zA-Z0-9_]+$/,
								onError: function(e, data) {
									if(RegexpValCont === 0) {
										$.noty.setText(notifications('error', 101).options.id, 'Hum... El nombre de usuario s\u00f3lo puede tener letras y n\u00fameros');
										RegexpValCont++;
									}							
								},
								onSuccess: function(e, data) {
									RegexpValCont = 0;
									$.noty.close(101);
								}
							},
							remote: {
								enabled: false,
								url: Home + 'user/checkuser/',
								type: 'POST',
								message: 'error',
								onError: function(e, data) {
									if(UserCheckCont === 0) {
										$.noty.setText(notifications('error', 102).options.id, 'Uyyy... El nombre de usuario ya est\u00E1 ocupado');
										UserCheckCont++;
									}							
								},
								onSuccess: function(e, data) {
									UserCheckCont = 0;
									$.noty.close(102);
								}
							}
						}
					},
					'ramas[]': {
						validators: {
							notEmpty: {
								message: 'error',
								onError: function(e, data) {
									if(RamasValCont === 0) {
										$.noty.setText(notifications('error', 400).options.id, '\u00A1Pop! Falta la rama');
										RamasValCont++;
									}
								},
								onSuccess: function(e, data) {
									RamasValCont = 0;
									$.noty.close(400);
								}
							}
						}
					}
				}
			}).on('success.form.fv', function(e) {
				// Form Submit
				$("#users").submit(function(event){
					event.preventDefault();
					var data = {
						"login": $('[name="login"]').val(),
						"pass" : $('[name="pass"]').val(),
						"rol"  : $('[name="rol"]').val(),
						"ramas": $('[name="ramas[]"]').val()
					};
					data = $(this).serialize();
					$.ajax({
						type: "POST",
						dataType: "json",
						url: Action,
						data: data,
					}).done(function(data) {
						if(data.response === "success") {
							$.gritter.add({
								// (string | mandatory) the heading of the notification
								title: '\u00a1Listo!',
								// (string | mandatory) the text inside the notification
								text: '\u00a1Se edit\u00f3 con \u00e9xito!',
								time: 2000,
								class_name: 'gritter-success'
							});
							window.setTimeout(function() {
								window.location.href = Home + 'user/';
							}, 2800);
						}
						else {
							if(data.serversays.error[0].errorcode == 0) {
								$.gritter.add({
									// (string | mandatory) the heading of the notification
									title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
									// (string | mandatory) the text inside the notification
									text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
									sticky: true,
									class_name: 'gritter-error'
								});
							}
							else {
								var html = '<ul class="fa-ul">';
								$.each(data.serversays.error, function(key){ html +=  '<li><i class="ace-icon fa fa-hand-o-right"></i> ' + data.serversays.error[key].errormessage + '</li>';});
								html += '</ul>';
								$.gritter.add({
									// (string | mandatory) the heading of the notification
									title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh vaya! ' + data.serversays.title + '<br />',
									// (string | mandatory) the text inside the notification
									text: html + data.serversays.texts,
									sticky: true,
									class_name: 'gritter-error gritter-light'
								});
							}
						}
					}).fail(function() {
						$.gritter.add({
							// (string | mandatory) the heading of the notification
							title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! Algo raro pasa aqu\u00ed<br />',
							// (string | mandatory) the text inside the notification
							text: 'La informaci\u00f3n no la puedo enviar.<br />Por favor, ay\u00fadanos a corregirlo report\u00E1ndolo.',
							sticky: true,
							class_name: 'gritter-error'
						});
					});
					return false;
				});
			}).on('err.field.fv', function(e, data) {
				// $(e.target)  --> The field element
				// data.fv      --> The FormValidation instance
				// data.field   --> The field name
				// data.element --> The field element
				
				// Hide the messages
				data.element
					.data('fv.messages')
					.find('.help-block[data-fv-for="' + data.field + '"]').hide();
			}).on('success.field.fv', function(e, data) {
				if (data.fv.getSubmitButton()) {
					data.fv.disableSubmitButtons(false);
				}
			}).on('keydown', '[name="login"]', function() {
				$('#users').formValidation('enableFieldValidators', 'login', false, 'remote');
			}).on('blur', '[name="login"]', function() {
				$('#users').formValidation('enableFieldValidators', 'login', true, 'remote');
				$('#users').formValidation('revalidateField', 'login');
			});
			$('#selectall').click(function() {
				var isChecked = $('#selectall').is(':checked');
				if(isChecked) {
					$('.case').each(function() {
						//this.checked = true;
						$(this).prop("checked", true);
					});
				}
				else {
					$('.case').each(function(){
						//this.checked = false;
						$(this).prop("checked", false);
					});
				}
			});
			$('.case').click(function () {
				if ($(this).is(":checked")) {
					var isAllChecked = 0;
					$('.case').each(function() {
						if(!this.checked)
							isAllChecked = 1;
					})              
					if(isAllChecked === 0) {
						$("#selectall").prop("checked", true); }     
				}
				else {
					$("#selectall").prop("checked", false);
				}
			});
			// Hide or show the other form which requires validation
			$('#skip-validation').removeAttr('checked').on('click', function() {
				if(this.checked) {
					$('#users').formValidation('enableFieldValidators', 'login', false)
								.formValidation('enableFieldValidators', 'ramas[]', false);
					$.noty.closeAll();
					if($('#noty_bottom_layout_container').length)
						$('#noty_bottom_layout_container').remove();
				}
				else {
					$('#users').formValidation('enableFieldValidators', 'login', true)
								.formValidation('enableFieldValidators', 'ramas[]', true);
				}
			});
			$('[data-rel=tooltip]').tooltip();
			$('[data-rel=popover]').popover({container:'body'});
			$('[type="reset"]').on("click", function() {
				$.noty.closeAll();
				if($('#noty_bottom_layout_container').length)
					$('#noty_bottom_layout_container').remove();
				$('button[type="submit"]').removeAttr('disabled');
				$('button[type="submit"]').removeClass('disabled');
			});
			// Change Admin Tools position
			$('#ace-settings-container').prependTo('.page-content');
			// Notifications
			function notifications(type, id) {
				if(typeof id !== 'undefined')	
					var n = noty({
						id			: id,
						text        : type,
						type        : type,
						dismissQueue: true,
						killer		: false,
						layout      : 'bottom',
						maxVisible  : 1
					});
				else
					var n = noty({
						text        : type,
						type        : type,
						dismissQueue: true,
						killer		: false,
						layout      : 'bottom',
						maxVisible  : 1
					});
				return n;
			}
		});
	</script>