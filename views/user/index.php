	<div class="page-header">
		<h1>
			Control de usuarios
			<small>
				<i class="ace-icon fa fa-angle-double-right"></i>
					Nuevo usuario
			</small>
		</h1>
	</div><!-- /.page-header -->
	<div class="ace-settings-container" id="ace-settings-container">
		<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
			<i class="ace-icon fa fa-cog bigger-130"></i>
		</div>
		<div class="ace-settings-box clearfix" id="ace-settings-box">
			<div class="pull-left">
				<div class="widget-header widget-header-blue widget-header-flat">
					<h4 class="widget-title lighter"><i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer blue"></i>Admin Tools</h4>
				</div>
				<div class="ace-settings-item">
					<input type="checkbox" class="ace ace-switch ace-switch-4" id="skip-validation">
					<label class="lbl" for="ace-settings-breadcrumbs">
						&nbsp;Validaciones
					</label>
				</div>
				<div class="ace-settings-item">
					<a href="<?php echo URL ?>user/clear/" class="btn-alt"><i class="ace-icon fa fa-trash-o"></i></a>
					<label class="lbl" for="ace-settings-breadcrumbs">
						Mensajes
					</label>
				</div>
			</div><!-- /.pull-left -->
		</div><!-- /.ace-settings-box -->
	</div><!-- /.ace-settings-container -->
<?php
	$errorFlag = false;
	if (isset($_SESSION['createuser'])) {
		$errorFlag = true;
		$errors = json_decode($_SESSION['createuser']);
		$errorHandler = json_decode($_SESSION["createuser"]);
		$title = $errorHandler->serversays->title;
		$text = $errorHandler->serversays->texts;
		$error = $errorHandler->serversays->error;
		if($error[0]->errorcode == 0) {
			$message = '<li><i class="fa-li fa fa-database"></i>' . $error[0]->errormessage . '</li>';
			$excl = "&iexcl;Oh no!";
			$faIcon = "fa-times";
			$faFace = "fa-frown-o";
			$alertType = "danger"; 
		}
		else {
			$message = '';
			foreach($error as $key => $value)
				$message.= '<li><i class="fa-li fa fa-hand-o-right"></i>' . $value->errormessage . '</li>';
			$excl = "&iexcl;Oh vaya!";
			$faIcon = "fa-times";
			$faFace = "fa-frown-o";
			$alertType = "danger"; 
		}
		$id = $errorHandler->values->id;
		$login = $errorHandler->values->login;
		$pass = $errorHandler->values->pass;
		$rol = $errorHandler->values->rol;
		if(isset($errorHandler->values->ramas))
			$ramas = implode("-", $errorHandler->values->ramas);
	}
	if($errorFlag) {
?>
	<div class="alert alert-block alert-<?php echo $alertType; ?>">
		<button data-dismiss="alert" class="close" type="button">
			<i class="ace-icon fa fa-times"></i>
		</button>
		<p>
			<strong>
				<i class="ace-icon fa <?php echo $faIcon; ?>"></i>
				<?php echo $excl; ?>
			</strong><?php echo $title; ?> <i class="ace-icon fa <?php echo $faFace; ?>"></i>:
		</p>
		<p>
			<ul class="fa-ul">
				<?php echo $message.PHP_EOL; ?>
			</ul>
		</p>
		<p>
			<?php echo $text.PHP_EOL; ?>
		</p>
	</div>
	<?php
		}
	?>
	<form class="form-horizontal" id="users" role="form" method="post" action="<?php echo URL; ?>user/create">
		<div class="form-group">
			<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-1">Usuario</label>
			<div class="col-xs-12 col-sm-4">
				<input type="text"<?php if(isset($login)) echo ' value="' . $login . '"'; ?> name="login" placeholder="Usuario" class="col-xs-12 col-sm-12" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-1">Contrase&ntilde;a</label>
			<div class="col-xs-12 col-sm-4">
				<input type="text"<?php if(isset($pass)) echo ' value="' . $pass . '"'; ?> name="pass" placeholder="Contrase&ntilde;a" class="col-xs-12 col-sm-12" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label no-padding-right" for="form-field-1">Rol</label>
			<div class="col-sm-4">
				<select name="rol" class="col-xs-12 col-sm-12">
					<option value="default"<?php if(isset($rol)) { if($rol == 'default') echo ' selected';} ?>>Default</option>
					<option value="admin"<?php if(isset($rol)) { if($rol == 'admin') echo ' selected';} ?>>Admin</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label no-padding-right" for="form-field-1">Permisos</label>
			<div class="col-sm-4">
				<div class="checkbox">
					<label>
						<input type="checkbox" class="ace" id="selectall" name="ramas[]" value="0"<?php if(isset($ramas)) {if($ramas == 0) echo ' checked'; }?>>
						<span class="lbl"> <b>Todos</b></span>
					</label>
				</div>
			<?php
				foreach($this->branchList as $key => $value) {
					$n = $key + 1;
					$checkbox = '';
					if(isset($ramas)) {
						if($ramas == 0)
							$checkbox = ' checked';
						else {
							if(strpos($ramas, (string)$n) === false)
								$checkbox = '';
							else
								$checkbox = ' checked';
						}
					}
					echo '<div class="checkbox">'.PHP_EOL;
					echo '	<label>'.PHP_EOL;
					echo '		<input type="checkbox" class="ace case" name="ramas[]" value="'.$value['idBranch'] .'"' . $checkbox . '>'.PHP_EOL;
					echo '		<span class="lbl"> ' . $value['nombre'] . '</span>'.PHP_EOL;
					echo '	</label>'.PHP_EOL;
					echo '</div>'.PHP_EOL;
				}
			?>
			</div>
		</div>
		<div class="clearfix form-actions">
			<div class="text-center col-xs-12 col-sm-12">
				<button class="btn btn-info" type="submit">
					<i class="ace-icon fa fa-check bigger-110"></i>
					Guardar
				</button>
				&nbsp;&nbsp;&nbsp;
				<button class="btn" type="reset">
					<i class="ace-icon fa fa-undo bigger-110"></i>
					Reset
				</button>
			</div>
		</div>
	</form>
	<h3 class="header smaller red">Lista de usuarios</h3>
	<?php
		if(in_array(false, $this->userList, true)) {
			$errorHandler = json_decode($this->error);
			$messages = $errorHandler->messages;
	?>
	<div class="alert alert-block alert-danger">
		<button data-dismiss="alert" class="close" type="button">
			<i class="ace-icon fa fa-times"></i>
		</button>
		<p>
			<strong>
				<i class="ace-icon fa fa-times"></i>
				&iexcl;Oh no!
			</strong><?php echo $errorHandler->texts->dberror->title; ?> <i class="ace-icon fa fa-frown-o"></i>:
		</p>
		<p>
			<ul class="fa-ul">
				<?php echo '<li><i class="fa-li fa fa-database"></i>' . $messages->{0} . ' (' . $this->userList[1][1] . ')</li>'; ?>
			</ul>
		</p>
		<p>
			<?php echo $errorHandler->texts->dberror->texts; ?>
		</p>
	</div>
	<?php
		}
		else {
	?>
	<div class="modal-body no-padding">
		<table id="table" class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
			<thead>
				<tr>
					<th class="center">ID</th>
					<th>Nombre</th>
					<th>Rol</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach($this->userList[1] as $key => $value) {
						if($value['rol'] == "admin")
							$gliph = '<i class="ace-icon glyphicon glyphicon-star"></i>';
						else
							$gliph = '<i class="ace-icon glyphicon glyphicon-user"></i>';
						echo '<tr>';
						echo '<td class="center">' . $value['idusuariosAdm'] . '</td>';
						echo '<td>' . $value['login'] . '</td>';
						echo '<td>' . $gliph . ' ' . ucfirst($value['rol']) . '</td>';
						echo '<td>
								<div class="hidden-sm hidden-xs btn-group">
									<a class="btn btn-xs btn-info  tooltip-info" href="'.URL.'user/edit/'.$value['idusuariosAdm'].'/" data-rel="tooltip2"  data-placement="top" data-original-title="Editar"><i class="ace-icon fa fa-pencil bigger-120"></i></a> 
									<a id="delete" class="btn btn-xs btn-danger tooltip-error" href="'.URL.'user/delete/'.$value['idusuariosAdm'].'/" data-rel="tooltip2"  data-placement="top" data-original-title="Borrar"><i class="ace-icon fa fa-trash-o bigger-120"></i></a>
								</div>
								<div class="hidden-md hidden-lg">
									<div class="inline pos-rel">
										<button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
											<i class="ace-icon fa fa-cog icon-only bigger-110"></i>
										</button>
										<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
											<li>
												<a href="'.URL.'user/edit/'.$value['idusuariosAdm'].'/" class="tooltip-success" data-rel="tooltip" title="Editar">
													<span class="green">
														<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
													</span>
												</a>
											</li>
											<li>
												<a id="delete" href="'.URL.'user/delete/'.$value['idusuariosAdm'].'/" class="tooltip-error" data-rel="tooltip" title="Borrar">
													<span class="red">
														<i class="ace-icon fa fa-trash-o bigger-120"></i>
													</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</td>';
						echo '</tr>';
					}
				?>
			</tbody>
		</table>
	</div>
	<?php
		}
	?>
	<script type="text/javascript">
		$(function() {
			var LoginValCont = 0;
			var PassValCont = 0;
			var RegexpValCont = 0;
			var RamasValCont = 0;
			var UserCheckCont = 0;
			var Home = $('body').attr('data-home-url');
			var Action = $('#users').attr('action');
			var Key = 0;
			$('#users').formValidation({
				framework: 'bootstrap',
				icon: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					login: {
						validators: {
							notEmpty: {
								message: 'error',
								onError: function(e, data) {
									if(LoginValCont === 0) {
										$.noty.setText(notifications('error', 100).options.id, '\u00A1Oh no! El nombre de usuario est\u00E1 vac\u00edo');
										LoginValCont++;
									}
								},
								onSuccess: function(e, data) {
									LoginValCont = 0;
									$.noty.close(100);
								}
							},
							regexp: {
								message: 'error',
								regexp: /^[a-zA-Z0-9_]+$/,
								onError: function(e, data) {
									if(RegexpValCont === 0) {
										$.noty.setText(notifications('error', 101).options.id, 'Hum... El nombre de usuario s\u00f3lo puede tener letras y n\u00fameros');
										RegexpValCont++;
									}							
								},
								onSuccess: function(e, data) {
									RegexpValCont = 0;
									$.noty.close(101);
								}
							},
							remote: {
								enabled: false,
								url: Home + 'user/checkuser/',
								type: 'POST',
								message: 'error',
								onError: function(e, data) {
									if(UserCheckCont === 0) {
										$.noty.setText(notifications('error', 102).options.id, 'Uyyy... El nombre de usuario ya est\u00E1 ocupado');
										UserCheckCont++;
									}							
								},
								onSuccess: function(e, data) {
									UserCheckCont = 0;
									$.noty.close(102);
								}
							}
						}
					},
					pass: {
						validators: {
							notEmpty: {
								message: 'error',
								onError: function(e, data) {
									if(PassValCont === 0) {
										$.noty.setText(notifications('error', 200).options.id, '\u00A1Glup! La contrase\u00f1a est\u00E1 vac\u00eda');
										PassValCont++;
									}
								},
								onSuccess: function(e, data) {
									PassValCont = 0;
									$.noty.close(200);
								}
							}
						}
					},
					'ramas[]': {
						validators: {
							notEmpty: {
								message: 'error',
								onError: function(e, data) {
									if(RamasValCont === 0) {
										$.noty.setText(notifications('error', 400).options.id, '\u00A1Pop! Falta la rama');
										RamasValCont++;
									}
								},
								onSuccess: function(e, data) {
									RamasValCont = 0;
									$.noty.close(400);
								}
							}
						}
					}
				}
			}).on('success.form.fv', function(e) {
				// Form Submit
				$("#users").submit(function(event){
					event.preventDefault();
					var data = {
						"login": $('[name="login"]').val(),
						"pass" : $('[name="pass"]').val(),
						"rol"  : $('[name="rol"]').val(),
						"ramas": $('[name="ramas[]"]').val()
					};
					data = $(this).serialize();
					$.ajax({
						type: "POST",
						dataType: "json",
						url: Action,
						data: data,
					}).done(function(data) {
						if(data.response === "success") {
							$.gritter.add({
								// (string | mandatory) the heading of the notification
								title: '\u00a1Listo!',
								// (string | mandatory) the text inside the notification
								text: '\u00a1Se cre\u00f3 con \u00e9xito!',
								time: 2000,
								class_name: 'gritter-success'
							});
							window.setTimeout(function() {
								window.location.href = Home + 'user/';
							}, 2800);
						}
						else {
							if(data.serversays.error[0].errorcode == 0) {
								$.gritter.add({
									// (string | mandatory) the heading of the notification
									title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
									// (string | mandatory) the text inside the notification
									text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
									sticky: true,
									class_name: 'gritter-error'
								});
							}
							else {
								var html = '<ul class="fa-ul">';
								$.each(data.serversays.error, function(key){ html +=  '<li><i class="ace-icon fa fa-hand-o-right"></i> ' + data.serversays.error[key].errormessage + '</li>';});
								html += '</ul>';
								$.gritter.add({
									// (string | mandatory) the heading of the notification
									title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh vaya! ' + data.serversays.title + '<br />',
									// (string | mandatory) the text inside the notification
									text: html + data.serversays.texts,
									sticky: true,
									class_name: 'gritter-error gritter-light'
								});
							}
						}
					}).fail(function() {
						$.gritter.add({
							// (string | mandatory) the heading of the notification
							title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! Algo raro pasa aqu\u00ed<br />',
							// (string | mandatory) the text inside the notification
							text: 'La informaci\u00f3n no la puedo enviar.<br />Por favor, ay\u00fadanos a corregirlo report\u00E1ndolo.',
							sticky: true,
							class_name: 'gritter-error'
						});
					});
					return false;
				});
			}).on('err.field.fv', function(e, data) {
				// $(e.target)  --> The field element
				// data.fv      --> The FormValidation instance
				// data.field   --> The field name
				// data.element --> The field element
				
				// Hide the messages
				data.element
					.data('fv.messages')
					.find('.help-block[data-fv-for="' + data.field + '"]').hide();
			}).on('success.field.fv', function(e, data) {
				if (data.fv.getSubmitButton()) {
					data.fv.disableSubmitButtons(false);
				}
			}).on('keydown', '[name="login"]', function() {
				$('#users').formValidation('enableFieldValidators', 'login', false, 'remote');
			}).on('blur', '[name="login"]', function() {
				$('#users').formValidation('enableFieldValidators', 'login', true, 'remote');
				$('#users').formValidation('revalidateField', 'login');
			});
			// Delete an user
			$('a[id="delete"]').click(function(event) {
				event.preventDefault();
				$.ajax({
					type: "GET",
					dataType: "json",
					url: $(this).attr('href')
				}).done(function(data) {
					if(data.response === "success") {
						$.gritter.add({
							// (string | mandatory) the heading of the notification
							title: '\u00a1Listo!',
							// (string | mandatory) the text inside the notification
							text: '\u00a1Se edit\u00f3 con \u00e9xito!',
							time: 2000,
							class_name: 'gritter-success'
						});
					}
					else {
						if(data.serversays.error[0].errorcode == 0) {
							$.gritter.add({
								// (string | mandatory) the heading of the notification
								title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
								// (string | mandatory) the text inside the notification
								text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
								sticky: true,
								class_name: 'gritter-error'
							});
						}
					}
				}).fail(function() {
					$.gritter.add({
						// (string | mandatory) the heading of the notification
						title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! Algo raro pasa aqu\u00ed<br />',
						// (string | mandatory) the text inside the notification
						text: 'No puedo enviar la informaci\u00f3n.<br />Por favor, ay\u00fadanos a corregirlo report\u00E1ndolo.',
						sticky: true,
						class_name: 'gritter-error'
					});
				});
				return false;
			});
			$('#selectall').click(function() {
				var isChecked = $('#selectall').is(':checked');
				if(isChecked) {
					$('.case').each(function() {
						//this.checked = true;
						$(this).prop("checked", true);
					});
				}
				else {
					$('.case').each(function(){
						//this.checked = false;
						$(this).prop("checked", false);
					});
				}
			});
			$('.case').click(function () {
				if ($(this).is(":checked")) {
					var isAllChecked = 0;
					$('.case').each(function() {
						if(!this.checked)
							isAllChecked = 1;
					})              
					if(isAllChecked === 0) {
						$("#selectall").prop("checked", true); }     
				}
				else {
					$("#selectall").prop("checked", false);
				}
			});
			// Hide or show the other form which requires validation
			$('#skip-validation').removeAttr('checked').on('click', function() {
				if(this.checked) {
					$('#users').formValidation('enableFieldValidators', 'login', false)
								.formValidation('enableFieldValidators', 'pass', false)
								.formValidation('enableFieldValidators', 'ramas[]', false);
					$.noty.closeAll();
					if($('#noty_bottom_layout_container').length)
						$('#noty_bottom_layout_container').remove();
				}
				else {
					$('#users').formValidation('enableFieldValidators', 'login', true)
								.formValidation('enableFieldValidators', 'pass', true)
								.formValidation('enableFieldValidators', 'ramas[]', true);
				}
			});
			// Add tooltip for small view action buttons in dropdown menu
			$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
			$('[data-rel="tooltip2"]').tooltip();
			$('[type="reset"]').on("click", function() {
				$.noty.closeAll();
				if($('#noty_bottom_layout_container').length)
					$('#noty_bottom_layout_container').remove();
				$('button[type="submit"]').removeAttr('disabled');
				$('button[type="submit"]').removeClass('disabled');
			});
			// Change Admin Tools position
			$('#ace-settings-container').prependTo('.page-content');
			// Tooltip placement on right or left
			function tooltip_placement(context, source) {
				var $source = $(source);
				var $parent = $source.closest('table')
				var off1 = $parent.offset();
				var w1 = $parent.width();
				var off2 = $source.offset();
				if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
				return 'left';
			}
			// Notifications
			function notifications(type, id) {
				if(typeof id !== 'undefined')	
					var n = noty({
						id			: id,
						text        : type,
						type        : type,
						dismissQueue: true,
						killer		: false,
						layout      : 'bottom',
						maxVisible  : 1
					});
				else
					var n = noty({
						text        : type,
						type        : type,
						dismissQueue: true,
						killer		: false,
						layout      : 'bottom',
						maxVisible  : 1
					});
				return n;
			}
		});
	</script>