							<div class="page-header">
								<h1>
									Ramas
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
											Nuevo rama
									</small>
								</h1>
							</div><!-- /.page-header -->
							<div class="row branches-list">
								<div class="col-xs-12">
									<div class="table-header"></div>
									<div class="table-responsive">
										<table id="branches-table" class="table table-striped table-bordered table-hover dt-responsive responsive nowrap">
											<thead>
												<tr>
													<th>Id de la Rama</th>
													<th>Id de Galaxy</th>
													<th>Nombre</th>
													<th>URL</th>
													<th>Imagen de la Rama</th>
													<th class="no-orderable">Acciones</th>
												</tr>
											</thead>
										</table>
									</div><!-- /.table-responsive -->
								</div><!-- /.col -->
							</div><!-- /.row -->
							<div class="row new-branch">
								<div class="col-xs-12">
									<h3 class="header smaller lighter blue">Nueva rama</h3>
								</div>
								<form class="form-horizontal" id="branch" role="form" method="post" action="<?php echo URL; ?>rama/create">
									<div class="col-sm-offset-1 col-sm-10">
										<div class="form-group">
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-name">Nombre</label>
											<div id="field-name" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="name" placeholder="Nombre de la rama" class="col-xs-12 col-sm-12" id="form-field-name">
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-galaxy">Galaxy</label>
											<div id="field-galaxy" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="galaxy" placeholder="Id de la rama en Galaxy" class="col-xs-12 col-sm-12" id="form-field-galaxy">
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-url">Dirección</label>
											<div id="field-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="url" placeholder="Dirección de la rama" class="col-xs-12 col-sm-12" id="form-field-url">
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-image-url">Imagen</label>
											<div id="field-image-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="image-url" placeholder="Dirección de la imagen" class="col-xs-12 col-sm-12" id="form-field-image-url">
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="text-center col-xs-12 col-sm-12">
												<button type="submit" name="submitButton" class="btn btn-info">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Guardar
												</button>
												&nbsp;&nbsp;&nbsp;
												<button type="reset" class="btn">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div>
									</div><!-- /.col-sm-offset-1 -->
								</form>
							</div><!-- /.row -->
							<!-- Inline scripts -->
							<script>
								$(function() {
									// Init vars
									var Home = $('body').attr('data-home-url');
									// DataTable Start
									var oldStart = 0; // Pagination scroll to top
									var branchesTable = $('#branches-table').on('xhr.dt', function(e, settings, json, xhr) {
										// Check for database error
										if(json.dberror === 1 || json.dberror === 2 || json.dberror === 3) {
											// Shows a custom error report and stops table's execution
											$('.branches-list > .col-xs-12').hide();
											$('.branches-list').append('<div class="alert alert-block alert-danger"><button data-dismiss="alert" class="close" type="button"><i class="ace-icon fa fa-times"></i></button><p><strong><i class="ace-icon fa fa-times"></i> &iexcl;Oh no!</strong> Tenemos un problema <i class="ace-icon fa fa-frown-o"></i>:</p><p><ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>Error en la consulta a la base de datos (Error ' + json.dberror +')</li></ul></p></div>');
											return true;
										}
									}).DataTable({
										// DataTable Start
										saveState: true,
										processing: true,
										serverSide: true,
										responsive: true,
										ajax: {
											url: Home + 'rama/ramaDataTable/',
											type: 'post',
											dataSrc: function(json) {
												// Database error
												if(json.dberror)
													return [];
												else
													return json.data;
											}
										},
										order: [0, 'asc'],
										columns: [
											{
												width: '8%',
												'class': 'center',
												data: 'ramaId'
											},
											{
												width: '8%',
												'class': 'center',
												data: 'ramaGalaxyId',
											},
											{
												width: '10%',
												data: 'ramaNombre'
											},
											{
												width: '20%',
												data: 'ramaUrl'
											},
											{
												width: '20%',
												data: null,
												render: function (data, type, full, meta) {
													return '<span onmouseover="document.getElementById(\'place-holder-' + data.ramaId + '\').src=\'' + data.ramaImagen + '\';" onmouseout="document.getElementById(\'place-holder-' + data.ramaId + '\').src=\''+ Home +'public/images/pixel.png\';">' + data.ramaImagen + '<img src="'+ Home + 'public/images/pixel.png" id="place-holder-' + data.ramaId + '" style="zindex: 100; position: absolute;"></span>';
												}
											},
											{
												width: '5%',
												'class': 'center',
												data: null,
												render: function (data, type, full, meta) {
												return '<div class="hidden-sm hidden-xs action-buttons"><a data-original-title="Editar" data-placement="top" data-rel="tooltip" href="' + Home + 'rama/edit/' + data.ramaId + '" class="blue tooltip-info tt"><i class="ace-icon fa fa-pencil bigger-130"></i></a><a data-original-title="Borrar" data-placement="top" data-rel="tooltip" href="' + Home + 'rama/delete/' + data.ramaId + '" class="red tooltip-error tt"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>';
												}
											}
										],
										columnDefs: [
											{
												orderable: false,
												targets: 'no-orderable'
											}
										],
										autoWidth: false,
										language: {
											'lengthMenu': '_MENU_ ramas por p\u00e1gina',
											'zeroRecords': 'No encontramos la rama que est\u00e1s buscando',
											'info': 'P\u00e1gina _PAGE_ de _PAGES_',
											'infoEmpty': 'No hay registros de ramas a\u00fan (;-;)',
											'infoFiltered': '(filtrado de _MAX_ ramas totales)',
											'search': 'Buscar',
											'paginate': {
												'previous': 'Anterior',
												'next': 'Siguiente'
											},
											'processing': '<div role="alert" class="tree-loader center" style=""><div class="tree-loading"><i class="ace-icon fa fa-refresh fa-spin blue bigger-250"></i></div></div>'
										},
										select: {
											style: 'multi'
										},
										fnDrawCallback: function (o) {
											if (o._iDisplayStart != oldStart) {
												var targetOffset = $('#brnaches-table_wrapper').offset().top;
												$('html, body').animate({scrollTop: targetOffset}, 500);
												oldStart = o._iDisplayStart;
											}
										}
									});
									// Action buttons tooltips
									$('body').tooltip({
										selector: '.tt'
									});
									// DataTable End
									// Validation Start
									// Validation Vars
									var BranchValCount = 0,
										BranchCheckCount = 0,
										GalaxyValCount = 0,
										GalaxyIntCount = 0,
										GalaxyCheckCount = 0,
										URLValCount = 0,
										URLUriCount = 0,
										ImageValCount = 0,
										ImageUriCount = 0,
										validation = $('#branch').formValidation({
											framework: 'bootstrap',
											icon: {
												valid: 'glyphicon glyphicon-ok',
												invalid: 'glyphicon glyphicon-remove',
												validating: 'glyphicon glyphicon-refresh'
											},
											fields: {
												name: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(BranchValCount === 0) {
																	$.noty.setText(notifications('error', 100).options.id, '\u00A1Oh no! El nombre de la rama est\u00E1 vac\u00edo');
																	BranchValCount++;
																}
															},
															onSuccess: function(e, data) {
																BranchValCount = 0;
																$.noty.close(100);
															}
														},
														remote: {
															enabled: false,
															url: Home + 'rama/checkbranch/',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar el nombre de la rama en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																});
																else {
																	if(BranchCheckCount === 0) {
																		$.noty.setText(notifications('error', 101).options.id, '\u00A1Oh no! Esa rama ya est\u00E1 registrada');
																		BranchCheckCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																BranchCheckCount = 0;
																$.noty.close(101);
															}
														}
													}
												},
												galaxy: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(GalaxyValCount === 0) {
																	$.noty.setText(notifications('error', 200).options.id, '\u00A1Oh no! El Id de Galaxy est\u00E1 vac\u00edo');
																	GalaxyValCount++;
																}
															},
															onSuccess: function(e, data) {
																GalaxyValCount = 0;
																$.noty.close(200);
															}
														},
														integer: {
															message: 'error',
															onError: function(e, data) {
																if(GalaxyIntCount === 0) {
																	$.noty.setText(notifications('error', 201).options.id, '\u00A1Oh no! El Id de Galaxy debe ser num\u00e9rico');
																	GalaxyIntCount++;
																}
															},
															onSuccess: function(e, data) {
																GalaxyIntCount = 0;
																$.noty.close(201);
															}
														},
														remote: {
															enabled: false,
															url: Home + 'rama/checkgalaxy/',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar el Id de Galaxy en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																});
																else {
																	if(GalaxyCheckCount === 0) {
																		$.noty.setText(notifications('error', 202).options.id, '\u00A1Oh no! Esa Id de Galaxy ya est\u00E1 registrado');
																		GalaxyCheckCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																GalaxyCheckCount = 0;
																$.noty.close(202);
															}
														}
													}
												},
												url: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(URLValCount === 0) {
																	$.noty.setText(notifications('error', 300).options.id, '\u00A1Oh no! La direcci\u00f3n de la rama est\u00E1 vac\u00eda');
																	URLValCount++;
																}
															},
															onSuccess: function(e, data) {
																URLValCount = 0;
																$.noty.close(300);
															}
														},
														uri: {
															enabled: false,
															message: 'error',
															onError: function(e, data) {
																if(URLUriCount === 0) {
																	$.noty.setText(notifications('error', 301).options.id, '\u00A1Oh no! La direcci\u00f3n de la rama no  v\u00e1lida');
																	URLUriCount++;
																}
															},
															onSuccess: function(e, data) {
																URLUriCount = 0;
																$.noty.close(301);
															}
														}
													}
												},
												'image-url': {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(ImageValCount === 0) {
																	$.noty.setText(notifications('error', 400).options.id, '\u00A1Oh no! La direcci\u00f3n de la  imagen est\u00E1 vac\u00eda');
																	ImageValCount++;
																}
															},
															onSuccess: function(e, data) {
																ImageValCount = 0;
																$.noty.close(400);
															}
														},
														regexp: {
															enabled: false,
															message: 'error',
															regexp: /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[\/?#]\S*)?(?:jpe?g|gif|png)$/i,
															onError: function(e, data) {
																if(ImageUriCount === 0) {
																	$.noty.setText(notifications('error', 401).options.id, '\u00A1Oh no! La direcci\u00f3n de la imagen parece estar mal escrita (s\u00f3lo acepta terminaciones .jpg, .png o .gif)');
																	ImageUriCount++;
																}							
															},
															onSuccess: function(e, data) {
																ImageUriCount = 0;
																$.noty.close(401);
															}
														}
													}
												}
											}
										}).on('err.field.fv', function(e, data) {
											// $(e.target)  --> The field element
											// data.fv      --> The FormValidation instance
											// data.field   --> The field name
											// data.element --> The field element
											if (data.fv.getSubmitButton()) {
												data.fv.disableSubmitButtons(false);
											}
											// Hide the messages
											data.element
												.data('fv.messages')
												.find('.help-block[data-fv-for="' + data.field + '"]').hide();
										}).on('success.field.fv', function(e, data) {
											if (data.fv.getSubmitButton()) {
												data.fv.disableSubmitButtons(false);
											}
										}).on('keydown', '[name="name"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'name', false, 'remote');
										}).on('blur', '[name="name"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'name', true, 'remote');
											$('#branch').formValidation('revalidateField', 'name');
										}).on('keydown', '[name="galaxy"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'galaxy', false, 'remote');
										}).on('blur', '[name="galaxy"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'galaxy', true, 'remote');
											$('#branch').formValidation('revalidateField', 'galaxy');
										}).on('keydown', '[name="url"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'url', false, 'uri');
										}).on('blur', '[name="url"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'url', true, 'uri');
											$('#branch').formValidation('revalidateField', 'url');
										}).on('keydown', '[name="image-url"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'image-url', false, 'regexp');
										}).on('blur', '[name="image-url"]', function() {
											$('#branch').formValidation('enableFieldValidators', 'image-url', true, 'regexp');
											$('#branch').formValidation('revalidateField', 'image-url');
										});
									// Validation End
									// Notifications
									function notifications(type, id) {
										if(typeof id !== 'undefined')	
											var n = noty({
												id			: id,
												text        : type,
												type        : type,
												dismissQueue: true,
												killer		: false,
												layout      : 'bottom',
												maxVisible  : 1
											});
										else
											var n = noty({
												text        : type,
												type        : type,
												dismissQueue: true,
												killer		: false,
												layout      : 'bottom',
												maxVisible  : 1
											});
										return n;
									}
									// Tooltip placement on right or left
									function tooltip_placement(context, source) {
										var $source = $(source);
										var $parent = $source.closest('table')
										var off1 = $parent.offset();
										var w1 = $parent.width();
										var off2 = $source.offset();
										if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
										return 'left';
									}
								});
							</script>