<h1>Equipo</h1>
<div id="formulario">
	<form method="post" action="<?php echo URL; ?>equipos/create">
		<label for="nombre">Nombre</label> 
		<input id ="nombre" type="text" name="nombre" required /> 
		<label for="pais">Pa&iacute;s</label> 
		<input id ="pais" type="text" name="pais" required /> 
		<label for="aniofundacion">Anio de fundaci&oacute;n</label> 
		<input id ="aniofundacion" type="text" name="aniofundacion" required> 
		<label for="campoCancha">Cancha</label>
		<input id ="campoCancha" type="text" name="campoCancha" required> 
		<label for="liga">liga</label> 
		<input id ="liga" type="text" name="liga" required> 
		<label for="ubicacion">Ubicaci&oacute;n</label> 
		<input id ="ubicacion" type="text" name="ubicacion"> 
		<label for="division">Divisi&oacute;n</label> 
		<input id ="division" type="text" name="division"> 

		<div id="acciones">
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrar('existente', '-1');">-</a> <a
					onclick="javascript:mostrar('existente', '1');">+</a>
				</span> Persona Relacionada existente
			</p>
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrar('nueva', '-1');">-</a> <a
					onclick="javascript:mostrar('nueva', '1');">+</a>
				</span> Nueva Persona Relacionada
			</p>
		</div>

		<select name="personaRelacionada" id="personaRelacionada" MULTIPLE>
            <?php
            foreach ($this->programLista as $key => $value) {
                echo '<option value="' . $value['idprograma'] . '">' . $value['programaTitulo'] . '</option>';
            }
            ?>
        </select>

		<div id="relacionNueva"></div>

		<div id="acomodarG">
			<p id="mensaje"></p>
			<input type="button" onclick="javascript:send(this.form, 'mensaje','personaRelacionada');" value="Guardar" name="Guardar" id="botones" />
			<!-- <input type="button" onclick="javascript:generateXML();" value="Generar XML" name="Generar XML" id="botones"/> -->
		</div>

	</form>
</div>




<table width="100%" id="listado" border="0">
	<tr>
		<td colspan="5"><hr></td>
	</tr>
	<tr>
		<td colspan="5">
			<div id="Busqueda">
				<form name="buscar" action="<?php echo URL; ?>equipos/search"
					method="post">
					<input type="text" name="Nombre" id="nombre" placeholder="Nombre"> 
					<input type="text" name="programaDescrip" id="programaDescrip" placeholder="Descripción"> 
					<input type="button" name="enviar" value="Buscar" id="botones" onclick="javascript:searchTeam(this.form);">
					<input type="button" name="Limpiar" value="Limpiar" id="botones" onclick="javascript:clearTeam(this.form);">
				</form>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="5"><hr></td>
	</tr>
	<tr>
		<td id="buscador" width = 20%><p>Nombre</p></td>
		<td id="buscador" width = 20%><p>Pa&iacute;s</p></td>
		<td id="buscador" width = 20%><p>Liga</p></td>
		<td id="buscador" width = 2%><p>Editar</p></td>
		<td id="buscador" width = 2%><p>Eliminar</p></td>
	</tr>
   <tr>
   		<table width="100%" id="resultados" border="0">
   		 <?php
		    
		    foreach ($this->equiposList as $key => $value) {
		        if(($key%2) == 1){
		            echo '<tr bgcolor="#F2F2F2">';
		        }else{
		            echo '<tr>';
		        } 
		        echo '<td width = 20%>' . $value['nombre'] . '</td>';
		        echo '<td width = 20%>' . $value['pais'] . '</td>';
		        echo '<td width = 20%>' . $value['liga'] . '</td>';
		        echo '<td width = 5%><a href = "' . URL . 'equipos/edit/' . $value['idEquipo'] . '"><center><img src="'.URL.'public/images/editar.png" alt="Editar" width="16" heigth="16"><center></a></td>';
		        echo '<td width = 5%><a class = "delete" href="' . URL . 'equipos/delete/' . $value['idEquipo'] . '"><center><img src="'.URL.'public/images/eliminar.png" alt="Eliminar" width="16" heigth="16"><center></a></td>';
		        echo '</tr>';
		    }
    ?>
   		</table>	
   </tr>
    <tr>
		<td colspan="4"><hr></td>
	</tr>
	<tr>
		<td colspan="4">
			<?php 
   				 $v = $this->nPaginar;
   				 echo '<div id="paginador">';
   				 for($i = 1 ; $i<=$v; $i++){
   				 	echo '<div id="pagina"><a href="'. URL.'equipos/show/'.$i.'">'.$i.'</div></a>';
   				 }
   				 echo '</div>';
			?>
		
		</td>
	</tr>
</table>

<script>
$(function() {
    $('.delete').click(function(e) {
        var c = confirm("Estas seguro de eliminar?");
        if (c == false)
            return false;
    });
});
</script>