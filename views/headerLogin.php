<!doctype html>
<html lang="es" dir="ltr">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="author" content="Televisa TIM">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=(isset($this->title)) ? $this->title : 'admin'; ?></title>
		<!-- Bootstrap -->
		<link href="<?php echo URL; ?>public/css/bootstrap.css" rel="stylesheet">
		<!-- Fontawesome -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/font-awesome/4.6.3/css/font-awesome.min.css">
		<!-- Text Fonts -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/fonts/fonts.googleapis.com.css">
		<!-- Form Validator -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/css/formValidation.min.css">
		<!-- Gritter -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/jquery.gritter.min.css">
		<!-- Pace -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/pace.css">
 <?php
if(isset($this->css)) {
    echo '		<!-- Extra css -->'.PHP_EOL;
    foreach ($this->css as $cssfile)
        echo '		<link rel="stylesheet" href="' . URL . $cssfile .'">'.PHP_EOL;
}
 ?>                
		<!-- Ace styles -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace.min.css">
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace-part2.min.css">
		<![endif]-->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace-rtl.min.css">
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace-ie.min.css">
		<![endif]-->
		<!-- Inline styles -->
		<style>
			.form-group {
				margin-bottom: 0;
			}
			.form-control-feedback {
				right: -2.3em;
			}
		</style>
		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/html5shiv.min.js"></script>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/respond.min.js"></script>
		<![endif]-->
		<!-- Basic scripts -->
		<!--[if !IE]> -->
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/jquery.2.1.1.min.js"></script>
		<!-- <![endif]-->
		<!--[if IE]>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/jquery.1.11.1.min.js"></script>
		<![endif]-->
		<!-- Form Validator -->
		<script src="<?php echo URL; ?>public/js/jquery.formValidation.min.js"></script>
		<script src="<?php echo URL; ?>public/js/framework/bootstrap.min.js"></script>
		<!-- Notifications -->
		<script src="<?php echo URL; ?>public/js/jquery.noty.min.js"></script>
		<!-- Gritter -->
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/jquery.gritter.min.js"></script>
		<!-- Pace -->		
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/pace.min.js"></script>
<?php
if(isset($this->js)) {
    echo "		<!-- Extra scripts -->".PHP_EOL;
    foreach ($this->js as $jsfile)
        echo '		<script src="' . URL . $jsfile .'"></script>'.PHP_EOL;
}
?>		
	</head>
	<body class="login-layout">
