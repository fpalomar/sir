<!doctype html>
<html lang="es" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="author" content="Televisa TIM">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=(isset($this->title)) ? $this->title : 'admin'; ?></title>
		<!-- Bootstrap -->
		<link href="<?php echo URL; ?>public/css/bootstrap.css" rel="stylesheet">
		<!-- Fontawesome -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/font-awesome/4.6.3/css/font-awesome.min.css">
		<!-- Text Fonts -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/fonts/fonts.googleapis.com.css">
		<!-- Form Validator -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/css/formValidation.min.css">
		<!-- Gritter -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/jquery.gritter.min.css">
		<!-- Pace -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/pace.css">
		<!-- General -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/css/general.css">
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace-part2.min.css" class="ace-main-stylesheet">
		<![endif]-->
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace-ie.min.css">
		<![endif]-->        
 <?php
if(isset($this->css)) {
    echo "		<!-- Extra css -->".PHP_EOL;
    foreach ($this->css as $cssfile)
        echo '		<link rel="stylesheet" href="' . URL . $cssfile .'">'.PHP_EOL;
}
 ?>
                <!-- Theme Styles -->
		<link rel="stylesheet" href="<?php echo URL; ?>public/themes/ace/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style">		
                <!-- Theme Settings Handler -->
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/ace-extra.min.js"></script>
		<!--[if lt IE 9]>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/html5shiv.min.js"></script>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/respond.min.js"></script>
		<![endif]-->
		<!--[if !IE]> -->
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/jquery.2.1.1.min.js"></script>
		<!-- <![endif]-->
		<!--[if IE]>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/jquery.1.11.1.min.js"></script>
		<![endif]-->
		<!-- Form Validator -->
		<script src="<?php echo URL; ?>public/js/jquery.formValidation.min.js"></script>
		<script src="<?php echo URL; ?>public/js/framework/bootstrap.min.js"></script>
		<!-- Notifications -->
		<script src="<?php echo URL; ?>public/js/jquery.noty.min.js"></script>
		<!-- Gritter -->
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/jquery.gritter.min.js"></script>
		<!-- Pace -->		
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/pace.min.js"></script>
<?php
if(isset($this->js)) {
    echo "		<!-- Extra scripts -->".PHP_EOL;
    foreach ($this->js as $jsfile)
        echo '		<script src="' . URL . $jsfile .'"></script>'.PHP_EOL;
}
?>
	</head>
	<body class="no-skin" data-home-url="<?php echo URL; ?>">
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>
			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="navbar-header pull-left">
					<a href="<?php echo URL; ?>dashboard/" class="navbar-brand">
						<small>
							<i class="fa fa-google"></i>
							SIR
						</small>
					</a>
				</div>
				<?php
					$string = $_SESSION["user"];
					if(isset($string)) {
						$s = explode("@", $string);
						$u = explode(".", $s[0]);
						$user = $u[0];
					}
					else 
						$user = "usuario";
				?>
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo URL; ?>/public/themes/ace/assets/avatars/brightkite.png" alt="<?php echo $user; ?>" />
								<span class="user-info">
									<small>Bienvenid&commat;,</small>
									<?php echo ucfirst($user); ?>
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo URL; ?>dashboard/logout/">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
		<?php
			$url = $_GET["url"];
			if(substr($url, -1) != "/")
				$url.= "/";
			$urlParts = explode("/", $url);	
		?>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>
			<div id="sidebar" class="sidebar responsive">
				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
				</script>
				<ul class="nav nav-list">
					<li class="<?php echo ($urlParts[0] == "dashboard" ? "active" : "")?>">
						<a href="<?php echo URL; ?>dashboard/">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php echo ($urlParts[0] == "programa" ? "active" : "")?>">
						<a href="<?php echo URL; ?>programa/">
							<i class="menu-icon fa fa-film"></i>
							<span class="menu-text"> Programas </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php echo ($urlParts[0] == "persona" ? "active" : "")?>">
						<a href="<?php echo URL; ?>persona/">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Personas </span>
						</a>
						<b class="arrow"></b>
					</li>
					<?php
						if (Session::get('rol') == 'admin'):
					?>
					<li class="<?php echo ($urlParts[0] == "user" ? "active" : "")?>">
						<a href="<?php echo URL; ?>user/">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Control de usuarios </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php echo ($urlParts[0] == "rama" ? "active" : "")?>">
						<a href="<?php echo URL; ?>rama/">
							<i class="menu-icon fa fa-leaf"></i>
							<span class="menu-text"> Ramas </span>
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php echo ($urlParts[0] == "log" ? "active" : "")?>">
						<a href="<?php echo URL; ?>log/">
							<i class="menu-icon fa fa-terminal"></i>
							<span class="menu-text"> Logs </span>
						</a>
						<b class="arrow"></b>
					</li>
					<?php
						endif;
					?>
</ul><!-- /.nav-list -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<script type="text/javascript">
					try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
				</script>
			</div>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo URL; ?>dashboard/">Home</a>
							</li>
							<?php
								$count = count($urlParts) - 1;
								$url = URL;
								for($i = 0; $i < $count; $i++) {
									$url.= $urlParts[$i] . '/';
									if($i == $count - 1)
										echo '<li class="active">' . ucfirst($urlParts[$i]) . '</li>';
									else
										echo '<li><a href="' . $url . '">' . ucfirst($urlParts[$i]) . '</a></li>';
								}
							?>
						</ul><!-- /.breadcrumb -->
					</div>
					<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
