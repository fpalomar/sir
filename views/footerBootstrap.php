							<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">SIR</span> 2014-<?php echo date("Y"); ?></span>
					</div>
				</div>
			</div>
			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
		<!-- Basic scripts -->
		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo URL; ?>public/themes/ace/assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>
		<!-- <![endif]-->
		<!--[if IE]>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo URL; ?>public/themes/ace/assets/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo URL; ?>public/themes/ace/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<!-- Bootstrap scripts -->
		<script src="<?php echo URL; ?>public/js/bootstrap.js"></script>
		<!-- Theme scripts -->
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/ace-elements.min.js"></script>
		<script src="<?php echo URL; ?>public/themes/ace/assets/js/ace.min.js"></script>
	</body>
</html>