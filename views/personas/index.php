<h1>Personas</h1>
<table width="100%" id="listado" border="0">
	<tr>
		<td colspan="8"><hr></td>
	</tr>
	<tr>
		<td colspan="8">
			<div id="Busqueda">
				<form name="buscar" action="<?php echo URL; ?>personas/search"
					method="post">
					<input type="text" name="personaNom" id="personaNom" placeholder="Nombre"> 
					<input type="text" name="personaResumenBio" id="personaResumenBio" placeholder="Resumen Bio"> 
					<input type="text" name="programaFechaIngreso" id="personaFechaIngreso" placeholder="Fecha de Ingreso"> 
					<input type="button" name="enviar" value="Buscar" id="botones" onclick="javascript:searchPersona(this.form);">
					<input type="button" name="Limpiar" value="Limpiar" id="botones" onclick="javascript:clearPrograma(this.form);">
				</form>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="8"><hr></td>
	</tr>
	<tr id="personTitles">
		<td id="buscador" width="5%">&nbsp;</td>
		<td id="buscador" width="18%"><p>Nombre</p></td>
		<td id="buscador" width="30%"><p>Resumen bio</p></td>
		<td id="buscador" width="18%"><p>Fecha</p></td>
		<td id="buscador" width="5%"><p>Editar</p></td>
		<!--<td id="buscador" width = 2%><p>Eliminar</p></td>-->
		<td id="buscador" width="6%"><p>Publicar</p></td>
		<td id="buscador" width="8%"><p>Despublicar</p></td>
		<td id="buscador" width="6%"><p>Eliminar</p></td>
	</tr>
   <tr>
   </table>
   	    <form method="post" action="<?php echo URL; ?>personas/generateXml" name="varios" id="varios"> 	
   
   		<table width="100%" id="resultados" border="0">
   		
   		 <?php
		    foreach ($this->personasList as $key => $value) {
		        if(($key%2) == 1){
		            echo '<tr bgcolor="#F2F2F2">';
		        }else{
		            echo '<tr>';
		        }
				if($value['personaStatus'] == 0)
					$statusImage = URL.'public/images/published.png';
				else
					$statusImage = URL.'public/images/saved.png';
		        echo '<td width="5%">' . '<img src="'. $statusImage .  '" alt="estatus"></td>';
		        echo '<td width="18%">' . $value['personaNom'] . '</td>';
		        echo '<td width="30%">' . $value['personaResumenBio'] . '</td>';
		        echo '<td width="18%">' . $value['personaFechaIngreso'] . '</td>';
		        echo '<td width="6%"><a href = "' . URL . 'personas/edit/' . $value['idPersona'] . '"><center><img src="'.URL.'public/images/editar.png" alt="Editar" width="16" heigth="16"><center></a></td>';
		        //echo '<td width="5%"><a class = "delete" href="' . URL . 'personas/delete/' . $value['idPersona'] . '"><center><img src="'.URL.'public/images/eliminar.png" alt="Eliminar" width="16" heigth="16"><center></a></td>';
		        echo '<td width="7%"><center><input type="checkbox" name="editar" value="'.$value['idPersona'].'" id="ediarV"></center></td>';
				echo '<td width="8%"><center><input type="checkbox" name="despublicar" value="'.$value['idPersona'].'" id="despublicarV"></center></td>';
		        echo '<td width="7%"><center><input type="checkbox" name="borrar" value="'.$value['idPersona'].'" id="eliminarV"></center></td>';
		        
		        echo '</tr>';
		    }
    ?>
		</table>
		<table align="right"  id="personButtons">
			<tr>
				<?php if (Session::get('rol') == 'admin'):?>
				<td><input type="button" onclick="javascript:generateXMLPersona('nuevo','');" value="Publicar contenido" name="Generar XML" id="botones"/>
				</td>
				<?php endif; ?>
				<td><input name="generar XML" type="button" value="Publicar selecc." id="botones" class ="editar"></td>
				<td><input name="generar XML" type="button" value="Despublicar selecc." id="botones" class ="despublicar"></td>
				<td><input name="generar XML" type="button" value="Borrar selecc." id="botones" class = "delete"></td>
   			</tr>
   		</table>
   		</form>	
   		   		<table width="100%"  border="0">	
    <tr>
		<td colspan="4"><hr></td>
	</tr>
	<tr>
		<td colspan="4" id="personPaginate">
			<?php 
   				 $v = $this->nPaginar;
				 $separator = "...";
				 echo '<div id="paginador">';
   				 for($i = 1 ; $i<=$v; $i++){
   				 	if($i == $this->wiam) {
						echo '<div id="pagina"><strong>'.$i.'</strong></div>';
					}
					else if($i == 1 || $i == $v || ($i >= $this->wiam - RPP && $i <= $this->wiam + RPP)) {
						if($i == $this->wiam - RPP && $i > 1 + 1)
							echo $separator;
						echo '<div id="pagina"><a href="'. URL.'personas/show/'.$i.'">'.$i.'</a></div>';
						if($i == $this->wiam + RPP && $i < $v - 1)
							echo $separator;
					}
   				 }
   				 echo '</div>';
			?>
		</td>
	</tr>
</table>

<div id="addElement" style="font: 100% verdana,arial,sans-serif">
	<span> 
		<a id="addDisplayLink" onclick="javascript:toggle('formulario',1);" style="font-size: 20px;">+</a>
	</span>
	<span style="font-size: 15px; font-weight: bolder;"><a id="addDisplayText" onclick="javascript:toggle('formulario',1);">A&ntilde;adir Persona</a></span>
</div>

<div id="formulario" style="display:none">
	<img id="opener" src="<?php echo URL;?>/public/images/ayuda.png" alt="A" title="Ayuda (?)">
	<img id="opener2" src="<?php echo URL;?>/public/images/tip.png" alt="T" title="Tip (!)">
    <form method="post" action="<?php echo URL;?>personas/create" id="form">
		<div class="dropzone" id="dropzone">
			<div id="thumbnail"></div>
			<span id="textThumb">Thumbnail<span>o</span></span>
		</div>
		<label for="personaNom">Nombre</label>
        <input id="personaNom" type="text" name="nombre" onblur="namecheck(this.value)"><span id="namecheck"></span>
		<label for="personaBranch">Categor&iacute;a</label>
		<select id="personaBranch" name="branches">
			<option selected>-- Seleccione una categor&iacute;a --</option>
			<?php
				foreach($this->branchList as $key => $value) {
					echo '<option name="'.$value['nombre'].'" value="'.$value['idBranch'].'">'.$value['nombre'].'</option>';
				}
			?>
		</select>
		<label for="personaNacimiento">Fecha de Nacimiento</label>
		<div class="picker" id="nacimiento"></div>
		<input id="personaNacimiento" type="hidden" name="personaNacimiento">
        <label for="personaUrlBio">Url de la biograf&iacute;a</label>
        <input id="personaUrlBio" type="text" name="urlBio">
        <label for="personaResumenBio">Resumen de la Biograf&iacute;a</label>
        <textarea id="personaResumenBio" name="resumenBio"></textarea>
        <label for="personaUrlThumb" id="UrlThumb">Url del Thumbnail</label>
        <input id="personaUrlThumb" type="text" name="urlThumb">
		<label for="personaKeywords">Keywords</label>
        <input id="personaKeywords" type="text" name="keywords">
        <label for="personaNomProgramaAct">Nombre del programa Actual</label>
        <input id="personaNomProgramaAct" type="text" name="nomProgramaActual">
        <label for="personaUrlProgramActual">Url Programa Actual</label>
        <input id="personaUrlProgramActual" type="text" name="urlProgramActual">
        <label for="personaUrlNoticias">Url noticias</label>
        <input id="personaUrlNoticias" type="text" name="urlNoticias">
        <label for="personaUrlFotos">Url fotos</label>
        <input id="personaUrlFotos" type="text" name="urlFotos">
        <label for="personaUrlVideos">Url Videos</label>
        <input id="personaUrlVideos" type="text" name="urlVideos">
        <label for="personaUrlFacebook">Url Facebook</label>
        <input id="personaUrlFacebook" type="text" name="urlFacebook">
        <label for="personaUrlTwitter">Url Twitter</label>
        <input id="personaUrlTwitter" type="text" name="urlTwitter">
        <label for="personaUrlYoutube">Url Youtube</label>
        <input id="personaUrlYoutube" type="text" name="urlYoutube">
		<label for="personaUrlInstagram">Url Instagram</label>
		<input id="personaUrlInstagram" type="text" name="urlInstagram">
        <label for="personaUrlPinterest">Url Pinterest</label>
        <input id="personaUrlPinterest" type="text" name="urlPinterest">
        <label for="personaUrlGplus">Url Google plus</label>
        <input id="personaUrlGplus" type="text" name="urlGplus">
        <label for="personaContent">Contenido</label>
        <textarea id="personaContent" name="content"></textarea>
        <label style="display:none;" id="personaConsultRelacionada">Consulta Relacionada</label>
        <textarea style="display:none;" id = "personaConsultRelacionada" name="sinonimo"></textarea>
        <input type="hidden" id="personaFechaIngreso" name="personaFechaIngreso" value="<?php echo date("Y-m-d H:i:s"); ?>">
        <div id="acciones">
			<p>
				<span style="font-size: 20px;"> 
				 <a	onclick="javascript:mostrarPrograma('existente', '-1');">-</a>
				 <a	onclick="javascript:mostrarPrograma('existente', '1');">+</a>
				</span> Programa Relacionado existente
			</p>
			<p>
				<span style="font-size: 20px;"> 
				<a onclick="javascript:mostrarPrograma('nueva', '-1');">-</a>
				<a onclick="javascript:mostrarPrograma('nueva', '1');">+</a>
				</span> Nuevo Programa Relacionado
			</p>
		</div>

		
		
		<select name="programaRelacionado" id="programaRelacionado" size="5">
            <?php
            foreach ($this->programaLista as $key => $value) {
                echo '<option value="' . $value['idprograma'] . '">' . $value['programaTitulo'] . '</option>';
            }
            ?>
        </select>
       	<div id="orden">
	        <input name="del" value="<<" type="button" onclick="moverElementos(this.form,1);" id="pRelacionada2"/>
			<input name="add" value=">>" type="button" onclick="moverElementos(this.form,0);" id="pRelacionada3"/>
		</div>
		<select name="programaRelacionado2" size="5" id="programaRelacionado2" size="5">
			
		</select>
		
		


		<div id="relacionNueva"></div>

		<div id="acomodarG">
			<p id="mensaje"></p>
			<input type="button" onclick="javascript:sendPersona(this.form, 'mensaje','programaRelacionada','a');" value="Guardar" name="Guardar" class="botones" id="Guardar" />
		</div>
           
    </form>
</div>

<div id="help_dialog" title="Ayuda (?)"><img src="<?php echo URL;?>/public/images/help.png"></div>
<div id="tip_dialog" title="Tip (!)"><img src="<?php echo URL;?>/public/images/informacion.png"></div>


<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.noty.css';?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL.'public/css/jquery.qtip.css';?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo URL.'public/css/jquery.nailthumb.css';?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'/public/css/dropzone.css';?>"/>
<script src="<?php echo URL.'public/js/jquery.bdaypicker.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.noty.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/dropzone.js';?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.slickhover.js';?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo URL.'public/js/jquery.qtip.js'; ?>"></script>
<script type="text/javascript" src="<?php echo URL.'public/js/jquery.nailthumb.js'; ?>"></script>
<script>
var aviability = true;
function namecheck(name) {
	$('#namecheck').html('<img src="' + URL + 'public/images/loading.gif" />');
	$.post(URL + "personas/availability", {n: name} , function(data) {
		$('#namecheck').show();
		$('.qtip').each(function(){
			$(this).data('qtip').destroy();
		});
		var objects = jQuery.parseJSON(data);
		if(objects && objects.length !== 0) {
			aviability = false;
			$('#namecheck').delay(1000).fadeOut(400, function() {
				var link = "";
				var image = "";
				if(objects[0].edit)
					link = '<tr><td colspan="2" align="center"><a href="' + objects[0].edit + '">Editar</a></td></tr>';
				if(objects[0].thumb)
					image = '<td><div class="thumb-container name-image"><img src="' + objects[0].thumb + '" alt="' + objects[0].name + '" width="75" height="75"></div></td>';
				$('#namecheck').html('<img id="nameerror" src="' + URL + 'public/images/error.png" />').fadeIn(400);
				$("input[id=personaNom]").css("border", "solid 1px #FA5858");
				$('#nameerror').qtip({
					content: '<table border="0"><tr>' + image + '<td align="center"><h2>' + objects[0].name + '</h2>ya existe en la base de datos</td></tr>' + link + '</table>',
					style: {
						classes: 'qtip qtip-red qtip-shadow qtip-rounded'
					},
					show: {
						ready: true
					},
					hide: {
						event: false
					},
					events: {
						show: function () {
							$('.thumb-container').nailthumb({
								width: 75,
								height: 75
							});
						}
					}
				});
			});
		}	
		else {	
			aviability = true;
			$("input[id=personaNom]").css("border", "");
			$('#namecheck').delay(1000).fadeOut(400, function() {
				$('#namecheck').html('')
			});
		}
	}, 'json');
}
$(function() {
	
	$( "#nacimiento" ).birthdaypicker({ monthFormat: "long", dateFormat: "bigEndian" });
	
	$( "#help_dialog" ).dialog({ autoOpen: false, width: 999 });
	$( "#tip_dialog" ).dialog({ autoOpen: false, width: 939 });
	$( "#opener" ).click(function() {
		$( "#help_dialog" ).dialog( "open" );
	});
	$( "#opener2" ).click(function() {
		$( "#tip_dialog" ).dialog( "open" );
	});

    $('.delete').click(function(e) {
    	var checkboxes = $("form input:checkbox");
    	var cont = 0;
    	var vv = new Array();
	    for (var x=0; x < checkboxes.length; x++) {
			if (checkboxes[x].checked == true && checkboxes[x].name == 'borrar') {
				cont++;
				vv.push(checkboxes[x].value);
			}
	    }
	    if(cont == 0){
	    	alert("Seleccione los elementos a eliminar");
	    }else{
        
	        var c = confirm("\u00BFEst\u00E1 seguro de eliminar completamente?");
	        if (c == false)
	            return false;
	        else if(c == true){
	            eliminarVPersonas(vv);
	        }
	    }
    });
	
	
	$('.despublicar').click(function(e) {
    	var checkboxes = $("form input:checkbox");
    	var cont = 0;
    	var vv = new Array();
	    for (var x=0; x < checkboxes.length; x++) {
			if (checkboxes[x].checked == true && checkboxes[x].name == 'despublicar') {
				cont++;
				vv.push(checkboxes[x].value);
			}
	    }
	    if(cont == 0){
	    	alert("Seleccione los elementos para despublicar");
	    }else{
				guardarVPersonas(vv);
	        
	    }
    });
	
    
    $('.editar').click(function(e) {
    	var checkboxes = $("form input:checkbox");
    	var cont = 0;
    	var vv = new Array();
	    for (var x=0; x < checkboxes.length; x++) {
			if (checkboxes[x].checked == true && checkboxes[x].name == 'editar') {
				cont++;
				vv.push(checkboxes[x].value);
			}
	    }
	    if(cont == 0){
	    	alert("Seleccione los elementos para publicar");
	    }else{
	            generarXMLVariosPersonas(vv);
	        
	    }
    });
	var hover = function() {
		$("#thumbnail").slickhover({
			icon: URL + "public/images/slickhover/plus-black.png",
			animateIn: true,
			opacity: 0.1,
			speed: 600
		});
	}
	var removeHover = function() {
		$("#thumbnail").off();
		$(".iconslickhoverWrapper").remove();
		var slickhoverContent = $(".slickhoverWrapper").contents();
		$(".slickhoverWrapper").replaceWith(slickhoverContent);
		$("#thumbnail").css("opacity", "");
	}
	Dropzone.autoDiscover = false;
	$("#thumbnail").dropzone({ 
		url: URL + "<?php echo IMG_PERSON_UPLOAD_HANDLER; ?>",
		maxFilesize: "<?php echo IMG_MAX_UPLOAD_SIZE; ?>",
		addRemoveLinks: true,
		acceptedFiles: "image/*",
		dictInvalidFileType: "No es un formato de imagen v\u00E1lido",
		dictFileTooBig: "La imagen es demasiado grande. <?php echo IMG_MAX_UPLOAD_SIZE; ?> MB m\u00E1ximo",
		dictResponseError: "Hubo un error en el servidor",
		dictRemoveFile: "Remover",
		accept: function(file, done) {
			console.log("accept");
			removeHover();
			done();
		},
		success: function(file, response) {
			console.log("success");
			removeHover();
			response = JSON.parse(response);
			if(response.response == "error") {
				var node, _i, _len, _ref, _results;
				var message = response.serversays;
				file.previewElement.classList.add("dz-error");
				_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
				_results = [];
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				  node = _ref[_i];
				  _results.push(node.textContent = message);
				}
				return _results;
			}
			else {
				$("#personaUrlThumb").val(response.image_url).prop("readonly", true).css({'background-color' :  '#C0C0C0'});
				return file.previewElement.classList.add("dz-success");
			}
		},
		error: function (file, message) {
			console.log("error");
			removeHover();
			var node, _i, _len, _ref, _results;
			if (file.previewElement) {
			  file.previewElement.classList.add("dz-error");
			  if (typeof message !== "String" && message.error) {
				message = message.error;
			  }
			  _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
			  _results = [];
			  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				node = _ref[_i];
				_results.push(node.textContent = message);
			  }
			  return _results;
			}
		},
		removedfile: function(file) {
			console.log("removed");
			hover();
			$("#personaUrlThumb").val("").prop("readonly", false).css({'background-color' :  ''});
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		},
		init: function () {
			console.log("init");
			hover();
		}
	});
});
</script>