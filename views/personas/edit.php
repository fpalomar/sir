<h1>Personas: Editar</h1>

<div id="formulario">
		<img id="opener" src="<?php echo URL;?>/public/images/ayuda.png" alt="A" title="Ayuda (?)">
		<img id="opener2" src="<?php echo URL;?>/public/images/tip.png" alt="T" title="Tip (!)">
 <form method="post" action="<?php echo URL; ?>personas/editSave/<?php echo $this->personas[0]['idPersona']; ?>" >
		<div class="dropzone dropzoneEdit" id="dropzone">
			<div id="thumbnail"></div>
			<span id="textThumb">Thumbnail<span>o</span></span>
		</div>
		<label for="personaNom">Nombre</label>
		<?php
			$readonly = '';
			if($this->personas[0]['personaStatus'] == 0)
				$readonly = ' readonly="readonly" ';
		?><input id="personaNom" type="text" name="nombre" value="<?php echo $this->personas[0]['personaNom'];?>"<?php echo $readonly; ?>/>
        <label for="personaNacimiento">Fecha de Nacimiento</label>
		<div class="picker" id="nacimiento"></div>
		<?php
			$bd = true;
			if($this->personas[0]['personaNacimiento'] == "0000-00-00")
				$bd = false;
			else if($this->personas[0]['personaNacimiento'] == "" || $this->personas[0]['personaNacimiento'] == NULL)
				$bd = false;
		?><input id="personaNacimiento" type="hidden" name="personaNacimiento" <?php if($bd) echo 'value="'.$this->personas[0]['personaNacimiento'].'"';?>>
		<label for="personaBranch">Categor&iacute;a</label>
		<select id="personaBranch" name="branches">
			<option>-- Seleccione una categor&iacute;a --</option>
			<?php
				foreach($this->branchList as $key => $value) {
					if($this->personas[0]['personaBranch'] == $value['idBranch'])
						$selected = " selected";
					else
						$selected = "";
					echo '<option name="'.$value['nombre'].'" value="'.$value['idBranch'].'"'.$selected.'>'.$value['nombre'].'</option>';
				}
			?>
		</select>
		<label for="personaUrlBio">Url de la biograf&iacute;a</label>
        <input id="personaUrlBio" type="text" name="urlBio" value="<?php echo $this->personas[0]['personaUrlBio'];?>">
        <label for="personaResumenBio">Resumen de la Biograf&iacute;a</label>
        <textarea id="personaResumenBio" name="resumenBio"><?php echo $this->personas[0]['personaResumenBio'];?></textarea>
        <label for="personaUrlThumb" id="UrlThumb">Url del Thumbnail</label>
        <input id="personaUrlThumb" type="text" name="urlThumb" value="<?php echo $this->personas[0]["personaUrlThumb"];?>">
		<label for="personaKeywords">Keywords</label>
        <input id="personaKeywords" type="text" name="keywords" value="<?php echo $this->personas[0]["personaKeywords"];?>">
        <label for="personaNomProgramaAct">Nombre del programa Actual</label>
        <input id="personaNomProgramaAct" type="text" name="nomProgramaActual" value="<?php echo $this->personas[0]['personaNomProgramaAct'];?>">
        <label for="personaUrlProgramActual">Url Programa Actual</label>
        <input id="personaUrlProgramActual" type="text" name="urlProgramActual" value="<?php echo $this->personas[0]['personaUrlProgramActual'];?>">
        <label for="personaUrlNoticias">Url noticias</label>
        <input id="personaUrlNoticias" type="text" name="urlNoticias"  value="<?php echo $this->personas[0]['personaUrlNoticias'];?>">
        <label for="personaUrlFotos">Url fotos</label>
        <input id="personaUrlFotos" type="text" name="urlFotos" value="<?php echo $this->personas[0]['personaUrlFotos'];?>">
        <label for="personaUrlVideos">Url Videos</label>
        <input id="personaUrlVideos" type="text" name="urlVideos" value="<?php echo $this->personas[0]['personaUrlVideos'];?>">
        <label for="personaUrlFacebook">Url Facebook</label>
        <input id="personaUrlFacebook" type="text" name="urlFacebook" value="<?php echo $this->personas[0]['personaUrlFacebook'];?>">
        <label for="personaUrlTwitter" >Url Twitter</label>
        <input id="personaUrlTwitter" type="text" name="urlTwitter" value="<?php echo $this->personas[0]['personaUrlTwitter'];?>">
        <label for="personaUrlYoutube">Url Youtube</label>
        <input id="personaUrlYoutube" type="text" name="urlYoutube" value="<?php echo $this->personas[0]['personaUrlYoutube'];?>">
		<label for="personaUrlInstagram">Url Instagram</label>
        <input id="personaUrlInstagram" type="text" name="urlInstagram" value="<?php echo $this->personas[0]['personaUrlInstagram'];?>">
        <label for="personaUrlPinterest">Url Pinterest</label>
        <input id="personaUrlPinterest" type="text" name="urlPinterest" value="<?php echo $this->personas[0]['personaUrlPinterest'];?>">
        <label for="personaUrlGplus" >Url Google plus</label>
        <input id="personaUrlGplus" type="text" name="urlGplus" value="<?php echo $this->personas[0]['personaUrlGplus'];?>">
        <label for="personaContent">Contenido</label>
        <textarea id="personaContent" name="content"><?php echo $this->personas[0]['personaContent'];?></textarea>
        <label style="display:none;" id = "personaConsultRelacionada">Consulta Relacionada</label>
        <textarea style="display:none;" id = "personaConsultRelacionada" name="sinonimo"><?php echo $this->personas[0]['personaConsultRelacionada'] ?></textarea>

          <div id="acciones">
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrarPrograma('existente', '-1');">-</a> <a
					onclick="javascript:mostrarPrograma('existente', '1');">+</a>
				</span> Programa Relacionado existente
			</p>
			<p>
				<span style="font-size: 20px;"> <a
					onclick="javascript:mostrarPrograma('nueva', '-1');">-</a> <a
					onclick="javascript:mostrarPrograma('nueva', '1');">+</a>
				</span> Nuevo Programa Relacionado
			</p>
		</div>
        
		<select name="programaRelacionado" id="programaRelacionado" size="5">
            <?php
            foreach ($this->programaLista as $key => $value) {
                echo '<option value="' . $value['idprograma'] . '">' . $value['programaTitulo'] . '</option>';
            }
            ?>
        </select>
       	<div id="orden">
	        <input name="del" value="<<" type="button" onclick="moverElementos(this.form,1);" id="pRelacionada2"/>
			<input name="add" value=">>" type="button" onclick="moverElementos(this.form,0);" id="pRelacionada3"/>
		</div>
		<select name="programaRelacionado2" size="5" id="programaRelacionado2" size="5">
			<?php
            foreach ($this->programasSeleccionados as $key => $value) {
				echo '<option value="' . $value['idprograma'] . '">' . $value['programaTitulo'] . '</option>';
            }
            ?>
		</select>
		
       

		<div id="relacionNueva"></div>
 
		<div id="acomodarG">
			<p id="mensaje"></p>
			<input type="button" onclick="javascript:sendPersona(this.form, 'mensaje','programaRelacionada', 'e');" value="Guardar" name="Guardar" id="botones" />
			<input type="button" onclick="javascript:generateXMLPersona('editar',<?php echo $this->personas[0]['idPersona']?>);" value="Generar XML" name="GenerarXML" id="botones" /> 
		
		</div>
        
           
    </form>
</div>

<div id="help_dialog" title="Ayuda (?)"><img src="<?php echo URL;?>/public/images/help.png"></div>
<div id="tip_dialog" title="Tip (!)"><img src="<?php echo URL;?>/public/images/informacion.png"></div>

<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'public/css/jquery.noty.css';?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo URL.'/public/css/dropzone.css';?>"/>
<script src="<?php echo URL.'public/js/jquery.bdaypicker.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.noty.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/dropzone.js';?>" type="text/javascript"></script>
<script src="<?php echo URL.'public/js/jquery.slickhover.js';?>" type="text/javascript"></script>
<script>
$(function() {
	$( "#nacimiento" ).birthdaypicker({ monthFormat: "long", dateFormat: "bigEndian"<?php if($bd){ echo ', defaultDate: "' . date('Y-m-d', strtotime($this->personas[0]['personaNacimiento'])) . '"';} ?>});
	
	$( "#help_dialog" ).dialog({ autoOpen: false, width: 999 });
	$( "#tip_dialog" ).dialog({ autoOpen: false, width: 939 });
	$( "#opener" ).click(function() {
		$( "#help_dialog" ).dialog( "open" );
	});
	$( "#opener2" ).click(function() {
		$( "#tip_dialog" ).dialog( "open" );
	});
	var hover = function() {
		$("#thumbnail").slickhover({
			icon: URL + "public/images/slickhover/plus-black.png",
			animateIn: true,
			opacity: 0.1,
			speed: 600
		});
	}
	var removeHover = function() {
		$("#thumbnail").off();
		$(".iconslickhoverWrapper").remove();
		var slickhoverContent = $(".slickhoverWrapper").contents();
		$(".slickhoverWrapper").replaceWith(slickhoverContent);
		$("#thumbnail").css("opacity", "");
	}
	Dropzone.autoDiscover = false;
	$("#thumbnail").dropzone({ 
		url: URL + "<?php echo IMG_PERSON_UPLOAD_HANDLER; ?>",
		maxFilesize: "<?php echo IMG_MAX_UPLOAD_SIZE; ?>",
		addRemoveLinks: true,
		acceptedFiles: "image/*",
		dictInvalidFileType: "No es un formato de imagen v\u00E1lido",
		dictFileTooBig: "La imagen es demasiado grande. <?php echo IMG_MAX_UPLOAD_SIZE; ?> MB m\u00E1ximo",
		dictResponseError: "Hubo un error en el servidor",
		dictRemoveFile: "Remover",
		accept: function(file, done) {
			console.log("accept");
			removeHover();
			done();
		},
		success: function(file, response) {
			console.log("success");
			removeHover();
			response = JSON.parse(response);
			if(response.response == "error") {
				var node, _i, _len, _ref, _results;
				var message = response.serversays;
				file.previewElement.classList.add("dz-error");
				_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
				_results = [];
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				  node = _ref[_i];
				  _results.push(node.textContent = message);
				}
				return _results;
			}
			else {
				$("#personaUrlThumb").val(response.image_url).prop("readonly", true).css({'background-color' :  '#C0C0C0'});
				return file.previewElement.classList.add("dz-success");
			}
		},
		error: function (file, message) {
			console.log("error");
			removeHover();
			var node, _i, _len, _ref, _results;
			if (file.previewElement) {
			  file.previewElement.classList.add("dz-error");
			  if (typeof message !== "String" && message.error) {
				message = message.error;
			  }
			  _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
			  _results = [];
			  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				node = _ref[_i];
				_results.push(node.textContent = message);
			  }
			  return _results;
			}
		},
		removedfile: function(file) {
			console.log("removed");
			hover();
			$("#personaUrlThumb").val("").prop("readonly", false).css({'background-color' :  ''});
			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		},
		init: function () {
			console.log("init");
			<?php
				if($this->personas[0]["personaUrlThumb"] != "") {
			?>removeHover();
			$("#personaUrlThumb").prop("readonly", true).css({'background-color' :  '#C0C0C0'});
			<?php
					if($this->isImage !== false) {
						$imgObjects = json_decode($this->getImageInfo);
			?>var mockFile = { name: "<?php echo $imgObjects -> {'name'}; ?>", size: <?php echo $imgObjects -> {'length'}; ?>, type: "<?php echo $imgObjects -> {'type'}; ?>"}; 
			this.emit("addedfile", mockFile);
			this.options.thumbnail.call(this, mockFile, "<?php echo $this->personas[0]["personaUrlThumb"]; ?>");
			<?php
					}
					else {
			?>var mockFile = { name: "<?php echo basename($this->personas[0]["personaUrlThumb"]);?>", size: 0}; 
			this.emit("addedfile", mockFile);
			this.emit("error", mockFile, "No existe la imagen o no est\u00e1 disponible en este momento");
			$('#dropzone').addClass('dz-error');
			this.options.thumbnail.call(this, "", "<?php echo $this->personas[0]["personaUrlThumb"]; ?>");
			<?php
					}
				}
				else {
			?>hover();
			<?php
				}
			?>
		}
	});
});
</script>


