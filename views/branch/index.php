<h1>Rama</h1>
<div id="formulario">

<form method="post" action="<?php echo URL;?>branch/create">
    <label>Rama</label><input type="text" name="nombre" /><br />
	<label>ID de la rama en Galaxy</label><input type="text" name="galaxy" /><br />
    <label>URL</label><input type="text" name="url" /><br />
    <label>Imagen</label><input type="text" name="imagen" /><br />
    <label>&nbsp;</label><input type="submit" />
</form>
</div>

<hr />

<table width="100%" id="resultdos" border="0">
	<tr>
		<td><b>ID</b></td>
		<td><b>G-ID</b></td>
		<td><b>Nombre</b></td>
		<td><b>URL</b></td>
		<td><b>URL Imagen</b></td>
		<td colspan="2" align="center"><b>Opciones</b></td>
	</tr>
<?php
    foreach($this->branchList as $key => $value) {
		if(($key%2) == 1){
			echo '<tr bgcolor="#F2F2F2">';
		}else{
			echo '<tr>';
		}
        echo '<td>' . $value['idBranch'] . '</td>';
		echo '<td>' . $value['idGalaxy'] . '</td>';
        echo '<td>' . $value['nombre'] . '</td>';
        echo '<td>' . $value['url'] . '</td>';
		echo '<td onmouseover="document.getElementById(\'place-holder-'.$key.'\').src=\'' . $value['imagen'] . '\';" onmouseout="document.getElementById(\'place-holder-'.$key.'\').src=\''.URL.'public/images/pixel.png\';">' . $value['imagen'] . '<img src="'.URL.'public/images/pixel.png" id="place-holder-'.$key.'" style="zindex: 100; position: absolute;" /></td>';
        echo '<td>
                <a href="'.URL.'branch/edit/'.$value['idBranch'].'">Editar</a> 
                <a href="'.URL.'branch/delete/'.$value['idBranch'].'">Borrar</a></td>';
        echo '</tr>';
    }
?>
</table>