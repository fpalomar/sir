		<div class="container hide">	
			<div class="main-container">
				<div class="main-content">
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<div class="login-container">
								<div class="center">
									<h1>
										<i class="ace-icon fa fa-google blue"></i>
										<span class="red">SIR</span>
									</h1>
									<h4 class="televisa" id="id-company-text">&copy; Televisa Interactive Media</h4>
								</div>
								<div class="space-6"></div>
								<div class="position-relative">
									<div id="login-box" class="login-box visible widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header blue lighter bigger">
													<i class="ace-icon fa fa-coffee green"></i>
													Por favor, escribe tus datos
												</h4>
												<div class="space-6"></div>
												<form action="<?php echo URL; ?>login/run" method="post" id="login">
													<fieldset>
														<label class="block form-group clearfix">
															<span class="block input-icon input-icon-right">
																<input type="text" class="form-control" placeholder="correo@televisatim.com" name="login">
																<i class="ace-icon fa fa-user"></i>
															</span>
														</label>
														<label class="block form-group clearfix">
															<span class="block input-icon input-icon-right">
																<input type="password" class="form-control" placeholder="contraseña de Galaxy" name="pass">
																<i class="ace-icon fa fa-lock"></i>
															</span>
														</label>
														<div class="space"></div>
														<div class="clearfix">
															<label>
																<input type="checkbox" class="ace" id="remember-me" value="remember-me">
																<span class="lbl"> Recordarme</span>
															</label>
															<button id="btn-submit" type="submit" class="width-35 pull-right btn btn-sm btn-primary">
																<i class="ace-icon fa fa-key"></i>
																<span class="bigger-110">Login</span>
															</button>
														</div>
														<div class="space-4"></div>
													</fieldset>
												</form>
											</div><!-- /.widget-main -->
											<div class="toolbar clearfix">
												<div>
													<a href="#" data-target="#forgot-box" class="forgot-password-link">
														<i class="ace-icon fa fa-arrow-left"></i>
														Olvidé mi contraseña
													</a>
												</div>
											</div>
										</div><!-- /.widget-body -->
									</div><!-- /.login-box -->
									<div id="forgot-box" class="forgot-box widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header red lighter bigger">
													<i class="ace-icon fa fa-key"></i>
													Restablecer mi contraseña
												</h4>
												<div class="space-6"></div>
												<p>
													<strong>SIR</strong> funciona con tu usuario y contraseña de <strong>Galaxy</strong>, si olvidaste la contraseña visita el siguiente enlace para restablecerla:
												</p>
												<p class="center">
													<a href="http://galaxy.esmas.com/forgot_pwd/" target="_blank" class="width-35 btn btn-sm btn-danger">
														<i class="ace-icon fa fa-lightbulb-o"></i>
														<span class="bigger-110">¡Ir!</span>
													</a>
												</p>
											</div><!-- /.widget-main -->
											<div class="toolbar center">
												<a href="#" data-target="#login-box" class="back-to-login-link">
													Regresar al login
													<i class="ace-icon fa fa-arrow-right"></i>
												</a>
											</div>
										</div><!-- /.widget-body -->
									</div><!-- /.forgot-box -->
								</div><!-- /.position-relative -->
							</div><!-- /.login-container -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container -->
		</div><!-- /.cotainer -->