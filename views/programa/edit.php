							<div class="page-header">
								<h1>
									Programas
									<small>
										<i class="ace-icon fa fa-angle-double-right"></i>
											<?php $data = json_decode($this->programaGetInfo, true); ?>
											Editar <span class="p-name"><?php echo $data[0]['programaTitulo']; ?></span>
									</small>
								</h1>
							</div><!-- /.page-header -->
							<div class="row programs-edit">
								<form class="form-horizontal" id="program" role="form" method="post" action="<?php echo URL; ?>programa/editSave">
									<input type="hidden" name="id" value="<?php echo $data[0]['programaId']; ?>">
									<div class="col-sm-offset-1 col-sm-10">
										<h4 class="header blue bolder smaller">General</h4>
										<div class="form-group"><!-- Field name -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-name">Nombre</label>
											<div id="field-name" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="name" placeholder="Nombre del programa" class="col-xs-12 col-sm-12" id="form-field-name" value="<?php echo $data[0]['programaTitulo']; ?>" readonly="readonly">
											</div>
										</div><!-- /Field name -->
										<div class="form-group"><!-- Field branch -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-branch">Canal</label>
											<div id="field-branch" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<select class="form-control branches" id="form-field-branch" name="branch">
													<option value="" selected>Selecciona el canal al que pertenece</option>
<?php
$branch = json_decode($this->ramaList, true);
foreach($branch as $key => $value) {
	if($data[0]['programaRama'] == $value['ramaId'])
		$selected = " selected";
	else
		$selected = "";
	echo '													<option name="' . $value['ramaNombre'] . '" value="' . $value['ramaId'] . '"' . $selected . '>' . $value['ramaNombre'] . '</option>'.PHP_EOL;
}
?>
												</select>
											</div>
										</div><!-- /Field branch -->
<?php
$type = $data[0]['programaTipo'];
?>
										<div class="form-group"><!-- Field type -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-type">Tipo</label>
											<div id="field-branch" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<select class="form-control types" id="form-field-type" name="type">
													<option value="" selected>Selecciona el tipo de programa</option>
													<option value="1" name="Noticiero"<?php if($type == 1) echo ' selected';?>>Noticiero</option>
													<option value="2" name="Novela"<?php if($type == 2) echo ' selected';?>>Novela</option>
													<option value="3" name="Programa"<?php if($type == 3) echo ' selected';?>>Programa</option>
													<option value="4" name="Serie"<?php if($type == 4) echo ' selected';?>>Serie</option>
													<option value="5" name="Web Novela"<?php if($type == 5) echo ' selected';?>>Web Novela</option>
													<option value="6" name="Web Serie"<?php if($type == 6) echo ' selected';?>>Web Serie</option>
													<option value="7" name="Otro"<?php if($type == 7) echo ' selected';?>>Otro</option>
												</select>
											</div>
										</div><!-- /Field type -->
										<div class="form-group"><!-- Field schedule -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-schedule">Horario</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4 timepair">
												<div class="col-xs-5 no-padding-right no-padding-left inline">
													<div class="input-group">
														<input type="text" name="start-time" placeholder="Hora de inicio" class="time start col-xs-12" id="form-field-start-time" value="<?php echo $data[0]['programaHoraInicio']; ?>">
														<span class="input-group-addon">
															<i class="ace-icon fa fa-clock-o"></i>
														</span>
													</div>
												</div>
												<div class="col-xs-2 control-label no-padding-right no-padding-left inline center">
													a
												</div>
												<div class="col-xs-5 no-padding-right no-padding-left inline">
													<div class="input-group">
														<input type="text" name="end-time" placeholder="Hora de fin" class="time end col-xs-12" id="form-field-end-time" value="<?php echo $data[0]['programaHoraFin']; ?>">
														<span class="input-group-addon">
															<i class="ace-icon fa fa-clock-o"></i>
														</span>
													</div>
												</div>
											</div>
										</div><!-- /Field schedule -->
										<div class="form-group"><!-- Field season -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-season">Temporadas</label>
											<div id="field-season" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="season" placeholder="Número de temporadas" class="col-xs-12 col-sm-12" id="form-field-season" value="<?php echo $data[0]['programaTemporadas']; ?>">
											</div>
										</div>
<?php
	if($data[0]['programaFechaInicio'] == '0000-00-00' || empty($data[0]['programaFechaInicio']))
		$startdate = '';
	else {
		$sd = explode('-', $data[0]['programaFechaInicio']);
		$startdate = $sd[2] . '-' . $sd[1] . '-' . $sd[0];
	}
?>
										<div class="form-group"><!-- Field start -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-start-date">Fecha de inicio</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div id="field-start-date" class="input-group date">
													<input type="text" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy" id="form-field-start-date" class="col-xs-12 col-sm-12" name="startdate" value="<?php echo $startdate; ?>">
													<span class="input-group-addon">
														<i class="ace-icon fa fa-calendar"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field start -->
<?php
	if($data[0]['programaFechaFin'] == '0000-00-00' || empty($data[0]['programaFechaFin']))
		$enddate = '';
	else {
		$ed = explode('-', $data[0]['programaFechaFin']);
		$enddate = $ed[2] . '-' . $ed[1] . '-' . $ed[0];
	}
?>
										<div class="form-group"><!-- Field end -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-end-date">Fecha de fin</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div id="field-end-date" class="input-group date">
													<input type="text" placeholder="dd-mm-yyyy" data-date-format="dd-mm-yyyy" id="form-field-end-date" class="col-xs-12 col-sm-12" name="enddate" value="<?php echo $enddate; ?>">
													<span class="input-group-addon">
														<i class="ace-icon fa fa-calendar"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field end -->
										<h4 class="header blue bolder smaller">Imagen</h4>
										<div class="form-group"><!-- Field image url -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-image-url">Dirección</label>
											<div id="field-image-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="image-url" placeholder="Ej.: http://www.televisa.com/programa/imagen.jpg" class="col-xs-12 col-sm-12" id="form-field-image-url" id="programaThumbnail" value="<?php echo $data[0]['programaThumbnail']; ?>">
											</div>
										</div><!-- /Field image url -->
										<div class="form-group"><!-- Dropzone image -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right">&nbsp;</label>
											<div class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div id="dropzone" class="dropzone well dz-clickable">
												</div>
											</div>
										</div><!-- /Dropzone image -->										
										<h4 class="header blue bolder smaller">Contenido</h4>
										<div class="form-group"><!-- Field url -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-url">Dirección</label>
											<div id="field-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="url" placeholder="Ej.: http://www.televisa.com/programa/" class="col-xs-12 col-sm-12" id="form-field-url" value="<?php echo $data[0]['programaUrl']; ?>">
											</div>
										</div><!-- /Field url -->
										<div class="form-group"><!-- Field resume -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-resume">Resumen</label>
											<div id="field-resume" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<textarea placeholder="Resumen de la descripción del programa" class="autosize-transition form-control" id="resume" name="resume"><?php echo $data[0]['programaDescripcion']; ?></textarea>
											</div>
										</div><!-- /Field resume -->
										<div class="form-group"><!-- Field complete -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-complete">Contenido</label>
											<div id="field-complete" class="col-xs-12 col-sm-12 col-md-8 col-lg-12">
												<textarea placeholder="Contenido completo de la descripción del programa" class="autosize-transition form-control" id="complete" name="complete"><?php echo $data[0]['programaDescripcionCompleta']; ?></textarea>
											</div>
										</div><!-- /Field complete -->
<?php
	$curiousFacts = json_decode($this->programaGetCuriousFacts, true);
	if(empty($curiousFacts)) {
?>
										<div class="form-group"><!-- Field curious fact -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-curious-fact">Dato Curioso</label>
											<div id="field-curious-fact" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
												<input type="text" name="curious-fact[]" placeholder="Dato Curioso" class="col-xs-12 col-sm-12" id="form-field-curious-fact">
											</div>
											<div class="col-xs-1 no-padding">
												<button type="button" class="btn btn-sm btn-default addFact tt" title="Agregar otro dato curioso" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
											</div>
										</div><!-- /Field curious fact -->
<?php
	}
	else {
		$countCurious = count($curiousFacts);
		for($i = 0; $i < $countCurious; $i++) {
			if($i != 0) {
?>
										<div class="fact">
<?php
			}
?>	
											<div class="form-group"><!-- Field curious fact -->
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-curious-fact">Dato Curioso</label>
												<div id="field-curious-fact" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="curious-fact[<?php echo $i; ?>]" placeholder="Dato Curioso" class="col-xs-12 col-sm-12" id="form-field-curious-fact" value="<?php echo $curiousFacts[$i]['programacuriosidadesContenido']; ?>">
												</div>
<?php
			if($i == 0) {
?>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default addFact tt" title="Agregar otro dato curioso" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
												</div>
<?php
			}
			else {
?>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default removeFact tt" title="Quitar dato curioso" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
												</div>
<?php
			}
?>
											</div><!-- /Field curious fact -->
<?php 
			if($i != 0) {
?>
										</div>
<?php
			}
		}
	}
?>
										<div class="hide" id="curious-fact-template"><!-- The template for adding new field -->
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-curious-fact">Dato Curioso</label>
												<div id="field-new-curious-fact" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="curious-fact[]" placeholder="Dato Curioso" class="col-xs-12 col-sm-12" id="form-field-curious-fact">
												</div>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default removeFact tt" title="Quitar dato curioso" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
												</div>
											</div>
										</div><!-- /Template -->
										<h4 class="header blue bolder smaller">Personas Actuales</h4>
										<div class="form-group"><!-- Field actual person -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-actual-person">Personas actuales</label>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<select multiple="multiple" size="10" name="duallistbox_actualperson[]" id="actual-person">
<?php
$list = json_decode($this->personaSingleList, true);
$programRelations = json_decode($this->programaGetRelations, true);
$actualPersons = $programRelations;
$actualIdPerson = array();
foreach ($actualPersons as $key => $value) {
	if($value['programaActual'] == 1)
		array_push($actualIdPerson, $value['personaId']);
}
foreach ($list as $key => $value) {
	if(in_array($value['personaId'], $actualIdPerson, true))
		echo '													<option value="' . $value['personaId'] . '" title="' . trim($value['personaNombre']) . '" selected="selected">' . trim($value['personaNombre']) . '</option>'.PHP_EOL;
	else
		echo '													<option value="' . $value['personaId'] . '" title="' . trim($value['personaNombre']) . '">' . trim($value['personaNombre']) . '</option>'.PHP_EOL;
	
}
?>
												</select>
											</div>
										</div>
										<div class="form-group no-margin no-padding relative new-actual-person-group">
											<div class="form-group"><!-- Field new actual person -->
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-name">Nueva persona actual</label>
												<div id="field-new-actual-person-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="new-actual-person-name[]" placeholder="Nombre de la nueva persona actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-name">
												</div>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default addNewActualPerson tt" title="Agregar otra persona actual" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-surname">Apellido Paterno</label>
												<div id="field-new-actual-person-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" name="new-actual-person-surname[]" placeholder="Apellido Paterno de la nueva persona actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-surname">
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-maidenname">Apellido Materno</label>
												<div id="field-new-actual-person-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" name="new-actual-person-maidenname[]" placeholder="Apellido Materno de la nueva persona actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-maidenname">
												</div>
											</div>
											<!-- Create a hidden field which is combined by 3 fields above -->
											<input type="hidden" name="new-actual-person-complete-name[]" disabled="disabled">
										</div>
										<div class="form-group">
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-bio-url">Dirección</label>
											<div id="field-new-actual-person-bio-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="new-actual-person-bio-url[]" placeholder="Ej.: http://www.televisa.com/interprete/biografia/" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-bio-url">
											</div>
										</div>
										<!-- The template for adding new field -->
										<div class="hide" id="new-actual-person-template">
											<div class="form-group no-margin no-padding relative new-actual-person-group">
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-name">Nueva persona actual</label>
													<div id="field-new-actual-person-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
														<input type="text" name="new-actual-person-name[]" placeholder="Nombre de la nueva persona actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-name">
													</div>
													<div class="col-xs-1 no-padding">
														<button type="button" class="btn btn-sm btn-default removeNewActualPerson tt" title="Quitar persona actual" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-surname">Apellido Paterno</label>
													<div id="field-new-actual-person-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<input type="text" name="new-actual-person-surname[]" placeholder="Apellido Paterno de la nueva persona actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-surname">
													</div>
												</div>
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-maidenname">Apellido Materno</label>
													<div id="field-new-actual-person-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<input type="text" name="new-actual-person-maidenname[]" placeholder="Apellido Materno de la nueva persona actual" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-maidenname">
													</div>
												</div>
												<!-- Create a hidden field which is combined by 3 fields above -->
												<input type="hidden" name="new-actual-person-complete-name[]" disabled="disabled">
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-actual-person-bio-url">Dirección</label>
												<div id="field-new-actual-person-bio-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" name="new-actual-person-bio-url[]" placeholder="Ej.: http://www.televisa.com/interprete/biografia/" class="col-xs-12 col-sm-12" id="form-field-new-actual-person-bio-url">
												</div>
											</div>
										</div>
										<h4 class="header blue bolder smaller">Personas Relacionadas</h4>
										<div class="form-group"><!-- Field related person -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-related-person">Personas relacionadas</label>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<select multiple="multiple" size="10" name="duallistbox_relatedperson[]" id="related-person">
<?php
$relatedPersons = $programRelations;
$relatedIdPerson = array();
foreach ($relatedPersons as $key => $value)
	array_push($relatedIdPerson, $value['personaId']);
$list = json_decode($this->personaSingleList, true);
foreach ($list as $key => $value) {
	if(in_array($value['personaId'], $relatedIdPerson, true))
		echo '													<option value="' . $value['personaId'] . '" title="' . trim($value['personaNombre']) . '" selected="selected">' . trim($value['personaNombre']) . '</option>'.PHP_EOL;
	else
		echo '													<option value="' . $value['personaId'] . '" title="' . trim($value['personaNombre']) . '">' . trim($value['personaNombre']) . '</option>'.PHP_EOL;
}
?>
												</select>
											</div>
										</div><!-- /Field related person -->
										<div class="form-group no-margin no-padding relative new-related-person-group">
											<div class="form-group"><!-- Field new related person name -->
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-name">Nueva persona relacionada</label>
												<div id="field-new-related-person-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
													<input type="text" name="new-related-person-name[]" placeholder="Nombre de la nueva persona relacionada" class="col-xs-12 col-sm-12" id="form-field-new-related-person-name">
												</div>
												<div class="col-xs-1 no-padding">
													<button type="button" class="btn btn-sm btn-default addNewRelatedPerson tt" title="Agregar otra persona relacionada" data-placement="top" data-rel="tooltip"><i class="fa fa-plus"></i></button>
												</div>
											</div><!-- /Field new related person name -->
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-surname">Apellido Paterno</label>
												<div id="field-new-related-person-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" name="new-related-person-surname[]" placeholder="Apellido Paterno de la nueva persona relacionada" class="col-xs-12 col-sm-12" id="form-field-new-related-person-surname">
												</div>
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-maidenname">Apellido Materno</label>
												<div id="field-new-related-person-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" name="new-related-person-maidenname[]" placeholder="Apellido Materno de la nueva persona relacionada" class="col-xs-12 col-sm-12" id="form-field-new-related-person-maidenname">
												</div>
											</div>
											<!-- Create a hidden field which is combined by 3 fields above -->
											<input type="hidden" name="new-related-person-complete-name[]" disabled="disabled">
										</div>
										<div class="form-group"><!-- Field new related person url -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-bio-url">Dirección</label>
											<div id="field-new-related-person-bio-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="new-related-person-bio-url[]" placeholder="Ej.: http://www.televisa.com/interprete/biografia/" class="col-xs-12 col-sm-12" id="form-field-new-related-person-bio-url">
											</div>
										</div><!-- /Field new related person url -->
										<div class="hide" id="new-related-person-template"><!-- The template for adding new field -->
											<div class="form-group no-margin no-padding relative new-related-person-group">
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-name">Nueva persona relacionada</label>
													<div id="field-new-related-person-name" class="col-xs-10 col-sm-4 col-md-7 col-lg-4">
														<input type="text" name="new-related-person-name[]" placeholder="Nombre de la nueva persona relacionada" class="col-xs-12 col-sm-12" id="form-field-new-related-person-name">
													</div>
													<div class="col-xs-1 no-padding">
														<button type="button" class="btn btn-sm btn-default removeNewRelatedPerson tt" title="Quitar persona relacionada" data-placement="top" data-rel="tooltip"><i class="fa fa-minus"></i></button>
													</div>
												</div>
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-surname">Apellido Paterno</label>
													<div id="field-new-related-person-surname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<input type="text" name="new-related-person-surname[]" placeholder="Apellido Paterno de la nueva persona relacionada" class="col-xs-12 col-sm-12" id="form-field-new-related-person-surname">
													</div>
												</div>
												<div class="form-group">
													<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-maidenname">Apellido Materno</label>
													<div id="field-new-related-person-maidenname" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
														<input type="text" name="new-related-person-maidenname[]" placeholder="Apellido Materno de la nueva persona relacionada" class="col-xs-12 col-sm-12" id="form-field-new-related-person-maidenname">
													</div>
												</div>
												<!--
												 Create a hidden field which is combined by 3 fields above -->
												<input type="hidden" name="new-related-person-complete-name[]" disabled="disabled">
											</div>
											<div class="form-group">
												<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-new-related-person-bio-url">Dirección</label>
												<div id="field-new-related-person-bio-url" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
													<input type="text" name="new-related-person-bio-url[]" placeholder="Ej.: http://www.televisa.com/interprete/biografia/" class="col-xs-12 col-sm-12" id="form-field-new-related-person-bio-url">
												</div>
											</div>
										</div><!-- /Template -->
										<h4 class="header blue bolder smaller">Personajes Actuales</h4>
										<div class="form-group"><!-- Field actual person -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-actual-character">Personajes actuales</label>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<select multiple="multiple" size="10" name="duallistbox_actualcharacter[]" id="actual-character">
<?php
	$actualCharacters = $programRelations;
	$actualIdCharacter = array();
	foreach ($actualCharacters as $key => $value) {
		if($value['personajeActual'] == 1)
			array_push($actualIdCharacter, $value['personaId'] . '|' . $value['personajeId']);
	}
	$list = json_decode($this->personajeList, true);
	$actualCharactersList = $programaGetRelations['actualCharactersList'];
	foreach ($list as $key => $value) {
		if(in_array($value['personaId'] . '|' . $value['personajeId'], $actualIdCharacter, true))
			echo '													<option value="' . $value['personaId'] . '|' . $value['personajeId'] . '" title="' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '" selected="selected">' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '</option>'.PHP_EOL;
		else
			echo '													<option value="' . $value['personaId'] . '|' . $value['personajeId'] . '" title="' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '">' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '</option>'.PHP_EOL;
		
	}
?>
												</select>
											</div>
										</div>
										<h4 class="header blue bolder smaller">Personajes Relacionados</h4>
										<div class="form-group"><!-- Field actual person -->
											<label class="col-xs-12 col-sm-12 control-label center" for="form-field-related-character">Personajes relacionados</label>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<select multiple="multiple" size="10" name="duallistbox_relatedcharacter[]" id="related-character">
<?php
	$relatedCharacters = $programRelations;
	$relatedIdCharacter = array();
	foreach ($relatedCharacters as $key => $value)
		array_push($relatedIdCharacter, $value['personajeId']);
	$list = json_decode($this->personajeList, true);
	foreach ($list as $key => $value) {
		if(in_array($value['personaId'], $relatedIdCharacter, true))
			echo '													<option value="' . $value['personaId'] . '|' . $value['personajeId'] . '" title="' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '" selected="selected">' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '</option>'.PHP_EOL;
		else
			echo '													<option value="' . $value['personaId'] . '|' . $value['personajeId'] . '" title="' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '">' . $value['personaNombre'] . ' - ' . $value['personajeNombre'] . '</option>'.PHP_EOL;
		
	}
?>
												</select>
											</div>
										</div>
										<h4 class="header blue bolder smaller">Información complementaria</h4>
										<div class="form-group"><!-- Field keywords -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-keywords">Keywords</label>
											<div id="field-keywords" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="width-100">
													<input type="text" placeholder="Keywords separados por coma" id="form-field-keywords" name="keywords" value="<?php echo $data[0]['programaKeywords']; ?>">
												</div>
											</div>
										</div><!-- /Field keywords -->
										<div class="form-group"><!-- Field news -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-news">Capítulos</label>
											<div id="field-chapters" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="chapters" placeholder="Ej.: http://www.televisa.com/programa/capitulos/" class="col-xs-12 col-sm-12" id="form-field-chapters" value="<?php echo $data[0]['programaUrlCapitulosCompletos']; ?>">
											</div>
										</div><!-- /Field news -->
										<div class="form-group"><!-- Field news -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-news">Noticias</label>
											<div id="field-news" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="news" placeholder="Ej.: http://www.televisa.com/programa/noticias/" class="col-xs-12 col-sm-12" id="form-field-news" value="<?php echo $data[0]['programaUrlNoticias']; ?>">
											</div>
										</div><!-- /Field news -->
										<div class="form-group"><!-- Field photos -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-photos">Fotos</label>
											<div id="field-photos" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="photos" placeholder="Ej.: http://www.televisa.com/programa/imagenes/" class="col-xs-12 col-sm-12" id="form-field-photos" value="<?php echo $data[0]['programaUrlFotos']; ?>">
											</div>
										</div><!-- /Field photos -->
										<div class="form-group"><!-- Field videos -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-videos">Videos</label>
											<div id="field-videos" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<input type="text" name="videos" placeholder="Ej.: http://www.televisa.com/programa/videos/" class="col-xs-12 col-sm-12" id="form-field-videos"  value="<?php echo $data[0]['programaUrlVideos']; ?>">
											</div>
										</div><!-- /Field videos -->
										<h4 class="header blue bolder smaller">Redes sociales</h4>
										<div class="form-group"><!-- Field facebook -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-facebook">Facebook</label>
											<div id="field-facebook" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="facebook" placeholder="Ej.: https://www.facebook.com/programa" class="col-xs-12 col-sm-12" id="form-field-facebook" value="<?php echo $data[0]['programaUrlFacebook']; ?>">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-facebook facebook"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field facebook -->
										<div class="form-group"><!-- Field twitter -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-twitter">Twitter</label>
											<div id="field-twitter" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="twitter" placeholder="Ej.: https://twitter.com/programa" class="col-xs-12 col-sm-12" id="form-field-twitter" value="<?php echo $data[0]['programaUrlTwitter']; ?>">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-twitter twitter"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field twitter -->
										<div class="form-group"><!-- Field youtube -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-youtube">YouTube</label>
											<div id="field-youtube" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="youtube" placeholder="Ej.: https://www.youtube.com/channel/pr0gr4m4" class="col-xs-12 col-sm-12" id="form-field-youtube" value="<?php echo $data[0]['programaUrlYoutube']; ?>">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-youtube youtube"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field youtube -->
										<div class="form-group"><!-- Field instagram -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-instagram">Instagram</label>
											<div id="field-instagram" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="instagram" placeholder="Ej.: https://www.instagram.com/programa" class="col-xs-12 col-sm-12" id="form-field-instagram" value="<?php echo $data[0]['programaUrlInstagram']; ?>">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-instagram instagram"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field instagram -->
										<div class="form-group"><!-- Field pinterest -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-pinterest">Pinterest</label>
											<div id="field-pinterest" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="pinterest" placeholder="Ej.: https://pinterest.com/programa" class="col-xs-12 col-sm-12" id="form-field-pinterest" value="<?php echo $data[0]['programaUrlPinterest']; ?>">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-pinterest pinterest"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field pinterest -->
										<div class="form-group"><!-- Field google plus -->
											<label class="col-xs-12 col-sm-4 control-label no-padding-right" for="form-field-gplus">Google Plus</label>
											<div id="field-gplus" class="col-xs-12 col-sm-4 col-md-8 col-lg-4">
												<div class="input-group">
													<input type="text" name="gplus" placeholder="Ej.: https://plus.google.com/1234567890" class="col-xs-12 col-sm-12" id="form-field-gplus"  value="<?php echo $data[0]['programaUrlGplus']; ?>">
													<span class="input-group-addon sn">	
														<i class="ace-icon fa fa-google-plus gplus"></i>
													</span>
												</div>
											</div>
										</div><!-- /Field google plus -->										
										<div class="clearfix form-actions">
											<!-- The default publishing status -->
											<input type="hidden" name="status" value="unpublished">
											<div class="text-center col-xs-12 col-sm-12">
												<button type="submit" name="save" id="save" class="btn btn-info">
													<i class="ace-icon fa fa-floppy-o bigger-110"></i>
													Guardar
												</button>
												&nbsp;&nbsp;&nbsp;
												<button type="submit" name="publish" id="publish" class="btn btn-success">
													<i class="ace-icon fa fa-cloud-upload bigger-110"></i>
													Guardar y publicar
												</button>
												&nbsp;&nbsp;&nbsp;
												<button type="reset" class="btn">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
										</div><!-- /.form-actions -->
									</div><!-- /.col-sm-offset-1 -->
									<div id="preview-template" class="hide"><!-- Dropzone template -->
										<div class="dz-preview dz-file-preview">
											<div class="dz-image">
												<img data-dz-thumbnail="" />
											</div>
											<div class="dz-details">
												<div class="dz-size">
													<span data-dz-size=""></span>
												</div>
												<div class="dz-filename">
													<span data-dz-name=""></span>
												</div>
											</div>
											<div class="dz-progress">
												<span class="dz-upload" data-dz-uploadprogress=""></span>
											</div>
											<div class="dz-error-message">
												<span data-dz-errormessage=""></span>
											</div>
											<div class="dz-success-mark">
												<span class="fa-stack fa-lg bigger-150">
													<i class="fa fa-circle fa-stack-2x white"></i>
													<i class="fa fa-check fa-stack-1x fa-inverse green"></i>
												</span>
											</div>
											<div class="dz-error-mark">
												<span class="fa-stack fa-lg bigger-150">
													<i class="fa fa-circle fa-stack-2x white"></i>
													<i class="fa fa-remove fa-stack-1x fa-inverse red"></i>
												</span>
											</div>
										</div>
									</div><!-- /Dropzone template -->
								</form>
							</div><!-- /.persons-new -->
							<script>
								$(function() {
									var Home = $('body').attr('data-home-url'),
										program = $('.breadcrumb li:last-child').text(),
										pen = $('.breadcrumb li:last').prev('li'),
										linkPen = $('a', pen).attr('href'),
										newLinkPen =  linkPen + program + '/',
										pname = $('.p-name').text();
									$('a', pen).prop('href', newLinkPen);
									$('.breadcrumb li:last-child').text(pname);
									// Form
									var form = $('#program'),
										Action = form.attr('action'),
									// Time pair functionality
										time = $('.timepair .time').timepicker({
    									'showDuration': true,
										'timeFormat': 'g:i a'
    								}),
									// Time pairing
										timepair = $('.timepair').datepair(),
									// Startdate functionality
										startdate = $('#field-start-date').datepicker({
										autoclose: true,
										language: 'es',
										clearBtn: true,
										format: 'dd-mm-yyyy',
										endDate: '0d'
									}).children('[name="startdate"]').mask('99-99-9999'),
									// Enddate functionality
										deathday = $('#field-end-date').datepicker({
										autoclose: true,
										language: 'es',
										clearBtn: true,
										format: 'dd-mm-yyyy',
										endDate: '0d'
									}).children('[name="enddate"]').mask('99-99-9999');
									// Seasons funtionality
										seasons = $('[name="season"]').jStepper();
									// Image functionality
									Dropzone.autoDiscover = false;
									var dropzone = $('#dropzone').dropzone({
										url: Home + '<?php echo IMG_PROGRAM_UPLOAD_HANDLER; ?>',
										maxFilesize: '<?php echo IMG_MAX_UPLOAD_SIZE; ?>',
										addRemoveLinks: true,
										acceptedFiles: 'image/*',
										thumbnailWidth: '260',
										thumbnailHeight: '260',
										dictInvalidFileType: 'No es un formato de imagen v\u00E1lido',
										dictFileTooBig: 'La imagen es demasiado grande. <?php echo IMG_MAX_UPLOAD_SIZE; ?> MB m\u00E1ximo',
										dictResponseError: 'Hubo un error en el servidor',
										dictRemoveFile: 'Remover',
										dictDefaultMessage: '<span class="smaller-80 bolder">Si no tienes la direcci&oacuten de la imagen, puedes subir una<br /> \<span class="smaller-70 grey">Arrastra el archivo o haz click aqu\u00ed</span><br />\<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
										accept: function(file, done) {
											done();
										},
										success: function(file, response) {
											response = JSON.parse(response);
											if(response.response === 'error') {
												var node, _i, _len, _ref, _results;
												var message = response.serversays;
												file.previewElement.classList.add('dz-error');
												_ref = file.previewElement.querySelectorAll('[data-dz-errormessage]');
												_results = [];
												for (_i = 0, _len = _ref.length; _i < _len; _i++) {
												  node = _ref[_i];
												  _results.push(node.textContent = message);
												}
												return _results;
											}
											else {
												$('#form-field-image-url').val(response.image_url).prop('readonly', true).css({'background-color' :  '#C0C0C0'});
												return file.previewElement.classList.add('dz-success');
											}
										},
										error: function (file, message) {
											var node, _i, _len, _ref, _results;
											if (file.previewElement) {
											  file.previewElement.classList.add('dz-error');
											  if (typeof message !== 'String' && message.error) {
												message = message.error;
											  }
											  _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]');
											  _results = [];
											  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
												node = _ref[_i];
												_results.push(node.textContent = message);
											  }
											  return _results;
											}
										},
										removedfile: function(file) {
											$('#form-field-image-url').val('').prop('readonly', false).css({'background-color' :  ''});
											var _ref;
											return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
										},
										init: function () {
											<?php
												if($data[0]['programaThumbnail'] !== '') {
											?>$('#form-field-image-url').prop('readonly', true).css({'background-color' :  '#C0C0C0'});
											<?php
												if($this->isImage !== false) {
													$imgObjects = json_decode($this->getImageInfo);
											?>var mockFile = { name: '<?php echo $imgObjects -> {'name'}; ?>', size: <?php echo $imgObjects -> {'length'}; ?>, type: '<?php echo $imgObjects -> {'type'}; ?>'}; 
											this.emit('addedfile', mockFile);
											this.emit('thumbnail', mockFile, '<?php echo $data[0]['programaThumbnail']; ?>');
											<?php
												}
												else {
											?>var mockFile = { name: '<?php echo basename($data[0]['programaThumbnail']);?>', size: 0}; 
											this.emit('addedfile', mockFile);
											this.emit('error', mockFile, 'No existe la imagen o no est\u00e1 disponible en este momento');
											$('#dropzone').addClass('dz-error');
											this.options.thumbnail.call(this, '', '<?php echo $data[0]['programaThumbnail']; ?>');
											<?php
												}
												}
											?>
										}
									}),
									// Summernote
										summernote = $('#complete').summernote({
										height: 200,
										tabsize: 2,
										lang: 'es-ES',
										placeholder: $('#complete').attr('placeholder'),
										toolbar: [
											['style', ['style']],
											['font', ['bold', 'italic', 'underline', 'clear']],
											['color', ['color']],
											['para', ['ul', 'ol', 'paragraph']],
											['height', ['height']],
											['table', ['table']],
											['insert', ['link', 'picture', 'hr', 'video']],
											['view', ['fullscreen', 'codeview']],
											['help', ['help']]
										 ]
									  }),
									// Dualist actual person
										actualPerson = $('select[name="duallistbox_actualperson[]"]').bootstrapDualListbox({
										filterTextClear: 'Mostrar todos',filterPlaceHolder: 'Filtrar',
										moveSelectedLabel: 'Mover seleccionados',
										moveAllLabel: 'Mover todos',
										removeSelectedLabel: 'Remover seleccionados',
										removeAllLabel: 'Remover todos',
										infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
										infoText: 'Mostrando {0}',
										infoTextEmpty: 'Vac\u00eda'
									}),
									// Dualist related person
										relatedPerson = $('select[name="duallistbox_relatedperson[]"]').bootstrapDualListbox({
										filterTextClear: 'Mostrar todos',filterPlaceHolder: 'Filtrar',
										moveSelectedLabel: 'Mover seleccionados',
										moveAllLabel: 'Mover todos',
										removeSelectedLabel: 'Remover seleccionados',
										removeAllLabel: 'Remover todos',
										infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
										infoText: 'Mostrando {0}',
										infoTextEmpty: 'Vac\u00eda'
									}),
								// Add new actual person
									addNewActualPerson = $('.addNewActualPerson').click(function() {
										// Add new related person click handler
										var $template = $('#new-actual-person-template'),
											$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('actual-person').insertBefore($template).hide().slideDown(1000);
									}),
								// Remove new actual person
									removeNewActualPerson = form.on('click', '.removeNewActualPerson', function() {
										// Remove new actual person click handler
										var $row = $(this).parents('.actual-person');
										// Remove element containing the field
										$row.slideUp(1000, function() {
											$(this).remove();
										});
									}),
								// Add new related person
									addNewRelatedPerson = $('.addNewRelatedPerson').click(function() {
										// Add new related person click handler
										var $template = $('#new-related-person-template'),
											$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('related-person').insertBefore($template).hide().slideDown(1000);
									}),
								// Remove new related person
									removeNewRelatedPerson = form.on('click', '.removeNewRelatedPerson', function() {
										// Remove new related person click handler
										var $row = $(this).parents('.related-person');
										// Remove element containing the field
										$row.slideUp(1000, function() {
											$(this).remove();
										});
									}),
								// Dualist actual character
										actualCharacter = $('select[name="duallistbox_actualcharacter[]"]').bootstrapDualListbox({
										filterTextClear: 'Mostrar todos',
										filterPlaceHolder: 'Filtrar',
										moveSelectedLabel: 'Mover seleccionados',
										moveAllLabel: 'Mover todos',
										removeSelectedLabel: 'Remover seleccionados',
										removeAllLabel: 'Remover todos',
										infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
										infoText: 'Mostrando {0}',
										infoTextEmpty: 'Vac\u00eda',
										selectorMinimalHeight: 270
									}),
								// Dualist related character
										relatedCharacter = $('select[name="duallistbox_relatedcharacter[]"]').bootstrapDualListbox({
										filterTextClear: 'Mostrar todos',filterPlaceHolder: 'Filtrar',
										moveSelectedLabel: 'Mover seleccionados',
										moveAllLabel: 'Mover todos',
										removeSelectedLabel: 'Remover seleccionados',
										removeAllLabel: 'Remover todos',
										infoTextFiltered: '<span class="label label-purple label-lg">Filtrado</span>',
										infoText: 'Mostrando {0}',
										infoTextEmpty: 'Vac\u00eda',
										selectorMinimalHeight: 270
									});
									relatedContainer = relatedPerson.bootstrapDualListbox('getContainer'),
									relatedContainer.find('.btn').addClass('btn-white btn-info btn-bold'),
								// Add new actual character
									addNewActualCharacter = $('.addNewActualCharacter').click(function() {
										// Add new related character click handler
										var $template = $('#new-actual-character-template'),
											$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('actual-character').insertBefore($template).hide().slideDown(1000);
									}),
								// Remove new actual character
									removeNewActualCharacter = form.on('click', '.removeNewActualCharacter', function() {
										// Remove new actual character click handler
										var $row = $(this).parents('.actual-character');
										// Remove element containing the field
										$row.slideUp(1000, function() {
											$(this).remove();
										});
									}),
								// Add new related character
									addNewRelatedCharacter = $('.addNewRelatedCharacter').click(function() {
										// Add new related character click handler
										var $template = $('#new-related-character-template'),
											$clone = $template.clone().removeClass('hide').removeAttr('id').addClass('related-character').insertBefore($template).hide().slideDown(1000);
									}),
								// Remove new related character
									removeNewRelatedCharacter = form.on('click', '.removeNewRelatedCharacter', function() {
										// Remove new related character click handler
										var $row = $(this).parents('.related-character');
										// Remove element containing the field
										$row.slideUp(1000, function() {
											$(this).remove();
										});
									}),
								// Keywords functionality
										keywords = $('#form-field-keywords').tag({
										placeholder: $('#form-field-keywords').attr('placeholder')
									}),
								// Award years functionality
										awardYear = $('.award-year').tag({
											placeholder: $('.award-year').attr('placeholder'),
											onChange: function(elem, elem_tags) {
												console.log('d');
											}
									}).parent().find('input:last').mask('9999,'),
								$('#field-award-year div.width-100 div.tags').change(function(event) {
										$(this).find('.tag').each(function() {
											$('.tag:contains(\'____\')').remove();
										})
									});
								// Update award row
								var updateAward = function() {
									var $row = $('.award-result-row')
									$row.each(function(index) {
										var $input = $(this).find('input');
										$input.each(function () {
											var $newInputName = $(this).prop('name').substring(0, $(this).prop('name').indexOf('['));
											$(this).prop('name', $newInputName + '[' + index + ']');
										});
									});
								},
								// Remove fact
									removeFact = form.on('click', '.removeFact', function() {
										// Remove fact click handler
										var $row = $(this).parents('.fact');
										// Remove element containing the field
										$row.slideUp(1000, function() {
											$(this).remove();
										});
									}),
									// Validation Start
									// Validations vars
										NameValCount = 0,
										BranchValCount = 0,
										StartdateValCount = 0,
										EnddateValCount = 0,
										ImageURLValCount = 0,
										ImageCheckURLValCount = 0,
										URLValCount = 0,
										CompleteValCount = 0,
										NewActualPersonCheckCount = 0,
										NewRelatedPersonCheckCount = 0,
										KeywordsValCount = 0,
										ChaptersValCount = 0,
										NewsValCount = 0,
										PhotosValCount = 0,
										VideosValCount = 0,
										FacebookValCount = 0,
										TwitterValCount = 0,
										YouTubeValCount = 0,
										InstagramValCount = 0,
										PinterestValCount = 0,
										PlusValCount = 0,
										validation = form.formValidation({
											framework: 'bootstrap',
											excluded: ':disabled',
											icon: {
												valid: 'glyphicon glyphicon-ok',
												invalid: 'glyphicon glyphicon-remove',
												validating: 'glyphicon glyphicon-refresh'
											},
											fields: {
												name: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(NameValCount === 0) {
																	$.noty.setText(notifications('error', 100).options.id, '\u00A1Oh no! El nombre del programa est\u00E1 vac\u00edo');
																	NameValCount++;
																}
															},
															onSuccess: function(e, data) {
																NameValCount = 0;
																$.noty.close(100);
															}
														}
													}
												},
												branch: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(BranchValCount === 0) {
																	$.noty.setText(notifications('error', 200).options.id, '\u00A1Oh no! Necesitas seleccionar el canal al que pertenece');
																	BranchValCount++;
																}
															},
															onSuccess: function(e, data) {
																BranchValCount = 0;
																$.noty.close(200);
															}
														}
													}
												},
												startdate: {
													enabled: false,
													validators: {
														date: {
															format: 'DD-MM-YYYY',
															message: 'error',
															onError: function(e, data) {
																if(StartdateValCount === 0) {
																	$.noty.setText(notifications('error', 300).options.id, '\u00A1Oh no! La fecha de inicio est\u00e1 mal escrita');
																	StartdateValCount++;
																}							
															},
															onSuccess: function(e, data) {
																StartdateValCount = 0;
																$.noty.close(300);
															}
														}
													}
												},
												enddate: {
													enabled: false,
													validators: {
														date: {
															format: 'DD-MM-YYYY',
															message: 'error',
															onError: function(e, data) {
																if(EnddateValCount === 0) {
																	$.noty.setText(notifications('error', 400).options.id, '\u00A1Oh no! La fecha de fin est\u00e1 mal escrita');
																	EnddateValCount++;
																}							
															},
															onSuccess: function(e, data) {
																EnddateValCount = 0;
																$.noty.close(400);
															}
														}
													}
												},
												'image-url': {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(ImageURLValCount === 0) {
																	$.noty.setText(notifications('error', 500).options.id, '\u00A1Oh no! La direcci\u00f3n de la  imagen est\u00E1 vac\u00eda');
																	ImageURLValCount++;
																}
															},
															onSuccess: function(e, data) {
																ImageURLValCount = 0;
																$.noty.close(600);
															}
														},
														regexp: {
															enabled: false,
															message: 'error',
															regexp: /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[\/?#]\S*)?(?:jpe?g|gif|png)$/i,
															onError: function(e, data) {
																if(ImageCheckURLValCount === 0) {
																	$.noty.setText(notifications('error', 501).options.id, '\u00A1Oh no! La direcci\u00f3n de la imagen parece estar mal escrita (s\u00f3lo acepta terminaciones .jpg, .png o .gif)');
																	ImageCheckURLValCount++;
																}							
															},
															onSuccess: function(e, data) {
																ImageCheckURLValCount = 0;
																$.noty.close(501);
															}
														}
													}
												},
												url: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(URLValCount === 0) {
																	$.noty.setText(notifications('error', 600).options.id, '\u00A1Oh no! La direcci\u00f3n del programa no es una direcci\u00f3n v\u00e1lida');
																	URLValCount++;
																}
															},
															onSuccess: function(e, data) {
																URLValCount = 0;
																$.noty.close(600);
															}
														}
													}
												},
												complete: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(CompleteValCount === 0) {
																	$.noty.setText(notifications('error', 800).options.id, '\u00A1Oh no! El contenido completo de la descripci\u00f3n del programa est\u00E1 vac\u00edo');
																	CompleteValCount++;
																}
															},
															onSuccess: function(e, data) {
																CompleteValCount = 0;
																$.noty.close(700);
															}
														}
													}
												},
												'new-actual-person-complete-name[]': {
													enabled: false,
													excluded: false,
													validators: {
														remote: {
															url: Home + 'persona/checkperson',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar a la persona en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																	});
																else {
																	if(NewActualPersonCheckCount === 0) {
																		$.noty.setText(notifications('error', 800).options.id, '\u00A1Oh no! El nombre de esa persona ya est\u00E1 registrado');
																		NewActualPersonCheckCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																NewActualPersonCheckCount = 0;
																$.noty.close(800);
															}
														}
													}
												},
												'new-related-person-complete-name[]': {
													enabled: false,
													excluded: false,
													validators: {
														remote: {
															url: Home + 'persona/checkperson',
															type: 'POST',
															message: 'error',
															onError: function(e, data) {
																if(data.result.dberror)
																	$.gritter.add({
																		// (string | mandatory) the heading of the notification
																		title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no!',
																		// (string | mandatory) the text inside the notification
																		text: '<i class="ace-icon fa fa fa-database"></i> Tenemos un error al momento de comprobar a la persona en la base de datos.<br /><br />Por favor, intenta de nuevo en un momento...',
																		time: 8000,
																		class_name: 'gritter-error'
																	});
																else {
																	if(NewRelatedPersonCheckCount === 0) {
																		$.noty.setText(notifications('error', 900).options.id, '\u00A1Oh no! El nombre de esa persona ya est\u00E1 registrado');
																		NewRelatedPersonCheckCount++;
																	}
																}
															},
															onSuccess: function(e, data) {
																NewRelatedPersonCheckCount = 0;
																$.noty.close(900);
															}
														}
													}
												},
												keywords: {
													validators: {
														notEmpty: {
															message: 'error',
															onError: function(e, data) {
																if(KeywordsValCount === 0) {
																	$.noty.setText(notifications('error', 1300).options.id, '\u00A1Oh no! Los keywords no pueden quedar vac\u00edos');
																	KeywordsValCount++;
																}
																$('#field-keywords div.width-100 div.tags').css({'border-color': '#f2a696', 'box-shadow': 'none', 'color': '#d68273'});
															},
															onSuccess: function(e, data) {
																KeywordsValCount = 0;
																$.noty.close(1300);
																$('#field-keywords div.width-100 div.tags').css({'border-color': '#9cc573', 'box-shadow': 'none', 'color': '#8bad4c'});
															}
														}
													}
												},
												chapters: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(ChaptersValCount === 0) {
																	$.noty.setText(notifications('error', 1000).options.id, '\u00A1Oh no! La direcci\u00f3n de los cap\u00edtulos no es v\u00e1lida');
																	ChaptersValCount++;
																}
															},
															onSuccess: function(e, data) {
																ChaptersValCount = 0;
																$.noty.close(1000);
															}
														}
													}
												},
												news: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(NewsValCount === 0) {
																	$.noty.setText(notifications('error', 1400).options.id, '\u00A1Oh no! La direcci\u00f3n de las noticias no es v\u00e1lida');
																	NewsValCount++;
																}
															},
															onSuccess: function(e, data) {
																NewsValCount = 0;
																$.noty.close(1400);
															}
														}
													}
												},
												photos: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(PhotosValCount === 0) {
																	$.noty.setText(notifications('error', 1500).options.id, '\u00A1Oh no! La direcci\u00f3n de las fotos no es v\u00e1lida');
																	PhotosValCount++;
																}
															},
															onSuccess: function(e, data) {
																PhotosValCount = 0;
																$.noty.close(1500);
															}
														}
													}
												},
												videos: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(VideosValCount === 0) {
																	$.noty.setText(notifications('error', 1600).options.id, '\u00A1Oh no! La direcci\u00f3n de las videos no es v\u00e1lida');
																	VideosValCount++;
																}
															},
															onSuccess: function(e, data) {
																VideosValCount = 0;
																$.noty.close(1600);
															}
														}
													}
												},
												facebook: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(FacebookValCount === 0) {
																	$.noty.setText(notifications('error', 1700).options.id, '\u00A1Oh no! La direcci\u00f3n de Facebook no es v\u00e1lida');
																	FacebookValCount++;
																}
															},
															onSuccess: function(e, data) {
																FacebookValCount = 0;
																$.noty.close(1700);
															}
														}
													}
												},
												twitter: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(TwitterValCount === 0) {
																	$.noty.setText(notifications('error', 1800).options.id, '\u00A1Oh no! La direcci\u00f3n de Twitter no es v\u00e1lida');
																	TwitterValCount++;
																}
															},
															onSuccess: function(e, data) {
																TwitterValCount = 0;
																$.noty.close(1800);
															}
														}
													}
												},
												youtube: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(YouTubeValCount === 0) {
																	$.noty.setText(notifications('error', 1900).options.id, '\u00A1Oh no! La direcci\u00f3n de YouTube no es v\u00e1lida');
																	YouTubeValCount++;
																}
															},
															onSuccess: function(e, data) {
																YouTubeValCount = 0;
																$.noty.close(1900);
															}
														}
													}
												},
												instagram: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(InstagramValCount === 0) {
																	$.noty.setText(notifications('error', 2000).options.id, '\u00A1Oh no! La direcci\u00f3n de Instagram no es v\u00e1lida');
																	InstagramValCount++;
																}
															},
															onSuccess: function(e, data) {
																InstagramValCount = 0;
																$.noty.close(2000);
															}
														}
													}
												},
												pinterest: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(PinterestValCount === 0) {
																	$.noty.setText(notifications('error', 2100).options.id, '\u00A1Oh no! La direcci\u00f3n de Pinterest no es v\u00e1lida');
																	PinterestValCount++;
																}
															},
															onSuccess: function(e, data) {
																PinterestValCount = 0;
																$.noty.close(2100);
															}
														}
													}
												},
												gplus: {
													enabled: false,
													validators: {
														uri: {
															message: 'error',
															onError: function(e, data) {
																if(PlusValCount === 0) {
																	$.noty.setText(notifications('error', 2200).options.id, '\u00A1Oh no! La direcci\u00f3n de Google Plus no es v\u00e1lida');
																	PlusValCount++;
																}
															},
															onSuccess: function(e, data) {
																PlusValCount = 0;
																$.noty.close(2200);
															}
														}
													}
												}
											}
										}).on('err.field.fv', function(e, data) {
											// $(e.target)  --> The field element
											// data.fv      --> The FormValidation instance
											// data.field   --> The field name
											// data.element --> The field element
											if (data.fv.getSubmitButton()) {
												data.fv.disableSubmitButtons(false);
											}
											// Hide the messages
											data.element
												.data('fv.messages')
												.find('.help-block[data-fv-for="' + data.field + '"]').hide();
										}).on('success.field.fv', function(e, data) {
											if (data.fv.getSubmitButton()) {
												data.fv.disableSubmitButtons(false);
											}
										}).on('keydown', '[name="name"]', function() {
											form.formValidation('enableFieldValidators', 'name', false, 'remote');
										}).on('blur', '[name="name"]', function() {
											form.formValidation('enableFieldValidators', 'name', true, 'remote');
											form.formValidation('revalidateField', 'name');
										}).on('change', '[name="startdate"]', function() {
											form.formValidation('enableFieldValidators', 'startdate', true);
											form.formValidation('revalidateField', 'startdate');
										}).on('change', '[name="enddate"]', function() {
											form.formValidation('enableFieldValidators', 'enddate', true);
											form.formValidation('revalidateField', 'enddate');
										}).on('keydown', '[name="image-url"]', function() {
											form.formValidation('enableFieldValidators', 'image-url', false, 'regexp');
										}).on('blur', '[name="image-url"]', function() {
											form.formValidation('enableFieldValidators', 'image-url', true, 'regexp');
											form.formValidation('revalidateField', 'image-url');
										}).on('keydown', '[name="url"]', function() {
											form.formValidation('enableFieldValidators', 'url', false);
										}).on('blur', '[name="url"]', function() {
											form.formValidation('enableFieldValidators', 'url', true);
											form.formValidation('revalidateField', 'url');
										}).on('keydown', '[name="new-actual-person-name[]"], [name="new-actual-person-surname[]"], [name="new-actual-person-maidenname[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-actual-person-complete-name[]', false);
										}).on('blur', '[name="new-actual-person-name[]"], [name="new-actual-person-surname[]"], [name="new-actual-person-maidenname[]"]', function() {
											var n = $(this).parents('.new-actual-person-group').find('[name="new-actual-person-name[]"]').val(),
												s = $(this).parents('.new-actual-person-group').find('[name="new-actual-person-surname[]"]').val(),
												m = $(this).parents('.new-actual-person-group').find('[name="new-actual-person-maidenname[]"]').val();
												$(this).parent().parent().parent().find('[name="new-actual-person-complete-name[]"]').val([n, s, m].join(' '));
												form.formValidation('enableFieldValidators', 'new-actual-person-complete-name[]', true);
												// Revalidate it
												form.formValidation('revalidateField', 'new-actual-person-complete-name[]');
										}).on('click', '.addNewActualPerson', function() {
											var $actualPersonName = form.find('.actual-person').find('[name="new-actual-person-complete-name[]"]');
											// Add new field
											form.formValidation('addField', $actualPersonName);
										}).on('click', '.removeNewActualPerson', function() {
											var	$actualPersonName = $(this).parent().parent().parent().find('[name="new-actual-person-complete-name[]"]');
											// Remove field
											form.formValidation('removeField', $actualPersonName);
										}).on('keydown', '[name="new-related-person-name[]"], [name="new-related-person-surname[]"], [name="new-related-person-maidenname[]"]', function() {
											form.formValidation('enableFieldValidators', 'new-related-person-complete-name[]', false);
										}).on('blur', '[name="new-related-person-name[]"], [name="new-related-person-surname[]"], [name="new-related-person-maidenname[]"]', function() {
											var n = $(this).parents('.new-related-person-group').find('[name="new-related-person-name[]"]').val(),
												s = $(this).parents('.new-related-person-group').find('[name="new-related-person-surname[]"]').val(),
												m = $(this).parents('.new-related-person-group').find('[name="new-related-person-maidenname[]"]').val();
												$(this).parent().parent().parent().find('[name="new-related-person-complete-name[]"]').val([n, s, m].join(' '));
												form.formValidation('enableFieldValidators', 'new-related-person-complete-name[]', true);
												// Revalidate it
												form.formValidation('revalidateField', 'new-related-person-complete-name[]');
										}).on('click', '.addNewRelatedPerson', function() {
											var $relatedPersonName = form.find('.related-person').find('[name="new-related-person-complete-name[]"]');
											// Add new field
											form.formValidation('addField', $relatedPersonName);
										}).on('click', '.removeNewRelatedPerson', function() {
											var	$relatedPersonName = $(this).parent().parent().parent().find('[name="new-related-person-complete-name[]"]');
											// Remove field
											form.formValidation('removeField', $relatedPersonName);
										}).on('keydown', '#field-keywords input', function() {
											form.formValidation('enableFieldValidators', 'keywords', false);
										}).on('blur', '#field-keywords input', function() {
											form.formValidation('enableFieldValidators', 'keywords', true);
											form.formValidation('revalidateField', 'keywords');
										}).on('keydown', '[name="news"]', function() {
											form.formValidation('enableFieldValidators', 'news', false);
										}).on('blur', '[name="news"]', function() {
											form.formValidation('enableFieldValidators', 'news', true);
											form.formValidation('revalidateField', 'news');
										}).on('keydown', '[name="photos"]', function() {
											form.formValidation('enableFieldValidators', 'photos', false);
										}).on('blur', '[name="photos"]', function() {
											form.formValidation('enableFieldValidators', 'photos', true);
											form.formValidation('revalidateField', 'photos');
										}).on('keydown', '[name="videos"]', function() {
											form.formValidation('enableFieldValidators', 'videos', false);
										}).on('blur', '[name="videos"]', function() {
											form.formValidation('enableFieldValidators', 'videos', true);
											form.formValidation('revalidateField', 'videos');
										}).on('keydown', '[name="facebook"]', function() {
											form.formValidation('enableFieldValidators', 'facebook', false);
										}).on('blur', '[name="facebook"]', function() {
											form.formValidation('enableFieldValidators', 'facebook', true);
											form.formValidation('revalidateField', 'facebook');
										}).on('keydown', '[name="twitter"]', function() {
											form.formValidation('enableFieldValidators', 'twitter', false);
										}).on('blur', '[name="twitter"]', function() {
											form.formValidation('enableFieldValidators', 'twitter', true);
											form.formValidation('revalidateField', 'twitter');
										}).on('keydown', '[name="youtube"]', function() {
											form.formValidation('enableFieldValidators', 'youtube', false);
										}).on('blur', '[name="youtube"]', function() {
											form.formValidation('enableFieldValidators', 'youtube', true);
											form.formValidation('revalidateField', 'youtube');
										}).on('keydown', '[name="instagram"]', function() {
											form.formValidation('enableFieldValidators', 'instagram', false);
										}).on('blur', '[name="instagram"]', function() {
											form.formValidation('enableFieldValidators', 'instagram', true);
											form.formValidation('revalidateField', 'instagram');
										}).on('keydown', '[name="pinterest"]', function() {
											form.formValidation('enableFieldValidators', 'pinterest', false);
										}).on('blur', '[name="pinterest"]', function() {
											form.formValidation('enableFieldValidators', 'pinterest', true);
											form.formValidation('revalidateField', 'pinterest');
										}).on('keydown', '[name="gplus"]', function() {
											form.formValidation('enableFieldValidators', 'gplus', false);
										}).on('blur', '[name="gplus"]', function() {
											form.formValidation('enableFieldValidators', 'gplus', true);
											form.formValidation('revalidateField', 'gplus');
										}).on('success.form.fv', function(e) {
											var $form = $(e.target), // Form instance
												$button = $form.data('formValidation').getSubmitButton(), // Get the clicked button
												$statusField = $form.find('[name="status"]'); //update the "status" field before submitting the form
											switch ($button.attr('id')) {
												case 'save':
													$statusField.val('save');
													//bootbox.alert('La informaci');
													break;

												case 'publish':
													$statusField.val('publish');
													//bootbox.alert('The article will be saved as a draft');
													break;

												case 'saveButton':
												default:
													$statusField.val('unpublished');
													//bootbox.alert('The article will be saved');
													break;
											}
											// Form submit
											form.submit(function(event) {
												event.preventDefault();
												var data = {
													'name'		   	  	  	    	   : $('[name="name"]').val(),
													'branch'	   	  	  	    	   : $('[name="branch"]').val(),
													'type'	   	  	  	   	    	   : $('[name="type"]').val(),
													'start-time'			    	   : $('[name="start-time"]').val(),
													'end-time'	   	  	  	    	   : $('[name="end-time"]').val(),
													'season'				    	   : $('[name="season"]').val(),
													'startdate'	   	  	  	    	   : $('[name="startdate"]').val(),
													'enddate'	   	  	  	   		   : $('[name="enddate"]').val(),
													'birthplace'   	  	  	    	   : $('[name="birthplace"]').val(),
													'image'		   	  	  	    	   : $('[name="image-url"]').val(),
													'url'		   	  	  	    	   : $('[name="url"]').val(),
													'resume'	   	  	  	    	   : $('[name="resume"]').val(),
													'complete' 		  	  	    	   : $('[name="complete"]').val(),
													'curious-fact'			    	   : $('[name="curious-fact[]"]').val(),
													'actual-persons'	  	    	   : $('[name="duallistbox_actualperson[]"]').val(),
													'new-actual-person-name'    	   : $('[name="new-actual-person-name[]"]').val(),
													'new-actual-person-surname'		   : $('[name="new-actual-person-surname[]"]').val(),
													'new-actual-person-maidenname' 	   : $('[name="new-actual-person-maidenname[]"]').val(),
													'new-actual-person-bio-url'   	   : $('[name="new-actual-person-bio-url[]"]').val(),
													'new-actual-person-complete-name'  : $('[mname="new-actual-person-complete-name[]"]').val(),
													'related-persons'		   		   : $('[name="duallistbox_relatedperson[]"]').val(),
													'new-related-person-name' 		   : $('[name="new-related-person-name[]"]').val(),
													'new-related-person-surname' 	   : $('[name="new-related-person-surname[]"]').val(),
													'new-related-person-maidenname'    : $('[name="new-related-person-maidenname[]"]').val(),
													'new-related-person-bio-url'  	   : $('[name="new-related-person-bio-url[]"]').val(),
													'new-related-person-complete-name' : $('[mname="new-related-person-complete-name[]"]').val(),
													'actual-characters'	  	   		   : $('[name="duallistbox_actualcharacter[]"]').val(),
													'related-characters'	   		   : $('[name="duallistbox_relatedcharacter[]"]').val(),
													'keywords'						   : $('[name="keywords"]').val(),
													'chapters'						   : $('[name="chapters"]').val(),
													'news'					  		   : $('[name="news"]').val(),
													'photos'				   		   : $('[name="photos"]').val(),
													'videos'				   		   : $('[name="videos"]').val(),
													'facebook'				   		   : $('[name="facebook"]').val(),
													'twitter'				   		   : $('[name="twitter"]').val(),
													'youtube'				   		   : $('[name="youtube"]').val(),
													'instagram'				   		   : $('[name="instagram"]').val(),
													'pinterest'				   		   : $('[name="pinterest"]').val(),
													'gplus'					   		   : $('[name="gplus"]').val(),
													'status'				   		   : $statusField
												};
												data = $(this).serialize();
												$.ajax({
													type: 'POST',
													dataType: 'json',
													url: Action,
													data: data
												}).done(function(data) {
													console.log(data);
													if(data.response === 'success') {
														$.gritter.add({
															// (string | mandatory) the heading of the notification
															title: '\u00a1Listo!',
															// (string | mandatory) the text inside the notification
															text: '\u00a1Se edit\u00f3 con \u00e9xito!',
															time: 2000,
															class_name: 'gritter-success'
														});
														/*window.setTimeout(function() {
															window.location.href = Home + 'user/';
														}, 2800);*/
													}
													else {
														if(data.serversays.error[0].errorcode == 0) {
															$.gritter.add({
																// (string | mandatory) the heading of the notification
																title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! ' + data.serversays.title + '<br />',
																// (string | mandatory) the text inside the notification
																text: '<ul class="fa-ul"><li><i class="fa-li fa fa-database"></i>' + data.serversays.error[0].errormessage + '</li></ul><b>' + data.serversays.texts + '</b>',
																sticky: true,
																class_name: 'gritter-error'
															});
														}
														else {
															var html = '<ul class="fa-ul">';
															$.each(data.serversays.error, function(key){ html +=  '<li><i class="ace-icon fa fa-hand-o-right"></i> ' + data.serversays.error[key].errormessage + '</li>';});
															html += '</ul>';
															$.gritter.add({
																// (string | mandatory) the heading of the notification
																title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh vaya! ' + data.serversays.title + '<br />',
																// (string | mandatory) the text inside the notification
																text: html + data.serversays.texts,
																sticky: true,
																class_name: 'gritter-error gritter-light'
															});
														}
													}
												}).fail(function() {
													$.gritter.add({
														// (string | mandatory) the heading of the notification
														title: '<i class="ace-icon fa fa-frown-o"></i> \u00a1Oh no! Algo raro pasa aqu\u00ed<br />',
														// (string | mandatory) the text inside the notification
														text: 'La informaci\u00f3n no la puedo enviar.<br />Por favor, ay\u00fadanos a corregirlo report\u00E1ndolo.',
														sticky: true,
														class_name: 'gritter-error'
													});
												});
												return false;
											});
										});
									// Validation End
									// Tooltip placement on right or left
									function tooltip_placement(context, source) {
										var $source = $(source);
										var $parent = $source.closest('table')
										var off1 = $parent.offset();
										var w1 = $parent.width();
										var off2 = $source.offset();
										if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
										return 'left';
									}
									// Notifications
									function notifications(type, id) {
										if(typeof id !== 'undefined')	
											var n = noty({
												id			: id,
												text        : type,
												type        : type,
												dismissQueue: true,
												killer		: false,
												layout      : 'bottom',
												maxVisible  : 1
											});
										else
											var n = noty({
												text        : type,
												type        : type,
												dismissQueue: true,
												killer		: false,
												layout      : 'bottom',
												maxVisible  : 1
											});
										return n;
									}
								});
							</script>