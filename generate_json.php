<?php
	require 'config.php';
	require 'libs/Model.php';
	require 'libs/Database.php';
	require 'models/program_model.php';
	
	if(isset($argv[1], $argv[2], $argv[3])) {
		$personas_model = new Personas_Model();
		$personasJson = $personas_model->personasJson();
		$nameFile = $argv[2];
		$pathFile = $argv[3];
		$json = "<?php".PHP_EOL;
		$json .= 'header("Content-Type: application/json");'.PHP_EOL;
		$json .= 'echo isset($_GET["callback"]) . \'' . json_encode($personasJson) . '\';'.PHP_EOL;
		$file = filejson . $pathFile . "/" . $nameFile;
		file_put_contents($file, $json);
	}