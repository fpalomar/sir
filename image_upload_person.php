<?php
/**
* Image Upload Person 
*
* Este archivo se encarga del procesamiento y manejo de las im�genes de la parte de personas
*/
include 'config.php';
require 'aws/aws-autoloader.php';
use Aws\S3\S3Client;

header('Content-Type: text/plain; charset=utf-8');

/**
* image_url
* Regresa la URL completa de la imagen para guardar en la base de datos
*
* @param string $fileName nombre del archivo
* @return string $imageUrl direcci�n de la imagen
* @version original: 2014/10/13 15:53 | modified: 
*/
function image_url($fileName) {
	$imageUrl = AWS_BUCKET_URL.IMG_PERSON_UPLOAD_DIR.$fileName;
	
	return $imageUrl;
}

try {
   
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (
        !isset($_FILES['file']['error']) ||
        is_array($_FILES['file']['error'])
    ) {
        throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 101, 'serversays' => utf8_encode('Par�metros inv�lidos'))));
    }

    // Check $_FILES['file']['error'] value.
    switch ($_FILES['file']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 202, 'serversays' => utf8_encode('No se envi� la imagen'))));
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 204, 'serversays' => utf8_encode('Se excedi� el tama�o de la imagen'))));
        default:
            throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 200, 'serversays' => utf8_encode('Se produjo un error desconocido'))));
    }

    if ($_FILES['file']['size'] > IMG_MAX_UPLOAD_SIZE * 1024 * 1024) {
        throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 301, 'serversays' => utf8_encode('Se excedi� el tama�o m�ximo de la imagen'))));
    }
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if (false === $ext = array_search(
        $finfo->file($_FILES['file']['tmp_name']),
        array(
            'jpg'  => 'image/jpeg',
			'jpeg' => 'image/jpeg',
            'png'  => 'image/png',
            'gif'  => 'image/gif',
        ),
        true
    )) {
        throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 401, 'serversays' => utf8_encode('El formato de la imagen es inv�lido'))));
    }
	$s3Client = S3Client::factory(array(
		'key'    => AWS_KEY,
		'secret' => AWS_SECRET,
	));
	$move = $s3Client->putObject(array(
		'Bucket' 	  => AWS_BUCKET,
		'Key'    	  => '/'.IMG_PERSON_UPLOAD_DIR.$_FILES['file']['name'],
		'Body' 	 	  => fopen($_FILES['file']['tmp_name'], 'r'),
		'ContentType' => $_FILES['file']['type'],
		'ACL'    	  => 'public-read'
	));
    if ($move === FALSE) {
        throw new RuntimeException(json_encode(array('response' => 'error', 'responsecode' => 501, 'serversays' => utf8_encode('Se produjo un fallo al subir la imagen al bucket'))));
    }
    echo json_encode(array('response' => "success", 'serversays' => utf8_encode('Se subi� la imagen con �xito'), 'image_url' => image_url($_FILES['file']['name'])));
} catch (RuntimeException $e) {
    echo $e->getMessage();
}