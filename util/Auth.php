<?php
# Auth
class Auth
{
    
    public static function handleLogin()
    {
        Session::init();
        $logged = $_SESSION['loggedIn'];
        if ($logged == false) {
            Session::destroy();
            header('location: ' . URL . 'login/');
			exit;
        }
    }
    
	public static function onlyAdmin()
    {
		if (Session::get('rol') !== 'admin')
			header('location: ' . URL . '404.html');
	}
}