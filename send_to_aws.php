<?php
	require 'config.php';
	require 'aws/aws-autoloader.php';
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	
	if(isset($argv[1], $argv[2], $argv[3])) {
		$file = $argv[1];
		$category = $argv[2];
		$fileType = $argv[3];
		$pathParts = pathinfo($file);
		$route = $fileType."/".$category."/".$pathParts['basename'];
		if($fileType == "xml")
			$contentType = "text/xml";
		else if($fileType == "json")
			$contentType = "application/javascript";
		
		$s3Client = S3Client::factory(array(
			'key'    => AWS_KEY,
			'secret' => AWS_SECRET,
		));
		
		try {
			$result = $s3Client->putObject(array(
				'Bucket' 	  => AWS_BUCKET,
				'Key'    	  => $route,
				'Body' 	 	  => file_get_contents($file, FILE_USE_INCLUDE_PATH),
				'ContentType' => $contentType,
				'ACL'    	  => 'public-read'
			));
		}
		catch (S3Exception $e) {
		}
	}