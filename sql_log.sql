SELECT * FROM usuarioslogCREATE TABLE `usuarioslog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `seccion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `accion` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `modificacion` text COLLATE utf8_bin,
  `respuesta` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
