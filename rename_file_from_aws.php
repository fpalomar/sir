<?php
/**
* Rename file from AWS 
*
* Este archivo se encarga de renombrar un archivo del bucket de AWS. Recibe como parámetros via $_POST $argv[1] el path del archivo, $argv[2] el viejo nombre de archivo, $argv[3] el nuevo nombre del archivo y $argv[4] el mime content type del archivo
*/
require 'config.php';
require 'aws/aws-autoloader.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
	
	if(isset($argv[1], $argv[2], $argv[3], $argv[4])) {
		$path = $argv[1];
		$oldName = $argv[2];
		$newName = $argv[3];
		$mimeType = $argv[4];
		
		switch($mimeType) {
			case 'jpg':
				$mimeType = "image/jpeg";
				break;
			case 'jpeg':
				$mimeType = 'image/jpeg';
				break;
			case 'png':
				$mimeType = 'image/png';
				break;
			case 'gif':
				$mimeType = 'image/gif';
				break;
			default:
				$mimeType = $mimeType;
				break;
		}
		
		$bucket = AWS_BUCKET;
		$oldKey = $path.$oldName;
		$newKey = $path.$newName;
		
		$s3Client = S3Client::factory(array(
			'key'    => AWS_KEY,
			'secret' => AWS_SECRET
		));
		
		$s3Client->registerStreamWrapper();
		
		try {
			if(is_file("s3://{$bucket}/{$oldKey}"))
				$fileSizeOldFile = filesize("s3://{$bucket}/{$oldKey}");
			if(is_file("s3://{$bucket}/{$newKey}"))
				$fileSizeNewFile = filesize("s3://{$bucket}/{$newKey}");
		}
		catch (S3Exception $e) {
		}
		
		if (isset($fileSizeOldFile) && isset($fileSizeNewFile)) {
			if($fileSizeNewFile != $fileSizeOldFile)
				rename("s3://{$bucket}/{$oldKey}", "s3://{$bucket}/{$newKey}", stream_context_create(array(
					's3' => array(
						'MetadataDirective' => 'REPLACE',
						'ACL'               => 'public-read',
						'ContentType'       => $mimeType,
					)
				)));
		}
		else {
			rename("s3://{$bucket}/{$oldKey}", "s3://{$bucket}/{$newKey}");
		}
	}