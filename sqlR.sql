
CREATE SCHEMA IF NOT EXISTS `admin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `admin` ;

-- -----------------------------------------------------
-- Table `admin`.`programa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`programa` (
  `idprograma` INT NOT NULL AUTO_INCREMENT,
  `programaTitulo` VARCHAR(100) NULL,
  `programaUrl` VARCHAR(100) NULL,
  `programaDescrip` VARCHAR(1000) NULL,
  `programatThumbmail` VARCHAR(100) NULL,
  `programaUrlCapitulosCompletos` VARCHAR(100) NULL,
  `programaUrlNoticias` VARCHAR(45) NULL,
  `programaUrlFotos` VARCHAR(45) NULL,
  `programaUrlVideos` VARCHAR(100) NULL,
  `programaUrlFacebook` VARCHAR(100) NULL,
  `programaUrlTwitter` VARCHAR(100) NULL,
  `programaUrlYoutube` VARCHAR(100) NULL,
  `programaUrlPinterest` VARCHAR(100) NULL,
  `programaUrlGplus` VARCHAR(100) NULL,
  `programaContent` VARCHAR(100) NULL,
  `programaConsultRelacionada` VARCHAR(45) NULL,
  `programaFechaIngreso` DATETIME NULL,
  PRIMARY KEY (`idprograma`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`persona` (
  `idPersona` INT NOT NULL AUTO_INCREMENT,
  `personaNom` VARCHAR(100) NULL,
  `personaUrlBio` VARCHAR(100) NULL,
  `personaResumenBio` VARCHAR(120) NULL,
  `personaUrlThumb` VARCHAR(100) NULL,
  `personaNomProgramaAct` VARCHAR(45) NULL,
  `personaUrlProgramActual` VARCHAR(100) NULL,
  `personaUrlNoticias` VARCHAR(100) NULL,
  `personaUrlFotos` VARCHAR(100) NULL,
  `personaUrlVideos` VARCHAR(100) NULL,
  `personaUrlFacebook` VARCHAR(100) NULL,
  `personaUrlTwitter` VARCHAR(100) NULL,
  `personaUrlYoutube` VARCHAR(100) NULL,
  `personaUrlPinterest` VARCHAR(100) NULL,
  `personaUrlGplus` VARCHAR(100) NULL,
  `personaContent` VARCHAR(1000) NULL,
  `personaConsultRelacionada` VARCHAR(100) NULL,
  `personaFechaIngreso` DATETIME NULL,
  PRIMARY KEY (`idPersona`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`usuariosAdm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`usuariosAdm` (
  `idusuariosAdm` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NULL,
  `rol` VARCHAR(45) NULL,
  `pass` VARCHAR(150) NULL,
  PRIMARY KEY (`idusuariosAdm`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `admin`.`branchAdm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`branchAdm` (
  `idBranch` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(1000) NULL,
  `url` TEXT NOT NULL,
  `imagen` TEXT NOT NULL,
  PRIMARY KEY (`idBranch`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`ProgramaPersona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`ProgramaPersona` (
  `programa` INT NULL,
  `persona` INT NULL,
  `idProgramaEquipo` INT NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`Equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`Equipo` (
    `idEquipo` INT NOT NULL AUTO_INCREMENT,
    `nombre` varchar(100) NOT NULL,
    `pais` VARCHAR(45) NULL,
    `aniofundacion` YEAR NULL,
    `campoCancha` VARCHAR(150) NULL,
    `liga` VARCHAR(150) NULL,
    `ubicacion` VARCHAR(150) NULL,
    `division` VARCHAR(150) NULL,
    PRIMARY KEY (`idEquipo`)
)  ENGINE=InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`personaEquipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`personaEquipo` (
  `persona` INT NULL,
  `equipo` INT NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`profesion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin`.`profesion` (
  `idprofesion` INT NOT NULL,
  `nombre` VARCHAR(100) NULL,
  `programa_idprograma` INT NOT NULL,
  PRIMARY KEY (`idprofesion`),
  INDEX `fk_profesion_programa_idx` (`programa_idprograma` ASC),
  CONSTRAINT `fk_profesion_programa`
    FOREIGN KEY (`programa_idprograma`)
    REFERENCES `admin`.`programa` (`idprograma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE USER 'admin' IDENTIFIED BY 'qwerty789';

GRANT ALL ON `admin`.* TO 'admin';
INSERT INTO usuariosAdm (idusuariosAdm, login, rol, pass) VALUES (null,'arlen','admin','edc8a4e6e3437f47d2395034a3ed74bb3cfe8f56786bb5fe17f380b961bf6672');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
