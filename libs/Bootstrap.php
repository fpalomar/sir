<?php
/**
 * 
 * Class Bootstrap
 */

class Bootstrap {

    private $_url = null;
    private $_controller = null;
    
    private $_controllerPath = 'controllers/'; 
    private $_modelPath = 'models/'; 
    private $_errorFile = 'error.php';
    private $_defaultFile = 'login.php';
    
    /**
     * Here starts the init!
     *
     * @return void
     */
    public function init()
    {
        // gets URL query
        $this->_getUrl();
        // no controller was called, so loads index page
        if (empty($this->_url[0])) {
            $this->_loadDefaultController();
            return false;
        }
        $this->_loadExistingController();
        $this->_callControllerMethod();
    }
    
    /**
     * Sets controller path
     *
     * @param string $path path
     *
     * @return void
     */
    public function setControllerPath($path)
    {
        $this->_controllerPath = trim($path, '/') . '/';
    }

    /**
     * Sets model path, models are optional
     *
     * @param string $path path
     *
     * @return void
     */
    public function setModelPath($path)
    {
        $this->_modelPath = trim($path, '/') . '/';
    }
    
    /**
     * Sets error file
     *
     * @param string $path path
     *
     * @return void
     */
    public function setErrorFile($path)
    {
        $this->_errorFile = trim($path, '/');
    }
    
    /**
     * Seperates URL data in a String
     * 
     * @return null
     */
    public function setDefaultFile($path)
    {
        $this->_defaultFile = trim($path, '/');
    }
    
    /**
     * Seperates URL data in a String
     * 
     * @return null
     */
    private function _getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }
    
    /**
     * Call: /admin/login
     *
     * @return null
     */
    private function _loadDefaultController()
    {
        require $this->_controllerPath . $this->_defaultFile;
        $this->_controller = new Login();
        $this->_controller->index();
    }
    
    /**
     * Call: /admin/XXX
     *
     * @return boolean
     */
    private function _loadExistingController()
    {
        $file = $this->_controllerPath . $this->_url[0] . '.php';
        
        if (file_exists($file)) {
            require $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->loadModel($this->_url[0], $this->_modelPath);
        } else {
            $this->_error();
            return false;
        }
    }
    
    /**
     * Already checked, if controller is given, check for existence and loading
     * 
     * @return mixed
     *
     * http://localhost/controller/method/(param)/(param)/(param)/(param)
     * url[0] = Controller
     * url[1] = Method
     * url[2] = Param
     * url[3] = Param
     * url[4] = Param
	 * url[5] = Param
	 * url[6] = Param
	 * url[7] = Param
     */
    private function _callControllerMethod()
    {
        $length = count($this->_url);
        
        // Make sure the method we are calling exists
        if ($length > 1) {
            if (!method_exists($this->_controller, $this->_url[1])) {
                $this->_error();
            }
        }
        
        // Determine what to load
        switch ($length) {
			case 9:
				// Controller->Method(Param1, Param2, Param3, Param4, Param5, Param6, Param7)
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5], $this->_url[6], $this->_url[7], $this->_url[8]);
                break;
			case 8:
				// Controller->Method(Param1, Param2, Param3, Param4, Param5, Param6)
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5], $this->_url[6], $this->_url[7]);
                break;
			case 7:
				// Controller->Method(Param1, Param2, Param3, Param4, Param5)
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5], $this->_url[6]);
                break;
			case 6:
				// Controller->Method(Param1, Param2, Param3, Param4)
				$this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4], $this->_url[5]);
                break;
            case 5:
                // Controller->Method(Param1, Param2, Param3)
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                break;
            
            case 4:
                // Controller->Method(Param1, Param2)
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                break;
            
            case 3:
                // Controller->Method(Param1, Param2)
                $this->_controller->{$this->_url[1]}($this->_url[2]);
                break;
            
            case 2:
                // Controller->Method(Param1, Param2)
                $this->_controller->{$this->_url[1]}();
                break;
            
            default:
                $this->_controller->index();
                break;
        }
    }
    
    /**
     * For error controller
     *
     * @param int $arg Errorcode
     *
     * @return void
     */
    private function _error()
    {
        require $this->_controllerPath . $this->_errorFile;
        $this->_controller = new Error();
        $this->_controller->index();
        exit;
    }
}