<?php
if($_SERVER['SERVER_NAME'] == 'localhost' || $_SERVER['SERVER_NAME'] == '192.168.5.37') {
	// Variables locales
	define('ENVIRONMENT', 'local');
	define('URL', 'http://' . $_SERVER['SERVER_NAME'] . '/admin/');
	define('DB_TYPE', 'mysql');
	define('DB_HOST', 'localhost');
	define('DB_NAME', 'sistema_inteligente_v2');
	define('DB_USER', 'user_sist_int');
	define('DB_PASS', 'qwerty789');
	/** Variable para enviar los XML a GSA **/
	define('SEND_TO_GOOGLE', false);
	/** AWS config **/
	define('SEND_TO_AWS', true);
	define('DELETE_FROM_AWS', true);
	define('DIR_PATH', 'C://Apache24//htdocs//admin//');
	define('PHP_PATH', 'C://php//php.exe');
	define('PYTHON_PATH', 'C://Python27//python.exe');
	define('PUSHFEED_PATH', 'C://Python27//pushfeed_client.py');
}
else if($_SERVER['SERVER_NAME'] == 'ec2-54-83-124-179.compute-1.amazonaws.com') {
	// Variables de desarrollo
	define('ENVIRONMENT', 'develop');
	define('URL', 'http://' . $_SERVER['SERVER_NAME'] . '/');
	define('DB_TYPE', 'mysql');
	define('DB_HOST', 'instace-db-sir.czjbsfcs6nbq.us-east-1.rds.amazonaws.com');
	define('DB_NAME', 'sistema_inteligente_v2');
	define('DB_USER', 'user_sis_int');
	define('DB_PASS', 'qwerty789');
	/** Variable para enviar los XML a GSA **/
	define('SEND_TO_GOOGLE', true);
	/** AWS config **/
	define('SEND_TO_AWS', true);
	define('DELETE_FROM_AWS', true);
	define('DIR_PATH', '/var/www/html/');
	define('PHP_PATH', '/usr/bin/php');
	define('PYTHON_PATH', '/usr/bin/python');
	define('PUSHFEED_PATH', '/var/www/html/python/pushfeed_client.py');
}

/** AWS config **/
define('AWS_KEY','AKIAJURHCEZE4UGYWT6Q');
define('AWS_SECRET','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');
define('AWS_BUCKET','sir-esmas-dev');
define('AWS_BUCKET_URL', 'http://sir-esmas-dev.s3-website-us-east-1.amazonaws.com/');

define('filexml','public/_XML/');
define('filejson', 'public/_JSON/');
define('LIBS', 'libs/');
define('limiteM', '10');
define('RPP', '2');

define('INDEXED_URL', 'http://dev-sir.televisa.com/');

/** Galaxy config **/
define('SHARED_SECRET','9spt045ww4tp1ksif34ape042assdadp0ps45');
define('GALAXY_WSDL', 'http://galaxy.esmas.com/wbServices/login/wbservice.wsdl');

/** Images config **/
define('IMG_MAX_UPLOAD_SIZE', '2'); // En MB
define('IMG_PERSON_UPLOAD_DIR', 'images/personas/');
define('IMG_PROGRAM_UPLOAD_DIR', 'images/programas/');
define('IMG_PERSON_UPLOAD_HANDLER', 'image_upload_person.php');
define('IMG_PROGRAM_UPLOAD_HANDLER', 'image_upload_program.php');

/** Image domains **/
define('ESMAS_IMAGE_SUBDOMAIN', 'i2');
define('INVALID_IMAGE_SUBDOMAIN', '(origin-i2|galaxypreview|dynamic-www2)');

/** Dummy Person **/
define('DUMMY_PERSON_URL_THUMB', URL . 'public/images/user.jpg');
define('DUMMY_PERSON_BIO', 'Esta es una biografía temporal. Para comenzar con la nueva biografía borra este texto.');

/** Dummy Program **/
define('DUMMY_PROGRAM_URL_THUMB', URL . 'public/images/program.png');
define('DUMMY_PROGRAM_DESCRIPTION', 'Esta es una descipción temporal. Para comenzar con la nueva descripción del programa borra este texto.');

define('HASH_GENERAL_KEY', 'MixitUp200');

define('HASH_PASSWORD_KEY', 'catsFLYhigh2000miles');