<?php

// Config stuff
require 'config.php';
require 'util/Auth.php';

// Register Autoloader
function SIRautoloader($class) {
    include LIBS . $class . '.php';
}
spl_autoload_register('SIRautoloader');

// Set Parameter
$bootstrap = new Bootstrap();

// Init the action!
$bootstrap->init();