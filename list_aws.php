<?php
	require 'config.php';
	require 'aws/aws-autoloader.php';
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;
	
	$s3Client = S3Client::factory(array(
		'key'    => AWS_KEY,
		'secret' => AWS_SECRET,
	));

	// Use the high-level iterators (returns ALL of your objects).
	try {
		$objects = $s3Client->getIterator('ListObjects', array(
			'Bucket' => AWS_BUCKET
		));

		echo "Keys retrieved!\n";
		foreach ($objects as $object) {
			echo $object['Key'] . "\n";
		}
	} catch (S3Exception $e) {
		echo $e->getMessage() . "\n";
	}

	// Use the plain API (returns ONLY up to 1000 of your objects).
	try {
		$result = $s3Client->listObjects(array('Bucket' => AWS_BUCKET));

		echo "Keys retrieved!\n";
		foreach ($result['Contents'] as $object) {
			echo $object['Key'] . "\n";
		}
	}
	catch (S3Exception $e) {
		echo $e->getMessage() . "\n";
	}