<?php

class Personas extends Controller {
    
    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
        //$this->view->js = array('personas/js/default.js');

    }
    
    public function index()
    {
        $this->view->title = 'Personas';
        $this->view->personasList = $this->model->personasList();
        $this->view->programaLista = $this->model->programaLista();
        $this->view->nPaginar = $this->model->nPaginar();
		$this->view->branchList = $this->model->branchList();
		$this->view->wiam		   = $this->model->wiam();
        
        
        $this->view->render('header');
        $this->view->render('personas/index');
        $this->view->render('footer');
                
    }
	public function check() {
		$values		 = explode(",",$_POST['values']);
		$valuesP	 = explode(",",$_POST['valuesP']);
		$type		 = explode(",",$_POST['type']);
		$val 		 = $this->model->check($values,$valuesP,$type);
		echo json_encode($val);
	}
	public function availability() {
		$name 		 = htmlentities(utf8_decode($_POST['n']), ENT_QUOTES, "ISO8859-1");
		$val		 = $this->model->availability($name);
		echo json_encode($val);
	}
    public function create()
    {
               
        $indices     = explode(",",$_POST['indices']);
        $values      = explode(",",$_POST['values']);
        
        $indicesP    = explode(',',$_POST["indicesP"]);
        $valuesP     = explode(',',$_POST["valuesP"]);
        
        $indicesP1   = explode(',',$_POST["indicesP1"]);
        $valuesP1    = explode(',',$_POST["valuesP1"]);
               
        $this->model->create($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1);
		                
        echo(URL . 'personas');
        
    }
    
    
    public function edit ($id)
    {
        $this->view->personas = $this->model->personasSingleList($id);
        $this->view->programasSeleccionados = $this->model->programasSeleccionados($id);
        $this->view->programaLista = $this->model->programaLista();
		$this->view->branchList = $this->model->branchList();
		$this->view->isImage = $this->model->isImage($id);
		$this->view->getImageInfo = $this->model->getImageInfo($id);
        
        if(empty($this->view->personas)){
            die ('Persona no valida');
        }
        $this->view->title = 'Editar Persona';       
        $this->view->render("header");
        $this->view->render("personas/edit");
        $this->view->render("footer");
        
                
    }
    public function editSave($idP){
    	
    	$indices     = explode(",",$_POST['indices']);
    	$values      = explode(",",$_POST['values']);
    	
    	$indicesP    = explode(',',$_POST["indicesP"]);
    	$valuesP     = explode(',',$_POST["valuesP"]);
    	
    	$indicesP1   = explode(',',$_POST["indicesP1"]);
    	$valuesP1    = explode(',',$_POST["valuesP1"]);
    	
    	$this->model->editSave($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1, $idP);
    	
        echo(URL . 'personas');
     }
    
    public function delete($personaId)
    {
        $this->model->delete($personaId);
        echo(URL . 'personas');
        
    }
	
	public function onlySave($personaId) {
		$this->model->onlySave($personaId);
		echo(URL . 'personas');
	}
	
	public function publish($personaId) {
		$this->model->publish($personaId);
		echo(URL . 'personas');
	}
	
	public function permanentlyDeleted() {
		$valores = explode(',',$_POST["valores"]);
		return $this->model->permanentlyDeleted($valores);
	}
	
     function mostrarPrograma()
    {
        $this->model->getListingsProgramas();
    }
    public function search()
    {
    	$indices  = explode(",",$_POST['indices']);
    	$values   = explode(",",$_POST['values']);
    	$val      = $this->model->search($indices,$values);
    	echo $val;
    }
    public function show($paquete){
    
    	$this->view->title         = 'Personas';
    	$this->view->personasList  = $this->model->personasList($paquete);
    	$this->view->programaLista = $this->model->programaLista();
    	$this->view->nPaginar      = $this->model->nPaginar();
		$this->view->branchList = $this->model->branchList();
		$this->view->wiam		   = $this->model->wiam($paquete);
    	 
    	$this->view->render('header');
    	$this->view->render('personas/index');
    	$this->view->render('footer');
    	 
    }
     
    public function generateXml(){
    	$keys   = explode(',',$_POST['indices']);
    	$values = explode(',',$_POST["values"]);
    	$arr3 = array_combine($keys, $values);
    	return $this->model->generateXml($arr3['tipo'], $arr3["id"]);
    }
    
    public function generateXmlEliminados(){
    	$valores = explode(',',$_POST["valores"]);
    	return $this->model->generateXmlEliminados($valores);
    }
    public function generateXmlVarios(){
    	$valores = explode(',',$_POST["valores"]);
    	return $this->model->generateXmlVarios($valores);
    }
    
    function pagina(){
    	$pag = $this->model->personasList();
    	
    	echo json_encode($pag);
    }
}

?>
