<?php
class Log extends Controller {
	public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
	public function index() {
		$this->view->title = 'Logs';
		$this->view->logType = $this->model->logType();
		$this->view->render('header');
		$this->view->render('log/index');
		$this->view->render('footer');
	}
	public function show() {
		$this->view->title = 'Mostrar';
		$this->view->showType = $this->model->showType($_GET["url"]);
		$this->view->wpaio = $this->model->wpaio();
		$this->view->lpp = $this->model->lpp();
		$this->view->tos = $this->model->tos();
		$this->view->too = $this->model->too();
		$this->view->wop = $this->model->wop();
		$this->view->woo = $this->model->woo();
		$this->view->wos = $this->model->wos();
		$this->view->wol = $this->model->wol();
		$this->view->type = $this->model->type();
		$this->view->pages = $this->model->pages();
		$this->view->search = $this->model->search($_POST);
		$this->view->render('header');
		$this->view->render('log/show');
		$this->view->render('footer');
	}
	public function clearSearch() {
		$this->view->clearSearch = $this->model->clearSearch();
	}
}