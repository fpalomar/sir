<?php

class Branch extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index() 
    {    
        $this->view->title = 'Ramas';
		$this->view->branchList = $this->model->branchList();
        
        $this->view->render('header');
        $this->view->render('branch/index');
        $this->view->render('footer');
    }
    
    public function create() 
    {
        $data = array();
        $data['nombre'] = htmlentities($_POST['nombre'], ENT_QUOTES, "ISO8859-1");
		$data['galaxyId'] = $_POST['galaxy'];
        $data['url'] = $_POST['url'];
		$data['imagen'] = $_POST['imagen'];
        
        // @TODO: Do your error checking!
        
        $this->model->create($data);
        header('location: ' . URL . 'branch');
    }
    
    public function edit($id) 
    {
        $this->view->title = 'Editar Rama';
        $this->view->branch = $this->model->branchSingleList($id);
        
        $this->view->render('header');
        $this->view->render('branch/edit');
        $this->view->render('footer');
    }
    
    public function editSave($id)
    {
        //print_r();
		//die();
		
		$data = array();
        $data['branchId'] = $id;
        $data['nombre'] = htmlentities($_POST['nombre'], ENT_QUOTES, "ISO8859-1");
		$data['galaxyId'] = $_POST['galaxy'];
        $data['url'] = $_POST['url'];
        $data['imagen'] = $_POST['imagen'];
        
        // @TODO: Do your error checking!
        
        $this->model->editSave($data);
        header('location: ' . URL . 'branch');
    }
    
    public function delete($id)
    {
        $this->model->delete($id);
        header('location: ' . URL . 'branch');
    }
}