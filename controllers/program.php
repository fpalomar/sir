<?php

class Program extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
        
    }
    
    public function index() 
    {   
        $this->view->title = 'Programa';
        $this->view->programList = $this->model->programList();
        $this->view->personaLista = $this->model->personaLista();
        $this->view->nPaginar = $this->model->nPaginar();
		$this->view->branchList = $this->model->branchList();
		$this->view->wiam		   = $this->model->wiam();

        $this->view->render('header');
        $this->view->render('program/index');
        $this->view->render('footer');
    }
	public function check() {
		$values 	 = explode(",",$_POST['values']);
		$valuesP	 = explode(",",$_POST['valuesP']);
		$type		 = explode(",",$_POST['type']);
		$val		 = $this->model->check($values,$valuesP,$type);
		echo json_encode($val);
	}
	public function availability() {
		$name 		 = htmlentities(utf8_decode($_POST['n']), ENT_QUOTES, "ISO8859-1");
		$val		 = $this->model->availability($name);
		echo json_encode($val);
	}
    public function create() 
    {    	
        $indices     = explode(",",$_POST['indices']);
        $values      = explode(",",$_POST['values']);             

        $indicesP    = explode(',',$_POST["indicesP"]);
        $valuesP     = explode(',',$_POST["valuesP"]);
        
        $indicesP1   = explode(',',$_POST["indicesP1"]);
        $valuesP1    = explode(',',$_POST["valuesP1"]);

        $this->model->create($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1);
 
        //header('Location:' . URL . 'program');
        echo(URL . 'program');
     }
   
      
    public function edit($id) 
    {
        $this->view->program = $this->model->programSingleList($id);
        $this->view->personaLista = $this->model->personaLista();
        $this->view->personasSeleccionadas = $this->model->personasSeleccionadas($id);
		$this->view->branchList = $this->model->branchList();
		$this->view->isImage = $this->model->isImage($id);
		$this->view->getImageInfo = $this->model->getImageInfo($id);
		
        if (empty($this->view->program)) {
            die('Programa no valido!');
        }
        
        $this->view->title = 'Editar programa';
        
        $this->view->render('header');
        $this->view->render('program/edit');
        $this->view->render('footer');
    }
    
    public function editSave($idP){
    	
    	$indices     = explode(",",$_POST['indices']);
    	$values      = explode(",",$_POST['values']);
    	
    	$indicesP    = explode(',',$_POST["indicesP"]);
    	$valuesP     = explode(',',$_POST["valuesP"]);
    	
    	$indicesP1   = explode(',',$_POST["indicesP1"]);
    	$valuesP1    = explode(',',$_POST["valuesP1"]);
    	
    	$this->model->editSave($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1, $idP);
        echo(URL . 'program');
     }
    
    
    public function delete($id)
    {
        $this->model->delete($id);
        echo(URL . 'program');
    }
	
	public function onlySave($personaId) {
		$this->model->onlySave($personaId);
		echo(URL . 'personas');
	}
	
	public function publish($personaId) {
		$this->model->publish($personaId);
		echo(URL . 'personas');
	}
	
	public function permanentlyDeleted() {
		$valores = explode(',',$_POST["valores"]);
		return $this->model->permanentlyDeleted($valores);
	}
    
    public function search()
    {
    	$indices  = explode(",",$_POST['indices']);
    	$values   = explode(",",$_POST['values']);
        $val      = $this->model->search($indices,$values);
       
        echo $val;
    }
   public function show($paquete){
   	
    	$this->view->title        = 'Programa';
    	$this->view->programList  = $this->model->programList($paquete);
    	$this->view->personaLista = $this->model->personaLista();
    	$this->view->nPaginar     = $this->model->nPaginar();
		$this->view->branchList = $this->model->branchList();
		$this->view->wiam		   = $this->model->wiam($paquete);
    	
    	$this->view->render('header');
    	$this->view->render('program/index');
    	$this->view->render('footer');
    	
    }
   
    public function generateXml(){
    	$keys   = explode(',',$_POST['indices']);
    	$values = explode(',',$_POST["values"]);
    	$arr3 = array_combine($keys, $values);
    	return $this->model->generateXml($arr3['tipo'], $arr3["id"]);
    }
    
    public function generateXmlEliminados(){
    	$valores = explode(',',$_POST["valores"]);
    	return $this->model->generateXmlEliminados($valores);
    }
    public function generateXmlVarios(){
    	$valores = explode(',',$_POST["valores"]);
    	return $this->model->generateXmlVarios($valores);
    }
   function pagina(){
   		$pag = $this->model->programList();
   		echo json_encode($pag);
   }



}