<?php
/*
 * 
 * Class Programa
 */

class Programa extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
        Auth::handleLogin();
    }

    public function index()
    {
        $this->view->title = 'Programas';
        $this->view->css = array('public/themes/ace/assets/css/bootstrap-datepicker.min.css',
                                 'public/themes/ace/assets/css/bootstrap-datepicker3.min.css',
                                 'public/themes/ace/assets/css/dropzone.min.css',
                                 'public/themes/ace/assets/css/bootstrap-duallistbox.min.css',
                                 'public/themes/ace/assets/summernote/dist/summernote.css',
                                 'public/themes/ace/assets/css/jquery.timepicker.css');
        $this->view->js = array('public/themes/ace/assets/js/jquery.dataTables.min.js',
                                'public/themes/ace/assets/js/jquery.dataTables.bootstrap.min.js',
                                'public/themes/ace/assets/js/dataTables.buttons.min.js',
                                'public/themes/ace/assets/js/bootstrap-datepicker.min.js',
                                'public/themes/ace/assets/js/bootstrap-datepicker.es.min.js',
                                'public/themes/ace/assets/js/jquery.maskedinput.min.js',
                                'public/themes/ace/assets/js/dropzone.min.js',
                                'public/themes/ace/assets/js/bootstrap-tag.min.js',
                                'public/themes/ace/assets/js/jquery.bootstrap-duallistbox.min.js',
                                'public/themes/ace/assets/js/bootbox.min.js',
                                'public/themes/ace/assets/js/jquery-ui.min.js',
                                'public/themes/ace/assets/summernote/dist/summernote.min.js',
                                'public/themes/ace/assets/summernote/dist/lang/summernote-es-ES.min.js',
                                'public/themes/ace/assets/js/datepair.min.js',
                                'public/themes/ace/assets/js/jquery.datepair.min.js',
                                'public/themes/ace/assets/js/jquery.timepicker.min.js',
                                'public/themes/ace/assets/js/jquery.jstepper.min.js');
        $this->view->ramaList = $this->model->ramaList();
        $this->view->personaSingleList = $this->model->personaSingleList();
        $this->view->personajeList = $this->model->personajeList();

        $this->view->render('headerBootstrap');
        $this->view->render('programa/index');
        $this->view->render('footerBootstrap');
    }

    public function edit($id = null)
    {
        $this->view->title = 'Editar';
        $this->view->css = array('public/themes/ace/assets/css/bootstrap-datepicker.min.css',
                                 'public/themes/ace/assets/css/bootstrap-datepicker3.min.css',
                                 'public/themes/ace/assets/css/dropzone.min.css',
                                 'public/themes/ace/assets/css/bootstrap-duallistbox.min.css',
                                 'public/themes/ace/assets/summernote/dist/summernote.css',
                                 'public/themes/ace/assets/css/jquery.timepicker.css');
        $this->view->js = array('public/themes/ace/assets/js/bootstrap-datepicker.min.js',
                                'public/themes/ace/assets/js/bootstrap-datepicker.es.min.js',
                                'public/themes/ace/assets/js/jquery.maskedinput.min.js',
                                'public/themes/ace/assets/js/dropzone.min.js',
                                'public/themes/ace/assets/js/bootstrap-tag.min.js',
                                'public/themes/ace/assets/js/jquery.bootstrap-duallistbox.min.js',
                                'public/themes/ace/assets/js/bootbox.min.js',
                                'public/themes/ace/assets/js/chosen.jquery.min.js',
                                'public/themes/ace/assets/js/jquery-ui.min.js',
                                'public/themes/ace/assets/summernote/dist/summernote.min.js',
                                'public/themes/ace/assets/summernote/dist/lang/summernote-es-ES.min.js',
                                'public/themes/ace/assets/js/datepair.min.js',
                                'public/themes/ace/assets/js/jquery.datepair.min.js',
                                'public/themes/ace/assets/js/jquery.timepicker.min.js',
                                'public/themes/ace/assets/js/jquery.jstepper.min.js');
        $this->view->programaGetInfo = $this->model->programaGetInfo($id);
        $this->view->ramaList = $this->model->ramaList();
        $this->view->isImage = $this->model->isImage($id);
        $this->view->getImageInfo = $this->model->getImageInfo($id);
        $this->view->programaGetCuriousFacts = $this->model->programaGetCuriousFacts($id);
        $this->view->personaSingleList = $this->model->personaSingleList();
        $this->view->personajeList = $this->model->personajeList();
        $this->view->programaGetRelations = $this->model->programaGetRelations($id);
        
        if(empty($this->view->programaGetInfo)){
            die ('Programa no válido');
        }

        $this->view->render('headerBootstrap');
        $this->view->render('programa/edit');
        $this->view->render('footerBootstrap');    
    }

    public function programaDataTable()
    {
        if($this->model->isAjax())
            echo $this->model->programaDataTable($_POST);
        else
            $this->model->programaDataTable($_POST);
    }
    
    public function checkprogram() 
    {
        foreach ($_POST as $key => $value) {
            if (array_key_exists('name', $_POST))
                $val = $this->model->checkprogram($value);
            else
                $val = $this->model->checkprogram($value[0]);
        }
        if($val)
            echo json_encode(array('valid' => false));
	else
            echo json_encode(array('valid' => true));
    }
    
    public function create()
    {
        if($this->model->isAjax())
            echo $this->model->create($_POST);
        else
            $this->model->create($_POST);
    }

    public function editSave($id = null)
    {
        if($this->model->isAjax())
            echo $this->model->editSave($_POST);
        else
            $this->model->editSave($_POST);
    }

    public function publish()
    {
        if($this->model->isAjax())
            echo $this->model->publish($_POST);
        else
            $this->model->publish($_POST);
    }

    public function unpublish()
    {
        if($this->model->isAjax())
            echo $this->model->unpublish($_POST);
        else
            $this->model->unpublish($_POST);
    }

    public function delete()
    {
        if($this->model->isAjax())
            echo $this->model->delete($_POST);
        else
            $this->model->delete($_POST);
    }

    public function publishAll() {
        if($this->model->isAjax())
            echo $this->model->publishAll($_POST);
        else
            $this->model->publishAll($_POST);
    }

    public function unpublishAll() {
        if($this->model->isAjax())
            echo $this->model->unpublishAll($_POST);
        else
            $this->model->unpublishAll($_POST);
    }

    public function deleteAll() {
        if($this->model->isAjax())
            echo $this->model->deleteAll($_POST);
        else
            $this->model->deleteAll($_POST);
    }
    
}
