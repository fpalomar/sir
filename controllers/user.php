<?php

class User extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
		Auth::onlyAdmin();
    }
    
    public function index() {    
        $this->view->title = 'Usuarios';
        $this->view->userList = $this->model->userList();
		$this->view->branchList = $this->model->branchList();
		$this->view->error = $this->model->error();
        
        $this->view->render('headerBootstrap');
        $this->view->render('user/index');
        $this->view->render('footerBootstrap');
    }
    
    public function create() {
		if($this->model->isAjax())
			echo $this->model->create($_POST);
		else
			$this->model->create($_POST);
	}
    
    public function edit($id = null) {
        $this->view->title = 'Editar Usuarios';
        $this->view->user = $this->model->userSingleList($id);
		$this->view->branchList = $this->model->branchList();
		$this->view->error = $this->model->error();
        
        $this->view->render('headerBootstrap');
        $this->view->render('user/edit');
        $this->view->render('footerBootstrap');
    }
    
    public function editSave($id) {
		$_POST = array('id' => $id) + $_POST;
		if($this->model->isAjax())
			echo $this->model->editSave($_POST);
		else
			$this->model->editSave($_POST);
    }
    
    public function delete($id) {
		if($this->model->isAjax())
			echo $this->model->delete($id);
		else
			$this->model->delete($id);
    }
	
	public function test() {    
        $this->view->title = 'Users';
        
        $this->view->render('headerBootstrap');
		$this->view->render('footerBootstrap');
    }
	
	public function clear() {
		$this->model->clearData();
	}
	
	public function checkuser() {
		$name = htmlentities(utf8_decode($_POST['login']), ENT_QUOTES, "ISO8859-1");
		$val  = $this->model->checkuser($name);
		if($val)
			echo json_encode(array('valid' => false));
		else
			echo json_encode(array('valid' => true));
	}
}