<?php
/*
 * 
 * Class Persona
 */

class Persona extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index()
    {
        $this->view->title = 'Personas';
        $this->view->css = array('public/themes/ace/assets/css/bootstrap-datepicker.min.css',
                                 'public/themes/ace/assets/css/bootstrap-datepicker3.min.css',
                                 'public/themes/ace/assets/css/dropzone.min.css',
                                 'public/themes/ace/assets/css/bootstrap-duallistbox.min.css',
                                 'public/themes/ace/assets/css/chosen.min.css',
                                 'public/themes/ace/assets/summernote/dist/summernote.css');
        $this->view->js = array('public/themes/ace/assets/js/jquery.dataTables.min.js',
                                'public/themes/ace/assets/js/jquery.dataTables.bootstrap.min.js',
                                'public/themes/ace/assets/js/dataTables.buttons.min.js',
                                'public/themes/ace/assets/js/bootstrap-datepicker.min.js',
                                'public/themes/ace/assets/js/bootstrap-datepicker.es.min.js',
                                'public/themes/ace/assets/js/jquery.maskedinput.min.js',
                                'public/themes/ace/assets/js/dropzone.min.js',
                                'public/themes/ace/assets/js/bootstrap-tag.min.js',
                                'public/themes/ace/assets/js/jquery.bootstrap-duallistbox.min.js',
                                'public/themes/ace/assets/js/bootbox.min.js',
                                'public/themes/ace/assets/js/chosen.jquery.min.js',
                                'public/themes/ace/assets/js/jquery-ui.min.js',
                                'public/themes/ace/assets/summernote/dist/summernote.min.js',
                                'public/themes/ace/assets/summernote/dist/lang/summernote-es-ES.min.js');
        $this->view->programaList = $this->model->programaList();
        $this->view->ramaList = $this->model->ramaList();
        $this->view->personaSingleList = $this->model->personaSingleList();
        $this->view->personajeList = $this->model->personajeList();

        $this->view->render('headerBootstrap');
        $this->view->render('persona/index');
        $this->view->render('footerBootstrap');
    }
    
    public function edit($id = null)
    {
        $this->view->title = 'Editar';
        $this->view->css = array('public/themes/ace/assets/css/bootstrap-datepicker.min.css',
                                 'public/themes/ace/assets/css/bootstrap-datepicker3.min.css',
                                 'public/themes/ace/assets/css/dropzone.min.css',
                                 'public/themes/ace/assets/css/bootstrap-duallistbox.min.css',
                                 'public/themes/ace/assets/css/chosen.min.css',
                                 'public/themes/ace/assets/summernote/dist/summernote.css');
        $this->view->js = array('public/themes/ace/assets/js/bootstrap-datepicker.min.js',
                                'public/themes/ace/assets/js/bootstrap-datepicker.es.min.js',
                                'public/themes/ace/assets/js/jquery.maskedinput.min.js',
                                'public/themes/ace/assets/js/dropzone.min.js',
                                'public/themes/ace/assets/js/bootstrap-tag.min.js',
                                'public/themes/ace/assets/js/jquery.bootstrap-duallistbox.min.js',
                                'public/themes/ace/assets/js/bootbox.min.js',
                                'public/themes/ace/assets/js/chosen.jquery.min.js',
                                'public/themes/ace/assets/js/jquery-ui.min.js',
                                'public/themes/ace/assets/summernote/dist/summernote.min.js',
                                'public/themes/ace/assets/summernote/dist/lang/summernote-es-ES.min.js');
        $this->view->personaGetInfo = $this->model->personaGetInfo($id);
        $this->view->personaGetRelations = $this->model->personaGetRelations($id);
        $this->view->personajeGetRelations = $this->model->personajeGetRelations($id);
        $this->view->personajeGetActualRelations = $this->model->personajeGetActualRelations($id);
        $this->view->personaGetAwards = $this->model->personaGetAwards($id);
        $this->view->personaGetCuriousFacts = $this->model->personaGetCuriousFacts($id);
        $this->view->isImage = $this->model->isImage($id);
        $this->view->getImageInfo = $this->model->getImageInfo($id);
        $this->view->personajeGetList = $this->model->personajeGetList($id);
        $this->view->personajeList = $this->model->personajeList();
        $this->view->programaList = $this->model->programaList();
        $this->view->ramaList = $this->model->ramaList();
        $this->view->personaSingleList = $this->model->personaSingleList();
        
        if(empty($this->view->personaGetInfo)){
            die ('Persona no valida');
        }

        $this->view->render('headerBootstrap');
        $this->view->render('persona/edit');
        $this->view->render('footerBootstrap');    
    }

    public function editSave($id = null)
    {
        if($this->model->isAjax())
            echo $this->model->editSave($_POST);
        else
            $this->model->editSave($_POST);
    }

    public function publish()
    {
        if($this->model->isAjax())
            echo $this->model->publish($_POST);
        else
            $this->model->publish($_POST);
    }

    public function unpublish()
    {
        if($this->model->isAjax())
            echo $this->model->unpublish($_POST);
        else
            $this->model->unpublish($_POST);
    }
    
    public function personaDataTable()
    {
        if($this->model->isAjax())
            echo $this->model->personaDataTable($_POST);
        else
            $this->model->personaDataTable($_POST);
    }
    
    public function checkperson() 
    {
        foreach ($_POST as $key => $value) {
            if (array_key_exists('person-complete-name', $_POST))
                $val = $this->model->checkperson($value);
            else
                $val = $this->model->checkperson($value[0]);
        }
        if($val)
            echo json_encode(array('valid' => false));
	    else
            echo json_encode(array('valid' => true));
    }
    
    public function checkcharacter()
    {
        foreach ($_POST as $key => $value)
            $val  = $this->model->checkcharacter($value);
        if($val)
            echo json_encode(array('valid' => false));
        else
            echo json_encode(array('valid' => true));
    }
    
    public function create()
    {
        if($this->model->isAjax())
            echo $this->model->create($_POST);
        else
            $this->model->create($_POST);
    }
}