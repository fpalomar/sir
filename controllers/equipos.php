<?php

class Equipos extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
        
    }
    
    public function index() 
    {   
        $this->view->title = 'Equipos';
        $this->view->equiposList = $this->model->equiposList();
        $this->view->programLista = $this->model->programLista();
        $this->view->nPaginar = $this->model->nPaginar();

        $this->view->render('header');
        $this->view->render('equipos/index');
        $this->view->render('footer');
    }
    
    public function create() 
    {    	
        $indices     = explode(",",$_POST['indices']);
        $values      = explode(",",$_POST['values']);             

        $indicesP    = explode(',',$_POST["indicesP"]);
        $valuesP     = explode(',',$_POST["valuesP"]);
        
        $indicesP1   = explode(',',$_POST["indicesP1"]);
        $valuesP1    = explode(',',$_POST["valuesP1"]);
        
        $this->model->create($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1);
        header('Location:' . URL . 'equipos');
     }
   
      
    public function edit($id) 
    {
        $this->view->equipos = $this->model->programSingleList($id);
    
        if (empty($this->view->equipos)) {
            die('Programa no valido!');
        }
        
        $this->view->title = 'Editar programa';
        
        $this->view->render('header');
        $this->view->render('equipos/edit');
        $this->view->render('footer');
    }
    
    public function editSave($idPrograma)
    {
    	
        $data = array(
            'idPrograma' => $idPrograma,
            'programaTitulo' => $_POST['titulo'],
            'programaUrl' => $_POST["url"],
            'programaDescripcion' => $_POST["descripcion"],
            'programatThumbmail' => $_POST["thumbmail"],
            'programaUrlCapitulosCompletos' => $_POST["urlCapitulos"],
            'programaUrlNoticias' => $_POST['urlNoticias'],
            'programaUrlFotos' => $_POST['urlFotos'],
            'programaUrlVideos' => $_POST["urlVideos"],
            'programaUrlFacebook' => $_POST["urlFacebook"],
            'programaUrlTwitter' => $_POST["urlTwitter"],
            'programaUrlYoutube'=> $_POST["urlYoutube"],
            'programaUrlPinterest' => $_POST["urlPinterest"],
            'programaUrlGplus' => $_POST['urlGplus'],
            'programaContent' => $_POST['content'],
            'programaConsultRelacionada' => $_POST['sinonimo'],
        );
        $this->model->editSave($data);
        header('location: ' . URL . 'program');
    }
    
    public function delete($id)
    {
        $this->model->delete($id);
        header('location: ' . URL . 'program');
    }
    
    public function search()
    {
    	$indices  = explode(",",$_POST['indices']);
    	$values   = explode(",",$_POST['values']);
        $val      = $this->model->search($indices,$values);
       
        echo $val;
    }
   public function show($paquete){
   	
    	$this->view->title        = 'Programa';
    	$this->view->programList  = $this->model->programList($paquete);
    	$this->view->personaLista = $this->model->personaLista();
    	$this->view->nPaginar     = $this->model->nPaginar();
    	
    	$this->view->render('header');
    	$this->view->render('program/index');
    	$this->view->render('footer');
    	
    }
   
    public function generateXml(){
    	return $this->model->generateXml();
    }
    
   function pagina(){
   		$pag = $this->model->equiposList();
   		echo json_encode($pag);
   }



}