<?php

class Rama extends Controller {

    public function __construct() {
        parent::__construct();
        Auth::handleLogin();
    }
    
    public function index() 
    {    
        $this->view->title = 'Ramas';
        $this->view->css = array('public/themes/ace/assets/css/hover-lib.min.css');
        $this->view->js = array('public/themes/ace/assets/js/jquery.dataTables.min.js',
                                'public/themes/ace/assets/js/jquery.dataTables.bootstrap.min.js',
                                'public/themes/ace/assets/js/dataTables.buttons.min.js',
                                'public/themes/ace/assets/js/hover-lib.min.js');
	$this->view->ramaList = $this->model->ramaList();
        
        $this->view->render('headerBootstrap');
        $this->view->render('rama/index');
        $this->view->render('footerBootstrap');
    }
    
    public function ramaDataTable()
    {
        if($this->model->isAjax())
            echo $this->model->ramaDataTable($_POST);
        else
            $this->model->ramaDataTable($_POST);
    }
    
    public function checkbranch() 
    {
        $name = $_POST['name'];
        $val  = $this->model->checkbranch($name);
        if($val)
            echo json_encode(array('valid' => false));
	else
            echo json_encode(array('valid' => true));
    }
    
    public function checkgalaxy() 
    {
        $id = $_POST['galaxy'];
        $val  = $this->model->checkgalaxy($id);
        if($val)
            echo json_encode(array('valid' => false));
	else
            echo json_encode(array('valid' => true));
    }
    
    public function create() 
    {
        $data = array();
        $data['nombre'] = $_POST['nombre'];
        $data['galaxyId'] = $_POST['galaxy'];
        $data['url'] = $_POST['url'];
        $data['imagen'] = $_POST['imagen'];
        $this->model->create($data);
        header('location: ' . URL . 'rama/');
    }
    
    public function edit($id) 
    {
        $this->view->title = 'Editar Rama';
        $this->view->ramaSingleList = $this->model->ramaSingleList($id);
        
        $this->view->render('headerBootstrap');
        $this->view->render('rama/edit');
        $this->view->render('footerBootstrap');
    }
    
    public function editSave($id)
    {
		
        $data = array();
        $data['branchId'] = $id;
        $data['nombre'] = $_POST['nombre'];
        $data['galaxyId'] = $_POST['galaxy'];
        $data['url'] = $_POST['url'];
        $data['imagen'] = $_POST['imagen'];
        
        $this->model->editSave($data);
        header('location: ' . URL . 'rama/');
    }
    
    public function delete($id)
    {
        $this->model->delete($id);
        header('location: ' . URL . 'rama/');
    }
}