<?php

class Login extends Controller {

    function __construct() {
        parent::__construct();    
    }
    
    function index() 
    {    
        $this->view->css = array('public/themes/ace/assets/css/animate.min.css');
            
        $this->view->title = '¡Bienvenid@ a SIR!';
        $this->view->render('headerLogin');
        $this->view->render('login/index');
        $this->view->render('footerLogin');
    }
    
    function run()
    {
        $this->model->run();
    }
}