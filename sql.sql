
CREATE SCHEMA IF NOT EXISTS `admin4` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `admin` ;

-- -----------------------------------------------------
-- Table `admin`.`programa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin4`.`programa` (
  `idprograma` INT NOT NULL AUTO_INCREMENT,
  `programaTitulo` VARCHAR(100) NULL,
  `programaUrl` VARCHAR(100) NULL,
  `programaDescrip` VARCHAR(1000) NULL,
  `programatThumbmail` VARCHAR(100) NULL,
  `programaUrlCapitulosCompletos` VARCHAR(100) NULL,
  `programaUrlNoticias` VARCHAR(100) NULL,
  `programaUrlFotos` VARCHAR(100) NULL,
  `programaUrlVideos` VARCHAR(100) NULL,
  `programaUrlFacebook` VARCHAR(100) NULL,
  `programaUrlTwitter` VARCHAR(100) NULL,
  `programaUrlYoutube` VARCHAR(100) NULL,
  `programaUrlPinterest` VARCHAR(100) NULL,
  `programaUrlGplus` VARCHAR(100) NULL,
  `programaContent` VARCHAR(100) NULL,
  `programaConsultRelacionada` VARCHAR(100) NULL,
  `programaFechaIngreso` DATETIME NULL,
  PRIMARY KEY (`idprograma`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin4`.`persona` (
  `idPersona` INT NOT NULL AUTO_INCREMENT,
  `personaNom` VARCHAR(100) NULL,
  `personaUrlBio` VARCHAR(100) NULL,
  `personaResumenBio` VARCHAR(120) NULL,
  `personaUrlThumb` VARCHAR(100) NULL,
  `personaNomProgramaAct` VARCHAR(100) NULL,
  `personaUrlProgramActual` VARCHAR(100) NULL,
  `personaUrlNoticias` VARCHAR(100) NULL,
  `personaUrlFotos` VARCHAR(100) NULL,
  `personaUrlVideos` VARCHAR(100) NULL,
  `personaUrlFacebook` VARCHAR(100) NULL,
  `personaUrlTwitter` VARCHAR(100) NULL,
  `personaUrlYoutube` VARCHAR(100) NULL,
  `personaUrlPinterest` VARCHAR(100) NULL,
  `personaUrlGplus` VARCHAR(100) NULL,
  `personaContent` VARCHAR(1000) NULL,
  `personaConsultRelacionada` VARCHAR(100) NULL,
  `personaFechaIngreso` DATETIME NULL,
  `programa_idprograma` INT NOT NULL,
  PRIMARY KEY (`idPersona`),
  INDEX `fk_persona_programa_idx` (`programa_idprograma` ASC),
  CONSTRAINT `fk_personas_programa`
    FOREIGN KEY (`programa_idprograma`)
    REFERENCES `admin4`.`programa` (`idprograma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `admin`.`usuariosAdm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `admin4`.`usuariosAdm` (
  `idusuariosAdm` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NULL,
  `rol` VARCHAR(45) NULL,
  `pass` VARCHAR(130) NULL,
  PRIMARY KEY (`idusuariosAdm`))
ENGINE = InnoDB;

CREATE USER 'admin' IDENTIFIED BY 'qwerty789';

GRANT ALL ON `admin`.* TO 'admin';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
