<?php
set_time_limit(0);
include 'program_model.php';
class Log_Model extends Model {
	private $pag;
	private $l;
	private $o;
	private $s;
	private $u;
	private $activeSearch;
	private $type;
	public function __construct() {
        parent::__construct();
		$this->pag = 1;					// Página en la que se encuentra
		$this->l = limiteM;				// Límite de elementos por página
		$this->o = "fecha";				// Orden que lleva el query
		$this->s = "DESC";				// Tipo de orden
		$this->u = "";					// URL
		$this->activeSearch = FALSE;	// Búsqueda activa, útil en la paginación
		$this->type = "";				// Tipo de log que se usará
    }
	public function personasSingleList($id) {
		$newPer = new Personas_Model();
		return $newPer->personasSingleList($id);
	}
	public function programSingleList($id) {
		$newPro = new Program_Model();
		return $newPro->programSingleList($id);
	}
	/**
	* Log Type
	*
	* Regresa el tipo de log que se almacena en la base de datos, param
	* -Personas
	* -Programas
	* @return array $types tipos de de logs
	* @version original: 2014/12/15 10:32 | modified:
	*/
	public function logType() {
		$types = array('Persona', 'Programa');
		return $types;
	}
	/**
	* Show Type
	*
	* Muestra el contenido del log dependiendo del tipo que se le haga pasar en el param, puede ser:
	* -Personas
	* -Programas
	* @param string $url la url con que se trabajará
	* @return json $execute el resultado del query en formato json
	* @version original: 2014/01/14 11:56 | modified: 2015/01/21 16:25 mod. array_walk_recursive: respuesta
	*/
	public function showType($url) {
		if(!filter_has_var(INPUT_GET, "url"))
			return json_encode(array('response' => 'error', 'message' => 'Hubo un error al procesar la URL'));
		else {
			$url = filter_input(INPUT_GET, "url", FILTER_SANITIZE_URL);
			$this->u = $url;
			$parseUrl = parse_url($url);
			$parsePaths = explode("/", $parseUrl["path"]);
			$parsePaths = array_map("strtolower", $parsePaths);
			$parsePaths = array_map("htmlspecialchars", $parsePaths);
			$parsePaths = array_map("strip_tags", $parsePaths);
			// Named params: acepta hasta 6 [definido en Bootstrap.php]
			foreach($parsePaths as $values) {
				if (strpos($values, ":") !== false) {
					$vars  = explode(":",$values);
					$$vars[0]  = $vars[1];
				}
			}
			$this->type = $parsePaths[2];	// Segundo valor de la URL para el tipo de búsqueda que se hará
			if(isset($p) && is_numeric($p))
				$this->pag = $p;
			else
				$this->pag;
			$query = 'SELECT * FROM usuarioslog';
			if($this->type === "persona")
				$query.= ' WHERE seccion = "persona"';
			else if($this->type === "programa")
				$query.= ' WHERE seccion = "programa"';
			else
				return json_encode(array('response' => 'error', 'message' => 'No se reconoce el tipo de consulta'));
			// Inicio de parámetros para el query
			if(isset($s)) {	// Búsqueda
				if($this->search(INPUT_POST) == 400)
					return json_encode(array('response' => 'error', 'message' => 'Hubo un error al procesar la b&uacute;squeda'));
				else if($this->search(INPUT_POST) == 401)
					return json_encode(array('response' => 'error', 'message' => 'No ha introducido ning&uacute;n t&eacute;rmino de b&uacute;squeda'));
				else {
					$this->activeSearch = TRUE;
					$query.= $this->search(INPUT_POST);
				}
			}
			if(isset($o) && in_array($o, array("i", "u", "s", "a", "m", "r", "f"))) {	// Orden
				$o = filter_var($o, FILTER_SANITIZE_STRING);
				$this->o = $o;
				switch($this->o) {
					case "i" :
						$order = "id";
					break;
					case "u" :
						$order = "usuario";
					break;
					case "s" :
						$order = "seccion";
					break;
					case "a" :
						$order = "accion";
					break;
					case "m" :
						$order = "modificacion";
					break;
					case "r" :
						$order = "respuesta";
					break;
					case "f" :
						$order = "fecha";
					break;
					default :
						$order = "fecha";
					break;
				}
				$query.= ' ORDER BY ' . $order;
			}
			else
				$query.= ' ORDER BY ' . $this->o;	// Default
			if(isset($u) && in_array($u, array("a", "d"))) {	// Tipo de ordenamiento
				$u = filter_var($u, FILTER_SANITIZE_STRING);
				$this->s = $u;
				switch($this->s) {
					case "a" :
						$sort = "ASC";
					break;
					case "d" :
						$sort = "DESC";
					break;
					default :
						$sort = "DESC";
					break;
				}	
				$query.= ' ' . $sort;
			}
			else
				$query.= ' ' . $this->s;
			if(isset($l) && is_numeric($l) && $l <= 1000) {	// Límite
				$this->l = $l;
				$query.= ' LIMIT ' . (($this->pag - 1) * $this->l) . ', ' . $this->l;
			}
			else
				$query.= ' LIMIT ' . (($this->pag - 1) * $this->l) . ', ' . $this->l;
			//print_r($query);	// Query final para pruebas
			$execute = $this->db->select($query);
			if(empty($execute)) {
				return json_encode(array('response' => 'warning', 'message' => 'No se encontraron resultados'));
			}
			array_walk_recursive($execute, function (&$value, $key) {
				if($key == "modificacion")
					$value = $this->formatModification($value);
				if($key == "respuesta")
					$value = $this->formatResponse($value);
			});
			return json_encode($execute);
		}	
	}
	/**
	* Search
	*
	* Realiza la búsqueda en los logs por cuatro valores: usuario, acción, modificación y/o fecha 
	* @param string $url la url con que se trabajará
	* @return json resultado de la búsqueda
	* @version original: 2014/01/14 12:21 | modified:
	*/
	public function search($values) {
		$query = "";
		if(filter_has_var(INPUT_POST, "u") || filter_has_var(INPUT_POST, "a") || filter_has_var(INPUT_POST, "m") || filter_has_var(INPUT_POST, "f")) {
			$this->clearSearch();	// Quita el valor de la búsqueda que se guarda en la sesión
			$input	= filter_input_array(INPUT_POST, [
						'u' => FILTER_SANITIZE_STRING,
						'a' => FILTER_SANITIZE_STRING,
						'm'	=> FILTER_SANITIZE_STRING,
						'f'	=> FILTER_SANITIZE_STRING
						]);
			$accents = create_function('$string','return htmlentities($string, ENT_QUOTES, "ISO8859-1");');	// Codifica en ISO8859-1
			$input = array_map($accents, $input);
			$input = array_map("strtolower", $input);
			$input = array_map("strip_tags", $input);
			// Guarda cada término de búsqueda en una variable de sesión. Útil para la paginación
			if(!empty($input["u"])) {
				$user = $input["u"];
				$_SESSION["search"]["u"] = $user;
			}
			if(!empty($input["a"])) {
				$action = $input["a"];
				$_SESSION["search"]["a"] = $action;
			}
			if(!empty($input["m"])) {
				$modification = $input["m"];
				$_SESSION["search"]["m"] = $modification;
			}
			if(!empty($input["f"])) {
				$date = $input["f"];
				$_SESSION["search"]["f"] = $date;
			}
		}
		else if(isset($_SESSION["search"]) && INPUT_POST === 0) {
			// Toma los valores de las variables de sesión para seguir con la búsqueda
			$accents = create_function('$string','return htmlentities($string, ENT_QUOTES, "ISO8859-1");');
			array_map($accents, $_SESSION["search"]);
			array_map("strtolower", $_SESSION["search"]);
			array_map("strip_tags", $_SESSION["search"]);
			if(!empty($_SESSION["search"]["u"]))
				$user = filter_var($_SESSION["search"]["u"], FILTER_SANITIZE_STRING);
			if(!empty($_SESSION["search"]["a"]))
				$action = filter_var($_SESSION["search"]["a"], FILTER_SANITIZE_STRING);
			if(!empty($_SESSION["search"]["m"]))
				$modification = filter_var($_SESSION["search"]["m"], FILTER_SANITIZE_STRING);
			if(!empty($_SESSION["search"]["f"]))
				$date = filter_var($_SESSION["search"]["f"], FILTER_SANITIZE_STRING);
		}
		else 
			return 400; // Error: no está usando los inputs y no hay variables de sesión
		if(isset($user) || isset($action) || isset($modification) || isset($date)) {
			if(!empty($user)) {	// Búsqueda por usuario
				$query.= ' AND (';
				$userTerms = explode(",", $user);	// Búsqueda múltiple
				for($i = 0; $i < count($userTerms); $i++) {
					$query.= 'usuario LIKE "%' . trim($userTerms[$i]) . '%"';
					if($i != count($userTerms) - 1)
						$query.= ' OR ';
				}
				$query.= ')';
			}
			if(!empty($action)) {	// Búsqueda por acción
				$query.= ' AND (';
				$actionTerms = explode(",", $action);
				for($i = 0; $i < count($actionTerms); $i++) {
					$query.= 'accion LIKE "%' . trim($actionTerms[$i]) . '%"';
					if($i != count($actionTerms) - 1)
						$query.= ' OR ';
				}
				$query.= ')';
			}
			if(!empty($modification)) {	// Búsqueda por modificación
				$query.= ' AND (';
				$modTerms = explode(",", $modification);
				for($i = 0; $i < count($modTerms); $i++) {
					// En la base de datos, en ciertas ocasiones, no se guarda por nombre, hace la búsqueda sobre la tabla de personas o programas para traer el id y ponerlo dentro de los términos de búsqueda
					if($this->type === "persona")
						$id = $this->db->select("SELECT idPersona AS id FROM persona WHERE personaNom LIKE '%" . trim($modTerms[$i]) . "%' COLLATE utf8_general_ci");
					else if($this->type === "programa")
						$id = $this->db->select("SELECT idprograma AS id FROM programa WHERE programaTitulo LIKE '%" . trim($modTerms[$i]) . "%' COLLATE utf8_general_ci");
					$query.= 'modificacion COLLATE utf8_general_ci LIKE "%' . trim($modTerms[$i]) . '%"';
					if(!empty($id)) {
						foreach ($id as $key => $value)
							$query.= ' OR modificacion LIKE "%' . $value["id"] . '%"';
					}
					if($i != count($modTerms) - 1)
						$query.= ' OR ';
				}
				$query.= ')';
			}
			if(!empty($date)) {	// Búsqueda por fecha
				$query.= ' AND (';
				$dateTerms = explode(",", $date);
				for($i = 0; $i < count($dateTerms); $i++) {
					$between = explode("|", $dateTerms[$i]);	//	Búsqueda por rangos de fecha
					if(count($between) >= 2)
						$query.= 'fecha BETWEEN "' . trim($between[1]) . '" AND "' . trim($between[0]) . '"';
					else {
						$query.= 'fecha LIKE "%' . trim($dateTerms[$i]) . '%"';
						if($i != count($dateTerms) - 1)
							$query.= ' OR ';
					}
				}
				$query.= ')';
			}
			return $query;
		}
		else
			return 401;	// Error: no hay ningún término de búsqueda
	}
	/**
	* Clear Search
	*
	* Quita la variable de sesión para la búsqueda
	* @version original: 2014/01/14 12:46 | modified:
	*/
	public function clearSearch() {
		if(isset($_SESSION['search']))
			unset($_SESSION['search']);
	}
	/**
	* Format Modification
	*
	* Interpreta y le da formato a la variable que trae la columna de modificación 
	* @param string $string valor del campo modificacion
	* @return string $string campo modificado
	* @version original: 2014/01/14 12:48 | modified: 2014/01/21 13:17 Nuevos case: 3 y mod. case: 1 
	*/
	public function formatModification($string) {
		if($this->type === "persona") {
			switch($string) {
				case (is_numeric($string)) :
					$toReturn = "";
					$whoIs = $this->personasSingleList($string);
					if($whoIs)
						$toReturn = " - " . $whoIs[0]["personaNom"];
					return $string . $toReturn;
				break;
				case ($string === 'b:0;' || @unserialize($string) !== false) :
					$array = unserialize($string);
					$toReturn = "";
					if($this->all($array, "is_numeric")) {
						foreach($array as $key => $value) {
						$whoIs = $this->personasSingleList($value);
							$toReturn.= "<p>" . $whoIs[0]["personaNom"] . "</p>"; 
						}
					}
					else {
						for($i = 0; $i < count($array) - 1; $i++)
							$toReturn.= "<p>" . $array[$i] . "</p>";
						$whoIs = $this->personasSingleList(end($array));
						$toReturn.= "<p>Rel.: " . $whoIs[0]["personaNom"] . "</p>"; 
					}
					return $toReturn;
				break;
				case (is_object(json_decode($string))) :
					$toReturn = "";
					$array = json_decode($string);
					foreach($array as $key => $value)
						$toReturn.= "<p>" . $key . " - " . $value . "</p>";
					return $toReturn;
				break;
				default :
					return $string;
				break;
			}
		}
		else if($this->type === "programa") {
			switch($string) {
				case (is_numeric($string)) :
					$toReturn = "";
					$whoIs = $this->programSingleList($string);
					if($whoIs)
						$toReturn = " - " . $whoIs[0]["programaTitulo"];
					return $string . $toReturn;
				break;
				case ($string === 'b:0;' || @unserialize($string) !== false) :
					$array = unserialize($string);
					$toReturn = "";
					if($this->all($array, "is_numeric")) {
						foreach($array as $key => $value) {
						$whoIs = $this->programSingleList($value);
							$toReturn.= "<p>" . $whoIs[0]["programaTitulo"] . "</p>"; 
						}
					}
					else {
						for($i = 0; $i < count($array) - 1; $i++)
							$toReturn.= "<p>" . $array[$i] . "</p>";
						$whoIs = $this->programSingleList(end($array));
						$toReturn.= "<p>Rel.: " . $whoIs[0]["programaTitulo"] . "</p>"; 
					}
					return $toReturn;
				break;
				case (is_object(json_decode($string))) :
					$toReturn = "";
					$array = json_decode($string);
					foreach($array as $key => $value)
						$toReturn.= "<p>" . $key . " - " . $value . "</p>";
					return $toReturn;
				break;
				default :
					return $string;
				break;
			}
		}
	}
	/**
	* Format Response
	*
	* Interpreta y le da formato a la variable que trae la columna de respuesta 
	* @param string $string valor del campo modificacion
	* @return string $string campo modificado
	* @version original: 2014/01/21 15:44
	*/
	public function formatResponse($string) {
		switch($string) {
			case (@unserialize($string) !== false) :
				$array = unserialize($string);
				if(empty($array))
					return "ok";
				else {
					foreach($array as $key => $value) {
						if(preg_match("/warning/i", $value))
							$response = "w";
						else if(preg_match("/error/i", $value))
							$response = "e";
						else
							$response = $value;
						$toReturn.= "<p>" . $response . "</p>";
					}	
					return $toReturn;
				}
			break;
			default :
				return $string;
			break;
		}
	}
	/**
	* What Page Am I On 
	*
	* Devuelve el valor de la página en la que se encuentra
	* @return int $this->pag valor de la página 
	* @version original: 2014/01/14 12:51 | modified:
	*/
	public function wpaio() {
		return $this->pag;
	}
	/**
	* Limit per page
	*
	* Devuelve el valor del límite por página
	* @return int $this->l límite por página
	* @version original: 2014/01/14 12:52 | modified:
	*/
	public function lpp() {
		return RPP;
	}
	/**
	* Tos
	*
	* Devuelve el tipo de orden
	* @return string $this->s tipo de orden 
	* @version original: 2014/01/14 12:53 | modified:
	*/
	public function tos() {
		return $this->s;
	}
	/**
	* Too
	*
	* Devuelve el tipo de orden
	* @return string $this->o tipo de orden 
	* @version original: 2014/01/14 12:53 | modified:
	*/
	public function too() {
		return $this->o;
	}
	/**
	* Without page
	*
	* Devuelve la url sin la variable página (p:)
	* @return string $wop
	* @version original: 2014/01/14 12:54 | modified:
	*/
	public function wop() {
		$wop = $this->removeFromUrl($this->u, "p");
		return $wop;
	}
	/**
	* Without order
	*
	* Devuelve la url sin la variable de orden (o:)
	* @return string $woo
	* @version original: 2014/01/14 12:55 | modified:
	*/
	public function woo() {
		$woo = $this->removeFromUrl($this->u, "o");
		return $woo;
	}
	/**
	* Without sort
	*
	* Devuelve la url sin la variable de orden (s:)
	* @return string $wos
	* @version original: 2014/01/14 12:56 | modified:
	*/
	public function wos() {
		$wos = $this->removeFromUrl($this->u, "u");
		return $wos;
	}
	/**
	* Without limit
	*
	* Devuelve la url sin la variable de límite (l:)
	* @return string $wol
	* @version original: 2014/01/14 12:56 | modified:
	*/
	public function wol() {
		$wol = $this->removeFromUrl($this->u, "l");
		return $wol;
	}
	/**
	* Type
	*
	* Devuelve el tipo de elemento (persona o programa)
	* @return string $this->type
	* @version original: 2014/01/14 12:57 | modified:
	*/
	public function type() {
		return $this->type;
	}
	/**
	* Pages
	*
	* Devuelve el número de páginas de acuerdo a la consulta que se esté haciendo. Útil para la paginación
	* @return int $pages
	* @version original: 2014/01/14 14:29 | modified:
	*/
	public function pages() {
		// Paginación normal
		if($this->type === "persona")
			$query = 'SELECT COUNT(id) AS total FROM usuarioslog WHERE seccion = "persona"';
		else if($this->type === "programa")
			$query = 'SELECT COUNT(id) AS total FROM usuarioslog WHERE seccion = "programa"';
		else
			return 0;
		// Paginación en búsqueda
		if($this->activeSearch)
			$query.= $this->search(INPUT_POST);
		$execute = $this->db->select($query);
		$total = $execute[0]["total"];
		$pages = ceil($total/$this->l);
		return $pages;
	}
	/**
	* Remove From URL
	*
	* Quita de la URL el parámetro que le es pasado
	* @param string $url
	* @param string $parameter
	* @return string $toRemove URL sin parámetro
	* @version original: 2014/01/14 14:31 | modified:
	*/
	function removeFromUrl($url, $parameter) {
		$toRemove = $url;
		$toRemove = preg_replace("/\/" . $parameter . ":[a-zA-Z0-9].*?(\/|$)/", '/',   $toRemove);
		$toRemove = preg_replace("/(&)+/","/", $toRemove);
		return $toRemove;
	}
	/**
	* All
	*
	* Filtra elementos de un array
	* @param array $array
	* @param string $predicate
	* @return array array filtrado
	* @version original: 2014/01/14 14:33 | modified:
	*/
	function all($array, $predicate) {
		return array_filter($array, $predicate) === $array;
	}
}	