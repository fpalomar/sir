<?php
include 'Program_model.php';

class Equipos_Model extends Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function equiposList($paq = 1)
    {
		return $this->db->select('SELECT * FROM equipo order by nombre DESC limit '.(($paq-1)*limiteM).','.limiteM);
    }
    
    public function programLista(){
    	$newA = new Program_Model();
    	return $newA->programList();
    }
    

    public function create($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1)
    {        	
    	if (count($indices) == count($values)) {
    		$data = array_combine( $indices , $values);
    	}
    	$this->db->insert('equipo',$data);
    	$idPrograma = array('idEquipo' => intval($this->db->lastInsertId()));
    	$this->createA($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idPrograma);    	
    }
    
    
    public function createA ($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idEquipo)
    {
    	$data   = array();
    	$dataP  = array();
    	$nomPer = array();
    	$personaUrlBio = array();
    	$dataM = array();
    	$cont1 = 0;
    	$cont2 = 0;
    	$dataM = array();
    
    	if($valuesP[0] != '' || $valuesP[0] != 0){
    		for($m = 0; $m<count($valuesP); $m++){
    			if($m%2 == 0){
    				$nomPer[$cont1] = $valuesP[$m];
    				$cont1++;
    			}else{
    				$personaUrlBio[$cont2] = $valuesP[$m];
    				$cont2++;
    			}
    		}
    		for($l = 0;$l<count($valuesP)/2; $l++){
    			$dataM[] = array('personaNom'=>$nomPer[$l],'personaUrlBio'=>$personaUrlBio[$l],'personaFechaIngreso'=>date('Y-m-d H:i:s'));
    		}
    		for($i = 0; $i<count($dataM);$i++){
    			 
    			$this->db->insert('persona', $dataM[$i]);
    			$dat = array("idPersona" => intval($this->db->lastInsertId()));
    
    			$dataR1 = array('persona'=>$dat["idPersona"], 'equipo'=>$idEquipo['idEquipo']);
    			$this->createRelation($dataR1);
    		}
    	}
    
    	if($valuesP1 != '' && $valuesP1[0] != 0 && $idEquipo != '' ){
    		for($i = 0; $i<count($valuesP1); $i++){
    			$dataR = array('persona'=>$valuesP1[$i], 'equipo'=>$idEquipo['idEquipo']);
    			$this->createRelation($dataR);
    		}
    	}
    }
    
    
    function createRelation($data){
    	$this->db->insert('personaequipo',$data);
    	$datas = array('equipo' => intval($this->db->lastInsertId()));
    	return $datas;
    }    
    
    public function editSave($data)
    {
        $postData = array(
            'programaTitulo' => $data['programaTitulo'],
            'programaUrl' => $data["programaUrl"],
            'programaDescrip' => $data["programaDescripcion"],
            'programatThumbmail' => $data["programatThumbmail"],
            'programaUrlCapitulosCompletos' => $data["programaUrlCapitulosCompletos"],
            'programaUrlNoticias' => $data['programaUrlNoticias'],
            'programaUrlFotos' => $data['programaUrlFotos'],
            'programaUrlVideos' => $data["programaUrlVideos"],
            'programaUrlFacebook' => $data["programaUrlFacebook"],
            'programaUrlTwitter' => $data["programaUrlTwitter"],
            'programaUrlYoutube' => $data["programaUrlYoutube"],
            'programaUrlPinterest' => $data["programaUrlPinterest"],
            'programaUrlGplus' => $data['programaUrlGplus'],
            'programaContent' => $data['programaContent'],
            'programaConsultRelacionada' => $data['programaConsultRelacionada'],
        );
        
        $prog = "idPrograma =". $data['idPrograma'];
        $this->db->update('programa',$postData, $prog );
        
    }
    
    public function delete($id)
    {
        $this->db->delete('','idPrograma='.$id);
    }
    
    public function search($columna, $valor)
    {	
    	$consulta = "SELECT * FROM equipo where ";
    	if(count($columna) == 0 || count($columna) == 1){
    		$consulta.= $columna[0] ." like '%".$valor[0]."%' ";
    		
    	}else if(count($columna)>1){
    		$consulta.= $columna[0] ." like '%".$valor[0]."%' ";
    		for($i = 1; $i<count($columna); $i++){
    			$consulta.= "and ".$columna[$i]." like '%".$valor[$i]."%'";
    		}
    	}
    	
	    $valo = $this->db->select($consulta);
    	return json_encode($valo);
    	    	
    }
       
   public function nPaginar(){
    	$v = $this->db->select("SELECT count(*) as total FROM equipo") ;
    	$total = $v[0]["total"];
    	$paginas = ceil($total/limiteM);
    	return $paginas;
    }
    
       
    
}

?>
