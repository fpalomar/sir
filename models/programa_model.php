<?php
require_once 'rama_model.php';

class Programa_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Programa Data Table
     *
     * Muestra la lista completa de programas para el listado en el view de programa
     * @return array show
     * @version original: 2016/11/10 12:04 pm | modified: 
     */
    public function programaDataTable($data) {
        // Número total de elementos sin ninguna búsqueda o filtro
        $query = 'SELECT COUNT(*) as total FROM programa';
        $column = 'programaRama';
        $where = false;
        $extras = ' ORDER BY programaFechaModificacion DESC';
        try {
            $execute = $this->db->select($this->permissions($query, $column, $where, $extras));
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 1));
            die();
        }
        $totalData = $execute[0]['total'];
        $totalFiltered =  $totalData; // Cuando no hay parámetros de búsqueda
        // Valores a devolver
        $query = 'SELECT
                         programaId, programaTitulo, programaDescripcion, programaFechaModificacion, programaStatus
                    FROM
                         programa';
        // Array de columnas como se ordena dependiendo de los valores de POST
        $columns = array(
            // Datatable column index  => Database column name
            1 => 'programaTitulo',
            2 => 'programaDescripcion',
            3 => 'programaFechaModificacion',
            4 => 'programaStatus'
        );
        if(isset($data['order'][0]['column'])) {
            $orderBy = filter_var($data['order'][0]['column'], FILTER_SANITIZE_NUMBER_INT);
            $orderBy = $columns[$orderBy];
        }
        else
            $orderBy = 'programaFechaModificacion';
        if(isset($data['order'][0]['dir']))
            $orderDirection = filter_var($data['order'][0]['dir'], FILTER_SANITIZE_STRING);
        else
            $orderDirection = 'DESC';
        if(isset($data['start']))
            $limitStart = filter_var($data['start'], FILTER_SANITIZE_NUMBER_INT);
        else
            $limitStart = 0;
        if(isset($data['length']))
            $limitLenght = filter_var($data['length'], FILTER_SANITIZE_NUMBER_INT);
        else
            $limitLenght = limiteM;
        if(isset($data['draw']))
            $draw = filter_var($data['draw'], FILTER_SANITIZE_NUMBER_INT);
        else
            $draw = 1;
        // Si hay un parámetro de búsqueda
        $search = "";
        if(isset($data['search']['value']) && !empty($data['search']['value'])) {
            $where = true;
            $name =  filter_var($data['search']['value'], FILTER_SANITIZE_STRING);
            $search = " WHERE programaTitulo LIKE '%" . $name . "%'";
            $query.= $search;
        }
        // Cuando hay un parámetro de búsqueda se tiene que modificar  el número total de rows por resultado de búsqueda
        $countQuery = 'SELECT COUNT(*) as total FROM programa' . $search;
        try {
            $countExecute = $this->db->select($this->permissions($countQuery, $column, $where, $extras));
        }
         catch (RuntimeException $e) {
            return json_encode(array('dberror' => 2));
            die();
        }
        $totalFiltered = $countExecute[0]['total'];
        $extras = ' ORDER BY ' . $orderBy . ' ' . $orderDirection . ' LIMIT ' . $limitStart . ' ,' . $limitLenght;
        try {
            $execute = $this->db->select($this->permissions($query, $column, $where, $extras));
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 3));
            die();
        }
        $data = array(
                    'draw'            => intval($draw),                 // Para cada request/draw del lado del cliente, se envía un número como parámetro
                    'recordsTotal'    => intval($totalData),            // Número total de elementos
                    'recordsFiltered' => intval($totalFiltered),        // Número total de elementos después de la búsqueda, si no hay búsqueda totalFiltered = totalData
                    'data'            => $execute                       // Array con el total de los elementos ($data OR $newData)
                );
        return json_encode($data);
    }

    /**
     * Rama List
     * 
     * Muestra la lista completa de canales con los que se puede relacionar una persona o personaje
     * @return json query
     * @version original: | modified: 13/10/2016 10:21
     */
    public function ramaList() {
        $newRama = new Rama_Model();
        return json_encode($newRama->ramaList());
    }

    /**
     * Persona Single List
     * 
     * Función que muestra la lista completa de personas solamente sin ningún extra
     * @return json query
     * @version original: 29/09/2016 | modified: 06/10/2016 12:44
     */
    public function personaSingleList() {
        return json_encode($this->db->select('SELECT
                                                     personaId, CONCAT(personaNombre,\' \', personaApaterno ,\' \', personaAmaterno) AS personaNombre
                                                FROM
                                                     persona
                                              WHERE
                                                     personaTipo = 1       
                                            ORDER BY
                                                     personaNombre
                                                 ASC'));
        
    }

    /**
     * Personaje List
     * 
     * Función que muestra la lista completa de personajes con su programa relacionado
     * @return json query
     * @version original: 06/10/2016 12:45 | modified:
     */
    public function personajeList() {
        return json_encode($this->db->select('SELECT p.personaId AS personaId,
                                                     TRIM(CONCAT(p.personaNombre, \' \', p.personaApaterno, \' \', p.personaAmaterno)) AS personaNombre,
                                                     c.personaId AS personajeId,
                                                     TRIM(CONCAT(c.personaNombre, \' \', c.personaApaterno, \' \', c.personaAmaterno)) AS personajeNombre
                                                FROM
                                                     persona p
                                          INNER JOIN
                                                     personaprograma a
                                                  ON
                                                     p.personaId = a.personaId
                                          INNER JOIN
                                                     persona c
                                                  ON
                                                     c.personaId = a.personajeId
                                            ORDER BY
                                                     p.personaNombre
                                                 ASC'));
    }

    /**
     * Check Programa
     * 
     * Comprueba si ya existe la programa en la base de datos
     * @param string $name
     * @return bool
     * @version original: 2016/10/16 09:43 | modified: 
     */
    public function checkprogram($name) {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $name = trim($name);
        try {
            $result = $this->db->select('SELECT
                                                programaId
                                           FROM 
                                                programa
                                          WHERE
                                                programaTitulo
                                              =
                                                "' . $name . '"
                                        COLLATE utf8_spanish_ci');
        }
        catch (RuntimeException $e) {
            die(json_encode(array('dberror' => 1)));
        }
        return (bool) $result;
    }

    /**
     * Create
     * 
     * Crea un programa nuevo en la base de datos
     * @param array data información cruda del formulario 
     * @return json con el resultado del proceso de creación
     * @version original: 2016/11/21 13:07 | modified:
     */
    public function create($data) {    
        $errors = array();  // Array con todos los errores al momento de procesar los datos del formulario
        if(empty($data)) {  // Validación para información vacía
            array_push($errors, 100);
            return json_encode(array('response'	  => 'error',
                                     'serversays' => $this->composeErrors($errors),
                                     'values'	  => $data));
        }
        else {
            // Variables de error para los valores necesarios en la BD
            $countName = 0;     // Contador de error para la validación de nombre
            $countThumb = 0;    // Contador de error para la validación de thumb
            $countKeywords = 0; // Contador de error para la validación de keywords
            $countComplete = 0; // Contador de error para la validación de descripción completa
            $countBranch = 0;   // Contador de error para la validación de rama
            $countStatus = 0;   // Contador de error para la validación de estado
			// Validación de campo nombre
			if(!isset($data['name']))   // No existe el nombre
				array_push($errors, 101);
			else {
				if(empty($data['name'])) { // Nombre vacío
					array_push($errors, 102);
					$countName++;
				}
				else if($countName == 0) {
					$programName = filter_var($data['name'], FILTER_SANITIZE_STRING);
					$programName = trim($programName);
				}
			}
			// Validación para nombre existente
			if(isset($programName) && $countName == 0) {	
				if($this->checkprogram($programName))
					array_push($errors, 103);
			}
            // Validación de campo imagen
            if(!isset($data['image-url']))  // No existe el campo de imagen
                array_push($errors, 104);
            else {
                if(empty($data['image-url'])) {	// No hay imagen
                    array_push($errors, 105);
                    $countThumb++;
                }
                else if(!preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?(?:jpe?g|gif|png)$_iuS', $data['image-url'])) {
                    array_push($errors, 106);
                }
                else if($countThumb == 0) {
                    $imageUrl = filter_var($data['image-url'], FILTER_SANITIZE_URL);
                    $imageUrl = trim($imageUrl);
                    $imageUrl = $this->renameFile($imageUrl, $programName);
                    if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $imageUrl))
			            $imageUrl = $this->changeImageDomain($imageUrl);
                }
            }
            // Validación de campo keywords
            if(!isset($data['keywords']))   // No existe el campo de keywords
                array_push($errors, 107);
            else {
                if(empty($data['keywords'])) {	// No hay keywords
                    array_push($errors, 108);
                    $countKeywords++;
                }
                else if($countKeywords == 0) {
                    $keywords = filter_var($data['keywords'], FILTER_SANITIZE_STRING);
                    $keywords = trim($keywords);
                }
            }
            // Validación de campo de descripción completo
            if(!isset($data['complete']))   // No existe el campo de descripción completo
                array_push($errors, 109);
            else {
                if(empty($data['complete'])) {   // No hay descripción
                    array_push($errors, 110);
                    $countComplete++;
                }
                else if($countComplete == 0) {
                    $complete = $data['complete'];
                }
            }
            // Validación de campo de rama
            if(!isset($data['branch']))   // No existe el campo de ramas
                array_push($errors, 111);
            else {
                if(empty($data['branch'])) {   // No hay rama
                    array_push($errors, 112);
                    $countBranch++;
                }
                else if($countBranch == 0) {
                    $branch = filter_var($data['branch'], FILTER_SANITIZE_NUMBER_INT);
                    $branch = trim($branch);
                    $branch = intval($branch);
                }
            }
            // Validación de estado
            if(!isset($data['status'])) // No existe el estatus
                array_push($errors, 113);
            else {
                if(empty($data['status'])) {
                    array_push($errors, 114);
                    $countStatus++;
                }
                else if(!($data['status'] == 'save' || $data['status'] == 'publish')) {
                    array_push ($errors, 115);
                    $countStatus++;
                }
                else if($countStatus == 0) {
                    $s = filter_var($data['status'], FILTER_SANITIZE_STRING);
                    $s = trim($s);
                    if($s == 'publish')
                        $status = 0;
                    else if($s == 'save')
                        $status = 1;
                }
            }
            if(!empty($errors)) { // Respuesta con errores
                $json = json_encode(array('response'   => 'error',
                                          'serversays' => $this->composeErrors($errors),
                                          'values'     => $data));
                return $json;
            }
            else {
                if(Session::get('loggedIn')) {  // El usuario está loggeado
                    // Tipo
                    $type = '';
                    if(isset($data['type']) && !empty($data['type'])) {
                        $type = filter_var($data['type'], FILTER_SANITIZE_NUMBER_INT);
                        $type = trim($type);
                    }
                    // Hora de inicio
                    $starttime = '';
                    if(isset($data['start-time']) && !empty($data['start-time'])) {
                        $starttime = filter_var($data['start-time'], FILTER_SANITIZE_STRING);
                        $starttime = trim($starttime);
                    }
                    // Hora de fin
                    $endtime = '';
                    if(isset($data['end-time']) && !empty($data['end-time'])) {
                        $endtime = filter_var($data['end-time'], FILTER_SANITIZE_STRING);
                        $endtime = trim($endtime);
                    }
                    // Temporadas
                    $season = null;
                    if(isset($data['season']) && !empty($data['season'])) {
                        $season = filter_var($data['season'], FILTER_SANITIZE_NUMBER_INT);
                        $season = trim($season);
                    }
                    // Fecha de inicio
                    $startdate = '0000-00-00';
                    if(isset($data['startdate']) && !empty($data['startdate'])) {
                        $sd = explode('-', $data['startdate']);
                        if(checkdate($sd[1], $sd[0], $sd[2])) {
                            $startdate = $data['startdate'];
                            $startdate = date('Y-m-d', strtotime(str_replace('-', '-', $startdate)));
                        }
                    }
                    // Fecha de fin
                    $enddate = '0000-00-00';
                    if(isset($data['enddate']) && !empty($data['enddate'])) {
                        $ed = explode('-', $data['enddate']);
                        if(checkdate($ed[1], $ed[0], $ed[2])) {
                            $enddate = $data['enddate'];
                            $enddate = date('Y-m-d', strtotime(str_replace('-', '-', $enddate)));
                        }
                    }
                    // Url del programa
                    $url = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['url'])) {
                        $url = filter_var($data['url'], FILTER_SANITIZE_URL);
                        $url = trim($url);
                    }
                    // Resumen del contenido
                    $resume = '';
                    if(isset($data['resume']) && !empty($data['resume'])) {
                        $resume = filter_var($data['resume'], FILTER_SANITIZE_STRING);
                        $resume = trim($resume);
                    }
                    // Personas actuales existentes
                    $actualPersons = '';
                    if(isset($data['duallistbox_actualperson']) && !empty($data['duallistbox_actualperson'])) {
                        $ap = $data['duallistbox_actualperson'];   // Array con las personas actuales
                        $ap_temp = array(); // Array temporal con los personas actuales 
                        // Verificación de los ids en la base de datos
                        foreach($ap as $key => $value) {
                            if($this->checkPersonId($value)) {
                                if(!in_array($value, $ap_temp))
                                    array_push($ap_temp, $value);
                            }
                        }
                        // Filtra el resultado de la comprobación
                        if(!empty($ap_temp))
                            $actualPersons = serialize($ap_temp);
                    }
                    // Personas relacionadas existentes
                    $relatedPersons = '';
                    if(isset($data['duallistbox_relatedperson']) && !empty($data['duallistbox_relatedperson'])) {
                        $rp = $data['duallistbox_relatedperson'];   // Array con las personas relacionadas
                        $rp_temp = array(); // Array temporal con las personas relacionadas 
                        // Verificación de los ids en la base de datos
                        foreach($rp as $key => $value) {
                            if($this->checkPersonId($value))
                                array_push($rp_temp, $value);
                        }
						// Filtra el resultado de la comprobación
                        if(!empty($rp_temp))
                            $relatedPersons = serialize($rp_temp);
                    }
                    // Personajes actuales existentes
                    $actualCharacters = '';
                    if(isset($data['duallistbox_actualcharacter']) && !empty($data['duallistbox_actualcharacter'])) {
                        $ac = $data['duallistbox_actualcharacter']; // Array con los personajes actuales
                        $ac_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($ac as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkPersonId($vp[1]);
                                    array_push($ac_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($ac_temp, $vp[0] . '|');
                            }
                        }
						// Filtra el resultado de la comprobación
                        if(!empty($ac_temp)) 
                            $actualCharacters = serialize($ac_temp);
                    }
                    // Personajes relacionados existentes
                    $relatedCharacters = '';
                    if(isset($data['duallistbox_relatedcharacter']) && !empty($data['duallistbox_relatedcharacter'])) {
                        $rc = $data['duallistbox_relatedcharacter']; // Array con los personajes relacionados
                        $rc_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($rc as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkPersonId($vp[1]);
                                    array_push($rc_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($rc_temp, $vp[0] . '|');
                            }
                        }
						// Filtra el resultado de la comprobación
                        if(!empty($rc_temp))
                            $relatedCharacters = serialize($rc_temp);
                    }
					// Url de capítulos
                    $chaptersUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['chapters'])) {
                        $chaptersUrl = filter_var($data['chapters'], FILTER_SANITIZE_URL);
                        $chaptersUrl = trim($chaptersUrl);
                    }
                    // Url de noticias
                    $newsUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['news'])) {
                        $newsUrl = filter_var($data['news'], FILTER_SANITIZE_URL);
                        $newsUrl = trim($newsUrl);
                    }
                    // Url de fotos
                    $photosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['photos'])) {
                        $photosUrl = filter_var($data['photos'], FILTER_SANITIZE_URL);
                        $photosUrl = trim($photosUrl);
                    }
                    // Url de videos
                    $videosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['videos'])) {
                        $videosUrl = filter_var($data['videos'], FILTER_SANITIZE_URL);
                        $videosUrl = trim($videosUrl);
                    }
                    // Url de Facebook
                    $facebookUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['facebook'])) {
                        $facebookUrl = filter_var($data['facebook'], FILTER_SANITIZE_URL);
                        $facebookUrl = trim($facebookUrl);
                    }
                    // Url de Twitter
                    $twitterUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['twitter'])) {
                        $twitterUrl = filter_var($data['twitter'], FILTER_SANITIZE_URL);
                        $twitterUrl = trim($twitterUrl);
                    }
                    // Url de Youtube
                    $youtubeUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['youtube'])) {
                        $youtubeUrl = filter_var($data['youtube'], FILTER_SANITIZE_URL);
                        $youtubeUrl = trim($youtubeUrl);
                    }
                    // Url de Instagram
                    $instagramUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['instagram'])) {
                        $instagramUrl = filter_var($data['instagram'], FILTER_SANITIZE_URL);
                        $instagramUrl = trim($instagramUrl);
                    }
                    // Url de Pinterest
                    $pinterestUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['pinterest'])) {
                        $pinterestUrl = filter_var($data['pinterest'], FILTER_SANITIZE_URL);
                        $pinterestUrl = trim($pinterestUrl);
                    }
                    // Url de Google Plus
                    $gplusUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['gplus'])) {
                        $gplusUrl = filter_var($data['gplus'], FILTER_SANITIZE_URL);
                        $gplusUrl = trim($gplusUrl);
                    }
                    // Hechos curiosos
                    $curiousFacts = '';
                    if(isset($data['curious-fact']) && !empty($data['curious-fact'])) {
                        $curiousFacts = array_filter($data['curious-fact']);
                    }
                    // Nuevas personas actuales
                    $newActualPersons = array();
                    if(isset($data['new-actual-person-name']) && !empty($data['new-actual-person-name'])) {
                        $nap = array_filter($data['new-actual-person-name']);   // Array con las nuevas personas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nap as $key => $value) {
                            if(!$this->checkPerson($value))
                                array_push($newActualPersons, $value);
                        }
                    }
                    // Nuevas personas relacionados
                    $newRelatedPersons = array();
                    if(isset($data['new-related-person-name']) && !empty($data['new-related-person-name'])) {
                        $nrp = array_filter($data['new-related-person-name']);   // Array con las nuevas personas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nrp as $key => $value) {
                            if(!$this->checkPerson($value))
                                array_push($newRelatedPersons, $value);
                        }
                    }
                    $insertData = array('programaTitulo'             	=> $programName,
										'programaUrl'				 	=> $url,
										'programaDescripcion'		 	=> $resume,
										'programaThumbnail'			 	=> $imageUrl,
										'programaKeywords'              => $keywords,
										'programaUrlCapitulosCompletos' => $chaptersUrl,
										'programaUrlNoticias'			=> $newsUrl,
										'programaUrlFotos'				=> $photosUrl,
										'programaUrlVideos'				=> $videosUrl,
										'programaUrlFacebook'			=> $facebookUrl,
                                        'programaUrlTwitter'        	=> $twitterUrl,
                                        'programaUrlYoutube'        	=> $youtubeUrl,
                                        'programaUrlInstagram'      	=> $instagramUrl,
                                        'programaUrlPinterest'      	=> $pinterestUrl,
                                        'programaUrlGplus'          	=> $gplusUrl,
										'programaDescripcionCompleta'	=> $complete,
										'programaFechaIngreso'			=> date('Y-m-d H:i:s'),
										'programaFechaModificacion'		=> date('Y-m-d H:i:s'),
										'programaRama'					=> $branch,
										'programaHoraInicio'			=> $starttime,
										'programaHoraFin'				=> $endtime,
										'programaFechaInicio'			=> $startdate,
										'programaFechaFin'				=> $enddate,
										'programaStatus'				=> $status,
										'programaTemporadas'			=> $season,
										'programaTipo'					=> $type);
                    try {
                        $this->db->beginTransaction();
                        $result = $this->db->insert('programa', $insertData);
                        $lastInsertId = $this->db->lastInsertId();
                        // Creación de relaciones
                        $persons                = array();  // Array de personas actuales y relacionadas
                        $arrayPersons           = array();  // Array final de personas actuales y relacionadas
                        $characters             = array();  // Array de personajes actuales y relacionados
                        $arrayCharacters        = array();  // Array final de personajes actuales y relacionados
						$arrayActualPersons     = array();	// Array para personas actuales
						$arrayRelatedPersons    = array();	// Array para personas relacionadas
						$arrayActualCharacters  = array();	// Array para personajes actuales
						$arrayRelatedCharacters = array();	// Array para personajes relacionados
						if(!empty($actualPersons))
							$arrayActualPersons = unserialize($actualPersons);
						if(!empty($relatedPersons))
							$arrayRelatedPersons = unserialize($relatedPersons);
                        $persons = array_unique(array_merge($arrayActualPersons, $arrayRelatedPersons));    // Los dos arrays se unen, evitando ids repetidos
                        foreach ($persons as $person) {
                            $isActual = null;   // La persona es actual?
                            if(in_array($person, $arrayActualPersons))  // Busca el Id de la persona en el array deserializado de personas actuales
                                $isActual = 1;
                            array_push($arrayPersons, array('personaId'       => $person,
                                                            'programaId'      => $lastInsertId,
                                                            'personajeId'     => null,
                                                            'programaActual'  => $isActual,
                                                            'personajeActual' => null));
                        }
                        if(!empty($actualCharacters))
                            $arrayActualCharacters = unserialize($actualCharacters);
                        if(!empty($relatedCharacters))
                            $arrayRelatedCharacters = unserialize($relatedCharacters);
                        $characters = array_unique(array_merge($arrayActualCharacters, $arrayRelatedCharacters)); // Los dos arrays se unen, evitando ids repetidos
                        foreach($characters as $character) {
                            $isActual = null;   // La persona y personaje es actual?
                            if(in_array($character, $arrayActualCharacters))    // Busca los Ids de la persona y personaje en el array deserializado de personajes actuales
                                $isActual = 1;
                            $p = explode('|', $character);  // Separa los Ids del personaje y programa
                            $personId = null;
                            $characterId = null;
                            if(isset($p[0]))
                                $personId = intval($p[0]);
                            if(isset($p[1]))
                                $characterId = intval($p[1]);
                            array_push($arrayCharacters, array('personaId'       => $personId,
                                                               'programaId'      => $lastInsertId,
                                                               'personajeId'     => $characterId,
                                                               'programaActual'  => $isActual,
                                                               'personajeActual' => $isActual));
                        }
                        foreach ($arrayCharacters as $key => $value) {  // Quita del array de personas, los ids repetidos de la relación persona-programas-personajes
                            foreach ($arrayPersons as $k => $v) {
                                if($value['personId'] == $v['personId'])
                                    unset($arrayPersons[$k]);
                            }
                        }
                        $relations = array_merge($arrayPersons, $arrayCharacters);  // Une las relaciones de personas-programas y personas-programas-personajes sin repetición de Id
                        foreach ($relations as $key => $value) {
                            $this->db->insert('personaprograma', $value);
                        }
                        // Nuevas personas actuales
                        if(!empty($newActualPersons)) {
                            foreach($newActualPersons as $key => $value) {
                                $newActualPersonCompleteName = filter_var($value, FILTER_SANITIZE_STRING);
                                $newActualPersonCompleteName = trim($newActualPersonCompleteName);
                                $newActualPersonName = $data['new-actual-person-name'][$key];
                                $newActualPersonName = filter_var($newActualPersonName, FILTER_SANITIZE_STRING);
                                $newActualPersonName = trim($newActualPersonName);
                                $newActualPersonSurname = $data['new-actual-person-surname'][$key];
                                $newActualPersonSurname = filter_var($newActualPersonSurname, FILTER_SANITIZE_STRING);
                                $newActualPersonSurname = trim($newActualPersonSurname);
                                $newActualPersonMaidenname = $data['new-actual-person-maidenname'][$key];
                                $newActualPersonMaidenname = filter_var($newActualPersonMaidenname, FILTER_SANITIZE_STRING);
                                $newActualPersonMaidenname = trim($newActualPersonMaidenname);
                                $newActualPersonBioUrl = $data['new-actual-person-bio-url'][$key];
                                $newActualPersonBioUrl = filter_var($newActualPersonBioUrl, FILTER_SANITIZE_URL);
                                $newActualPersonBioUrl = trim($newActualPersonBioUrl);
                                $this->db->insert('persona', array('personaNombre'            => $newActualPersonName,
                                                                   'personaApaterno'          => $newActualPersonSurname,
                                                                   'personaAmaterno'          => $newActualPersonMaidenname,
                                                                   'personaUrlThumb'          => DUMMY_PERSON_URL_THUMB,
                                                                   'personaKeywords'          => $keywords,
                                                                   'personaContenidoBio'      => DUMMY_PERSON_BIO,
                                                                   'personaRama'              => $branch,
                                                                   'personaUrlBio'            => $newActualPersonBioUrl,
                                                                   'personaFechaIngreso'      => date('Y-m-d H:i:s'),
                                                                   'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                                                   'personaStatus'            => 1,
                                                                   'personaTipo'              => 1));
                                $newActualPersonId = $this->db->lastInsertId();
                                $this->db->insert('personaprograma', array('personaId'       => $newActualPersonId,
                                                                           'programaId'      => $lastInsertId,
                                                                           'personajeId'     => null,
                                                                           'programaActual'  => 1,
                                                                           'personajeActual' => null));
                            }
                        }
                        // Nuevos personas relacionadas
                        if(!empty($newRelatedPersons)) {
                            foreach($newRelatedPersons as $key => $value) {
                                $newRelatedPersonCompleteName = filter_var($value, FILTER_SANITIZE_STRING);
                                $newRelatedPersonCompleteName = trim($newRelatedPersonCompleteName);
                                $newRelatedPersonName = $data['new-related-person-name'][$key];
                                $newRelatedPersonName = filter_var($newRelatedPersonName, FILTER_SANITIZE_STRING);
                                $newRelatedPersonName = trim($newRelatedPersonName);
                                $newRelatedPersonSurname = $data['new-related-person-surname'][$key];
                                $newRelatedPersonSurname = filter_var($newRelatedPersonSurname, FILTER_SANITIZE_STRING);
                                $newRelatedPersonSurname = trim($newRelatedPersonSurname);
                                $newRelatedPersonMaidenname = $data['new-related-person-maidenname'][$key];
                                $newRelatedPersonMaidenname = filter_var($newRelatedPersonMaidenname, FILTER_SANITIZE_STRING);
                                $newRelatedPersonMaidenname = trim($newRelatedPersonMaidenname);
                                $newRelatedPersonBioUrl = $data['new-related-person-bio-url'][$key];
                                $newRelatedPersonBioUrl = filter_var($newRelatedPersonBioUrl, FILTER_SANITIZE_URL);
                                $newRelatedPersonBioUrl = trim($newRelatedPersonBioUrl);
                                $this->db->insert('persona', array('personaNombre'            => $newRelatedPersonName,
                                                                   'personaApaterno'          => $newRelatedPersonSurname,
                                                                   'personaAmaterno'          => $newRelatedPersonMaidenname,
                                                                   'personaUrlThumb'          => DUMMY_PERSON_URL_THUMB,
                                                                   'personaKeywords'          => $keywords,
                                                                   'personaContenidoBio'      => DUMMY_PERSON_BIO,
                                                                   'personaRama'              => $branch,
                                                                   'personaUrlBio'            => $newRelatedPersonBioUrl,
                                                                   'personaFechaIngreso'      => date('Y-m-d H:i:s'),
                                                                   'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                                                   'personaStatus'            => 1,
                                                                   'personaTipo'              => 1));
                                $newRelatedPersonId = $this->db->lastInsertId();
                                $this->db->insert('personaprograma', array('personaId'       => $newRelatedPersonId,
                                                                           'programaId'      => $lastInsertId,
                                                                           'personajeId'     => null,
                                                                           'programaActual'  => null,
                                                                           'personajeActual' => null));
                            }
                        }
                        // Hechos curiosos
                        if(!empty($curiousFacts)) {
                            foreach($curiousFacts as $key => $value) {
                                $content = filter_var($value, FILTER_SANITIZE_STRING);
                                $content = trim($content);
                                $this->db->insert('programacuriosidades', array('programaId'                    => $lastInsertId,
                                                                               'programacuriosidadesContenido' => $content));
                            }
                        }
                        if($status == 0) {
                            $this->createJSON($lastInsertId);
                            $this->createSingleXML($lastInsertId);
                            //$this->updateDatosWP($lastInsertId);
                        }
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se creó con éxito.'));
                        return $json;   // Respuesta exitosa
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;	// Error en la base de datos
                    }
                }
                else {
                    $json = json_encode(array('response'   => 'error',
                                              'serversays' => $this->composeErrors(array_push($errors, 127)),
                                              'values'     => $data));
                    return $json;   // Sin permisos
                }
            }
        }
    }

	/**
     * Error
     *
     * Función con la lista de errores y mensajes que se desplegaran
     * -De 0 a 99 errores de la base de datos
     * -De 100 a 1000 errores en los valores del inputs
     * --100s para errores en los procesos de creación
     * --200s para errores en los procesos de edición 
     * @return json contiene mensajes y textos
     * @version original: 2016/10/16 21:08 | modified:
     */
    public function error() {
        $messages = array(0 => 'Error en la base de datos.',
                        100 => 'No hay información enviada.',
                        101 => 'No existe el tipo.',
                        102 => 'No seleccionaste el tipo: persona o personaje.',
                        103 => 'No existe el tipo seleccionado.',
                        111 => 'No existe el nombre.',
                        112 => 'El nombre no puede estar vacío.',
                        113 => 'No se permiten esos caracteres en el nombre.',
                        114 => 'Ese persona ya está registrada.',
                        115 => 'No existe la imagen.',
                        116 => 'La imagen no puede estar vacía.',
                        117 => 'La imagen no tiene un formato correcto: sólo .JPG, .GIF o .PNG.',
                        118 => 'No existen los keywords.',
                        119 => 'Los keywords no pueden estar vacíos.',
                        120 => 'No existe la biografía.',
                        121 => 'La biografía no puede estar vacía.',
                        122 => 'No existe la rama,',
                        123 => 'La rama no puede estar vacía.',
                        124 => 'No existe el estatus.',
                        125 => 'El estatus no puede estar vacío.',
                        126 => 'El estatus no existe.',
                        127 => 'No estás loggeado.',
                        131 => 'No existe el nombre del personaje.',
                        132 => 'El nombre del personaje está vacío.',
                        133 => 'No se permiten esos caracteres en el nombre.',
                        134 => 'El nombre del intérprete del personaje está vacío.',
                        200 => 'No existe el Id.',
                        201 => 'El Id está vacío.',
                        202 => 'El Id no es un número.',
                        203 => 'El Id no existe en la base de datos.',
                        10  => 'Ese usuario no est&aacute; en la base de datos.',
                        201 => 'Ese usuario ya existe pero con otro id.');
        $texts = array('warning' => array('title' => 'El servidor encontró este error',
                                          'texts' => 'Pero no pasa nada, por favor cámbialo y vuelve a intentar.'),
                                          'dberror' => array('title' => 'El servidor encontró este grave error',
                                          'texts' => 'Por favor, ayúdanos a corregirlo reportándolo.'),
                                          'dbnull'  => array('title' => 'El servidor buscó y buscó pero',
                                          'texts' => 'Probablemente llegaste aquí por error pero si crees que es un error del servidor, ayúdanos a corregirlo reportándolo.</p><p align="center"><a href="' . URL .'persona/"><span class="btn btn-warning btn-sm ">Regresar</span></a>'));
        return json_encode(array('messages' => $messages, 'texts' => $texts));
    }
    
    /*
     * Compose Errors
     * 
     * Función que crea un array con los mensajes y textos así como de la lista errores de los procesos de creación o edición
     * @param array errorArray
     * @return array errorCompose
     * @version original: 2016/10/16 21:10 | modified:
     */
    public function composeErrors($errorArray) {
        $errorCompose = array();
        $error = array();
        $errors = array();
        $errorHandler = json_decode($this->error());
        $messages = $errorHandler->messages;
        $texts = $errorHandler->texts;
        $count = 0;
        if(in_array(0, $errorArray)) { // Error en base de datos
            $errorCompose['title'] = $texts->dberror->title;
            $errorCompose['texts'] = $texts->dberror->texts;
            $errorType = $errorArray[1];
            array_pop($errorArray);
        }
        else {
            $errorCompose['title'] = $texts->warning->title;
            $errorCompose['texts'] = $texts->warning->texts;
        }
        foreach ($errorArray as $key => $value) {
            switch($value) {
                case 0:
                    $errors[$count] = array('errorcode' => $value, 'errormessage' => $messages->{$value} . ' (' . $errorType .')');
                break;
                default:
                    $errors[$count] = array('errorcode' => $value, 'errormessage' => $messages->{$value});
            }
            $count++;
        } 
        $errorCompose['error'] = $errors;
        return $errorCompose;
    }

    /**
     * Edit Save
     * 
     * Edita la información del personaje dependiendo de su id
     * @param array data información cruda del formulario 
     * @return json con el resultado del proceso de edición
     * @version original: 2016/11/28 18:19 | modified:
     */
    public function editSave($data) {
        $errors = array();  // Array con todos los errores al momento de procesar los datos del formulario
        if(empty($data)) {  // Validación para información vacía
            array_push($errors, 100);
            return json_encode(array('response'   => 'error',
                                     'serversays' => $this->composeErrors($errors),
                                     'values'     => $data));
        }
        else {
            // Variables de error para los valores necesarios en la BD
            $countId = 0;       // Contador de error para la validación de id
            $countName = 0;     // Contador de error para la validación de nombre
            $countThumb = 0;    // Contador de error para la validación de thumb
            $countKeywords = 0; // Contador de error para la validación de keywords
            $countComplete = 0; // Contador de error para la validación de información del programa
            $countBranch = 0;   // Contador de error para la validación de rama
            $countStatus = 0;   // Contador de error para la validación de estado
            // Validación de id
            if(!isset($data['id'])) // Id no existe
                array_push($errors, 200);
            else {
                if(empty($data['id'])) {    // El Id está vacío
                    array_push($errors, 201);
                    $countId++;
                }
                else if(!ctype_digit($data['id'])) { // El Id no es un número
                    array_push($errors, 202);
                    $countId++;
                }
                else if(!$this->checkProgramaId($data['id'])) {  // El id no existe en la base de datos
                    array_push($data['id'], 203);
                    $countId++;
                }
                else if($countId == 0) {     // Asignación de Id a variable final
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                }
            }
            // Validación de campo nombre
            if(!isset($data['name']))   // No existe el nombre
                array_push($errors, 101);
            else {
                if(empty($data['name'])) { // Nombre vacío
                    array_push($errors, 102);
                    $countName++;
                }
                else if($countName == 0) {
                    $programName = filter_var($data['name'], FILTER_SANITIZE_STRING);
                    $programName = trim($programName);
                }
            }
            // Validación de campo imagen
            if(!isset($data['image-url']))  // No existe el campo de imagen
                array_push($errors, 115);
            else {
                if(empty($data['image-url'])) { // No hay imagen
                    array_push($errors, 116);
                    $countThumb++;
                }
                else if(!preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?(?:jpe?g|gif|png)$_iuS', $data['image-url'])) {
                    array_push($errors, 117);
                }
                else if($countThumb == 0) {
                    $imageUrl = filter_var($data['image-url'], FILTER_SANITIZE_URL);
                    $imageUrl = trim($imageUrl);
                    $imageUrl = $this->renameFile($imageUrl, $programName);
                    if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $imageUrl))
                        $imageUrl = $this->changeImageDomain($imageUrl);
                }
            }
            // Validación de campo keywords
            if(!isset($data['keywords']))   // No existe el campo de keywords
                array_push($errors, 107);
            else {
                if(empty($data['keywords'])) {  // No hay keywords
                    array_push($errors, 108);
                    $countKeywords++;
                }
                else if($countKeywords == 0) {
                    $keywords = filter_var($data['keywords'], FILTER_SANITIZE_STRING);
                    $keywords = trim($keywords);
                }
            }
            // Validación de descripción completo
            if(!isset($data['complete']))   // No existe el campo de descripción completo
                array_push($errors, 109);
            else {
                if(empty($data['complete'])) {   // No hay descripción
                    array_push($errors, 110);
                    $countComplete++;
                }
                else if($countComplete == 0) {
                    $complete = $data['complete'];
                }
            }
            // Validación de campo de rama
            if(!isset($data['branch']))   // No existe el campo de ramas
                array_push($errors, 111);
            else {
                if(empty($data['branch'])) {   // No hay rama
                    array_push($errors, 112);
                    $countBranch++;
                }
                else if($countBranch == 0) {
                    $branch = filter_var($data['branch'], FILTER_SANITIZE_NUMBER_INT);
                    $branch = trim($branch);
                    $branch = intval($branch);
                }
            }
            // Validación de estado
            if(!isset($data['status'])) // No existe el estatus
                array_push($errors, 113);
            else {
                if(empty($data['status'])) {
                    array_push($errors, 114);
                    $countStatus++;
                }
                else if(!($data['status'] == 'save' || $data['status'] == 'publish')) {
                    array_push ($errors, 115);
                    $countStatus++;
                }
                else if($countStatus == 0) {
                    $s = filter_var($data['status'], FILTER_SANITIZE_STRING);
                    $s = trim($s);
                    if($s == 'publish')
                        $status = 0;
                    else if($s == 'save')
                        $status = 1;
                }
            }
            if(!empty($errors)) { // Respuesta con errores
                $json = json_encode(array('response'   => 'error',
                                          'serversays' => $this->composeErrors($errors),
                                          'values'     => $data));
                return $json;
            }
            else {
                if(Session::get('loggedIn')) {  // El usuario está loggeado
                    // Tipo
                    $type = '';
                    if(isset($data['type']) && !empty($data['type'])) {
                        $type = filter_var($data['type'], FILTER_SANITIZE_NUMBER_INT);
                        $type = trim($type);
                    }
                    // Hora de inicio
                    $starttime = '';
                    if(isset($data['start-time']) && !empty($data['start-time'])) {
                        $starttime = filter_var($data['start-time'], FILTER_SANITIZE_STRING);
                        $starttime = trim($starttime);
                    }
                    // Hora de fin
                    $endtime = '';
                    if(isset($data['end-time']) && !empty($data['end-time'])) {
                        $endtime = filter_var($data['end-time'], FILTER_SANITIZE_STRING);
                        $endtime = trim($endtime);
                    }
                    // Temporadas
                    $season = null;
                    if(isset($data['season']) && !empty($data['season'])) {
                        $season = filter_var($data['season'], FILTER_SANITIZE_NUMBER_INT);
                        $season = trim($season);
                    }
                    // Fecha de inicio
                    $startdate = '0000-00-00';
                    if(isset($data['startdate']) && !empty($data['startdate'])) {
                        $sd = explode('-', $data['startdate']);
                        if(checkdate($sd[1], $sd[0], $sd[2])) {
                            $startdate = $data['startdate'];
                            $startdate = date('Y-m-d', strtotime(str_replace('-', '-', $startdate)));
                        }
                    }
                    // Fecha de fin
                    $enddate = '0000-00-00';
                    if(isset($data['enddate']) && !empty($data['enddate'])) {
                        $ed = explode('-', $data['enddate']);
                        if(checkdate($ed[1], $ed[0], $ed[2])) {
                            $enddate = $data['enddate'];
                            $enddate = date('Y-m-d', strtotime(str_replace('-', '-', $enddate)));
                        }
                    }
                    // Url del programa
                    $url = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['url'])) {
                        $url = filter_var($data['url'], FILTER_SANITIZE_URL);
                        $url = trim($url);
                    }
                    // Resumen del contenido
                    $resume = '';
                    if(isset($data['resume']) && !empty($data['resume'])) {
                        $resume = filter_var($data['resume'], FILTER_SANITIZE_STRING);
                        $resume = trim($resume);
                    }
                     // Personas actuales existentes
                    $actualPersons = '';
                    if(isset($data['duallistbox_actualperson']) && !empty($data['duallistbox_actualperson'])) {
                        $ap = $data['duallistbox_actualperson'];   // Array con las personas actuales
                        $ap_temp = array(); // Array temporal con los personas actuales 
                        // Verificación de los ids en la base de datos
                        foreach($ap as $key => $value) {
                            if($this->checkPersonId($value)) {
                                if(!in_array($value, $ap_temp))
                                    array_push($ap_temp, $value);
                            }
                        }
                        // Filtra el resultado de la comprobación
                        if(!empty($ap_temp))
                            $actualPersons = serialize($ap_temp);
                    }
                    // Personas relacionadas existentes
                    $relatedPersons = '';
                    if(isset($data['duallistbox_relatedperson']) && !empty($data['duallistbox_relatedperson'])) {
                        $rp = $data['duallistbox_relatedperson'];   // Array con las personas relacionadas
                        $rp_temp = array(); // Array temporal con las personas relacionadas 
                        // Verificación de los ids en la base de datos
                        foreach($rp as $key => $value) {
                            if($this->checkPersonId($value))
                                array_push($rp_temp, $value);
                        }
                        // Filtra el resultado de la comprobación
                        if(!empty($rp_temp))
                            $relatedPersons = serialize($rp_temp);
                    }
                    // Personajes actuales existentes
                    $actualCharacters = '';
                    //if(isset($data['duallistbox_actualcharacter']) && !empty(array_filter($data['duallistbox_actualcharacter']))) {
                    if(isset($data['duallistbox_actualcharacter']) && !empty($data['duallistbox_actualcharacter'])) {
                        $ac = $data['duallistbox_actualcharacter']; // Array con los personajes actuales
                        $ac_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($ac as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkPersonId($vp[1]);
                                    array_push($ac_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($ac_temp, $vp[0] . '|');
                            }
                        }
                        // Filtra el resultado de la comprobación
                        if(!empty($ac_temp)) 
                            $actualCharacters = serialize($ac_temp);
                    }
                    // Personajes relacionados existentes
                    $relatedCharacters = '';
                    if(isset($data['duallistbox_relatedcharacter']) && !empty($data['duallistbox_relatedcharacter'])) {
                        $rc = $data['duallistbox_relatedcharacter']; // Array con los personajes relacionados
                        $rc_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($rc as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkPersonId($vp[1]);
                                    array_push($rc_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($rc_temp, $vp[0] . '|');
                            }
                        }
                        // Filtra el resultado de la comprobación
                        if(!empty($rc_temp))
                            $relatedCharacters = serialize ($rc_temp);
                    }
                    // Url de capítulos
                    $chaptersUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['chapters'])) {
                        $chaptersUrl = filter_var($data['chapters'], FILTER_SANITIZE_URL);
                        $chaptersUrl = trim($chaptersUrl);
                    }
                    // Url de noticias
                    $newsUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['news'])) {
                        $newsUrl = filter_var($data['news'], FILTER_SANITIZE_URL);
                        $newsUrl = trim($newsUrl);
                    }
                    // Url de fotos
                    $photosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['photos'])) {
                        $photosUrl = filter_var($data['photos'], FILTER_SANITIZE_URL);
                        $photosUrl = trim($photosUrl);
                    }
                    // Url de videos
                    $videosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['videos'])) {
                        $videosUrl = filter_var($data['videos'], FILTER_SANITIZE_URL);
                        $videosUrl = trim($videosUrl);
                    }
                    // Url de Facebook
                    $facebookUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['facebook'])) {
                        $facebookUrl = filter_var($data['facebook'], FILTER_SANITIZE_URL);
                        $facebookUrl = trim($facebookUrl);
                    }
                    // Url de Twitter
                    $twitterUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['twitter'])) {
                        $twitterUrl = filter_var($data['twitter'], FILTER_SANITIZE_URL);
                        $twitterUrl = trim($twitterUrl);
                    }
                    // Url de Youtube
                    $youtubeUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['youtube'])) {
                        $youtubeUrl = filter_var($data['youtube'], FILTER_SANITIZE_URL);
                        $youtubeUrl = trim($youtubeUrl);
                    }
                    // Url de Instagram
                    $instagramUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['instagram'])) {
                        $instagramUrl = filter_var($data['instagram'], FILTER_SANITIZE_URL);
                        $instagramUrl = trim($instagramUrl);
                    }
                    // Url de Pinterest
                    $pinterestUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['pinterest'])) {
                        $pinterestUrl = filter_var($data['pinterest'], FILTER_SANITIZE_URL);
                        $pinterestUrl = trim($pinterestUrl);
                    }
                    // Url de Google Plus
                    $gplusUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['gplus'])) {
                        $gplusUrl = filter_var($data['gplus'], FILTER_SANITIZE_URL);
                        $gplusUrl = trim($gplusUrl);
                    }
                    // Hechos curiosos
                    $curiousFacts = '';
                    if(isset($data['curious-fact']) && !empty($data['curious-fact'])) {
                        $curiousFacts = array_filter($data['curious-fact']);
                    }
                    // Nuevas personas actuales
                    $newActualPersons = array();
                    if(isset($data['new-actual-person-name']) && !empty($data['new-actual-person-name'])) {
                        $nap = array_filter($data['new-actual-person-name']);   // Array con las nuevas personas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nap as $key => $value) {
                            if(!$this->checkPerson($value))
                                array_push($newActualPersons, $value);
                        }
                    }
                    // Nuevas personas relacionados
                    $newRelatedPersons = array();
                    if(isset($data['new-related-person-name']) && !empty($data['new-related-person-name'])) {
                        $nrp = array_filter($data['new-related-person-name']);   // Array con las nuevas personas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nrp as $key => $value) {
                            if(!$this->checkPerson($value))
                                array_push($newRelatedPersons, $value);
                        }
                    }
                    $updateData = array('programaTitulo'                => $programName,
                                        'programaUrl'                   => $url,
                                        'programaDescripcion'           => $resume,
                                        'programaThumbnail'             => $imageUrl,
                                        'programaKeywords'              => $keywords,
                                        'programaUrlCapitulosCompletos' => $chaptersUrl,
                                        'programaUrlNoticias'           => $newsUrl,
                                        'programaUrlFotos'              => $photosUrl,
                                        'programaUrlVideos'             => $videosUrl,
                                        'programaUrlFacebook'           => $facebookUrl,
                                        'programaUrlTwitter'            => $twitterUrl,
                                        'programaUrlYoutube'            => $youtubeUrl,
                                        'programaUrlInstagram'          => $instagramUrl,
                                        'programaUrlPinterest'          => $pinterestUrl,
                                        'programaUrlGplus'              => $gplusUrl,
                                        'programaDescripcionCompleta'   => $complete,
                                        'programaFechaModificacion'     => date('Y-m-d H:i:s'),
                                        'programaRama'                  => $branch,
                                        'programaHoraInicio'            => $starttime,
                                        'programaHoraFin'               => $endtime,
                                        'programaFechaInicio'           => $startdate,
                                        'programaFechaFin'              => $enddate,
                                        'programaStatus'                => $status,
                                        'programaTemporadas'            => $season,
                                        'programaTipo'                  => $type);
                    try {
                        $this->db->beginTransaction();
                        $result = $this->db->update('programa', $updateData, "`programaId` = {$id}");
                        // Actualización de relaciones
                        $this->db->deleteAll('personaprograma', "programaId = '$id'"); // Elimina todas las relaciones con ese Id
                        $persons                = array();  // Array de personas actuales y relacionadas
                        $arrayPersons           = array();  // Array final de personas actuales y relacionadas
                        $characters             = array();  // Array de personajes actuales y relacionados
                        $arrayCharacters        = array();  // Array final de personajes actuales y relacionados
                        $arrayActualPersons     = array();  // Array para personas actuales
                        $arrayRelatedPersons    = array();  // Array para personas relacionadas
                        $arrayActualCharacters  = array();  // Array para personajes actuales
                        $arrayRelatedCharacters = array();  // Array para personajes relacionados
                        if(!empty($actualPersons))
                            $arrayActualPersons = unserialize($actualPersons);
                        if(!empty($relatedPersons))
                            $arrayRelatedPersons = unserialize($relatedPersons);
                        $persons = array_unique(array_merge($arrayActualPersons, $arrayRelatedPersons));    // Los dos arrays se unen, evitando ids repetidos
                        foreach ($persons as $person) {
                            $isActual = null;   // La persona es actual?
                            if(in_array($person, $arrayActualPersons))  // Busca el Id de la persona en el array deserializado de personas actuales
                                $isActual = 1;
                            array_push($arrayPersons, array('personaId'       => $person,
                                                            'programaId'      => $id,
                                                            'personajeId'     => null,
                                                            'programaActual'  => $isActual,
                                                            'personajeActual' => null));
                        }
                        if(!empty($actualCharacters))
                            $arrayActualCharacters = unserialize($actualCharacters);
                        if(!empty($relatedCharacters))
                            $arrayRelatedCharacters = unserialize($relatedCharacters);
                        $characters = array_unique(array_merge($arrayActualCharacters, $arrayRelatedCharacters)); // Los dos arrays se unen, evitando ids repetidos
                        foreach($characters as $character) {
                            $isActual = null;   // La persona y personaje es actual?
                            if(in_array($character, $arrayActualCharacters))    // Busca los Ids de la persona y personaje en el array deserializado de personajes actuales
                                $isActual = 1;
                            $p = explode('|', $character);  // Separa los Ids del personaje y programa
                            $personId = null;
                            $characterId = null;
                            if(isset($p[0]))
                                $personId = intval($p[0]);
                            if(isset($p[1]))
                                $characterId = intval($p[1]);
                            array_push($arrayCharacters, array('personaId'       => $personId,
                                                               'programaId'      => $id,
                                                               'personajeId'     => $characterId,
                                                               'programaActual'  => $isActual,
                                                               'personajeActual' => $isActual));
                        }
                        foreach ($arrayCharacters as $key => $value) {  // Quita del array de personas, los ids repetidos de la relación persona-programas-personajes
                            foreach ($arrayPersons as $k => $v) {
                                if($value['personId'] == $v['personId'])
                                    unset($arrayPersons[$k]);
                            }
                        }
                        $relations = array_merge($arrayPersons, $arrayCharacters);  // Une las relaciones de personas-programas y personas-programas-personajes sin repetición de Id
                        foreach ($relations as $key => $value) {
                            $this->db->insert('personaprograma', $value);
                        }
                        // Nuevas personas actuales
                        if(!empty($newActualPersons)) {
                            foreach($newActualPersons as $key => $value) {
                                $newActualPersonCompleteName = filter_var($value, FILTER_SANITIZE_STRING);
                                $newActualPersonCompleteName = trim($newActualPersonCompleteName);
                                $newActualPersonName = $data['new-actual-person-name'][$key];
                                $newActualPersonName = filter_var($newActualPersonName, FILTER_SANITIZE_STRING);
                                $newActualPersonName = trim($newActualPersonName);
                                $newActualPersonSurname = $data['new-actual-person-surname'][$key];
                                $newActualPersonSurname = filter_var($newActualPersonSurname, FILTER_SANITIZE_STRING);
                                $newActualPersonSurname = trim($newActualPersonSurname);
                                $newActualPersonMaidenname = $data['new-actual-person-maidenname'][$key];
                                $newActualPersonMaidenname = filter_var($newActualPersonMaidenname, FILTER_SANITIZE_STRING);
                                $newActualPersonMaidenname = trim($newActualPersonMaidenname);
                                $newActualPersonBioUrl = $data['new-actual-person-bio-url'][$key];
                                $newActualPersonBioUrl = filter_var($newActualPersonBioUrl, FILTER_SANITIZE_URL);
                                $newActualPersonBioUrl = trim($newActualPersonBioUrl);
                                $this->db->insert('persona', array('personaNombre'            => $newActualPersonName,
                                                                   'personaApaterno'          => $newActualPersonSurname,
                                                                   'personaAmaterno'          => $newActualPersonMaidenname,
                                                                   'personaUrlThumb'          => DUMMY_PERSON_URL_THUMB,
                                                                   'personaKeywords'          => $keywords,
                                                                   'personaContenidoBio'      => DUMMY_PERSON_BIO,
                                                                   'personaRama'              => $branch,
                                                                   'personaUrlBio'            => $newActualPersonBioUrl,
                                                                   'personaFechaIngreso'      => date('Y-m-d H:i:s'),
                                                                   'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                                                   'personaStatus'            => 1,
                                                                   'personaTipo'              => 1));
                                $newActualPersonId = $this->db->lastInsertId();
                                $this->db->insert('personaprograma', array('personaId'       => $newActualPersonId,
                                                                           'programaId'      => $id,
                                                                           'personajeId'     => null,
                                                                           'programaActual'  => 1,
                                                                           'personajeActual' => null));
                            }
                        }
                        // Nuevos personas relacionadas
                        if(!empty($newRelatedPersons)) {
                            foreach($newRelatedPersons as $key => $value) {
                                $newRelatedPersonCompleteName = filter_var($value, FILTER_SANITIZE_STRING);
                                $newRelatedPersonCompleteName = trim($newRelatedPersonCompleteName);
                                $newRelatedPersonName = $data['new-related-person-name'][$key];
                                $newRelatedPersonName = filter_var($newRelatedPersonName, FILTER_SANITIZE_STRING);
                                $newRelatedPersonName = trim($newRelatedPersonName);
                                $newRelatedPersonSurname = $data['new-related-person-surname'][$key];
                                $newRelatedPersonSurname = filter_var($newRelatedPersonSurname, FILTER_SANITIZE_STRING);
                                $newRelatedPersonSurname = trim($newRelatedPersonSurname);
                                $newRelatedPersonMaidenname = $data['new-related-person-maidenname'][$key];
                                $newRelatedPersonMaidenname = filter_var($newRelatedPersonMaidenname, FILTER_SANITIZE_STRING);
                                $newRelatedPersonMaidenname = trim($newRelatedPersonMaidenname);
                                $newRelatedPersonBioUrl = $data['new-related-person-bio-url'][$key];
                                $newRelatedPersonBioUrl = filter_var($newRelatedPersonBioUrl, FILTER_SANITIZE_URL);
                                $newRelatedPersonBioUrl = trim($newRelatedPersonBioUrl);
                                $this->db->insert('persona', array('personaNombre'            => $newRelatedPersonName,
                                                                   'personaApaterno'          => $newRelatedPersonSurname,
                                                                   'personaAmaterno'          => $newRelatedPersonMaidenname,
                                                                   'personaUrlThumb'          => DUMMY_PERSON_URL_THUMB,
                                                                   'personaKeywords'          => $keywords,
                                                                   'personaContenidoBio'      => DUMMY_PERSON_BIO,
                                                                   'personaRama'              => $branch,
                                                                   'personaUrlBio'            => $newRelatedPersonBioUrl,
                                                                   'personaFechaIngreso'      => date('Y-m-d H:i:s'),
                                                                   'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                                                   'personaStatus'            => 1,
                                                                   'personaTipo'              => 1));
                                $newRelatedPersonId = $this->db->lastInsertId();
                                $this->db->insert('personaprograma', array('personaId'       => $newRelatedPersonId,
                                                                           'programaId'      => $id,
                                                                           'personajeId'     => null,
                                                                           'programaActual'  => null,
                                                                           'personajeActual' => null));
                            }
                        }
                        // Hechos curiosos
                        $factsToUpdate = array();
                        if(!empty($curiousFacts)) {
                            foreach($curiousFacts as $key => $value) {
                                $content = filter_var($value, FILTER_SANITIZE_STRING);
                                $content = trim($content);
                                array_push($factsToUpdate, array('programaId'                    => $id,
                                                                 'programacuriosidadesContenido' => $content));
                            }
                        }
                        $this->db->deleteAll('programacuriosidades', "programaId = '$id'");   // Elimina todos los hechos curiosos existentes con ese Id
                        foreach ($factsToUpdate as $key => $value)
                            $this->db->insert('programacuriosidades', $value);    // Inserta los nuevos hechos curiosos
                        if($status == 0) {
                            $this->createJSON($id);
                            $this->createSingleXML($id);
                        }
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se creó con éxito.'));
                        return $json;   // Respuesta exitosa
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;   // Error en la base de datos
                    }
                }
                else {
                    $json = json_encode(array('response'   => 'error',
                                              'serversays' => $this->composeErrors(array_push($errors, 127)),
                                              'values'     => $data));
                    return $json;   // Sin permisos
                }
            }
        }
    }

    /**
     * Create Single XML
     * 
     * Crea el XML del programa con base en su Id
     * @param int $programaId
     * @version original: 2016/11/30 10:09 | modified:
     */
    public function createSingleXML($programaId) {
        $imp     = new DOMImplementation;
        $dtd     = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
        $dom     = $imp->createDocument("", "", $dtd);
        $dom    -> encoding = "UTF-8";
        $dom    -> formatOutput = true;
        $node    = $dom->createElement('gsafeed');
        $header  = $dom->createElement('header');
        $data    = $dom->createElement('datasource');
        $feed    = $dom->createElement('feedtype');
        $data   -> nodeValue = 'programa';
        $feed   -> nodeValue = 'metadata-and-url';
        $header -> appendChild($data);
        $header -> appendChild($feed);
        $node   -> appendChild($header);
        $group   = $dom->createElement('group');
        $node   -> appendChild($group);
        $dom    -> appendChild($node);
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM 
                                                programa
                                          WHERE
                                                programaId
                                              =
                                                "' . $programaId . '"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        if(!empty($result)) {
            try {    // Obtiene el nombre de la rama
                $branchName = $this->db->select('SELECT
                                                        ramaNombre
                                                   FROM 
                                                        rama
                                                  WHERE
                                                        ramaId
                                                      =
                                                        "' . $result[0]['programaRama'] . '"');
            }
            catch (RuntimeException $e) {
                return false;
            }
            // Nombre del programa
            $name = $result[0]['programaTitulo'];
            $name = trim($name);
            $record    = $dom->createElement('record');
            $group    -> appendChild($record);
            $metadata  = $dom->createElement('metadata');
            $content   = $dom->createElement('content');
            $record   -> appendChild($metadata);
            $record   -> appendChild($content);
            $content  -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($result[0]['programaDescripcionCompleta']))));
            $record   -> setAttribute('url', INDEXED_URL . URLify::filter($branchName[0]['ramaNombre'], false) . '/programastvsa/' . URLify::filter($name, false) . '/'); 
            $record   -> setAttribute('action','add');
            $record   -> setAttribute('mimetype','text/html');
            $record   -> setAttribute('lock','true');
            $record   -> setAttribute('last-modified', date('D, d M Y G:i:s') . ' CDT');
            $ignore = array('programaConsultaRelacionada', 'programaRama', 'programaHoraFin', 'programaStatus'); // Ignora estos keys
            $data = $result[0]; // Copia para posteriormente usarla en las relaciones
            foreach($ignore as $k => $v)    // Remueve del array los keys ignorados
                unset($result[0][$v]);
            foreach($result[0] as $key => $value) {
                if(empty($value) || $value == NULL || $value == '0000-00-00') // Ignora valores nulos
                    continue;
                else {
                    switch($key) {
                        case 'programaId':
                            $metaName = 'id';
                            $metaContent = $value;
                            break;
                        case 'programaTitulo':
                            $metaName = 'titulo_programa';
                            $metaContent = $value;
                            break;
                        case 'programaUrl':
                            $metaName = 'url_programa';
                            $metaContent = $value;
                            break;
                        case 'programaDescripcion':
                            $metaName = 'descripcion_programa';
                            $metaContent = $value;
                            break;
                        case 'programaThumbnail':
                            $metaName = 'thumbnail_programa';
                            $metaContent = $value;
                            break;
                        case 'programaKeywords':
                            $metaName = 'keywords';
                            $metaContent = $value;
                            break;
                        case 'programaUrlCapitulosCompletos':
                            $metaName = 'url_capitulos_completos';
                            $metaContent = $value;
                            break;
                        case 'programaUrlNoticias':
                            $metaName = 'url_noticias';
                            $metaContent = $value;
                            break;
                        case 'programaUrlFotos':
                            $metaName = 'url_fotos';
                            $metaContent = $value;
                            break;
                        case 'programaUrlVideos':
                            $metaName = 'url_videos';
                            $metaContent = $value;
                            break;
                        case 'programaUrlFacebook':
                            $metaName = 'url_facebook';
                            $metaContent = $value;
                            break;
                        case 'programaUrlTwitter':
                            $metaName = 'url_twitter';
                            $metaContent = $value;
                            break;
                        case 'programaUrlYoutube':
                            $metaName = 'url_youtube';
                            $metaContent = $value;
                            break;
                        case 'programaUrlInstagram':
                            $metaName = 'url_instagram';
                            $metaContent = $value;
                            break;
                        case 'programaUrlPinterest':
                            $metaName = 'url_pinterest';
                            $metaContent = $value;
                            break;
                        case 'programaUrlGplus':
                            $metaName = 'url_gplus';
                            $metaContent = $value;
                            break;
                        case 'programaDescripcionCompleta':
                            $metaName = 'content';
                            $metaContent = strip_tags(html_entity_decode($value));
                            break;
                        case 'programaFechaIngreso':
                            $metaName = 'fecha_creacion';
                            $metaContent = $value;
                            break;
                        case 'programaFechaModificacion':
                            $metaName = 'fecha_modificacion';
                            $metaContent = $value;
                            break;
                        case 'programaHoraInicio':
                            $metaName = 'horario';
                            $schedule = '';
                            if($data['programaHoraFin'] != null || $data['programaHoraFin'] != '')
                                $schedule = ' - ' . $data['programaHoraFin'];
                            $metaContent = $value . $schedule;
                            break;
                        case 'programaFechaInicio':
                            $metaName = 'fecha_inicio';
                            $metaContent = $value;
                            break;
                        case 'programaFechaFin':
                            $metaName = 'fecha_fin';
                            $metaContent = $value;
                            break;
                        case 'programaTemporadas':
                            $metaName = 'temporadas';
                            $metaContent = $value;
                            break;
                        case 'programaTipo':
                            $metaName = 'tipo';
                            if($value == 1)
                                $metaContent = 'Noticiero';
                            else if($value == 2)
                                $metaContent = 'Novela';
                            else if($value == 3)
                                $metaContent = 'Programa';
                            else if($value == 4)
                                $metaContent = 'Serie';
                            else if($value == 5)
                                $metaContent = 'Web Novela';
                            else if($value == 6)
                                $metaContent = 'Web Serie';
                            else if($value == 7)
                                $metaContent = 'Otro';
                            break;
                    }
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', $metaName);
                    $meta     -> setAttribute('content', $metaContent);
                }
            }
            // Seed Content
            $meta      = $dom->createElement('meta');
            $metadata -> appendChild($meta);
            $meta     -> setAttribute('name', 'seed_content');
            $meta     -> setAttribute('content', 'programa');
            // Relaciones
            $actualPersons     = array();   // Array con el Id de las personas actuales
            $relatedPersons    = array();   // Array con el Id de las personas relacionadas
            $actualCharacters  = array();   // Array con el Id de los personajes actuales
            $relatedCharacters = array();   // Array con el Id de los personajes relacionados
            $relations = json_decode($this->programaGetRelations($programaId), true);   // Obtiene las relaciones del programa
            if(!empty($relations[0])) {
                // Personas y personajes relacionados
                foreach ($relations as $key => $value) {
                    if(!empty($value['personaId'])) {   // Obtiene el Id de la persona actual y relacionada
                        if($value['programaActual'] == 1)
                            array_push($actualPersons, $value['personaId']);
                        array_push($relatedPersons, $value['personaId']);
                    }
                    if(!empty($value['personajeId'])) { // Obtiene el Id del personaje actual y relacionado
                        if($value['personajeActual'] == 1)
                            array_push($actualCharacters, $value['personajeId']);
                        array_push($relatedCharacters, $value['personajeId']);
                    }
                }
                if(!empty($actualPersons)) {    // Muestra las personas actuales en el XML
                    // Obtiene la información de la persona actual
                    foreach($actualPersons as $key => $value) {
                        $person = json_decode($this->personaGetInfo($value), true);
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'nombre_persona_actual_' . $key);
                        $meta     -> setAttribute('content', $person[0]['personaNombre']);
                        if(!empty($person[0]['personaApaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_paterno_persona_actual_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaApaterno']);
                        }
                        if(!empty($person[0]['personaAmaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_materno_persona_actual_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaAmaterno']);
                        }
                        if(!empty($person[0]['personaUrlBio'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_bio_persona_actual_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                        }
                        if($person[0]['personaUrlThumb'] != NULL && !empty($person[0]['personaUrlThumb']) && basename($person[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_thumb_persona_actual_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaUrlThumb']);
                        }
                    }
                }
                if(!empty($relatedPersons)) {   // Muestra las personas relacionadas en el XML
                    // Obtiene la información de la persona relacionada
                    foreach ($relatedPersons as $key => $value) {
                        $person = json_decode($this->personaGetInfo($value), true);
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'nombre_persona_relacionada_' . $key);
                        $meta     -> setAttribute('content', $person[0]['personaNombre']);
                        if(!empty($person[0]['personaApaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_paterno_persona_relacionada_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaApaterno']);
                        }
                        if(!empty($person[0]['personaAmaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_materno_persona_relacionada_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaAmaterno']);
                        }
                        if(!empty($person[0]['personaUrlBio'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_bio_persona_relacionada_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                        }
                        if($person[0]['personaUrlThumb'] != NULL && !empty($person[0]['personaUrlThumb']) && basename($person[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_thumb_persona_relacionada_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaUrlThumb']);
                        }
                    }
                }
                if(!empty($actualCharacters)) { // Muestra los personajes actuales en el XML
                    // Obtiene la información del personaje actual
                    foreach($actualCharacters as $key => $value) {
                        $character = json_decode($this->personaGetInfo($value), true);
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'nombre_personaje_actual_' . $key);
                        $meta     -> setAttribute('content', $character[0]['personaNombre']);
                        if(!empty($character[0]['personaApaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_paterno_personaje_actual_' . $key);
                            $meta     -> setAttribute('content', $character[0]['personaApaterno']);
                        }
                        if(!empty($character[0]['personaAmaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_materno_personaje_actual_' . $key);
                            $meta     -> setAttribute('content', $character[0]['personaAmaterno']);
                        }
                        if(!empty($character[0]['personaUrlBio'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_bio_personaje_actual_' . $key);
                            $meta     -> setAttribute('content', $character[0]['personaUrlBio']);
                        }
                        if($character[0]['personaUrlThumb'] != NULL || !empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_thumb_personaje_actual_' . $key);
                            $meta     -> setAttribute('content', $character[0]['personaUrlThumb']);
                        }
                    }
                }
                if(!empty($relatedCharacters)) {    // Muestra los personajes relacionados en el XML
                    // Obtiene la información del personaje relacionada
                    foreach ($relatedPersons as $key => $value) {
                        if(!empty($value['personajeId'])) { // Se evitan valores vacíos, ya que si existe la relación persona - programa sin personaje
                            $character = json_decode($this->personaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_personaje_relacionado_' . $key);
                            $meta     -> setAttribute('content', $character[0]['personaNombre']);
                            if(!empty($character[0]['personaApaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_paterno_personaje_relacionado_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaApaterno']);
                            }
                            if(!empty($character[0]['personaAmaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_materno_personaje_relacionado_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaAmaterno']);
                            }
                            if(!empty($character[0]['personaUrlBio'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_bio_personaje_relacionado_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                            }
                            if($character[0]['personaUrlThumb'] != NULL || !empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_thumb_personaje_relacionado_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaUrlThumb']);
                            }
                        }
                    }
                }
            }
            // Programa Curiosidades
            $getCuriousFacts = json_decode($this->programaGetCuriousFacts($programaId), true);
            if(!empty($getCuriousFacts)) {
                foreach($getCuriousFacts as $key => $value) {
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', 'hecho_curioso_' . $key);
                    $meta     -> setAttribute('content', $value['programacuriosidadesContenido']);
                }
            }
            // JSON Path
            $meta      = $dom->createElement('meta');
            $metadata -> appendChild($meta);
            $meta     -> setAttribute('name', 'json_programa');
            $folder = 'programas';
            $meta     -> setAttribute('content', AWS_BUCKET_URL . 'json/programas/' . URLify::filter($name) . '.json');
            $xml = $dom->saveXML();
            $path = filexml . $folder;
            $file = $path . '/' . URLify::filter($name) . '.xml';
            if(!file_put_contents($file, $xml))
                throw new RuntimeException('No se pudo generar el archvio XML en la ruta indicada.');
            if(SEND_TO_GOOGLE) {
                $this->executePy($file, 'programas');
                if(SEND_TO_AWS)
                    $this->sendToAws($file, $folder, 'xml');
            }
        }
        else
            return false;
    }

    /**
     * Create Multiple XML
     * 
     * Crea el XML con múltiples ids
     * @param array $data
     * @param string $action
     * @version original: 2016/12/06 12:12 | modified:
     */
    public function createMultipleXML($ids, $action) {
        $imp     = new DOMImplementation;
        $dtd     = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
        $dom     = $imp->createDocument("", "", $dtd);
        $dom    -> encoding = "UTF-8";
        $dom    -> formatOutput = true;
        $node    = $dom->createElement('gsafeed');
        $header  = $dom->createElement('header');
        $data    = $dom->createElement('datasource');
        $feed    = $dom->createElement('feedtype');
        if($action == 'publish')
            $data   -> nodeValue = 'programa';
        else if($action == 'unpublish')
            $data   -> nodeValue = 'feed';
        $feed   -> nodeValue = 'metadata-and-url';
        $header -> appendChild($data);
        $header -> appendChild($feed);
        $node   -> appendChild($header);
        $group   = $dom->createElement('group');
        if($action == 'unpublish')
            $group  -> setAttribute('action', 'delete');
        $node   -> appendChild($group);
        $dom    -> appendChild($node);
        if(!empty($ids)) {
            foreach ($ids as $key => $programaId) {
                try {
                    $result = $this->db->select('SELECT
                                                        *
                                                   FROM 
                                                        programa
                                                  WHERE
                                                        programaId
                                                      =
                                                        "' . $programaId . '"');
                }
                catch (RuntimeException $e) {
                    return false;
                }
                try {    // Obtiene el nombre de la rama
                    $branchName = $this->db->select('SELECT
                                                            ramaNombre
                                                       FROM 
                                                            rama
                                                      WHERE
                                                            ramaId
                                                          =
                                                            "' . $result[0]['programaRama'] . '"');
                }
                catch (RuntimeException $e) {
                    return false;
                }
                if($action == 'publish') {
                    // Nombre del programa
                    $name = $result[0]['programaTitulo'];
                    $name = trim($name);
                    $record    = $dom->createElement('record');
                    $group    -> appendChild($record);
                    $metadata  = $dom->createElement('metadata');
                    $content   = $dom->createElement('content');
                    $record   -> appendChild($metadata);
                    $record   -> appendChild($content);
                    $content  -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($result[0]['programaDescripcionCompleta']))));
                    $record   -> setAttribute('url', INDEXED_URL . URLify::filter($branchName[0]['ramaNombre'], false) . '/programastvsa/' . URLify::filter($name, false) . '/'); 
                    $record   -> setAttribute('action','add');
                    $record   -> setAttribute('mimetype','text/html');
                    $record   -> setAttribute('lock','true');
                    $record   -> setAttribute('last-modified', date('D, d M Y G:i:s') . ' CDT');
                    $ignore = array('programaConsultaRelacionada', 'programaRama', 'programaHoraFin', 'programaStatus'); // Ignora estos keys
                    $data = $result[0]; // Copia para posteriormente usarla en las relaciones
                    foreach($ignore as $k => $v)    // Remueve del array los keys ignorados
                        unset($result[0][$v]);
                    foreach($result[0] as $key => $value) {
                        if(empty($value) || $value == NULL || $value == '0000-00-00') // Ignora valores nulos
                            continue;
                        else {
                            switch($key) {
                                case 'programaId':
                                    $metaName = 'id';
                                    $metaContent = $value;
                                    break;
                                case 'programaTitulo':
                                    $metaName = 'titulo_programa';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrl':
                                    $metaName = 'url_programa';
                                    $metaContent = $value;
                                    break;
                                case 'programaDescripcion':
                                    $metaName = 'descripcion_programa';
                                    $metaContent = $value;
                                    break;
                                case 'programaThumbnail':
                                    $metaName = 'thumbnail_programa';
                                    $metaContent = $value;
                                    break;
                                case 'programaKeywords':
                                    $metaName = 'keywords';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlCapitulosCompletos':
                                    $metaName = 'url_capitulos_completos';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlNoticias':
                                    $metaName = 'url_noticias';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlFotos':
                                    $metaName = 'url_fotos';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlVideos':
                                    $metaName = 'url_videos';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlFacebook':
                                    $metaName = 'url_facebook';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlTwitter':
                                    $metaName = 'url_twitter';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlYoutube':
                                    $metaName = 'url_youtube';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlInstagram':
                                    $metaName = 'url_instagram';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlPinterest':
                                    $metaName = 'url_pinterest';
                                    $metaContent = $value;
                                    break;
                                case 'programaUrlGplus':
                                    $metaName = 'url_gplus';
                                    $metaContent = $value;
                                    break;
                                case 'programaDescripcionCompleta':
                                    $metaName = 'content';
                                    $metaContent = strip_tags(html_entity_decode($value));
                                    break;
                                case 'programaFechaIngreso':
                                    $metaName = 'fecha_creacion';
                                    $metaContent = $value;
                                    break;
                                case 'programaFechaModificacion':
                                    $metaName = 'fecha_modificacion';
                                    $metaContent = $value;
                                    break;
                                case 'programaHoraInicio':
                                    $metaName = 'horario';
                                    $schedule = '';
                                    if($data['programaHoraFin'] != null || $data['programaHoraFin'] != '')
                                        $schedule = ' - ' . $data['programaHoraFin'];
                                    $metaContent = $value . $schedule;
                                    break;
                                case 'programaFechaInicio':
                                    $metaName = 'fecha_inicio';
                                    $metaContent = $value;
                                    break;
                                case 'programaFechaFin':
                                    $metaName = 'fecha_fin';
                                    $metaContent = $value;
                                    break;
                                case 'programaTemporadas':
                                    $metaName = 'temporadas';
                                    $metaContent = $value;
                                    break;
                                case 'programaTipo':
                                    $metaName = 'tipo';
                                    if($value == 1)
                                        $metaContent = 'Noticiero';
                                    else if($value == 2)
                                        $metaContent = 'Novela';
                                    else if($value == 3)
                                        $metaContent = 'Programa';
                                    else if($value == 4)
                                        $metaContent = 'Serie';
                                    else if($value == 5)
                                        $metaContent = 'Web Novela';
                                    else if($value == 6)
                                        $metaContent = 'Web Serie';
                                    else if($value == 7)
                                        $metaContent = 'Otro';
                                    break;
                            }
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', $metaName);
                            $meta     -> setAttribute('content', $metaContent);
                        }
                    }
                    // Seed Content
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', 'seed_content');
                    $meta     -> setAttribute('content', 'programa');
                    // Relaciones
                    $relations = json_decode($this->programaGetRelations($programaId), true);
                    // Personas actuales
                    $actualPersons = $relations['actualPersonsList'];
                    if(!empty($actualPersons)) {
                        // Obtiene la información de la persona actual
                        foreach($actualPersons as $key => $value) {
                            $person = json_decode($this->personaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_persona_actual_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaNombre']);
                            if(!empty($person[0]['personaApaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_paterno_persona_actual_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaApaterno']);
                            }
                            if(!empty($person[0]['personaAmaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_materno_persona_actual_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaAmaterno']);
                            }
                            if(!empty($person[0]['personaUrlBio'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_bio_persona_actual_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                            }
                            if($person[0]['personaUrlThumb'] != NULL && !empty($person[0]['personaUrlThumb']) && basename($person[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_thumb_persona_actual_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaUrlThumb']);
                            }
                        }
                    }
                    // Personas relacionadas
                    $relatedPersons = $relations['relationsList'];
                    if(!empty($relatedPersons)) {
                        // Obtiene la información de la persona relacionada
                        foreach ($relatedPersons as $key => $value) {
                            $person = json_decode($this->personaGetInfo($value['personaId']), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_persona_relacionada_' . $key);
                            $meta     -> setAttribute('content', $person[0]['personaNombre']);
                            if(!empty($person[0]['personaApaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_paterno_persona_relacionada_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaApaterno']);
                            }
                            if(!empty($person[0]['personaAmaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_materno_persona_relacionada_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaAmaterno']);
                            }
                            if(!empty($person[0]['personaUrlBio'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_bio_persona_relacionada_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                            }
                            if($person[0]['personaUrlThumb'] != NULL && !empty($person[0]['personaUrlThumb']) && basename($person[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_thumb_persona_relacionada_' . $key);
                                $meta     -> setAttribute('content', $person[0]['personaUrlThumb']);
                            }
                        }
                    }
                    // Personajes actuales
                    $actualCharacters = $relations['actualCharactersList'];
                    if(!empty($actualCharacters)) {
                        // Obtiene la información del personaje actual
                        foreach($actualCharacters as $key => $value) {
                            $character = json_decode($this->personaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_personaje_actual_' . $key);
                            $meta     -> setAttribute('content', $character[0]['personaNombre']);
                            if(!empty($character[0]['personaApaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_paterno_personaje_actual_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaApaterno']);
                            }
                            if(!empty($character[0]['personaAmaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_materno_personaje_actual_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaAmaterno']);
                            }
                            if(!empty($character[0]['personaUrlBio'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_bio_personaje_actual_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaUrlBio']);
                            }
                            if($character[0]['personaUrlThumb'] != NULL || !empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_thumb_personaje_actual_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaUrlThumb']);
                            }
                        }
                    }
                    // Personajes relacionados
                    $relatedCharacters = $relations['relationsList'];
                    if(!empty($relatedCharacters)) {
                        // Obtiene la información del personaje relacionada
                        foreach ($relatedPersons as $key => $value) {
                            if(!empty($value['personajeId'])) { // Se evitan valores vacíos, ya que si existe la relación persona - programa sin personaje
                                $character = json_decode($this->personaGetInfo($value['personajeId']), true);
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'nombre_personaje_relacionado_' . $key);
                                $meta     -> setAttribute('content', $character[0]['personaNombre']);
                                if(!empty($character[0]['personaApaterno'])) {
                                    $meta      = $dom->createElement('meta');
                                    $metadata -> appendChild($meta);
                                    $meta     -> setAttribute('name', 'apellido_paterno_personaje_relacionado_' . $key);
                                    $meta     -> setAttribute('content', $character[0]['personaApaterno']);
                                }
                                if(!empty($character[0]['personaAmaterno'])) {
                                    $meta      = $dom->createElement('meta');
                                    $metadata -> appendChild($meta);
                                    $meta     -> setAttribute('name', 'apellido_materno_personaje_relacionado_' . $key);
                                    $meta     -> setAttribute('content', $character[0]['personaAmaterno']);
                                }
                                if(!empty($character[0]['personaUrlBio'])) {
                                    $meta      = $dom->createElement('meta');
                                    $metadata -> appendChild($meta);
                                    $meta     -> setAttribute('name', 'url_bio_personaje_relacionado_' . $key);
                                    $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                                }
                                if($character[0]['personaUrlThumb'] != NULL || !empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                    $meta      = $dom->createElement('meta');
                                    $metadata -> appendChild($meta);
                                    $meta     -> setAttribute('name', 'url_thumb_personaje_relacionado_' . $key);
                                    $meta     -> setAttribute('content', $character[0]['personaUrlThumb']);
                                }
                            }
                        }
                    }
                    // Programa Curiosidades
                    $getCuriousFacts = json_decode($this->programaGetCuriousFacts($programaId), true);
                    if(!empty($getCuriousFacts)) {
                        foreach($getCuriousFacts as $key => $value) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'hecho_curioso_' . $key . '_' . $personaTipo);
                            $meta     -> setAttribute('content', $value['programacuriosidadesContenido']);
                        }
                    }
                    // JSON Path
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', 'json_programa');
                    $folder = 'programas';
                    $meta     -> setAttribute('content', AWS_BUCKET_URL . 'json/programas/' . URLify::filter($name) . '.json');
                    // File name
                    $fileName = 'programas';
                }
                else if($action == 'unpublish') {
                    $name = $result[0]['programaTitulo'];
                    $name = trim($name);
                    $record    = $dom->createElement('record');
                    $group    -> appendChild($record);
                    $record   -> setAttribute('url', INDEXED_URL . URLify::filter($branchName[0]['ramaNombre'], false) . '/programastvsa/' . URLify::filter($name, false) . '/');
                    $record   -> setAttribute('mimetype', 'text/plain');
                    $folder = 'programas';
                    $fileName = 'programasEliminados';
                }
            }
            $xml = $dom->saveXML();
            $path = filexml . $folder;
            $file = $path . '/' . URLify::filter($fileName) . '.xml';
            if(!file_put_contents($file, $xml))
                throw new RuntimeException('No se pudo generar el archvio XML en la ruta indicada.');
            if(SEND_TO_GOOGLE) {
                $this->executePy($file, 'programas');
                if(SEND_TO_AWS)
                    $this->sendToAws($file, $folder, 'xml');
            }
        }
        else
            return false;
    }

    /**
     * Create JSON
     * 
     * Crea el archivo JSON del personaje con base en su Id
     * @param int $programaId
     * @version original: 2016/12/01 10:09 | modified: 2017/01/26 16:50
     */
    public function createJson($programaId) {
        $json = array();
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM
                                                programa
                                          WHERE
                                                programaId
                                              =
                                                :programaId',
                                        array('programaId' => $programaId));
        }
        catch(RuntimeException $e) {
            die('Error al obtener la información del programa.');
        }
        // Nombre completo
        $name = $result[0]['programaTitulo'];
        $name = trim($name);
        $ignore = array('personaConsultaRelacionada',
                        'personaStatus');  // Ignora estos keys especiales
        foreach($result[0] as $key => $value) {
            if(array_key_exists($key, $ignore))
                continue;
            else {
                switch($key) {
                    case 'programaId':
                        $k = 'id';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaTitulo':
                        $k = 'nombre';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrl':
                        $k = 'url';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaDescripcion':
                        $k = 'descripcion';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaThumbnail':
                        $k = 'thumbnail';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlCapitulosCompletos':
                        $k = 'urlCapitulosCompletos';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlNoticias':
                        $k = 'urlNoticias';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlFotos':
                        $k = 'urlFotos';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlVideos':
                        $k = 'urlVideos';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlFacebook':
                        $k = 'urlFacebook';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlTwitter':
                        $k = 'urlTwitter';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlYoutube':
                        $k = 'urlYoutube';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlInstagram':
                        $k = 'urlInstagram';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlPinterest':
                        $k = 'urlPinterest';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaUrlGplus':
                        $k = 'urlGplus';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaDescripcionCompleta':
                        $k = 'descripcionCompleta';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaFechaIngreso':
                        $k = 'fechaCreacion';
                        if($value == '0000-00-00')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaFechaModificacion':
                        $k = 'fechaModificacion';
                        if($value == '0000-00-00')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaHoraInicio':
                        $k = 'horario';
                        $schedule = '';
                        if($result[0]['programaHoraFin'] != null || $result[0]['programaHoraFin'] != '')
                            $schedule = ' - ' . $result[0]['programaHoraFin'];
                        $v = $value . $schedule;
                        $json[$k] = $v;
                        break;
                    case 'programaFechaInicio':
                        $k = 'fechaInicio';
                        if($value == '0000-00-00')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaFechaFin':
                        $k = 'fechaFin';
                        if($value == '0000-00-00')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaTemporadas':
                        $k = 'temporadas';
                        if($value == NULL || $value == '')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'programaTipo':
                        $k = 'tipo';
                        if($value == 1)
                            $v = 'Noticiero';
                        else if($value == 2)
                            $v = 'Novela';
                        else if($value == 3)
                            $v = 'Programa';
                        else if($value == 4)
                            $v = 'Serie';
                        else if($value == 5)
                            $v = 'Web Novela';
                        else if($value == 6)
                            $v = 'Web Serie';
                        else if($value == 7)
                            $v = 'Otro';
                        $json[$k] = $v;
                        break;
                }
                $json['seed_content'] = 'programa';
            }
        }
        // Relaciones
        $aP                = array();
        $rP                = array();
        $aC                = array();
        $rC                = array();
        $actualPerson      = array();
        $actualPersons     = array();
        $relatedPerson     = array();
        $relatedPersons    = array();
        $actualCharacter   = array();
        $actualCharacters  = array();
        $relatedCharacter  = array();
        $relatedCharacters = array();
        $emptyArray        = array('nombre'          => '',
                                   'apellidoPaterno' => '',
                                   'apellidoMaterno' => '',
                                   'url'             => '',
                                   'urlThumb'        => '');    // Array para elementos vacíos
        $relations = json_decode($this->programaGetRelations($programaId), true);
        if(!empty($relations[0])) {
            // Personas y personajes relacionados
            foreach($relations as $key => $value) {
                if(!empty($value['personaId'])) {
                    if($value['programaActual'] == 1)
                        array_push($aP, $value['personaId']);
                    array_push($rP, $value['personaId']);
                }
                if(!empty($value['personajeId'])) {
                    if($value['personajeActual'] == 1)
                        array_push($aC, $value['personajeId']);
                    array_push($rC, $value['personajeId']);
                }
            }
            if(!empty($aP)) {   // Inserta las personas actuales en el JSON
                foreach ($aP as $key => $value) {
                    $person = json_decode($this->personaGetInfo($value), true);
                    $actualPerson['nombre'] = $person[0]['personaNombre'];
                    $actualPerson['apellidoPaterno'] = $person[0]['personaApaterno'];
                    $actualPerson['apellidoMaterno'] = $person[0]['personaAmaterno'];
                    if($person[0]['personaUrlBio'] == NULL || empty($person[0]['personaUrlBio']))
                        $actualPerson['url'] = '';
                    else
                        $actualPerson['url'] = $person[0]['personaUrlBio'];
                    if($person[0]['personaUrlThumb'] == NULL || empty($person[0]['personaUrlThumb']) || basename($person[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                        $actualPerson['urlThumb'] = '';
                    else
                        $actualPerson['urlThumb'] = $person[0]['personaUrlThumb'];
                    array_push($actualPersons, $actualPerson);
                }
            }
            if(!empty($rP)) {   // Inserta las personas relacionadas en el JSON
                foreach ($rP as $key => $value) {
                    $person = json_decode($this->personaGetInfo($value), true);
                    $relatedPerson['nombre'] = $person[0]['personaNombre'];
                    $relatedPerson['apellidoPaterno'] = $person[0]['personaApaterno'];
                    $relatedPerson['apellidoMaterno'] = $person[0]['personaAmaterno'];
                    if($person[0]['personaUrlBio'] == NULL || empty($person[0]['personaUrlBio']))
                        $relatedPerson['url'] = '';
                    else
                        $relatedPerson['url'] = $person[0]['personaUrlBio'];
                    if($person[0]['personaUrlThumb'] == NULL || empty($person[0]['personaUrlThumb']) || basename($person[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                        $relatedPerson['urlThumb'] = '';
                    else
                        $relatedPerson['urlThumb'] = $person[0]['personaUrlThumb'];
                    array_push($relatedPersons, $relatedPerson);
                }
            }
            if(!empty($aC)) {   // Inserta los personajes actuales en el JSON
                foreach ($aC as $key => $value) {
                    $character = json_decode($this->personaGetInfo($value), true);
                    $actualCharacter['nombre'] = $character[0]['personaNombre'];
                    $actualCharacter['apellidoPaterno'] = $character[0]['personaApaterno'];
                    $actualCharacter['apellidoMaterno'] = $character[0]['personaAmaterno'];
                    if($character[0]['personaUrlBio'] == NULL || empty($character[0]['personaUrlBio']))
                        $actualCharacter['url'] = $character[0]['personaUrlBio'];
                    if($character[0]['personaUrlThumb'] == NULL || empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                        $actualCharacter['urlThumb'] = '';
                    else
                        $actualCharacter['urlThumb'] = $character[0]['personaUrlThumb'];
                    array_push($actualCharacters, $actualCharacter);
                }
            }
            if(!empty($rC)) {   // Insera los personajes relacionados en el JSON
                foreach ($rC as $key => $value) {
                    $character = json_decode($this->personaGetInfo($value), true);
                    $relatedCharacter['nombre'] = $character[0]['personaNombre'];
                    $relatedCharacter['apellidoPaterno'] = $character[0]['personaApaterno'];
                    $relatedCharacter['apellidoMaterno'] = $character[0]['personaAmaterno'];
                    if($character[0]['personaUrlBio'] == NULL || empty($character[0]['personaUrlBio']))
                        $relatedCharacter['url'] = $character[0]['personaUrlBio'];
                    if($character[0]['personaUrlThumb'] == NULL || empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                        $relatedCharacter['urlThumb'] = '';
                    else
                        $relatedCharacter['urlThumb'] = $character[0]['personaUrlThumb'];
                    array_push($relatedCharacters, $relatedCharacter);
                }
            }
        }
        if(empty($actualPersons))
            $json['personasActuales'] = $emptyArray;
        else
            $json['personasActuales'] = $actualPersons;
        if(empty($relatedPersons))
            $json['personasRelacionadas'] = $emptyArray;
        else
            $json['personasRelacionadas'] = $relatedPersons;
        if(empty($actualCharacters))
            $json['personajesActuales'] = $emptyArray;
        else
            $json['personajesActuales'] = $actualCharacters;
        if(empty($relatedCharacters))
            $json['personajesRelacionados'] = $emptyArray;
        else
            $json['personajesRelacionados'] = $relatedCharacters;
        // Programa Curiosidades
        $getCuriousFacts = json_decode($this->programaGetCuriousFacts($programaId), true);
        $curiousFact = array(); // Array de hechos curiosos
        $curiousFacts = array();    // Array final de hechos curiosos
        if(!empty($getCuriousFacts)) {
            foreach($getCuriousFacts as $key => $value) {
                $curiousFact['contenido'] = $value['programacuriosidadesContenido'];
                array_push($curiousFacts, $curiousFact);
            }
        }
        $json['curiosidades'] = $curiousFacts;
        $folder = 'programas';
        $jsonStart = 'programa(';
        $jsonEnd = ');';
        $path = filejson . $folder;
        $file = $path . '/' . URLify::filter($name) . '.json';
        if(!file_put_contents($file, $jsonStart . json_encode($json) . $jsonEnd))
            throw new RuntimeException('No se pudo generar el archvio JSON en la ruta indicada.');
        if(SEND_TO_AWS)
            $this->sendToAws($file, $folder, 'json');
    }

    /**
     * Delete JSON
     * 
     * Borra el archivo JSON del programa con base en su Id
     * @param int $programaId
     * @version original: 2016/12/01 10:09 | modified:
     */
    public function deleteJson($programaId) {
        try {
            $result = $this->db->select('SELECT
                                                programaTitulo
                                           FROM
                                                programa
                                          WHERE
                                                programaId
                                              =
                                                :programaId',
                                        array('programaId' => $programaId));
        }
        catch(RuntimeException $e) {
            die('Error al obtener la información del programa.');
        }
        // Nombre completo
        $name = $result[0]['programaTitulo'];
        $name = trim($name);
        $folder = 'programas';
        $path = filejson . $folder;
        $file = $path . '/' . URLify::filter($name) . '.json';
        if(file_exists($file)) {    
            if(!unlink($file))
                throw new RuntimeException('No se pudo eliminar el archivo JSON en la ruta indicada.');
        }
        if(SEND_TO_AWS)
            $this->removeFromAws($file, $folder, 'json');
    }
	
	/**
     * Rename File
     *
     * Modifica y renombra el archivo y regresa ese nombre para ser insertado en la base de datos, si no viene del bucket de AWS lo deja igual
     * @param string $fileName URL del archivo de imagen
     * @param string $name nombre con el que se sustituirá el nombre del archivo
     * @return string $newFileName nueva URL del archivo con el nombre
     * @version original: 2014/10/20 16:43 | modified:
     */
    public function renameFile($fileName, $name) {
        $serverHost = parse_url($fileName);
        $newFileName = "";
        if(preg_match("/".AWS_BUCKET."/", $serverHost["host"])) {
            $path = parse_url($fileName);
            $pathPieces = explode("/", $path["path"]);
            $newName = URLify::filter($name, false);
            $pathInfo = pathinfo($path["path"]);
            $extension = strtolower($pathInfo["extension"]);
            $arrayNewUrl = array($path["scheme"].":/", $path["host"], IMG_PROGRAM_UPLOAD_DIR.$newName.".".$extension);
            $newFileName = implode("/", $arrayNewUrl);
            $dir = escapeshellarg(IMG_PROGRAM_UPLOAD_DIR);
            $oldName = escapeshellarg($pathInfo["basename"]);
            $newNameWithExt = escapeshellarg($newName.".".$extension);
            $ext = escapeshellarg($extension);
            $resval = array();
            $command = PHP_PATH . ' "' . DIR_PATH . 'rename_file_from_aws.php" '. $dir .' '. $oldName .' '. $newNameWithExt .' '. $ext .'';
            exec($command, $resval);
        }
        else 
            $newFileName = $fileName;
        return $newFileName;
    }

    /**
     * Is Image
     *
     * Determina si una URL está disponible vía CURL
     * @param int $personaId
     * @return bool $ret booleano con el resultado de la verificación
     * @version orginal 2014/10/13 11:54 | modified: 
     */
    public function isImage($programaId) {
        $array = json_decode($this->programaGetInfo($programaId), true);
        $url = $array[0]['programaThumbnail'];
        if($url != "") {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_NOBODY, true);
            $result = curl_exec($curl);
            $ret = false;
            $contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
            if($contentType != NULL) {
                preg_match('@([\w/+]+)(;\s+charset=(\S+))?@i', $contentType, $matches);
                $validTypes = array(
                    'jpg'  => 'image/jpeg',
                    'jpeg' => 'image/jpeg',
                    'png'  => 'image/png',
                    'gif'  => 'image/gif',
                );
                $imageStatus = array_search($matches[0], $validTypes);
                if ($result !== false) {
                    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
                    if ($statusCode == 200 && $imageStatus)
                        $ret = true;   
                }
                curl_close($curl);
            }
            return $ret;
        }
    }
    
    /**
     * Get Image Info
     *
     * Obtiene el nombre, el peso y el tipo de una imagen por su URL
     * @param int $personaId
     * @return string json $imageInfo json con los datos de la imagen más un boolean validando que es una imagen (isimage, length, type, name)
     * @version original: 2014/10/13 11:58 | modified: 2014/11/10 10:03
     */
    public function getImageInfo($programaId) {
        $array = json_decode($this->programaGetInfo($programaId), true);
        $data = $array[0]['programaThumbnail'];
        if($data != "" && $this->isImage($programaId) == true) {
            $imageUrl = $data;
            $fileName = basename($data);
            $contentLength = 0;
            $ch = curl_init($data);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $data = curl_exec($ch);
            curl_close($ch);
            if (preg_match('/Content-Length: (\d+)/', $data, $matches))
                $contentLength = (int)$matches[1]; // Contiene el tamaño del archivo en bytes
            if (preg_match('/Content-Type: (.*)/', $data, $matches))
                $contentType = $matches[1];
            if($contentLength === NULL || $contentLength === 0 || $contentLength === "") {
                ob_start();
                readfile($imageUrl);
                $imageData = ob_get_contents();
                $length = strlen($imageData);
                ob_end_clean();
                $contentLength = $length;
            }
            $contentType = trim(preg_replace('/\s+/', ' ', $contentType));
            $imageInfo = array('response' => 'isimage', 'length' => $contentLength, 'type' => $contentType, 'name' => $fileName);
            return json_encode($imageInfo);
        }
    }
	
	/**
     * Check Person Id
     * 
     * Verifica si el id que se manda está en la base de datos
     * @param int $personId
     * @return bool $result
     * @version original: 2016/11/21 14:21 | modified:
     */
    public function checkPersonId($personaId) {
        $personaId = filter_var($personaId, FILTER_SANITIZE_NUMBER_INT);
        $personaId = trim($personaId);
        try {
            $result = $this->db->select('SELECT
                                                personaId
                                           FROM 
                                                persona
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }
	
	/**
     * Check Person
     * 
     * Comprueba si ya existe la persona en la base de datos
     * @param string $name
     * @return bool
     * @version original: 2016/11/21 16:46 | modified: 
     */
    public function checkPerson($name) {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $name = trim($name);
        try {
            $result = $this->db->select('SELECT
                                                CONCAT(personaNombre, \' \', personaApaterno, \' \', personaAmaterno) as personaNombre
                                           FROM 
                                                persona
                                          WHERE
                                                personaNombre
                                              =
                                                "' . $name . '"
                                        COLLATE utf8_spanish_ci');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }

    /**
     * Check Programa Id
     * 
     * Verifica si el id que se manda está en la base de datos
     * @param int $programaId
     * @return bool $result
     * @version original: 2016/11/29 10:36 | modified:
     */
    public function checkProgramaId($programaId) {
        $programaId = filter_var($programaId, FILTER_SANITIZE_NUMBER_INT);
        $programaId = trim($programaId);
        try {
            $result = $this->db->select('SELECT
                                                programaId
                                           FROM 
                                                programa
                                          WHERE
                                                programaId
                                              =
                                                "' . $programaId . '"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }

    /**
     * Check Persona Personaje
     * 
     * Verifica si existe la relación persona y personaje
     * @param int $personaId
     * @param int $personajeId
     * @return bool $result
     * @version original: 2016/10/26 19:43 | modified:
     */
    public function checkPersonaPersonaje($personaId, $personajeId) {
        $personaId = filter_var($personaId, FILTER_SANITIZE_NUMBER_INT);
        $personaId = trim($personaId);
        $personajeId = filter_var($personajeId, FILTER_SANITIZE_NUMBER_INT);
        $personajeId = trim($personajeId);
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM 
                                                personaprograma
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"
                                            AND
                                                personajeId
                                              =
                                                "' . $personajeId .'"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }
	
    /**
     * Programa Get Info
     * 
     * Obtiene la información del programa dependiendo de su id
     * @param int $programaId
     * @return json
     * @version original 2016/11/01 18:49 | modified: 
     */
    public function programaGetInfo($programaId){
        return json_encode($this->db->select('SELECT * FROM programa WHERE programaId = :programaId', 
                                            array('programaId' => $programaId)));
    }

    /**
     * Programa Get Curiosidades
     *
     * Obtiene las curiosidades del programa
     * @param int $programaId
     * @return string json
     * @version original: 2016/11/24 10:36 | modified: 
     */
     public function programaGetCuriousFacts($programaId) {
        return json_encode($this->db->select('SELECT * FROM programacuriosidades WHERE programaId = :programaId',
                                            array('programaId' => $programaId)));
    }

    /**
     * Programa Get Relations
     * 
     * Obtiene la relación dependiendo del id del programa
     * @param int $programaId
     * @return json
     * @version original 2016/11/24 18:58 | modified: 2017/01/24
     */
    public function programaGetRelations($programaId) {
        return json_encode($this->db->select('SELECT * FROM personaprograma WHERE programaId = :programaId',
                                           array('programaId' => $programaId)));
    }

    /**
     * Persona Get Info
     * 
     * Obtiene la información de la persona o personaje dependiendo de su id
     * @param int $personaId
     * @return json
     * @version original: | modified: 
     */
    public function personaGetInfo($personaId){
        return json_encode($this->db->select('SELECT * FROM persona WHERE personaId = :personaId', 
                                            array('personaId' => $personaId)));
    }

    /**
     * Execute Py 
     * 
     * Ejecuta el llamado a Python para mandar el archivo a Google
     * @param string $nameXml
     * @param string $channel
     * @version original: 2016/10/23 16:58 | modified: 
     */
    public function executePy($nameXml, $channel) {
        $log = 'logs/execute.log';  // Log de resultado de la ejecución
        $nameXml = dirname($_SERVER['SCRIPT_FILENAME']) . '/' . $nameXml;
        $serverGsa['google03'] = '216.250.129.15:19900';
        $serverGsa['google04'] = '216.250.129.16:19900';
        while(list($server, $ipServer) = each($serverGsa)) {
            $resVal = array();
            $command = PYTHON_PATH . ' ' . PUSHFEED_PATH . ' --datasource="' . $channel . '" --feedtype="incremental" --url="http://' . $ipServer . '/xmlfeed" --xmlfilename="' . $nameXml . '"';
            exec($command, $resVal);
            if(isset($resVal[0]) && $resVal[0] == 'Success')
                $message = '[' . date('D, d M Y G:i:s T') . '] Insertado en ' . $server . PHP_EOL;
            else
                $message = '[' . date('D, d M Y G:i:s T') . '] Fallo [' . $resVal[0] . ']: ' . $command . PHP_EOL;
            file_put_contents($log, $message, FILE_APPEND);
        }
    }
    
    /**
     * Send To AWS
     * 
     * Envía un archivo al bucket de AWS
     * @param string $file
     * @param string $category
     * @param string $fileType
     * @version original: 2016/10/23 17:14 | modified: 
     */
    public function sendToAws($file, $category, $fileType) {
        $log = 'logs/sendToAws.log';  // Log de resultado de la ejecución
        $file = escapeshellarg($file);
        $category = escapeshellarg($category);
        $fileType = escapeshellarg($fileType);
        $resVal = array();
        $command = PHP_PATH . ' "' . DIR_PATH . 'send_to_aws.php" ' . $file . ' ' . $category . ' ' . $fileType . '';
        exec($command, $resVal);
        if(isset($resVal[0]))
            $message = '[' . date('D, d M Y G:i:s T') . '] Falla [' . $resVal[0] . ']: ' . $command . PHP_EOL;
        else
            $message = '[' . date('D, d M Y G:i:s T') . ']: ' . $command . PHP_EOL;
        file_put_contents($log, $message, FILE_APPEND);
    }

    /**
     * Remove From AWS
     * 
     * Borra un archivo al bucket de AWS
     * @param string $file
     * @param string $category
     * @param string $fileType
     * @version original: 2016/12/07 12:17 | modified: 
     */
    public function removeFromAws($file, $category, $fileType) {
        $log = 'logs/removeFromAws.log';  // Log de resultado de la ejecución
        $file = escapeshellarg($file);
        $category = escapeshellarg ($category);
        $fileType = escapeshellarg($fileType);
        $resVal = array();
        $command = PHP_PATH . ' "' . DIR_PATH . 'remove_from_aws.php" ' . $file . ' ' . $category . ' ' . $fileType . '';
        exec($command, $resVal);
        if(isset($resVal[0]))
            $message = '[' . date('D, d M Y G:i:s T') . '] Falla [' . $resVal[0] . ']: ' . $command . PHP_EOL;
        else
            $message = '[' . date('D, d M Y G:i:s T') . ']: ' . $command . PHP_EOL;
        file_put_contents($log, $message, FILE_APPEND);
    }

    /**
     * Publish
     *
     * Publica al programa desde el Data Table
     * @return json con el resultado del proceso de publicación
     * @version original: 2016/12/06 10:21 | modified: 
     */
    public function publish($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a publicar.',
                                     'values'     => ''));
        else {
            if(!isset($data['id']))
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'No existe el id.',
                                         'values'     => $data));
            else {
                if(empty($data['id']))
                    return json_encode(array('response'   => 'error',
                                             'serversays' => 'El id está vacío.',
                                             'values'     => $data));
                else {
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                    try {
                        $this->db->beginTransaction();
                        $this->db->update('programa', array('programaStatus'            => 0,
                                                            'programaFechaModificacion' => date('Y-m-d H:i:s')), "`programaId` = {$id}");
                        $this->createJSON($id);
                        $this->createSingleXML($id);
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se publicó con éxito.'));
                        return $json;
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
        }
    }

    /**
     * Unpublish
     *
     * Despublica al programa desde el Data Table
     * @return json con el resultado del proceso de despublicación
     * @version original: 2016/12/06 10:26 | modified: 
     */
    public function unpublish($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a despublicar.',
                                     'values'     => ''));
        else {
            if(!isset($data['id']))
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'No existe el id.',
                                         'values'     => $data));
            else {
                if(empty($data['id']))
                    return json_encode(array('response'   => 'error',
                                             'serversays' => 'El id está vacío.',
                                             'values'     => $data));
                else {
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                    try {
                        $this->db->beginTransaction();
                        $this->db->update('programa', array('programaStatus'            => 1,
                                                            'programaFechaModificacion' => date('Y-m-d H:i:s')), "`programaId` = {$id}");
                        $this->createMultipleXML(array($id), 'unpublish');
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se despublicó con éxito.'));
                        return $json;
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
        }
    }

    /**
     * Delete
     *
     * Borra al programa desde el Data Table
     * @return json con el resultado del proceso de eliminación
     * @version original: 2016/12/06 18:56 | modified: 
     */
    public function delete($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a borrar.',
                                     'values'     => ''));
        else {
            if(!isset($data['id']))
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'No existe el id.',
                                         'values'     => $data));
            else {
                if(empty($data['id']))
                    return json_encode(array('response'   => 'error',
                                             'serversays' => 'El id está vacío.',
                                             'values'     => $data));
                else {
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                    try {
                        $this->db->beginTransaction();
                        $this->deleteJSON($id);
                        $this->createMultipleXML(array($id), 'unpublish');
                        $this->db->delete('programacuriosidades', "`programaId` = {$id}");
                        $this->db->update('personaprograma', array('programaId' => NULL), "`programaId` = {$id}");
                        $this->db->delete('programa', "`programaId` = {$id}");
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se ha borrado con éxito.'));
                        return $json;
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
        }
    }

    /**
     * Publish All
     *
     * Publica todos los ids seleccionados desde el Data Table
     * @return json con el resultado del proceso de publicación
     * @version original: 2016/12/06 10:51 | modified: 
     */
    public function publishAll($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a publicar.',
                                     'values'     => ''));
        else {
            $ids = explode(', ', $data['ids']);
            $idsToPublish = array();
            foreach ($ids as $key => $value) {
                $id = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                $id = trim($id);
                if(!empty($id)) {
                    try {
                        $this->db->beginTransaction();
                        $this->db->update('programa', array('programaStatus'            => 0,
                                                            'programaFechaModificacion' => date('Y-m-d H:i:s')), "`programaId` = {$id}");

                        array_push($idsToPublish, $id);
                        $this->createJSON($id);
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se publicó con éxito.'));
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
            if(!empty($idsToPublish)) {
                try {
                    $this->createMultipleXML($idsToPublish, 'publish');
                    return $json;
                }
                catch (RuntimeException $e) {
                    $json = json_encode(array('response'   => 'error',
                                              'serversays' => 'Error al crear el XML múltiple.',
                                              'values'     => $data));
                    return $json;
                }
            }
        }
    }

    /**
     * Unpublish All
     *
     * Despublica todos los ids seleccionados desde el Data Table
     * @return json con el resultado del proceso de despublicación
     * @version original: 2016/12/07 10:39 | modified: 
     */
    public function unpublishAll($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a despublicar.',
                                     'values'     => ''));
        else {
            $ids = explode(', ', $data['ids']);
            $idsToUnpublish = array();
            foreach ($ids as $key => $value) {
                $id = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                $id = trim($id);
                if(!empty($id)) {
                    try {
                        $this->db->beginTransaction();
                        $this->db->update('programa', array('programaStatus'            => 1,
                                                            'programaFechaModificacion' => date('Y-m-d H:i:s')), "`programaId` = {$id}");

                        array_push($idsToUnpublish, $id);
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se despublicó con éxito.'));
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
            if(!empty($idsToUnpublish)) {
                try {
                    $this->createMultipleXML($idsToUnpublish, 'unpublish');
                    return $json;
                }
                catch (RuntimeException $e) {
                    $json = json_encode(array('response'   => 'error',
                                              'serversays' => 'Error al crear el XML múltiple.',
                                              'values'     => $data));
                    return $json;
                }
            }
        }
    }

    /**
     * Delete All
     *
     * Borra todos los ids seleccionados desde el Data Table
     * @return json con el resultado del proceso de eliminación
     * @version original: 2016/12/07 11:14 | modified: 
     */
    public function deleteAll($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a eliminar.',
                                     'values'     => ''));
        else {
            $ids = explode(', ', $data['ids']);
            try {
                $this->createMultipleXML($ids, 'unpublish');
            }
            catch (RuntimeException $e) {
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'Error al crear el XML múltiple.',
                                         'values'     => $data));
            }
            foreach ($ids as $key => $value) {
                try {
                    $this->db->beginTransaction();
                    $this->deleteJSON($value);
                    $this->db->delete('programacuriosidades', "`programaId` = {$value}");
                    $this->db->update('personaprograma', array('programaId' => NULL), "`programaId` = {$value}");
                    $this->db->delete('programa', "`programaId` = {$value}");
                    $this->db->commit();
                }
                catch (RuntimeException $e) {
                    $this->db->rollBack();
                    return json_encode(array('response'   => 'error',
                                             'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                             'values'     => $data));
                }
            }
            return json_encode(array('response'   => 'success',
                                     'serversays' => 'Se eliminó con éxito.'));
        }
    }

    /**
     * Permissions
     * 
     * Función que arma el query dependiendo de los permisos que tenga el usuario
     * @param string originalQuery
     * @param string column
     * @param string where
     * @param string extras
     * @return string query
     * @version orginal: | modified:
     */
    public function permissions($originalQuery, $column, $where, $extras) {
            $permissions = Session::get('permissions');
            if($permissions == 0)
                    $query = $originalQuery;
            else {
                    $pieces = explode("-", $permissions);
                    $piecesNum = count($pieces);
                    if($where)
                            $query = $originalQuery . ' AND (';
                    else
                            $query = $originalQuery . ' WHERE ';
                    if($piecesNum == 1)
                            $query .= $column .' = '.$pieces[0];
                    else {
                            $query .= $column .' = ' . $pieces[0];
                            for($i = 1; $i < $piecesNum; $i++)
                                    $query .= ' OR ' . $column . ' = ' . $pieces[$i];
                    }
                    if($where)
                            $query .= ')';
            }
            if($extras != "" || $extras != NULL)
                    $query .= $extras;

            return $query;
    }
    
    /**
     * Is Ajax
     *
     * Función que comprueba si la petición se realiza desde ajax 
     * @return request
     * @version original: 2014/02/27 16:18 | modified:
     */
    public function isAjax() {
       return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

}