<?php
	//require 'models/program_model.php';
	set_time_limit(0);
include 'branch_model.php';

class Personas_Model extends Model {
	
    public function __construct() 
    {
        parent::__construct();
    }
	public function branchList() {
		$newB = new Branch_Model();
		return $newB->branchList();
	}
	
	public function wiam($paq = 1) {
		return $paq;
	}
    
    public function personasList($paq = 1){
	
		$query = 'SELECT * FROM persona';
		$column = 'personaBranch';
		$where = false;
		$extras = ' order by personaFechaIngreso DESC limit ' . (($paq-1)*limiteM) . ',' . limiteM;	
		
		return $this->db->select($this->permissions($query, $column, $where, $extras));
    }
	
	public function personasLista(){
        return $this->db->select('SELECT * FROM persona order by personaFechaIngreso ASC');
    }
    
	public function programasSeleccionados($id){
		
    	return $this->db->select('select 
									    idprograma, programaTitulo, per.idPersona
									from
									    programa pro
									        inner join
									    programapersona pp ON pp.programa = pro.idprograma
									        inner join
									    persona per ON per.idPersona = pp.persona
									where per.idPersona = '.$id);
    }
    
    public function getElenco($id){
    	return $this->db->select('SELECT 
								    P.personaNom as nombre,
								    P.personaURLBio as urlBio,
								    P.idPersona as idPersona,
								    PP.programa as idPrograma
								FROM
								    persona P
								        inner join
								    programapersona PP ON P.idPersona = PP.persona
								        inner join
								    programa Po ON Po.idprograma = PP.programa
								where
								    idPrograma = '.$id);
    }
    public function programaLista(){
        return $this->db->select('SELECT * FROM programa order by programaFechaIngreso ASC');
    }
    public function personasSeleccionadas($id){
    	return $this->db->select('select 
									    idPersona, personaNom
									from
									    persona
									        inner join
									    programapersona ON programapersona.persona = persona.idPersona
									        inner join
									    programa ON programa.idprograma = programapersona.programa
									where
									    programa.idprograma = '.$id);
    }
    public function getListingsProgramas()
    {
        $result = $this->db->select("SELECT idprograma, programaTitulo FROM programa order by programaFechaIngreso ASC");
        echo json_encode($result);
    }
    public function getPrograma($id){
    	return $this->db->select('SELECT 
								    P.programaTitulo as titulo,
								    P.programaUrl as urlPro,
								    P.idPrograma as idPrograma,
								    PP.persona as idPersona
								FROM
								    programa P
								        inner join
								    programapersona PP ON P.idPrograma = PP.programa
								     inner join
								    persona Po ON Po.idPersona = PP.persona   
								where
								    PP.persona = '.$id);
    }
    
    public function personasSingleList($id){
        return $this->db->select('SELECT * FROM persona WHERE idPersona = :idPersona', 
            array( 'idPersona' => $id));
    }
    

    function programasSelect($id){
        return $this->db->select('SELECT * FROM programaPersona WHERE persona = :persona ',array( 'persona' => $id));        
    }
    
	public function check($values,$valuesP,$type) {
		$result = $this->db->select("SELECT * FROM persona WHERE LOWER(HTML_DECODE(personaNom)) LIKE LOWER(HTML_DECODE('%".trim($values[0])."%'))");
		if(count($result) > 0 && $type[0] != "e")
			return 1;
		else if(count($valuesP) > 1) {
			$arrayLength = count($valuesP)/2;
			if($arrayLength >= 1) {
				$sum = 0;
				for ($i = 0; $i < $arrayLength; $i++) {
					$persona = $this->db->select("SELECT * FROM persona WHERE LOWER(HTML_DECODE(personaNom)) LIKE LOWER(HTML_DECODE('%".trim($valuesP[$i+$sum])."%'))");
					if(count($persona) > 0) {
						return array(2, $valuesP[$i+$sum]);
						exit;
					}
					$programa = $this->db->select("SELECT * FROM programa WHERE LOWER(HTML_DECODE(programaTitulo)) LIKE LOWER(HTML_DECODE('%".trim($valuesP[$i+$sum])."%'))");
					if(count($programa) > 0) {
						return array(3, $valuesP[$i+$sum]);
						exit;
					}
					$sum++;
				}
				return 0;
			}
		}
		else
			return 0;
	}
        
    public function create($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1)
    {  	

    	/*print_r($indices);
		print_r($values);
		print_r($indicesP);
		print_r($valuesP);
		print_r($indicesP1);
		print_r($valuesP1);
		die();*/
		$name = "";
		
    	$data = Array();
		$cData = Array();
    	if (count($indices) == count($values)) {
    		$data = array_combine($indices , $values);
    	}
    	//print_r($data); die('termino todo');
		array_walk($data, function (&$value, $key) {
			if($key == "personaNom") {
				global $name;
				$name = $value;
			}
			else if($key == "personaNacimiento" && ($value != "" || $value != NULL))
				$value = date('Y-m-d', strtotime($value));
			else if($key == "personaNacimiento" && $value == "")
				$value = "0000-00-00";
			else if($key == "personaUrlThumb" && ($value != "" || $value != NULL)) {
				global $name;
				$value = $this->renameFile($value, $name);
				if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $value))
					$value = $this->changeImageDomain($value);
			}
		});
		$cData = $data;
		$this->stripAllFields($data);
    	$this->db->insert('persona', $data);
		$idPersona = array('idPersona' => intval($this->db->lastInsertId()));
    	$this->createAP($indices, $values, $indicesP, $valuesP, $indicesP1, $valuesP1, $idPersona);
		$this->userLog(Session::get('user'), 'persona', 'crea', $cData["personaNom"], 'ok');
    }
    
    public function createAP ($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idPersona)
    {
		$data = array();
    	$dataP = array();
    	$nomPer = array();
    	$personaUrlBio = array();
    	$dataM = array();
    	$cont1 = 0;
    	$cont2 = 0;
    	$dataM = array();
		$serializedData = array();
    
    	if($valuesP[0] != '' || $valuesP[0] != 0){
    		for($m = 0; $m<count($valuesP); $m++){
    			if($m%2 == 0){
    				$nomPer[$cont1] = $valuesP[$m];
    				$cont1++;
    			}else{
    				$personaUrlBio[$cont2] = $valuesP[$m];
    				$cont2++;
    			}
    		}
    		 
    		for($l = 0;$l<count($valuesP)/2; $l++){
    			$dataM[] = array('programaTitulo'=>$nomPer[$l],'programaUrl'=>$personaUrlBio[$l],'programaFechaIngreso'=>date('Y-m-d H:i:s'),'programaBranch'=>$values[1]);
    		}
    		for($i = 0; $i<count($dataM);$i++){
    
    			$this->db->insert('programa', $dataM[$i]);
    			$dat = array("idPrograma" => intval($this->db->lastInsertId()));
    			$dataR1 = array('programa'=>$dat["idPrograma"] , 'persona'=>$idPersona['idPersona']);
				$this->createRelation($dataR1);
    		}
			$serializedData = $nomPer;
			array_push($serializedData, $idPersona['idPersona']);
			$this->userLog(Session::get('user'), 'persona', 'crea relaciones', serialize($serializedData), 'ok');
    	}
    	if($valuesP1 != '' && $valuesP1[0] != 0 && $idPersona != '' ){
    		for($i = 0; $i<count($valuesP1); $i++){
    			$dataR = array('programa'=>$valuesP1[$i], 'persona'=>$idPersona['idPersona']);
				$d = $this->createRelation($dataR);
    		}
    	}
    }
    
           
    function createRelation($data){
    	$this->db->insert('programapersona',$data);
    	//$datas = array('programa' => intval($this->db->lastInsertId()));
    	//print_r($datas); die();
    	//return $data;
    }
    
    public function editSave($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idP){
		
		/*print_r($indices);
		print_r($values);
		print_r($indicesP);
		print_r($valuesP);
		print_r($indicesP1);
		print_r($valuesP1);
		die();*/
		$name = "";
		
		if (count($indices) == count($values)) {
    		$data = array_combine($indices , $values);
    	}
		
		array_walk($data, function (&$value, $key) {
			if($key == "personaNom") {
				global $name;
				$name = $value;
			}
			else if($key == "personaNacimiento" && ($value != "" || $value != NULL))
				$value = date('Y-m-d', strtotime($value));
			else if($key == "personaNacimiento" && $value == "") {
				$value = "0000-00-00";
			}
			else if($key == "personaUrlThumb" && ($value != "" || $value != NULL)) {
				global $name;
				$value = $this->renameFile($value, $name);
				if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $value))
					$value = $this->changeImageDomain($value);
			}
		});
		
		$this->stripAllFields($data);
		//print_r($data); die("termin� todo");
    	$this->db->update('persona',$data,'idPersona = '.$idP);
		$this->userLog(Session::get('user'), 'persona', 'actualiza', $data['personaNom'], 'ok');
    	//P1 son aquellos que ya existen.
    	//P son nuevos a insertar.
    	if(($valuesP1 != '') || ($valuesP != '' && $valuesP[0] != '')){
    		$this->updateA($values, $indicesP,$valuesP,$indicesP1,$valuesP1,$idP);
    	}
		//$this->generateJson();
    }
    
    
    public function updateA($values, $indicesP,$valuesP,$indicesP1,$valuesP1,$idP){
    	$this->db->deleteAll('programapersona','persona = '.$idP);

    	$data = array();
    	$dataP = array();
    	$nomPer = array();
    	$personaUrlBio = array();
    	$dataM = array();
    	$cont1 = 0;
    	$cont2 = 0;
    	$dataM = array();
		$serializedData = array();
		
    	if($valuesP[0] != '' || $valuesP[0] != 0){
    		for($m = 0; $m<count($valuesP); $m++){
    			if($m%2 == 0){
    				$nomPer[$cont1] = $valuesP[$m];
    				$cont1++;
    			}else{
    				$personaUrlBio[$cont2] = $valuesP[$m];
    				$cont2++;
    			}
    		}
    		 
    		for($l = 0;$l<count($valuesP)/2; $l++){
    			$dataM[] = array('programaTitulo'=>$nomPer[$l],'programaUrl'=>$personaUrlBio[$l],'programaFechaIngreso'=>date('Y-m-d H:i:s'), 'programaBranch'=>$values[2]);
    		}
    		for($i = 0; $i<count($dataM);$i++){
    	
    			$this->db->insert('programa', $dataM[$i]);
    			$dat = array("idPrograma" => intval($this->db->lastInsertId()));
    			 
    			$dataR1 = array('programa'=>$dat["idPrograma"] , 'persona'=>$idP);
    			$this->createRelation($dataR1);
    		}
			$serializedData = $nomPer;
			array_push($serializedData, $idP);
			$this->userLog(Session::get('user'), 'persona', 'actualiza relaciones', serialize($serializedData), 'ok');
    	}
    	if($valuesP1 != '' && $valuesP1[0] != 0 && $idP != '' ){
    		for($i = 0; $i<count($valuesP1); $i++){
    			$dataR = array('programa'=>$valuesP1[$i], 'persona'=>$idP);
    			$d = $this->createRelation($dataR);
    		}
    	}
    }
    
    
   
    
    public function search($columna, $valor)
    {
		$query = 'SELECT * FROM persona WHERE ';
		$column = 'personaBranch';
		$where = true;
		$extras = '';
		if(count($columna) == 0 || count($columna) == 1){
    		if($columna[0] == "personaNom")
				$query.= "LOWER(HTML_DECODE(". $columna[0] .")) LIKE LOWER(HTML_DECODE('%".$valor[0]."%')) ";
			else
				$query.= "LOWER(". $columna[0] .") LIKE LOWER('%".$valor[0]."%') ";
    	}
		else if(count($columna) > 1){
    		if($columna[0] == "personaNom")
				$query.= "LOWER(HTML_DECODE(". $columna[0] .")) LIKE LOWER(HTML_DECODE('%".$valor[0]."%')) ";
			else	
				$query.= "LOWER(". $columna[0] .") LIKE LOWER('%".$valor[0]."%') ";
    		for($i = 1; $i<count($columna); $i++){
				$query.= "AND ".$columna[$i]." LIKE LOWER('%".$valor[$i]."%')";
    		}
    	}
		/*$consulta = "SELECT * FROM persona where ";
    	if(count($columna) == 0 || count($columna) == 1){
    		$consulta.= "LOWER(". $columna[0] .") like LOWER('%".$valor[0]."%') ";
    
    	}else if(count($columna)>1){
    		$consulta.= "LOWER(". $columna[0] .") like LOWER('%".$valor[0]."%') ";
    		for($i = 1; $i<count($columna); $i++){
    			$consulta.= "and ".$columna[$i]." like LOWER('%".$valor[$i]."%')";
    		}
    	}*/
    	 
    	$valo = $this->db->select($this->permissions($query, $column, $where, $extras));
    	return json_encode($valo);
    
    }
     
    public function nPaginar(){
		$query = 'SELECT count(*) as total FROM persona';
		$column = 'personaBranch';
		$where = false;
		$extras = '';
		
    	$v = $this->db->select($this->permissions($query, $column, $where, $extras)) ;
    	$total = $v[0]["total"];
    	$paginas = ceil($total/limiteM);
    	return $paginas;
    }
	
	/*
	 * Ejecuta el llamado a Python para mandar el archivo a Google
	 */
	public function executePy($name_xml, $channel) {
		$log = 'logs/execute.log';
		$name_xml = dirname($_SERVER['SCRIPT_FILENAME'])."/".$name_xml;
		$server_gsa["google03"] = '216.250.129.15:19900';
		$server_gsa["google04"] = '216.250.129.16:19900';
		while(list($server, $ip_server) = each($server_gsa)) {
			$retval = array();
			$command = 'C://Python27//python.exe C://Python27//pushfeed_client.py --datasource="'.$channel.'" --feedtype="incremental" --url="http://'.$ip_server.'/xmlfeed" --xmlfilename="'.$name_xml.'"';
			//exec('/usr/bin/python /usr/local/apache2/htdocs/python/pushfeed_client.py --datasource="'.$channel.'" --feedtype="incremental" --url="http://'.$ip_server.'/xmlfeed" --xmlfilename="'.$name_xml.'"', $retval);
			exec($command, $retval);
			if(isset($retval[0]) && $retval[0]=="Success") {
				$mensaje = "[".date("D, d M Y G:i:s T")."] Insertado en ".$server.PHP_EOL;
				file_put_contents($log, $mensaje, FILE_APPEND);
			}
			else {
				$mensaje = "[".date("D, d M Y G:i:s T")."] Fallo [".$retval[0]."]: ".$command.PHP_EOL;
				file_put_contents($log, $mensaje, FILE_APPEND);
			}
			$this->userLog(Session::get('user'), 'persona', 'ejecuta Python', $name_xml, $retval[0]);
		}
	}
    
    public function generateXml($tipo, $id){
		$ids = array();
		$route = "personajes/";
    	$query = '';
    	if($tipo == 'nuevo'){
    		$queryA = $this->db->select("SELECT date(personaFechaIngreso) as fecha
									FROM persona group by date(personaFechaIngreso)
									order by personaFechaIngreso DESC limit 0,1;");
    		$query = $this->db->select("SELECT * FROM persona where date(personaFechaIngreso) = '".$queryA[0]["fecha"]."';");
    	}else if($tipo == 'editar'){
    		$query = $this->db->select("SELECT * FROM persona where idPersona = ".$id);
    	}
    //	print_r(strip_tags(html_entity_decode("&#13;&#10;c&oacute;locar"))); 
    
      //  die('termino');
    
    	//$dom     = new DOMDocument("1.0","ISO-8859-1");
		$imp 	 = new DOMImplementation;
		$dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
		$dom 	 = $imp->createDocument("", "", $dtd);
		$dom 	-> encoding = "ISO-8859-1";
		$dom    -> formatOutput = true;
		
    	$node    = $dom->createElement("gsafeed");
    	$header  = $dom->createElement("header");
    	$data    = $dom->createElement("datasource");
    	$feed    = $dom->createElement("feedtype");
    	$data   -> nodeValue = "personas";
    	$feed   -> nodeValue = 'metadata-and-url';
    	$header -> appendChild($data);
    	$header -> appendChild($feed);
    	$node   -> appendChild($header);
    	$group     = $dom->createElement("group");
    	$node   -> appendChild($group);
    	$dom    -> appendChild($node);
    	$tituloG = '';
    	 
    	$nombres = array(1=>'nombre_persona',
				2=>'url_biografia',
    			3=>'resumen_biografia',
    			4=>'thumbnail_persona',
				5=>'keywords',
    			6=>'nombre_programa_actual',
    			7=>'url_programa_actual',
				8=>'url_noticias',
				9=>'url_fotos',
				10=>'url_videos',
    			11=>'url_facebook',
				12=>'url_twitter',
				13=>'url_youtube',
				14=>'url_instagram',
				15=>'url_pinterest',
				16=>'url_gplus',
				17=>'content',
				18=>'consulta_relacionada',
				19=>'fecha_nacimiento'
    	);
    	$nombreTabla = array( 1=>'personaNom',
				2=>'personaUrlBio',
    			3=>'personaResumenBio',
    			4=>'personaUrlThumb',
				5=>'personaKeywords',
    			6=>'personaNomProgramaAct',
    			7=>'personaUrlProgramActual',
				8=>'personaUrlNoticias',
				9=>'personaUrlFotos',
				10=>'personaUrlVideos',
    			11=>'personaUrlFacebook',
				12=>'personaUrlTwitter',
				13=>'personaUrlYoutube',
				14=>'personaUrlInstagram',
				15=>'personaUrlPinterest',
				16=>'personaUrlGplus',
    			17=>'personaContent',
				18=>'personaConsultRelacionada',
				19=>'personaNacimiento'
    	);
    
    	for($i = 0; $i<count($query); $i++){
			array_push($ids, $query[$i]['idPersona']);			
			$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[$i]['personaBranch']);
			$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
			
    		$tituloG = $query[$i]['personaNom'];
    		
    		$record    = $dom->createElement('record');
    		$group     -> appendChild($record);
    		$metadata  = $dom->createElement('metadata');
    		$content   = $dom->createElement('content');
    		$record    -> appendChild($metadata);
    		$record    -> appendChild($content);
			$content   -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($query[$i]['personaContent']))));
    		$record    -> setAttribute('url','http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[$i]['personaNom']).'/');
    		$record    -> setAttribute('action','add');
    		$record    -> setAttribute('mimetype','text/html');
    		$record    -> setAttribute('lock','true');
    		$record    -> setAttribute('last-modified',date("D, d M Y G:i:s")." CDT");
    			
    		for($j = 1; $j<=count($nombres);$j++){
				if(strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == "" || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == " " || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == NULL || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == "0000-00-00") {
					continue;
				}				
				else {
					$meta      = $dom->createElement('meta');
					$metadata  ->appendChild($meta);
					$meta      -> setAttribute('name',$nombres[$j]);
					$meta      -> setAttribute("content",strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])));
				}
    		}
    		
    		$elenco =  $this->getPrograma($query[$i]['idPersona']);
    			
    		if(count($elenco)>0){
    			$cont = 0;
    			for($k = 0; $k<count($elenco);$k++){
    				if($query[$i]['idPersona'] == $elenco[$k]["idPersona"]){
    					$cont++;
    				}else{
    					$cont = 1;
    				}
    				$meta      = $dom->createElement('meta');
    				$metadata  -> appendChild($meta);
    				$meta      -> setAttribute('name','programa_relacionado_'.$cont);
    				$meta      -> setAttribute('content',strip_tags(html_entity_decode($elenco[$k]["titulo"])));
					if($elenco[$k]["urlPro"] == "" || $elenco[$k]["urlPro"] == NULL)
						continue;
					else {
						$meta      = $dom->createElement('meta');
						$metadata  ->appendChild($meta);
						$meta      -> setAttribute("name",'url_programa_relacionado_'.$cont);
						$meta      -> setAttribute("content",$elenco[$k]["urlPro"]);
					}
    			}
    		}	
    		$meta      = $dom->createElement('meta');
			$metadata  ->appendChild($meta);
			$meta      -> setAttribute('name','seed_content');
			$meta      -> setAttribute("content",'persona');	
    	}
    		
    	$xml = $dom->saveXML();
    	 
    	$path = filexml.'personas';
    	if($tipo == 'nuevo'){
			for($i = 0; $i<count($ids);$i++){
				$this->publish($ids[$i]);
			}
    		$nameFile = 'persona.xml';
    	}else if($tipo = 'editar'){
    		$tituloG = $this->limpiarCadena($tituloG);
    		$nameFile = $tituloG.'.xml';
			$this->publish($id);
    	}
    	$file = $path. "/" . $nameFile;
    	if(file_put_contents($file, $xml)){
			$this->userLog(Session::get('user'), 'persona', 'crea XML', $file, 'ok');
			if(SEND_TO_GOOGLE) {
				echo 'Ok';
				$this->executePy($file, "personas");
				if(SEND_TO_AWS) {
					$file = escapeshellarg($file);
					$category = escapeshellarg("personas");
					$fileType = escapeshellarg("xml");
					$resval = array();
					$command = '/usr/bin/php "/var/www/html/send_to_aws.php" '. $file .' '. $category .' '. $fileType .'';
					exec($command, $resval);
				}
				//unlink($file);
			}
			
    	}else {
    		echo 'false';
			$this->userLog(Session::get('user'), 'persona', 'crea XML', $file, 'false');
    	}
    
    }
    
    public function generateXmlEliminados($ids){
    	$route = "personajes/";
    	for($i = 0; $i<count($ids);$i++){
    		$query[$i] = $this->db->select("SELECT * FROM persona where idPersona = ".$ids[$i]);
    	}
    	//$dom     = new DOMDocument("1.0","ISO-8859-1");
    	$imp 	 = new DOMImplementation;
		$dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
		$dom 	 = $imp->createDocument("", "", $dtd);
		$dom 	-> encoding = "ISO-8859-1";
		$dom    -> formatOutput = true;
		
    	$node    = $dom->createElement("gsafeed");
    	$header  = $dom->createElement("header");
    	$data    = $dom->createElement("datasource");
    	$feed    = $dom->createElement("feedtype");
    	$data   -> nodeValue = "personas";
    	$feed   -> nodeValue = 'metadata-and-url';
    	$header -> appendChild($data);
    	$header -> appendChild($feed);
    	$node   -> appendChild($header);
    	$group     = $dom->createElement("group");
    	$group  -> setAttribute('action',"delete");
    	 
    	$node   -> appendChild($group);
    	$dom    -> appendChild($node);
    	$tituloG = '';
    	 
    	$nombres = array(1 =>'url_biografia');
    	$nombreTabla = array(1 =>'personaNom');
    	if(count($query)>1){
    		 
    	for($j = 0; $j<count($query); $j++){
		
			for($i = 0; $i<count($query[$j]); $i++){
			
				$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[$j][$i]['personaBranch']);
				$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
					
				$record    = $dom->createElement('record');
				$group     -> appendChild($record);	
				for($l = 1; $l<=count($nombres);$l++){
					$record -> setAttribute("url",'http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[$j][$i][$nombreTabla[$l]]).'/');
					$record -> setAttribute('mimetype',"text/plain");
				}
			}	
    	}
    	}else{
			$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[0][0]['personaBranch']);
			$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
    		$tituloG = $query[0][0]['personaNom'];
    		$record    = $dom->createElement('record');
    		$group     -> appendChild($record);
    		for($k = 1; $k<=count($nombres);$k++){
    			$record -> setAttribute("url",'http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[0][0][$nombreTabla[$k]]).'/');
    			$record -> setAttribute('mimetype',"text/plain");
    		}
    	}
    	    		
    	//$xml = $doctype.$dom->saveXML();
    	$xml = $dom->saveXML();
    	 
    	$path = filexml.'personas';
    	$nameFile = 'PersonasEliminadas.xml';
    	
    	$file = $path. "/" . $nameFile;
    	if(file_put_contents($file, $xml)){
			for($i = 0; $i < count($ids); $i++) {
				$name = $this->personasSingleList($ids[$i]);
				$array[$ids[$i]] = $name[0]["personaNom"];
			}
			for($i = 0; $i<count($ids);$i++){
    			$this->onlySave($ids[$i]);
    		}
			echo 'Ok';
    		$this->userLog(Session::get('user'), 'persona', 'crea XML eliminados', json_encode($array), 'ok');
			if(SEND_TO_AWS) {
				$fileToSend = escapeshellarg("/var/www/html/".$file);
				$category = escapeshellarg("personas");
				$fileType = escapeshellarg("xml");
				$resval = array();
				$command = '/usr/bin/php /var/www/html/send_to_aws.php '. $fileToSend .' '. $category .' '. $fileType .'';
				exec($command, $resval);
			}
			if(SEND_TO_GOOGLE)
				$this->executePy($file, "personas");
    	}else {
    		echo 'false';
			$this->userLog(Session::get('user'), 'persona', 'crea XML eliminados', serialize($ids), 'false');
    	}
        
    }
    
	public function onlySave($id) {
		$this->db->update('persona',array("personaStatus" => 1),'idPersona = '.$id);
		$this->userLog(Session::get('user'), 'persona', 'despublica', $id, 'ok');
	}
	
	public function publish($id) {
		$this->db->update('persona',array("personaStatus" => 0),'idPersona = '.$id);
		$this->userLog(Session::get('user'), 'persona', 'publica', $id, 'ok');
	}
	
	public function permanentlyDeleted($ids){
		$this->generateXmlEliminados($ids);
		for($i = 0; $i < count($ids); $i++) {
			$name = $this->personasSingleList($ids[$i]);
			$array[$ids[$i]] = $name[0]["personaNom"];
		}
		for($i = 0; $i<count($ids);$i++){
    		$this->delete($ids[$i]);
    	}
		$this->userLog(Session::get('user'), 'persona', 'elimina permanentemente', json_encode($array), 'ok');
	}
	
    public function delete($id){
    
    	$this->db->delete('persona', "`idPersona` = ".$id);
    
    }
    
    public function generateXmlVarios($ids){
		$route = "personajes/";
    	for($i = 0; $i<count($ids);$i++){
    		$query[$i] = $this->db->select("SELECT * FROM persona where idPersona = ".$ids[$i]);
    	}
    	   
    	$imp 	 = new DOMImplementation;
		$dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
		$dom 	 = $imp->createDocument("", "", $dtd);
		$dom 	-> encoding = "ISO-8859-1";
		$dom    -> formatOutput = true;
		
    	$node    = $dom->createElement("gsafeed");
    	$header  = $dom->createElement("header");
    	$data    = $dom->createElement("datasource");
    	$feed    = $dom->createElement("feedtype");
    	$data   -> nodeValue = "personas";
    	$feed   -> nodeValue = 'metadata-and-url';
    	$header -> appendChild($data);
    	$header -> appendChild($feed);
    	$node   -> appendChild($header);
    	$group     = $dom->createElement("group");
    	$node   -> appendChild($group);
    	$dom    -> appendChild($node);
    	$tituloG = '';
    	
    	$nombres = array(1=>'nombre_persona',
				2=>'url_biografia',
    			3=>'resumen_biografia',
    			4=>'thumbnail_persona',
				5=>'keywords',
    			6=>'nombre_programa_actual',
    			7=>'url_programa_actual',
				8=>'url_noticias',
				9=>'url_fotos',
				10=>'url_videos',
    			11=>'url_facebook',
				12=>'url_twitter',
				13=>'url_youtube',
				14=>'url_instagram',
				15=>'url_pinterest',
				16=>'url_gplus',
				17=>'content',
				18=>'consulta_relacionada',
				19=>'fecha_nacimiento'
    	);
    	$nombreTabla = array( 1=>'personaNom',
				2=>'personaUrlBio',
    			3=>'personaResumenBio',
    			4=>'personaUrlThumb',
				5=>'personaKeywords',
    			6=>'personaNomProgramaAct',
    			7=>'personaUrlProgramActual',
				8=>'personaUrlNoticias',
				9=>'personaUrlFotos',
				10=>'personaUrlVideos',
    			11=>'personaUrlFacebook',
				12=>'personaUrlTwitter',
				13=>'personaUrlYoutube',
				14=>'personaUrlInstagram',
				15=>'personaUrlPinterest',
				16=>'personaUrlGplus',
    			17=>'personaContent',
				18=>'personaConsultRelacionada',
				19=>'personaNacimiento'
    	);
    	
		for($j = 0; $j<count($query); $j++){
			for($i = 0; $i<count($query[$j]); $i++){
				$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[$j][$i]['personaBranch']);
				$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
				
	    		$record    = $dom->createElement('record');
	    		$group     -> appendChild($record);
	    		$metadata  = $dom->createElement('metadata');
	    		$content   = $dom->createElement('content');
	    		$record    -> appendChild($metadata);
	    		$record    -> appendChild($content);
				$content   -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($query[$j][$i]['personaContent']))));
	    		$record    -> setAttribute('url','http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[$j][$i]['personaNom']).'/');
	    		$record    -> setAttribute('action','add');
	    		$record    -> setAttribute('mimetype','text/html');
	    		$record    -> setAttribute('lock','true');
	    		$record    -> setAttribute('last-modified',date("D, d M Y G:i:s")." CDT");
	    		 
	    		for($l = 1; $l<=count($nombres);$l++){
					if(strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == "" || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == " " || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == NULL || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == "0000-00-00") {
						continue;
					}				
					else {
						$meta      = $dom->createElement('meta');
						$metadata  ->appendChild($meta);
						$meta      -> setAttribute('name',$nombres[$l]);
						$meta      -> setAttribute("content",strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])));
					}
	    		}
	    		$elenco =  $this->getPrograma($query[$j][$i]['idPersona']);
	    		 
	    		if(count($elenco)>0){
	    			$cont = 0;
	    			for($k = 0; $k<count($elenco);$k++){
	    				if($query[$j][$i]['idPersona'] == $elenco[$k]["idPersona"]){
	    					$cont++;
	    				}else{
	    					$cont = 1;
	    				}
	    				$meta      = $dom->createElement('meta');
	    				$metadata  ->appendChild($meta);
	    				$meta      -> setAttribute('name','programa_relacionado_'.$cont);
	    				$meta      -> setAttribute('content',strip_tags(html_entity_decode($elenco[$k]["titulo"])));
						if($elenco[$k]["urlPro"] == "" || $elenco[$k]["urlPro"] == NULL)
							continue;
						else {
							$meta      = $dom->createElement('meta');
							$metadata  ->appendChild($meta);
							$meta      -> setAttribute("name",'url_programa_relacionado_'.$cont);
							$meta      -> setAttribute("content",$elenco[$k]["urlPro"]);
						}
	    			}
	    		}
			}
	    	$meta      = $dom->createElement('meta');
	    	$metadata  ->appendChild($meta);
	    	$meta      -> setAttribute('name','seed_content');
	    	$meta      -> setAttribute("content",'persona');
    	}
    	//$xml = $doctype.$dom->saveXML();
    	$xml = $dom->saveXML();
    	 
    	$path = filexml.'personas';
    	$nameFile = 'variosPersonas.xml';
    	
    	$file = $path. "/" . $nameFile;
    	if(file_put_contents($file, $xml)){
			$this->userLog(Session::get('user'), 'persona', 'crea XML varios', serialize($ids), 'ok');
			for($i = 0; $i<count($ids);$i++){
    			$this->publish($ids[$i]);
    		}
    		echo 'Ok';
			if(SEND_TO_AWS) {
				$fileToSend = escapeshellarg("C://Program Files//Apache Software Foundation//Apache2.2//htdocs//admin//".$file);
				$category = escapeshellarg("personas");
				$fileType = escapeshellarg("xml");
				$resval = array();
				$command = 'C://php//php.exe "C://Program Files//Apache Software Foundation//Apache2.2//htdocs//admin//send_to_aws.php" '. $fileToSend .' '. $category .' '. $fileType .'';
				exec($command, $resval);
			}
			if(SEND_TO_GOOGLE)
				$this->executePy($file, "personas");
    	}else {
    		echo 'false';
			$this->userLog(Session::get('user'), 'persona', 'crea XML varios', serialize($ids), 'false');
    	}
    	
    
    }
    
    
    
    public function limpiarCadena($cadena){
    	$cadena = trim($cadena);
    	$cadena = strtr($cadena,
    			"�����������������������������������������������������",
    			"aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
    	$cadena = strtr($cadena,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz");
    	$cadena = preg_replace('#([^.a-z0-9]+)#i', '-', $cadena);
    	$cadena = preg_replace('#-{2,}#','-',$cadena);
    	$cadena = preg_replace('#-$#','',$cadena);
    	$cadena = preg_replace('#^-#','',$cadena);
    	return $cadena;
    }
    
	/*
	 * Funci�n que toma cualquier nombre quita los acentos y los espacios
	 * los sustituye por guiones bajos.
	 */
	public function createUrlString($string) {
		$characters = array("&Aacute;" => "A", "&Ccedil;" => "c", "&Eacute;" => "e", "&Iacute;" => "i", "&Ntilde;" => "n", "&Oacute;" => "o", "&Uacute;" => "u", "&aacute;" => "a", "&ccedil;" => "c", "&eacute;" => "e", "&iacute;" => "i", "&ntilde;" => "n", "&oacute;" => "o", "&uacute;" => "u", "&agrave;" => "a", "&egrave;" => "e", "&igrave;" => "i", "&ograve;" => "o", "&ugrave;" => "u");
		$string = strtr($string, $characters);
		$string = strtolower(trim($string));
		$string = preg_replace("/[^a-z0-9-]/", "-", $string);
		$string = preg_replace("/-+/", "_", $string);
		return $string;
	}
	
	public function stripAllFields(&$fields) {
		foreach ($fields as $key => $value) {
			if (is_array($fields[$key]))
				$this->stripAllFields($fields[$key]);
			else 
				$fields[$key] = trim(strip_tags($value));
		}
	}
    
	public function permissions($originalQuery, $column, $where, $extras) {
		$permissions = Session::get('permissions');
		if($permissions == 0)
			$query = $originalQuery;
		else {
			$pieces = explode("-", $permissions);
			$piecesNum = count($pieces);
			if($where)
				$query = $originalQuery . ' AND (';
			else
				$query = $originalQuery . ' WHERE ';
			if($piecesNum == 1)
				$query .= $column .' = '.$pieces[0];
			else {
				$query .= $column .' = ' . $pieces[0];
				for($i = 1; $i < $piecesNum; $i++)
					$query .= ' OR ' . $column . ' = ' . $pieces[$i];
			}
			if($where)
				$query .= ')';
		}
		if($extras != "" || $extras != NULL)
			$query .= $extras;
		
		return $query;
	}
	
	/**
	* Is Image
	*
	* Determina si una URL est� disponible v�a CURL
	* @param string $url string a comprobar
	* @return bool $ret booleano con el resultado de la verificaci�n
	* @version orginal 2014/10/13 11:54 | modified: 
	*/
	public function isImage($id) {
		$array = $this->personasSingleList($id);
		$url = $array[0]["personaUrlThumb"];
		if($url != "") {
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_NOBODY, true);
			$result = curl_exec($curl);
			$ret = false;
			$contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
			if($contentType != NULL) {
				preg_match('@([\w/+]+)(;\s+charset=(\S+))?@i', $contentType, $matches);
				$validTypes = array(
					'jpg'  => 'image/jpeg',
					'jpeg' => 'image/jpeg',
					'png'  => 'image/png',
					'gif'  => 'image/gif',
				);
				$imageStatus = array_search($matches[0], $validTypes);
				if ($result !== false) {
					$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
					if ($statusCode == 200 && $imageStatus)
						$ret = true;   
				}
				curl_close($curl);
			}
			
			return $ret;
		}
	}
	
	/**
	* Get Image Info
	*
	* Obtiene el nombre, el peso y el tipo de una imagen por su URL
	* @param string $data string con la URL de la imagen
	* @return string json $imageInfo json con los datos de la imagen m�s un boolean validando que es una imagen (isimage, length, type, name)
	* @version original: 2014/10/13 11:58 | modified: 2014/11/10 10:03 l�nea 935, 937 y l�nea 948-955: se modifica la funci�n que regresa el peso de la imagen cuando no se obtiene v�a CURL
	*/
	public function getImageInfo($id) {
		$array = $this->personasSingleList($id);
		$data = $array[0]["personaUrlThumb"];
		if($data != "" && $this->isImage($id) == true) {
			$imageUrl = $data;
			$fileName = basename($data);
			$contentLength = 0;
			$ch = curl_init($data);
			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$data = curl_exec($ch);
			curl_close($ch);
			if (preg_match('/Content-Length: (\d+)/', $data, $matches))
				$contentLength = (int)$matches[1]; // Contiene el tama�o del archivo en bytes
			if (preg_match('/Content-Type: (.*)/', $data, $matches))
				$contentType = $matches[1];
			if($contentLength === NULL || $contentLength === 0 || $contentLength === "") {
				ob_start();
				readfile($imageUrl);
				$imageData = ob_get_contents();
				$length = strlen($imageData);
				ob_end_clean();
				$contentLength = $length;
			}
			$contentType = trim(preg_replace('/\s+/', ' ', $contentType));
			$imageInfo = array('response' => 'isimage', 'length' => $contentLength, 'type' => $contentType, 'name' => $fileName);
			
			return json_encode($imageInfo);
		}
	}
	
	/**
	* Rename File
	*
	* Modifica y renombra el archivo y regresa ese nombre para ser insertado en la base de datos, si no viene del bucket de AWS lo deja igual
	* @param string $fileName URL del archivo de imagen
	* @param string $name nombre con el que se sustituir� el nombre del archivo
	* @return string $newFileName nueva URL del archivo con el nombre
	* @version original: 2014/10/20 16:43 | modified:
	*/
	public function renameFile($fileName, $name) {
		$serverHost = parse_url($fileName);
		$newFileName = "";
		if(preg_match("/".AWS_BUCKET."/", $serverHost["host"])) {
			$path = parse_url($fileName);
			$pathPieces = explode("/", $path["path"]);
			$newName = $this->createUrlString($name);
			$pathInfo = pathinfo($path["path"]);
			$extension = strtolower($pathInfo["extension"]);
			$arrayNewUrl = array($path["scheme"].":/", $path["host"], IMG_PERSON_UPLOAD_DIR.$newName.".".$extension);
			$newFileName = implode("/", $arrayNewUrl);
			$dir = escapeshellarg(IMG_PERSON_UPLOAD_DIR);
			$oldName = escapeshellarg($pathInfo["basename"]);
			$newNameWithExt = escapeshellarg($newName.".".$extension);
			$ext = escapeshellarg($extension);
			$resval = array();
			$command = 'C://php//php.exe "C://Program Files//Apache Software Foundation//Apache2.2//htdocs//admin//rename_file_from_aws.php" '. $dir .' '. $oldName .' '. $newNameWithExt .' '. $ext .'';
			exec($command, $resval);
			$this->userLog(Session::get('user'), 'persona', 'renombra imagen de AWS', $newNameWithExt, serialize($resval));
		}
		else 
			$newFileName = $fileName;
		return $newFileName;
	}
	
	/**
	* User  Log
	*
	* Escribe en el log lo que el usuario hace dentro de la secci�n de personas
	* @param string $user usuario que realiza la acci�n
	* @param string $section la secci�n donde se realiza la acci�n
	* @param string $action la acci�n que se realiza
	* @param string $mod que se modifica
	* @param string $response respuesta del sistema
	* @version original: 2014/12/08 12:38 | modified:
	*/ 
	public function userLog($user, $section, $action, $mod, $response){
		$data = array('usuario' => $user, 'seccion' => $section, 'accion' => $action, 'modificacion' => $mod, 'respuesta' => $response, 'fecha' => date("Y-m-d H:i:s"));
		$this->db->insert('usuarioslog', $data);
	}
	
	/**
	* Change Image Domain
	* 
	* Cambia el dominio de las im�genes de los thumbs para evitar el uso de:
	* -Origin
	* -Galaxypreview
	* -Dynamic
	* Y lo deja con el subdominio: i2
	* @param string url
	* @return string url url sin los subdominios
	* @version original: 2015/01/20 12:55 | modified:
	*/
	public function changeImageDomain($url) {
		return preg_replace("/" . INVALID_IMAGE_SUBDOMAIN . "/i", ESMAS_IMAGE_SUBDOMAIN, $url);
	}
	
	/**
	* Personas JSON
	*
	* Hace un consulta a la base de datos para crear un archivo JSON con el id, el nombre, la URL del thumbnail y la rama a la que pertenece
	* @return string query con los datos de la consulta
	* @version original: 2014/11/10 12:47 | modified: 2015/02/03 15:55 Se a�ade al query la funci�n HTML_DECODE definida en la base de datos
	*/
	public function personasJson($name) {
		$arraySelect = $this->db->select('SELECT
												idPersona, personaNom, personaUrlThumb, personaBranch
											FROM
												persona
											WHERE
												LOWER(HTML_DECODE(personaNom))
											 LIKE
												LOWER(HTML_DECODE("%' . trim($name) . '%"))');		
		$permissions = explode("-", Session::get('permissions'));
		if($arraySelect) {
			if(in_array($arraySelect[0]['personaBranch'], $permissions) || $permissions[0] == 0)
				$editable = URL . "personas/edit/" . $arraySelect[0]['idPersona'];
			else
				$editable = "";
		}
		$arraySelect = array_map(function($changedSelect){
			return array(
				'name' => $changedSelect['personaNom'],
				'thumb' => $changedSelect['personaUrlThumb']
			);
		}, $arraySelect);
		if($arraySelect)
			$arraySelect[0]['edit'] = $editable;
		return json_encode($arraySelect);
	}
	
	/**
	* Availability
	*
	* Comprueba si el nombre de la persona es viable, si est� en la base de datos permite su edici�n si los permisos lo permiten
	* @return string data
	* @version original: 2015/01/26 12:30 | modified:
	*/
	public function availability($name) {
		$name = trim($name);
		if($name != "" || $name != NULL) {
			$persona = $this->personasJson($name);
			return $persona;
		}
	}
}

?>
