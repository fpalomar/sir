<?php

class Branch_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
	public function branchList()
    {
		$permissions = Session::get('permissions');
		if($permissions == 0)
			$query = 'SELECT * FROM branchadm';
		else {
			$pieces = explode("-", $permissions);
			$piecesNum = count($pieces);
			$query = 'SELECT * FROM branchadm WHERE ';
			if($piecesNum == 1)
				$query .= 'idBranch = '.$pieces[0];
			else {
				$query .= 'idBranch = ' . $pieces[0];
				for($i = 1; $i < $piecesNum; $i++)
					$query .= ' OR idBranch = ' . $pieces[$i];
			}
		}
        return $this->db->select($query);
        
    }
	
    public function branchSingleList($branchId)
    {
        return $this->db->select('SELECT * FROM branchadm WHERE idBranch = :branchId', array(':branchId' => $branchId));
    }
    
    public function create($data)
    {
        $this->db->insert('branchadm', array(
            'nombre' => $data['nombre'],
			'idGalaxy' => $data['galaxyId'],
            'url' => $data['url'],
            'imagen' => $data['imagen']
        ));
    }
    
    public function editSave($data)
    {
        $postData = array(
            'nombre' => $data['nombre'],
			'idGalaxy' => $data['galaxyId'],
            'url' => $data['url'],
            'imagen' => $data['imagen']
        );
        $this->db->update('branchadm', $postData, "`idBranch` = {$data['branchId']}");
    }
    
    public function delete($branchId)
    {   
        $this->db->delete('branchadm', "idBranch = '$branchId'");
    }
}