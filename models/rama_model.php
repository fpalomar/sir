<?php

class Rama_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
    * Ramas List
    *
    * Muestra la lista completa de ramas para el listado en el view de ramas
    * @return array show
    * @version original: 12/10/2016 11:21 | modified:
    */
    public function ramaDataTable($data) {
        // Número total de elementos sin ninguna búsqueda o filtro
        $query = 'SELECT COUNT(*) as total FROM rama';
        try {
            $execute =  $this->db->select($query);
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 1));
            die();
        }
        $totalData = $execute[0]['total']; // Total de elementos encontrados
        $totalFiltered = $totalData; // Cuando no hay parámetros de búsqueda, no filtra
        // Elementos a devolver
        $query = 'SELECT
                         *
                    FROM
                         rama';
        // Array de columnas como se ordena dependiendo de los valores de POST
        $columns = array(
            // Datatable column index  => Database column name
            0 => 'ramaId',
            1 => 'ramaGalaxyId',
            2 => 'ramaNombre',
            3 => 'ramaUrl',
            4 => 'ramaImagen'
        );
        // Valores para ordenamiento en el DataTable
        if(isset($data['order'][0]['column'])) {
            $orderBy = filter_var($data['order'][0]['column'], FILTER_SANITIZE_NUMBER_INT);
            $orderBy = $columns[$orderBy];
        }
        else // Default
            $orderBy = 'ramaId'; 
        if(isset($data['order'][0]['dir']))
            $orderDirection = filter_var($data['order'][0]['dir'], FILTER_SANITIZE_STRING);
        else
            $orderDirection = 'ASC';
        if(isset($data['start']))
            $limitStart = filter_var($data['start'], FILTER_SANITIZE_NUMBER_INT);
        else
            $limitStart = 0;
        if(isset($data['length']))
            $limitLenght = filter_var($data['length'], FILTER_SANITIZE_NUMBER_INT);
        else
            $limitLenght = limiteM;
        if(isset($data['draw']))
            $draw = filter_var($data['draw'], FILTER_SANITIZE_NUMBER_INT);
        else
            $draw = 1;
        // Si hay un parámetro de búsqueda
        $search = "";
        if(isset($data['search']['value']) && !empty($data['search']['value'])) {
            $searchParameter =  filter_var($data['search']['value'], FILTER_SANITIZE_STRING);
            $search = " WHERE ramaId = '" . $searchParameter . "' OR ramaGalaxyId = '" . $searchParameter . "' OR ramaNombre LIKE '%" . $searchParameter . "%'";
            $query.= $search;
        }
        // Cuando hay un parámetro de búsqueda se tiene que modificar  el número total de rows por resultado de búsqueda
        $countQuery = 'SELECT COUNT(*) as total FROM rama' . $search;
        try {
            $countExecute = $this->db->select($countQuery);
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 2));
            die();
        }
        $totalFiltered = $countExecute[0]['total'];
        $extras = ' ORDER BY ' . $orderBy . ' ' . $orderDirection . ' LIMIT ' . $limitStart . ' ,' . $limitLenght;
        try {
            $execute = $this->db->select($query.$extras);
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 3));
            die();
        }
        // Array a devolver
        $data = array(
                    'draw'            => intval($draw),                 // Para cada request/draw del lado del cliente, se env�a un n�mero como par�metro
                    'recordsTotal'    => intval($totalData),            // N�mero total de elementos
                    'recordsFiltered' => intval($totalFiltered),        // N�mero total de elementos despu�s de la b�squeda, si no hay b�squeda totalFiltered = totalData
                    'data'            => $execute                       // Array con el total de los elementos ($data OR $newData)
	);
        return json_encode($data);
    }
    
    public function ramaList() {
        $permissions = Session::get('permissions');
        if($permissions == 0)
            $query = 'SELECT * FROM rama';
        else {
            $pieces = explode("-", $permissions);
            $piecesNum = count($pieces);
            $query = 'SELECT * FROM rama WHERE ';
            if($piecesNum == 1)
                $query .= 'ramaId = '.$pieces[0];
            else {
                $query .= 'ramaId = ' . $pieces[0];
                for($i = 1; $i < $piecesNum; $i++)
                    $query .= ' OR ramaId = ' . $pieces[$i];
            }
        }
        return $this->db->select($query);
    }
	
    public function ramaSingleList($ramaId) {
        return $this->db->select('SELECT * FROM rama WHERE ramaId = :branchId', array(':branchId' => $ramaId));
    }
    
    public function create($data) {
        $this->db->insert('rama', array(
            'ramaNombre'   => $data['nombre'],
            'ramaGalaxyId' => $data['galaxyId'],
            'ramaUrl'      => $data['url'],
            'ramaImagen'   => $data['imagen']
        ));
    }
    
    public function editSave($data) {
        $postData = array(
            'ramaNombre'   => $data['nombre'],
            'ramaGalaxyId' => $data['galaxyId'],
            'ramaUrl'      => $data['url'],
            'ramaImagen'   => $data['imagen']
        );
        $this->db->update('rama', $postData, "`ramaId` = {$data['branchId']}");
    }
    
    public function delete($ramaId) {   
        $this->db->delete('rama', "ramaId = '$branchId'");
    }
    
    /**
     * Check Branch
     * 
     * Comprueba si ya existe la rama en la base de datos
     * @param string name
     * @return bool
     * @version original: 2016/10/12 20:44 | modified:
     */
    public function checkbranch($name) {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $name = trim($name);
        try {
            $result = $this->db->select('SELECT
                                                ramaId
                                           FROM 
                                                rama
                                          WHERE
                                                ramaNombre
                                              =
                                                "' . $name . '"
                                        COLLATE utf8_spanish_ci');
        }
        catch (RuntimeException $e) {
            die(json_encode(array('dberror' => 1)));
        }
        return (bool) $result;
    }
    
    /**
     * Check Galaxy
     * 
     * Comprueba si ya existe el Id de Galaxy en la base de datos
     * @param string id
     * @return bool
     * @version original: 2016/10/12 20:44 | modified:
     */
    public function checkgalaxy($id) {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        $id = trim($id);
        try {
            $result = $this->db->select('SELECT
                                                ramaId
                                           FROM 
                                                rama
                                          WHERE
                                                ramaGalaxyId
                                              =
                                                "' . $id . '"');
        }
        catch (RuntimeException $e) {
            die(json_encode(array('dberror' => 2)));
        }
        return (bool) $result;
    }
    
    /**
     * Is Ajax
     *
     * Función que comprueba si la petición se realiza desde ajax 
     * @return request
     * @version original: 2014/02/27 16:18 | modified:
     */
    public function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
    
}