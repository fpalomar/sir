<?php
include 'personas_model.php';

class Program_Model extends Model {
    public function __construct()
    {
        parent::__construct();
    }
	
	public function branchList() {
		$newB = new Branch_Model();
		return $newB->branchList();
	}
	
	public function wiam($paq = 1) {
		return $paq;
	}

    public function programList($paq = 1)
    {
		$query = 'SELECT * FROM programa';
		$column = 'programaBranch';
		$where = false;
		$extras = ' order by programaFechaIngreso DESC limit '.(($paq-1)*limiteM).','.limiteM;
		return $this->db->select($this->permissions($query, $column, $where, $extras));
    }
    
    public function personaLista(){
    	$newA = new Personas_Model();
    	return $newA->personasLista();
    }
    public function personasSeleccionadas($id){
    	$newa = new Personas_Model();
    	return $newa->personasSeleccionadas($id);
    }
    
    
    
    public function programSingleList($idPrograma)
    {
        return $this->db->select('SELECT * FROM programa WHERE idprograma = :idPrograma', 
            array( 'idPrograma' => $idPrograma));
    }
    
    public function check($values,$valuesP,$type) {
		$result = $this->db->select("SELECT * FROM programa WHERE LOWER(HTML_DECODE(programaTitulo)) LIKE LOWER(HTML_DECODE('%".trim($values[0])."%'))");
		if(count($result) > 0 && $type[0] != "e")
			return 1;
		else if(count($valuesP) > 1) {
			$arrayLength = count($valuesP)/2;
			if($arrayLength >= 1) {
				$sum = 0;
				for ($i = 0; $i < $arrayLength; $i++) {
					$persona = "";
					$programa = "";
					$persona = $this->db->select("SELECT * FROM persona WHERE LOWER(HTML_DECODE(personaNom)) LIKE LOWER(HTML_DECODE('%".trim($valuesP[$i+$sum])."%'))");
					if(count($persona) > 0) {
						return array(2, $valuesP[$i+$sum]);
						exit;
					}
					$programa = $this->db->select("SELECT * FROM programa WHERE LOWER(HTML_DECODE(programaTitulo)) like LOWER(HTML_DECODE('%".trim($valuesP[$i+$sum])."%'))");
					if(count($programa) > 0) {
						return array(3, $valuesP[$i+$sum]);
						exit;
					}
					$sum++;
				}
				return 0;
			}
		}
		else
			return 0;
	}
        
        
    /**
    * Check Program
    * 
    * Comprueba si ya existe el programa en la base de datos
    * @param string name
    * @return bool
    * @version original: 2016/10/16 09:40 | modified:
    */
    public function checkprogram($name) {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $name = trim($name);
        try {
            $result = $this->db->select('SELECT
                                                programaId
                                           FROM 
                                                programa
                                          WHERE
                                                programaTitulo
                                              =
                                                "' . $name . '"');
        }
        catch (RuntimeException $e) {
            die(json_encode(array('dberror' => 1)));
        }
        return (bool) $result;
    }
    
    public function create($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1)
    {        	
    	/*print_r($indices);
		print_r($values);
		print_r($indicesP);
		print_r($valuesP);
		print_r($indicesP1);
		print_r($valuesP1);
		die();*/
		$title = "";
		
		$data = Array();
		$cData = Array();
		if (count($indices) == count($values)) {
    		$data = array_combine( $indices , $values);
    	}
		
		/*print_r($data); die();*/
		array_walk($data, function (&$value, $key) {
			if($key == "programaTitulo") {
				global $title;
				$title = $value;
			}
			else if($key == "programatThumbmail" && ($value != "" || $value != NULL)) {
				global $title;
				$value = $this->renameFile($value, $title);
				if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $value))
					$value = $this->changeImageDomain($value);
			}
		});
		$cData = $data;
		$this->stripAllFields($data);
    	$this->db->insert('programa',$data);
    	$idPrograma = array('idPrograma' => intval($this->db->lastInsertId()));	
    	$this->createA($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idPrograma);
		$this->userLog(Session::get('user'), 'programa', 'crea', $cData["programaTitulo"], 'ok');
    }
    
    
    public function createA ($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idPrograma)
    {
    	$data   = array();
    	$dataP  = array();
    	$nomPer = array();
    	$personaUrlBio = array();
    	$dataM = array();
    	$cont1 = 0;
    	$cont2 = 0;
    	$dataM = array();
		$serializedData = array();
    
    	if($valuesP[0] != '' || $valuesP[0] != 0){
    		for($m = 0; $m<count($valuesP); $m++){
    			if($m%2 == 0){
    				$nomPer[$cont1] = $valuesP[$m];
    				$cont1++;
    			}else{
    				$personaUrlBio[$cont2] = $valuesP[$m];
    				$cont2++;
    			}
    		}
    		 
    		for($l = 0;$l<count($valuesP)/2; $l++){
				$dataM[] = array('personaNom'=>$nomPer[$l],'personaUrlBio'=>$personaUrlBio[$l],'personaFechaIngreso'=>date('Y-m-d H:i:s'),'personaBranch'=>$values[1]);
    		}
    		for($i = 0; $i<count($dataM);$i++){
    			 
    			$this->db->insert('persona', $dataM[$i]);
    			$dat = array("idPersona" => intval($this->db->lastInsertId()));
    
    			$dataR1 = array('programa'=>$idPrograma['idPrograma'],'persona'=>$dat["idPersona"] );
    			$this->createRelation($dataR1);
    		}
			$serializedData = $nomPer;
			array_push($serializedData, $idPrograma['idPrograma']);
			$this->userLog(Session::get('user'), 'programa', 'actualiza relaciones', serialize($serializedData), 'ok');
    	}
    
    	if($valuesP1 != '' && $valuesP1[0] != 0 && $idPrograma != '' ){
    		for($i = 0; $i<count($valuesP1); $i++){
    			$dataR = array('programa'=>$idPrograma['idPrograma'],'persona'=>$valuesP1[$i]);
    			$this->createRelation($dataR);
    		}
    	}
    }
    
    
    function createRelation($data){
    	$this->db->insert('programaPersona', $data);
    	//$datas = array('persona' => intval($this->db->lastInsertId()));
    	//return $datas;
    }    
    
    public function editSave($indices,$values,$indicesP,$valuesP,$indicesP1,$valuesP1,$idP){
		
		$title = "";
		
    	if (count($indices) == count($values)) {
    		$data = array_combine( $indices , $values);
    	}
		
		array_walk($data, function (&$value, $key) {
			if($key == "programaTitulo") {
				global $title;
				$title = $value;
			}
			else if($key == "programatThumbmail" && ($value != "" || $value != NULL)) {
				global $title;
				$value = $this->renameFile($value, $title);
				if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $value))
					$value = $this->changeImageDomain($value);
			}
		});
		
		$this->stripAllFields($data);
    	$this->db->update('programa',$data,'idprograma = '.$idP);
		$this->userLog(Session::get('user'), 'programa', 'actualiza', $data['programaTitulo'], 'ok');
    	//P1 son aquellos que ya existen.
    	//P son nuevos a insertar
		
		/*print_r($valuesP1);
		die();*/
		
    	if(($valuesP1 != '') || ($valuesP != '' && $valuesP[0] != '')){
    		 $this->updateA($values, $indicesP,$valuesP,$indicesP1,$valuesP1,$idP);
    	}
    	
    }
    
    public function updateA($values, $indicesP,$valuesP,$indicesP1,$valuesP1,$idP){
    	
    	$this->db->deleteAll('programapersona','programa = '.$idP);
    	
    	$data   = array();
    	$dataP  = array();
    	$nomPer = array();
    	$personaUrlBio = array();
    	$dataM = array();
    	$cont1 = 0;
    	$cont2 = 0;
    	$dataM = array();
		$serializedData = array();
    	
    	if($valuesP[0] != '' || $valuesP[0] != 0){
    		for($m = 0; $m<count($valuesP); $m++){
    			if($m%2 == 0){
    				$nomPer[$cont1] = $valuesP[$m];
    				$cont1++;
    			}else{
    				$personaUrlBio[$cont2] = $valuesP[$m];
    				$cont2++;
    			}
    		}
    		 
    		for($l = 0;$l<count($valuesP)/2; $l++){
    			$dataM[] = array('personaNom'=>$nomPer[$l],'personaUrlBio'=>$personaUrlBio[$l],'personaFechaIngreso'=>date('Y-m-d H:i:s'),'personaBranch'=>$values[1]);
    		}
    		for($i = 0; $i<count($dataM);$i++){
    	
    			$this->db->insert('persona', $dataM[$i]);
    			$dat = array("idPersona" => intval($this->db->lastInsertId()));
    	
    			$dataR1 = array('programa'=>$idP,'persona'=>$dat["idPersona"] );
    			$this->createRelation($dataR1);
    		}
			$serializedData = $nomPer;
			array_push($serializedData, $idP);
			$this->userLog(Session::get('user'), 'programa', 'actualiza relaciones', serialize($serializedData), 'ok');
    	}
    	
    	if($valuesP1 != '' && $valuesP1[0] != 0 && $idP != '' ){
    		for($i = 0; $i<count($valuesP1); $i++){
    			$dataR = array('programa'=>$idP,'persona'=>$valuesP1[$i]);
    			$this->createRelation($dataR);
    		}
    	}
    }	
   
   
    public function search($columna, $valor)
    {	
    	$query = 'SELECT * FROM programa WHERE ';
		$column = 'programaBranch';
		$where = true;
		$extras = '';
		if(count($columna) == 0 || count($columna) == 1){
			if($columna[0] == "programaTitulo")
				$query.= "LOWER(HTML_DECODE(". $columna[0] .")) LIKE LOWER(HTML_DECODE('%".$valor[0]."%')) ";
			else
				$query.= "LOWER(". $columna[0] .") LIKE LOWER('%".$valor[0]."%') ";
    	}
		else if(count($columna)>1){
			if($columna[0] == "programaTitulo")
				$query.= "LOWER(HTML_DECODE(". $columna[0] .")) LIKE LOWER(HTML_DECODE('%".$valor[0]."%')) ";
			else
				$query.= "LOWER(". $columna[0] .") LIKE LOWER('%".$valor[0]."%') ";
    		for($i = 1; $i<count($columna); $i++){
    			$query.= "AND ".$columna[$i]." LIKE LOWER('%".$valor[$i]."%')";
    		}
    	}
		
		/*$consulta = "SELECT * FROM programa where ";
    	if(count($columna) == 0 || count($columna) == 1){
    		$consulta.= "LOWER(". $columna[0] .") like LOWER('%".$valor[0]."%') ";
    	}else if(count($columna)>1){
    		$consulta.= "LOWER(". $columna[0] .") like LOWER('%".$valor[0]."%') ";
    		for($i = 1; $i<count($columna); $i++){
    			$consulta.= "and ".$columna[$i]." like LOWER('%".$valor[$i]."%')";
    		}
    	}*/
    	
	    $valo = $this->db->select($this->permissions($query, $column, $where, $extras));
    	return json_encode($valo);
    	    	
    }
       
   public function nPaginar(){
		$query = 'SELECT count(*) as total FROM programa';
		$column = 'programaBranch';
		$where = false;
		$extras = '';
   
    	$v = $this->db->select($this->permissions($query, $column, $where, $extras)) ;
    	$total = $v[0]["total"];
    	$paginas = ceil($total/limiteM);
    	return $paginas;
    }
	
	/*
	 * Ejecuta el llamado a Python para mandar el archivo a Google
	 */
	public function executePy($name_xml, $channel) {
		$log = 'logs/execute.log';
		$name_xml = dirname($_SERVER['SCRIPT_FILENAME'])."/".$name_xml;
		$server_gsa["google03"] = '216.250.129.15:19900';
		$server_gsa["google04"] = '216.250.129.16:19900';
		while(list($server, $ip_server) = each($server_gsa)) {
			$retval = array();
			$command = 'C://Python27//python.exe C://Python27//pushfeed_client.py --datasource="'.$channel.'" --feedtype="incremental" --url="http://'.$ip_server.'/xmlfeed" --xmlfilename="'.$name_xml.'"';
			//exec('/usr/bin/python /usr/local/apache2/htdocs/python/pushfeed_client.py --datasource="'.$channel.'" --feedtype="incremental" --url="http://'.$ip_server.'/xmlfeed" --xmlfilename="'.$name_xml.'"', $retval);
			exec($command, $retval);
			if(isset($retval[0]) && $retval[0]=="Success") {
				$mensaje = "[".date("D, d M Y G:i:s T")."] Insertado en ".$server.PHP_EOL;
				file_put_contents($log, $mensaje, FILE_APPEND);
			}
			else {
				$mensaje = "[".date("D, d M Y G:i:s T")."] Fallo [".$retval[0]."]: ".$command.PHP_EOL;
				file_put_contents($log, $mensaje, FILE_APPEND);
			}
			$this->userLog(Session::get('user'), 'programa', 'ejecuta Python', $name_xml, $retval[0]);
		}
	}
    
   public function generateXml($tipo, $id){
		$ids = array();
		$route = "programastvsa/";
   		$query = '';
   		if($tipo == 'nuevo'){
   			$queryA = $this->db->select("SELECT date(programaFechaIngreso) as fecha
									FROM programa group by date(programaFechaIngreso)
									order by programaFechaIngreso DESC limit 0,1;");
   			$query = $this->db->select("SELECT * FROM programa where date(programaFechaIngreso) = '".$queryA[0]["fecha"]."';");
   		}else if($tipo == 'editar'){
   			$query = $this->db->select("SELECT * FROM programa where idprograma = ".$id);
   		}
   		
		$imp 	 = new DOMImplementation;
		$dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
		$dom 	 = $imp->createDocument("", "", $dtd);
		$dom 	-> encoding = "ISO-8859-1";
		$dom    -> formatOutput = true;
		
		$node    = $dom->createElement("gsafeed");
		$header  = $dom->createElement("header");
		$data    = $dom->createElement("datasource");
		$feed    = $dom->createElement("feedtype");
		$data   -> nodeValue = "programa";
		$feed   -> nodeValue = 'metadata-and-url';
		$header -> appendChild($data);
		$header -> appendChild($feed);
		$node   -> appendChild($header);
		$group     = $dom->createElement("group");
		$node   -> appendChild($group);
		$dom    -> appendChild($node);
		$tituloG = '';
		$nombres = array(1=>'titulo_programa',
						 2=>'url_programa',
						 3=>'descripcion_programa',
						 4=>'thumbnail_programa',
						 5=>'keywords',
					     6=>'url_capitulos_completos',
						 7=>'url_noticias',
						 8=>'url_fotos',
						 9=>'url_videos',
						 10=>'url_facebook',
						 11=>'url_twitter',
						 12=>'url_youtube',
						 13=>'url_instagram',
						 14=>'url_pinterest',
						 15=>'url_gplus',
						 16=>'content',
						 17=>'consulta_relacionada',
						 18=>'horario'
		);
		$nombreTabla = array( 1=>'programaTitulo',
							  2=>'programaUrl',
							  3=>'programaDescrip',
							  4=>'programatThumbmail',
							  5=>'programaKeywords',
							  6=>'programaUrlCapitulosCompletos',
							  7=>'programaUrlNoticias',
							  8=>'programaUrlFotos',
							  9=>'programaUrlVideos',
							  10=>'programaUrlFacebook',
							  11=>'programaUrlTwitter',
							  12=>'programaUrlYoutube',
							  13=>'programaUrlInstagram',
							  14=>'programaUrlPinterest',
							  15=>'programaUrlGplus',
							  16=>'programaContent',
							  17=>'programaConsultRelacionada',
							  18=>array('programaHoraInicio', 'programaHoraFin')
		);
		
		for($i = 0; $i<count($query); $i++){
			array_push($ids, $query[$i]['idprograma']);
			$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[$i]['programaBranch']);
			$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
			
			$tituloG = $query[$i]['programaTitulo'];
			$record    = $dom->createElement('record');
			$group     -> appendChild($record);
			$metadata  = $dom->createelement('metadata');
			$content   = $dom->createElement('content');
			$record    -> appendChild($metadata);
			$record    -> appendChild($content);
			$content   -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($query[$i]['programaContent']))));
			$record    -> setAttribute('url','http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[$i]['programaTitulo']).'/');
			$record    -> setAttribute('action','add');
			$record    -> setAttribute('mimetype','text/html');
			$record    -> setAttribute('lock','true');
			$record    -> setAttribute('last-modified',date("D, d M Y G:i:s")." CDT");
			
			for($j = 1; $j<=count($nombres);$j++){
				if($nombres[$j] == "horario") {
					$separator = " - ";
					if(strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][0]])) == "" || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][0]])) == " " || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][0]])) == NULL) {
						continue;
					}
					if(strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][1]])) == "" || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][1]])) == " " || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][1]])) == NULL) {
						$separator = "";
					}
					$meta      = $dom->createElement('meta');
					$metadata  ->appendChild($meta);
					$meta      -> setAttribute('name',$nombres[$j]);
					$meta      -> setAttribute("content",strip_tags(html_entity_decode($query[$i][$nombreTabla[$j][0]].$separator.$query[$i][$nombreTabla[$j][1]])));
				}
				else {
					if(strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == "" || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == " " || strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])) == NULL) {
						continue;
					}				
					else {
						$meta      = $dom->createElement('meta');
						$metadata  ->appendChild($meta);
						$meta      -> setAttribute('name',$nombres[$j]);
						$meta      -> setAttribute("content",strip_tags(html_entity_decode($query[$i][$nombreTabla[$j]])));
					}
				}
			}
			
			$newA   = new Personas_Model();
			$elenco =  $newA->getElenco($query[$i]['idprograma']);
			
			if(count($elenco)>0){
				$cont = 0;
				for($k = 0; $k<count($elenco);$k++){
					if($query[$i]['idprograma'] == $elenco[$k]["idPrograma"]){
						$cont++;
					}else{
						$cont = 1;
					}
					$meta      = $dom->createElement('meta');
					$metadata  ->appendChild($meta);
					$meta      -> setAttribute('name','nombre_elenco_'.$cont);
					$meta      -> setAttribute('content',strip_tags(html_entity_decode($elenco[$k]["nombre"])));
					if($elenco[$k]["urlBio"] == "" || $elenco[$k]["urlBio"] == NULL)
						continue;
					else {
						$meta      = $dom->createElement('meta');
						$metadata  ->appendChild($meta);
						$meta      -> setAttribute("name",'url_bio_elenco_'.$cont);
						$meta      -> setAttribute("content",$elenco[$k]["urlBio"]);
					}	
				}
			}
			$meta      = $dom->createElement('meta');
			$metadata  ->appendChild($meta);
			$meta      -> setAttribute('name','seed_content');
			$meta      -> setAttribute("content",'programa');
		}
			
		$xml = $dom->saveXML();
		
		$path = filexml.'programa';
		if($tipo == 'nuevo'){
			for($i = 0; $i<count($ids);$i++){
				$this->publish($ids[$i]);
			}
			$nameFile = 'programas.xml';
		}else if($tipo = 'editar'){
			$tituloG = $this->limpiarCadena($tituloG);
			$nameFile = $tituloG.'.xml';
			$this->publish($id);
		}
		$file = $path. "/" . $nameFile;
		if(file_put_contents($file, $xml)){
			echo 'Ok';
			$this->userLog(Session::get('user'), 'programa', 'crea XML', $file, 'ok');
			if(SEND_TO_AWS) {
				$fileToSend = escapeshellarg("/var/www/html/".$file);
				$category = escapeshellarg("programas");
				$fileType = escapeshellarg("xml");
				$resval = array();
				$command = '/usr/bin/php /var/www/html/send_to_aws.php '. $fileToSend .' '. $category .' '. $fileType .'';
				exec($command, $resval);
			}
			if(SEND_TO_GOOGLE) {
				$this->executePy($file, "programas");
				unlink($file);
			}
		}else {
			echo 'false';
			$this->userLog(Session::get('user'), 'programa', 'crea XML', $file, 'false');
		}
		
	}

	public function generateXmlEliminados($ids){
		$route = "programastvsa/";
		for($i = 0; $i<count($ids);$i++){
			$query[$i] = $this->db->select("SELECT * FROM programa where idprograma = ".$ids[$i]);
		}
		$imp 	 = new DOMImplementation;
		$dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
		$dom 	 = $imp->createDocument("", "", $dtd);
		$dom 	-> encoding = "ISO-8859-1";
		$dom    -> formatOutput = true;
		
		$node    = $dom->createElement("gsafeed");
		$header  = $dom->createElement("header");
		$data    = $dom->createElement("datasource");
		$feed    = $dom->createElement("feedtype");
		$data   -> nodeValue = "feed";
		$feed   -> nodeValue = 'metadata-and-url';
		$header -> appendChild($data);
		$header -> appendChild($feed);
		$node   -> appendChild($header);
		$group   = $dom->createElement("group");
		$group  -> setAttribute('action',"delete");
		
		$node   -> appendChild($group);
		$dom    -> appendChild($node);
		$nombres = array(1=>'url_programa');
		$nombreTabla = array( 1=>'programaTitulo');
		
		if(count($query)>1){
			for($j = 0; $j<count($query); $j++){
				for($i = 0; $i<count($query[$i]); $i++){
					$tituloG = $query[$j][$i]['programaTitulo'];		
					$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[$j][$i]['programaBranch']);
					$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
					
					$record    = $dom->createElement('record');
					$group     -> appendChild($record);
					for($k = 1; $k<=count($nombres);$k++){
						$record -> setAttribute("url",'http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[$j][$i][$nombreTabla[$k]]).'/');
						$record -> setAttribute('mimetype',"text/plain");
					}
				}
			}
		}else{
			$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[0][0]['programaBranch']);
			$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
			$tituloG = $query[0][0]['programaTitulo'];
			$record    = $dom->createElement('record');
			$group     -> appendChild($record);
			for($k = 1; $k<=count($nombres);$k++){
				$record -> setAttribute("url",'http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[0][0][$nombreTabla[$k]]).'/');
				$record -> setAttribute('mimetype',"text/plain");
			}
		}
		
		//$xml = $doctype.$dom->saveXML();
		$xml = $dom->saveXML();
		
		$path = filexml.'programa';
		$nameFile = 'programasEliminados.xml';
		
		$file = $path. "/" . $nameFile;
		if(@file_put_contents($file, $xml)){
			for($i = 0; $i < count($ids); $i++) {
				$name = $this->programSingleList($ids[$i]);
				$array[$ids[$i]] = $name[0]["programaTitulo"];
			}
			for($i = 0; $i<count($ids);$i++){
				$this->onlySave($ids[$i]);
			}
			echo 'Ok';
			$this->userLog(Session::get('user'), 'programa', 'crea XML eliminados', json_encode($array), 'ok');
			if(SEND_TO_AWS) {
				$fileToSend = escapeshellarg("/var/www/html/".$file);
				$category = escapeshellarg("programas");
				$fileType = escapeshellarg("xml");
				$resval = array();
				$command = '/usr/bin/php /var/www/html/send_to_aws.php '. $fileToSend .' '. $category .' '. $fileType .'';
				exec($command, $resval);
			}
			if(SEND_TO_GOOGLE) {
				$this->executePy($file, "programas");
				unlink($file);
			}
		}else {
			echo 'false';
			$this->userLog(Session::get('user'), 'programa', 'crea XML eliminados', serialize($ids), 'false');
		}
		
	}
	
	public function onlySave($id) {
		$this->db->update('programa',array("programaStatus" => 1),'idprograma = '.$id);
		$this->userLog(Session::get('user'), 'programa', 'despublica', $id, 'ok');
	}
	
	public function publish($id) {
		$this->db->update('programa',array("programaStatus" => 0),'idprograma = '.$id);
		$this->userLog(Session::get('user'), 'programa', 'publica', $id, 'ok');
	}
	
	public function permanentlyDeleted($ids){
		$this->generateXmlEliminados($ids);
		for($i = 0; $i < count($ids); $i++) {
			$name = $this->programSingleList($ids[$i]);
			$array[$ids[$i]] = $name[0]["programaTitulo"];
		}
		for($i = 0; $i<count($ids);$i++){
    		$this->delete($ids[$i]);
    	}
		$this->userLog(Session::get('user'), 'programa', 'elimina permanentemente', json_encode($array), 'ok');
	}
	
	public function delete($id)
	{
		$this->db->delete('programa','`idprograma` = '.$id);
	}
	
	public function generateXmlVarios($ids){
		$route = "programastvsa/";
		for($i = 0; $i<count($ids);$i++){
			$query[$i] = $this->db->select("SELECT * FROM programa where idprograma = ".$ids[$i]);
		}	
		$imp 	 = new DOMImplementation;
		$dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
		$dom 	 = $imp->createDocument("", "", $dtd);
		$dom 	-> encoding = "ISO-8859-1";
		$dom    -> formatOutput = true;
		
		$node    = $dom->createElement("gsafeed");
		$header  = $dom->createElement("header");
		$data    = $dom->createElement("datasource");
		$feed    = $dom->createElement("feedtype");
		$data   -> nodeValue = "programa";
		$feed   -> nodeValue = 'metadata-and-url';
		$header -> appendChild($data);
		$header -> appendChild($feed);
		$node   -> appendChild($header);
		$group     = $dom->createElement("group");
		$node   -> appendChild($group);
		$dom    -> appendChild($node);
		$tituloG = '';
		$nombres = array(1=>'titulo_programa',
						 2=>'url_programa',
						 3=>'descripcion_programa',
						 4=>'thumbnail_programa',
						 5=>'keywords',
					     6=>'url_capitulos_completos',
						 7=>'url_noticias',
						 8=>'url_fotos',
						 9=>'url_videos',
						 10=>'url_facebook',
						 11=>'url_twitter',
						 12=>'url_youtube',
						 13=>'url_instagram',
						 14=>'url_pinterest',
						 15=>'url_gplus',
						 16=>'content',
						 17=>'consulta_relacionada',
						 18=>'horario'
		);
		$nombreTabla = array( 1=>'programaTitulo',
							  2=>'programaUrl',
							  3=>'programaDescrip',
							  4=>'programatThumbmail',
							  5=>'programaKeywords',
							  6=>'programaUrlCapitulosCompletos',
							  7=>'programaUrlNoticias',
							  8=>'programaUrlFotos',
							  9=>'programaUrlVideos',
							  10=>'programaUrlFacebook',
							  11=>'programaUrlTwitter',
							  12=>'programaUrlYoutube',
							  13=>'programaUrlInstagram',
							  14=>'programaUrlPinterest',
							  15=>'programaUrlGplus',
							  16=>'programaContent',
							  17=>'programaConsultRelacionada',
							  18=>array('programaHoraInicio', 'programaHoraFin')
		);
		for($j = 0; $j<count($query); $j++){
		
			for($i = 0; $i<count($query[$j]); $i++){
				$arrBranch    = $this->db->select("SELECT nombre FROM branchadm WHERE idBranch = ".$query[$j][$i]['programaBranch']);
				$branch = $this->createUrlString($arrBranch[0]['nombre']).'/';
					
				$record    = $dom->createElement('record');
				$group     -> appendChild($record);
				$metadata  = $dom->createElement('metadata');
				$content   = $dom->createElement('content');
				$record    -> appendChild($metadata);
				$record    -> appendChild($content);
				$content   -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($query[$j][$i]['programaContent']))));
				$record    -> setAttribute('url', 'http://pruebas.esmas.com/'.$branch.$route.$this->createUrlString($query[$j][$i]['programaTitulo']).'/');
				$record    -> setAttribute('action','add');
				$record    -> setAttribute('mimetype','text/html');
				$record    -> setAttribute('lock','true');
				$record    -> setAttribute('last-modified',date("D, d M Y G:i:s")." CDT");
					
				for($l = 1; $l<=count($nombres);$l++){
					if($nombres[$l] == "horario") {
						$separator = " - ";
						if(strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][0]])) == "" || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][0]])) == " " || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][0]])) == NULL) {
							continue;
						}
						if(strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][1]])) == "" || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][1]])) == " " || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][1]])) == NULL) {
							$separator = "";
						}
						$meta      = $dom->createElement('meta');
						$metadata  ->appendChild($meta);
						$meta      -> setAttribute('name',$nombres[$l]);
						$meta      -> setAttribute("content",strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l][0]].$separator.$query[$j][$i][$nombreTabla[$l][1]])));
					}
					else {
						if(strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == "" || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == " " || strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])) == NULL) {
							continue;
						}				
						else {
							$meta      = $dom->createElement('meta');
							$metadata  ->appendChild($meta);
							$meta      -> setAttribute('name',$nombres[$l]);
							$meta      -> setAttribute("content",strip_tags(html_entity_decode($query[$j][$i][$nombreTabla[$l]])));
						}
					}
				}
					
				$newA   = new Personas_Model();
				$elenco =  $newA->getElenco($query[$j][$i]['idprograma']);
					
				if(count($elenco)>0){
					$cont = 0;
					for($k = 0; $k<count($elenco);$k++){
						if($query[$j][$i]['idprograma'] == $elenco[$k]["idPrograma"]){
							$cont++;
						}else{
							$cont = 1;
						}
						$meta      = $dom->createElement('meta');
						$metadata  ->appendChild($meta);
						$meta      -> setAttribute('name','nombre_elenco_'.$cont);
						$meta      -> setAttribute('content',strip_tags(html_entity_decode($elenco[$k]["nombre"])));
						if($elenco[$k]["urlBio"] == "" || $elenco[$k]["urlBio"] == NULL)
							continue;
						else {
							$meta      = $dom->createElement('meta');
							$metadata  ->appendChild($meta);
							$meta      -> setAttribute("name",'url_bio_elenco_'.$cont);
							$meta      -> setAttribute("content",$elenco[$k]["urlBio"]);
						}
					}
				}
			}
			$meta      = $dom->createElement('meta');
			$metadata  ->appendChild($meta);
			$meta      -> setAttribute('name','seed_content');
			$meta      -> setAttribute("content",'programa');
		}
			
		//$xml = $doctype.$dom->saveXML();
		$xml = $dom->saveXML();
		
		$path = filexml.'programa';
		$nameFile = 'variosProgramas.xml';
		
		$file = $path. "/" . $nameFile;
		if(file_put_contents($file, $xml)){
			$this->userLog(Session::get('user'), 'programa', 'crea XML varios', serialize($ids), 'ok');
			for($i = 0; $i<count($ids);$i++){
    			$this->publish($ids[$i]);
    		}
			echo 'Ok';
			if(SEND_TO_AWS) {
				$fileToSend = escapeshellarg("/var/www/html/".$file);
				$category = escapeshellarg("programas");
				$fileType = escapeshellarg("xml");
				$resval = array();
				$command = '/usr/bin/php /var/www/html/send_to_aws.php '. $fileToSend .' '. $category .' '. $fileType .'';
				exec($command, $resval);
			}
			if(SEND_TO_GOOGLE) {
				$this->executePy($file, "programas");
				unlink($file);
			}
		}else {
			echo 'false';
			$this->userLog(Session::get('user'), 'programa', 'crea XML varios', serialize($ids), 'false');
		}
	
	}
	
	
		
	public function limpiarCadena($cadena){
		$cadena = trim($cadena);
		$cadena = strtr($cadena,
				"�����������������������������������������������������",
				"aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
		$cadena = strtr($cadena,"ABCDEFGHIJKLMNOPQRSTUVWXYZ","abcdefghijklmnopqrstuvwxyz");
		$cadena = preg_replace('#([^.a-z0-9]+)#i', '-', $cadena);
		$cadena = preg_replace('#-{2,}#','-',$cadena);
		$cadena = preg_replace('#-$#','',$cadena);
		$cadena = preg_replace('#^-#','',$cadena);
		return $cadena;
	}
	
	/*
	 * Funci�n que toma cualquier nombre quita los acentos y los espacios
	 * los sustituye por guiones bajos.
	 */
	public function createUrlString($string) {
		$characters = array("&Aacute;" => "A", "&Ccedil;" => "c", "&Eacute;" => "e", "&Iacute;" => "i", "&Ntilde;" => "n", "&Oacute;" => "o", "&Uacute;" => "u", "&aacute;" => "a", "&ccedil;" => "c", "&eacute;" => "e", "&iacute;" => "i", "&ntilde;" => "n", "&oacute;" => "o", "&uacute;" => "u", "&agrave;" => "a", "&egrave;" => "e", "&igrave;" => "i", "&ograve;" => "o", "&ugrave;" => "u");
		$string = strtr($string, $characters);
		$string = strtolower(trim($string));
		$string = preg_replace("/[^a-z0-9-]/", "-", $string);
		$string = preg_replace("/-+/", "_", $string);
		return $string;
	}
	
	public function stripAllFields(&$fields) {
		foreach ($fields as $key => $value) {
			if (is_array($fields[$key]))
				$this->stripAllFields($fields[$key]);
			else 
				$fields[$key] = trim(strip_tags($value));
		}
	}
	
    public function permissions($originalQuery, $column, $where, $extras) {
		$permissions = Session::get('permissions');
		if($permissions == 0)
			$query = $originalQuery;
		else {
			$pieces = explode("-", $permissions);
			$piecesNum = count($pieces);
			if($where)
				$query = $originalQuery . ' AND (';
			else
				$query = $originalQuery . ' WHERE ';
			if($piecesNum == 1)
				$query .= $column .' = '.$pieces[0];
			else {
				$query .= $column .' = ' . $pieces[0];
				for($i = 1; $i < $piecesNum; $i++)
					$query .= ' OR ' . $column . ' = ' . $pieces[$i];
			}
			if($where)
				$query .= ')';
		}
		if($extras != "" || $extras != NULL)
			$query .= $extras;
		
		//echo $query;
		return $query;
	}
	
	/**
	* Is Image
	*
	* Determina si una URL est� disponible v�a CURL
	* @param string $url string a comprobar
	* @return bool $ret booleano con el resultado de la verificaci�n
	* @version orginal 2014/10/23 16:29 | modified: 
	*/
	public function isImage($id){
		$array = $this->programSingleList($id);
		$url = $array[0]["programatThumbmail"];
		if($url != "") {
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_NOBODY, true);
			$result = curl_exec($curl);
			$ret = false;
			$contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
			if($contentType != NULL) {
				preg_match('@([\w/+]+)(;\s+charset=(\S+))?@i', $contentType, $matches);
				$validTypes = array(
					'jpg'  => 'image/jpeg',
					'jpeg' => 'image/jpeg',
					'png'  => 'image/png',
					'gif'  => 'image/gif',
				);
				$imageStatus = array_search($matches[0], $validTypes);
				if ($result !== false) {
					$statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
					if ($statusCode == 200 && $imageStatus)
						$ret = true;   
				}
				curl_close($curl);
			}
			
			return $ret;
		}
	}
	
	/**
	* Get Image Info
	*
	* Obtiene el nombre, el peso y el tipo de una imagen por su URL
	* @param string $data string con la URL de la imagen
	* @return string json $imageInfo json con los datos de la imagen m�s un boolean validando que es una imagen (isimage, length, type, name)
	* @version original: 2014/10/23 16:35 | modified: 2014/11/10 10:03 l�nea 880, l�nea 882 y l�nea 893-900: se modifica la funci�n que regresa el peso de la imagen cuando no se obtiene v�a CURL
	*/
	public function getImageInfo($id) {
		$array = $this->programSingleList($id);
		$data = $array[0]["programatThumbmail"];
		if($data != "" && $this->isImage($id) == true) {
			$imageUrl = $data;
			$fileName = basename($data);
			$contentLength = 0;
			$ch = curl_init($data);
			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$data = curl_exec($ch);
			curl_close($ch);
			if (preg_match('/Content-Length: (\d+)/', $data, $matches))
				$contentLength = (int)$matches[1]; // Contiene el tama�o del archivo en bytes
			if (preg_match('/Content-Type: (.*)/', $data, $matches))
				$contentType = $matches[1];
			if($contentLength === NULL || $contentLength === 0 || $contentLength === "") {
				ob_start();
				readfile($imageUrl);
				$imageData = ob_get_contents();
				$length = strlen($imageData);
				ob_end_clean();
				$contentLength = $length;
			}
			$contentType = trim(preg_replace('/\s+/', ' ', $contentType));
			$imageInfo = array('response' => 'isimage', 'length' => $contentLength, 'type' => $contentType, 'name' => $fileName);
			
			return json_encode($imageInfo);
		}
	}
	
	/**
	* Rename File
	*
	* Modifica y renombra el archivo y regresa ese nombre para ser insertado en la base de datos, si no viene del bucket de AWS lo deja igual
	* @param string $fileName URL del archivo de imagen
	* @param string $name nombre con el que se sustituir� el nombre del archivo
	* @param string $newFileName nueva URL del archivo con el nombre
	* @version original: 2014/10/23 16:35 | modified:
	*/
	public function renameFile($fileName, $name) {
		$serverHost = parse_url($fileName);
		$newFileName = "";
		if(preg_match("/".AWS_BUCKET."/", $serverHost["host"])) {
			$path = parse_url($fileName);
			$pathPieces = explode("/", $path["path"]);
			$newName = $this->createUrlString($name);
			$pathInfo = pathinfo($path["path"]);
			$extension = strtolower($pathInfo["extension"]);
			$arrayNewUrl = array($path["scheme"].":/", $path["host"], IMG_PROGRAM_UPLOAD_DIR.$newName.".".$extension);
			$newFileName = implode("/", $arrayNewUrl);
			$dir = escapeshellarg(IMG_PROGRAM_UPLOAD_DIR);
			$oldName = escapeshellarg($pathInfo["basename"]);
			$newNameWithExt = escapeshellarg($newName.".".$extension);
			$ext = escapeshellarg($extension);
			$resval = array();
			$command = 'C://php//php.exe "C://Program Files//Apache Software Foundation//Apache2.2//htdocs//admin//rename_file_from_aws.php" '. $dir .' '. $oldName .' '. $newNameWithExt .' '. $ext .'';
			exec($command, $resval);
			$this->userLog(Session::get('user'), 'programa', 'renombra imagen de AWS', $newNameWithExt, serialize($resval));
		}
		else 
			$newFileName = $fileName;
		return $newFileName;
	}
	
	/**
	* User  Log
	*
	* Escribe en el log lo que el usuario hace dentro de la secci�n de programas
	* @param string $user usuario que realiza la acci�n
	* @param string $section la secci�n donde se realiza la acci�n
	* @param string $action la acci�n que se realiza
	* @param string $mod que se modifica
	* @param string $response respuesta del sistema
	* @version original: 2014/12/08 12:38 | modified:
	*/ 
	public function userLog($user, $section, $action, $mod, $response){
		$data = array('usuario' => $user, 'seccion' => $section, 'accion' => $action, 'modificacion' => $mod, 'respuesta' => $response, 'fecha' => date("Y-m-d H:i:s"));
		$this->db->insert('usuarioslog', $data);
	}
	
	/**
	* Change Image Domain
	* 
	* Cambia el dominio de las im�genes de los thumbs para evitar el uso de:
	* -Origin
	* -Galaxypreview
	* -Dynamic
	* Y lo deja con el subdominio: i2
	* @param string url
	* @return string url url sin los subdominios
	* @version original: 2015/01/21 10:05 | modified:
	*/
	public function changeImageDomain($url) {
		return preg_replace("/" . INVALID_IMAGE_SUBDOMAIN . "/i", ESMAS_IMAGE_SUBDOMAIN, $url);
	}
	
	/**
	* Programas JSON
	*
	* Hace un consulta a la base de datos para crear un archivo JSON con el id, el nombre, la URL del thumbnail y la rama a la que pertenece
	* @return string query con los datos de la consulta
	* @version original: 2015/01/30 12:23 | modified: 2015/02/03 18:15 Se a�ade al query la funci�n HTML_DECODE definida en la base de datos
	*/
	public function programasJson($name) {
		$arraySelect = $this->db->select('SELECT
												idprograma, programaTitulo, programatThumbmail, programaBranch
											FROM
												programa
											WHERE
												LOWER(HTML_DECODE(programaTitulo))
											 LIKE
												LOWER(HTML_DECODE("%' . trim($name) . '%"))');		
		$permissions = explode("-", Session::get('permissions'));
		if($arraySelect) {
			if(in_array($arraySelect[0]['programaBranch'], $permissions) || $permissions[0] == 0)
				$editable = URL . "program/edit/" . $arraySelect[0]['idprograma'];
			else
				$editable = "";
		}
		$arraySelect = array_map(function($changedSelect){
			return array(
				'name' => $changedSelect['programaTitulo'],
				'thumb' => $changedSelect['programatThumbmail']
			);
		}, $arraySelect);
		if($arraySelect)
			$arraySelect[0]['edit'] = $editable;
		return json_encode($arraySelect);
	}
	
	/**
	* Availability
	*
	* Comprueba si el nombre del programa es viable, si est� en la base de datos permite su edici�n si los permisos lo permiten
	* @return string data
	* @version original: 2015/01/30 12:27 | modified:
	*/
	public function availability($name) {
		$name = trim($name);
		if($name != "" || $name != NULL) {
			$programa = $this->programasJson($name);
			return $programa;
		}
	}
}

?>
