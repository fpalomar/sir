<?php
class Test_Model extends Model
{
	public function __construct()
    {
        parent::__construct();
    }
	
	public function testList() {
		return json_encode($this->db->select('SELECT
                                                     personaId, personaNombre
                                                FROM
                                                     persona
                                              WHERE
                                                     personaTipo = 1       
                                            ORDER BY
                                                     personaNombre
                                                 ASC'));
	}
}