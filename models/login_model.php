<?php

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
        $pass = filter_input(INPUT_POST, 'pass', FILTER_SANITIZE_STRING);
        // Web service de Galaxy
        if(preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $login)) {
            $client = new SoapClient(GALAXY_WSDL,array('trace' => 1, 'exceptions' => 0));
            try{
                $result = $client->__soapCall('validateLogin', array($login, $pass.SHARED_SECRET, '1'));
            }
            catch (SoapFault $exception) {
                header('location: ../login?e='.$exception);
            }
            $result = json_decode($result);
            if($result->{'result'} == 'false')
                header('location: ../login?e='.$result->{'message'});
            else {
                $gBranches = $result->{'permisos'};
                if (!empty($_SERVER['HTTP_CLIENT_IP']))
                    $ipId = $_SERVER['HTTP_CLIENT_IP'];
                elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipId = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else
                    $ipId = $_SERVER['REMOTE_ADDR'];
                $ipId = substr(strrchr($ipId,'.'),1);
                $pArray = array();
                try {
                    $query = $this->db->select('SELECT ramaId, ramaGalaxyId FROM rama');
                }
                catch (RuntimeException $e) {
                    header('location: ../login?e=Hay un error en la base de datos (1)');
                }
                foreach ($query as $key => $value) {
                    if(preg_match('/\b' . $value['ramaGalaxyId'] . '\b/i', $gBranches))
                        array_push($pArray, $value['ramaId']);
                }
                $permissions = implode ('-', $pArray);
                if($permissions == '' || $permissions == NULL)
                    header('location: ../login?e=No tienes permisos de acceso');
                else {
                    Session::init();
                    Session::set('rol', 'default');
                    Session::set('permissions', $permissions);
                    Session::set('loggedIn', true);
                    Session::set('idusuariosAdm', $ipId);
                    Session::set('user', $login);
                    header('location: ../dashboard/');
                }
            }
        }
        // Ingreso vía BD
        else {
            $sth = $this->db->prepare('SELECT
                                              usuarioId, usuarioRol, usuarioPermisos
                                         FROM
                                              usuario
                                        WHERE 
                                              usuarioLogin = :login AND usuarioPwd = :pass');
            try {
                $sth->execute(array(
                        ':login' => $login,
                        ':pass' => Hash::create('sha256', $pass, HASH_PASSWORD_KEY)
                ));
            }
            catch (RuntimeException $e) {
                header('location: ../login?e=Hubo un error al momento de hacer login, por favor intenta de nuevo');
            }
            $data = $sth->fetch();
            $count =  $sth->rowCount();
            if ($count > 0) {
                // Login
                Session::init();
                Session::set('rol', $data['usuarioRol']);
                Session::set('permissions', $data['usuarioPermisos']);
                Session::set('loggedIn', true);
                Session::set('idusuariosAdm', $data['usuarioId']);
                Session::set('user', $login);
                header('location: ../dashboard/');
            }
            else
                header('location: ../login?e=El usuario o password son incorrectos');
        }
    }
}