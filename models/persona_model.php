<?php
require_once 'rama_model.php';

class Persona_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Rama List
     * 
     * Muestra la lista completa de canales con los que se puede relacionar una persona o personaje
     * @return json query
     * @version original: | modified: 13/10/2016 10:21
     */
    public function ramaList() {
        $newRama = new Rama_Model();
        return json_encode($newRama->ramaList());
    }
    
    /**
     * Persona Data Table
     *
     * Muestra la lista completa de personas para el listado en el view de persona
     * @return array show
     * @version original: | modified: 13/10/2016 10:22
     */
    public function personaDataTable($data) {
        // Número total de elementos sin ninguna búsqueda o filtro
        $query = 'SELECT COUNT(*) as total FROM persona';
        $column = 'personaRama';
        $where = false;
        $extras = ' ORDER BY personaFechaModificacion DESC';
        try {
            $execute = $this->db->select($this->permissions($query, $column, $where, $extras));
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 1));
            die();
        }
        $totalData = $execute[0]['total'];
        $totalFiltered =  $totalData; // Cuando no hay parámetros de búsqueda
        // Valores a devolver
        $query = 'SELECT
                         personaId, CONCAT(personaNombre,\' \', personaApaterno ,\' \', personaAmaterno) AS personaNombre, personaResumenBio, personaFechaModificacion, personaStatus
                    FROM
                         persona';
        // Array de columnas como se ordena dependiendo de los valores de POST
        $columns = array(
            // Datatable column index  => Database column name
            1 => 'personaNombre',
            2 => 'personaResumenBio',
            3 => 'personaFechaModificacion',
            4 => 'personaStatus'
        );
        if(isset($data['order'][0]['column'])) {
            $orderBy = filter_var($data['order'][0]['column'], FILTER_SANITIZE_NUMBER_INT);
            $orderBy = $columns[$orderBy];
        }
        else
            $orderBy = 'personaFechaModificacion';
        if(isset($data['order'][0]['dir']))
            $orderDirection = filter_var($data['order'][0]['dir'], FILTER_SANITIZE_STRING);
        else
            $orderDirection = 'DESC';
        if(isset($data['start']))
            $limitStart = filter_var($data['start'], FILTER_SANITIZE_NUMBER_INT);
        else
            $limitStart = 0;
        if(isset($data['length']))
            $limitLenght = filter_var($data['length'], FILTER_SANITIZE_NUMBER_INT);
        else
            $limitLenght = limiteM;
        if(isset($data['draw']))
            $draw = filter_var($data['draw'], FILTER_SANITIZE_NUMBER_INT);
        else
            $draw = 1;
        // Si hay un parámetro de búsqueda
        $search = "";
        if(isset($data['search']['value']) && !empty($data['search']['value'])) {
            $where = true;
            $name =  filter_var($data['search']['value'], FILTER_SANITIZE_STRING);
            $search = " WHERE personaNombre LIKE '%" . $name . "%' OR personaApaterno LIKE '%" . $name . "%' OR personaAmaterno LIKE '%" . $name . "%'";
            $query.= $search;
        }
        // Cuando hay un parámetro de búsqueda se tiene que modificar  el número total de rows por resultado de búsqueda
        $countQuery = 'SELECT COUNT(*) as total FROM persona' . $search;
        try {
            $countExecute = $this->db->select($this->permissions($countQuery, $column, $where, $extras));
        }
         catch (RuntimeException $e) {
            return json_encode(array('dberror' => 2));
            die();
        }
        $totalFiltered = $countExecute[0]['total'];
        $extras = ' ORDER BY ' . $orderBy . ' ' . $orderDirection . ' LIMIT ' . $limitStart . ' ,' . $limitLenght;
        try {
            $execute = $this->db->select($this->permissions($query, $column, $where, $extras));
        }
        catch (RuntimeException $e) {
            return json_encode(array('dberror' => 3));
            die();
        }
        $data = array(
                    'draw'            => intval($draw),                 // Para cada request/draw del lado del cliente, se envía un número como parámetro
                    'recordsTotal'    => intval($totalData),            // Número total de elementos
                    'recordsFiltered' => intval($totalFiltered),        // Número total de elementos después de la búsqueda, si no hay búsqueda totalFiltered = totalData
                    'data'            => $execute                       // Array con el total de los elementos ($data OR $newData)
	            );
        return json_encode($data);
    }
    
    /*
     * Persona Single List
     * 
     * Función que muestra la lista completa de personas solamente sin ningún extra
     * @return json query
     * @version original: 29/09/2016 | modified: 06/10/2016 12:44
     */
    public function personaSingleList() {
        return json_encode($this->db->select('SELECT
                                                     personaId, CONCAT(personaNombre,\' \', personaApaterno ,\' \', personaAmaterno) AS personaNombre
                                                FROM
                                                     persona
                                              WHERE
                                                     personaTipo = 1       
                                            ORDER BY
                                                     personaNombre
                                                 ASC'));
        
    }
    
    /**
     * Personaje List
     * 
     * Función que muestra la lista completa de personajes con su programa relacionado
     * @return json query
     * @version original: 06/10/2016 12:45 | modified:
     */
    public function personajeList() {
        return json_encode($this->db->select('SELECT
                                                    a.personaId, TRIM(CONCAT(a.personaNombre, \' \', a.personaApaterno, \' \', a.personaAmaterno)) AS personaNombre, c.programaId, c.programaTitulo
                                               FROM
                                                    persona a
                                         INNER JOIN
                                                    personaprograma b
                                                 ON
                                                    a.personaId = b.personajeId
                                         INNER JOIN
                                                    programa c
                                                 ON
                                                    c.programaId = b.programaId
                                              WHERE
                                                    a.personaTipo = 2
                                           ORDER BY
                                                    a.personaNombre
                                                ASC'));
    }
    
    /**
     * Personaje Single List
     * 
     * Función que muestra la lista completa de personajes solamente sin ningún extra
     * @return json query
     * @version original: 06/09/2016 13:44 | modified:
     */
    public function personajeSingleList() {
        return json_encode($this->db->select('SELECT
                                                     personaId, CONCAT(personaNombre,\' \', personaApaterno ,\' \', personaAmaterno) AS personaNombre
                                                FROM
                                                     persona
                                              WHERE
                                                     personaTipo = 2       
                                            ORDER BY
                                                     personaFechaModificacion
                                                 ASC'));
        
    }
    
    /**
     * Programa List
     * 
     * Función que muestra la lista completa de programas
     * @return json query
     * @version original: 26/09/2016 | modified:
     */
    public function programaList() {
        return json_encode($this->db->select('SELECT
                                                     programaId, programaTitulo
                                                FROM 
                                                     programa
                                            ORDER BY
                                                     programaTitulo
                                                 ASC'));
    }
    
    /**
    * Check Person
    * 
    * Comprueba si ya existe la persona en la base de datos
    * @param string name
    * @return bool
    * @version original: 2016/09/27 20:10 | modified: 2016/09/30 10:12
    */
    public function checkperson($name) {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $name = trim($name);
        try {
            $result = $this->db->select('SELECT
                                                personaId
                                           FROM 
                                                persona
                                          WHERE
                                                TRIM((CONCAT(personaNombre, \' \', personaApaterno, \' \', personaAmaterno)
                                              =
                                                "' . $name . '"
                                        COLLATE utf8_spanish_ci))  
                                            AND 
                                                personaTipo
                                              =
                                                1');
        }
        catch (RuntimeException $e) {
            die(json_encode(array('dberror' => 1)));
        }
        return (bool) $result;
    }
    
    /**
     * Check Character
     * 
     * Comprueba si ya existe el personaje en la base de datos
     * @param string $completeName
     * @return bool
     * @version original: 2016/09/30 10:14 | modified: 2016/10/26 09:06
     */
    public function checkcharacter($completeName) {
        $nori_temp = explode('|', $completeName);
        $relation = false;
        try {
            $character = filter_var($nori_temp[1], FILTER_SANITIZE_STRING);
            $character = trim($character);
            $characterExecute = $this->db->select('SELECT
                                                          personaId
                                                     FROM 
                                                          persona
                                                    WHERE
                                                          TRIM((CONCAT(personaNombre, \' \', personaApaterno, \' \', personaAmaterno)
                                                     LIKE
                                                          "%' . $character . '%"
                                                  COLLATE utf8_spanish_ci))                                                         
                                                      AND 
                                                          personaTipo
                                                        =
                                                          2');
        }
        catch (RuntimeException $e) {
            die(json_encode(array('dberror' => 2)));
        }
        if($characterExecute)
            $relation = true;
        if(is_numeric($nori_temp[0])) {
            $id = filter_var($nori_temp[0], FILTER_SANITIZE_NUMBER_INT);
            $id = trim($id);
            foreach($characterExecute as $key => $value) {
                try {
                    $idExecute = $this->db->select('SELECT
                                                           *
                                                      FROM
                                                           personaprograma
                                                     WHERE
                                                           personaId
                                                         = 
                                                           ' . $id . '
                                                       AND
                                                           personajeId
                                                         = 
                                                           ' . $value['personaId'] . '');
                    if($idExecute) {
                        $relation = true;
                        break;
                    }
                }
                catch (RuntimeException $e) {
                    die(json_encode(array('dberror' => 3)));
                }
            }
        }
        else {
            $name = filter_var($nori_temp[0], FILTER_SANITIZE_STRING);
            $name = trim($nori_temp[0]);
            $isResgister = $this->checkperson($name);
            if($isResgister)
                $relation = true;
        }
        return (bool) $relation;
    }
    
    /**
    * Create
    * 
    * Crea una persona o personaje nuevo en la base de datos
    * @param array data información cruda del formulario 
    * @return json con el resultado del proceso de creación
    * @version original: 2016/10/16 20:03 | modified: 2017/01/09 16:17
    */
    public function create($data) {    
        $errors = array();  // Array con todos los errores al momento de procesar los datos del formulario
        if(empty($data)) {  // Validación para información vacía
            array_push($errors, 100);
            return json_encode(array('response'	  => 'error',
                                     'serversays' => $this->composeErrors($errors),
                                     'values'	  => $data));
        }
        else {
            // Variables de error para los valores necesarios en la BD
            $countType = 0;     // Contador de error para la validación de tipo
            $countName = 0;     // Contador de error para la validación de nombre
            $countCharName = 0; // Contador de error para la validación de nombre de personaje
            $countThumb = 0;    // Contador de error para la validación de thumb
            $countKeywords = 0; // Contador de error para la validación de keywords
            $countBio = 0;      // Contador de error para la validación de biografía
            $countBranch = 0;   // Contador de error para la validación de rama
            $countStatus = 0;   // Contador de error para la validación de estado
            // Validación de campo tipo
            if(!isset($data['type']))   // No existe el tipo: persona o personaje
                array_push($errors, 101);
            else {
                if(empty($data['type'])) { // Tipo vacío
                    array_push($errors, 102);
                    $countType++;
                }
                else if(!($data['type'] != 'person' || $data['type'] != 'character')) {    // No existe el tipo seleccionado
                    array_push($errors, 103);
                    $countType++;
                }
                else if($countType == 0) {  // Asignación de tipo a variable final
                    $typeName = filter_var($data['type'], FILTER_SANITIZE_STRING);
                    $typeName = trim($typeName);
                    if($typeName == 'person')
                        $type = 1;
                    else if($typeName == 'character')
                        $type = 2;
                }
            }
            // Si es persona
            if($type == 1) {
                // Validación de campo nombre
                if(!isset($data['person-complete-name']))   // No existe el nombre
                    array_push($errors, 111);
                else {
                    if(empty($data['person-complete-name'])) { // Nombre vacío
                        array_push($errors, 112);
                        $countName++;
                    }
                    else if(preg_match('#[^\p{L}\s-]#u', $data['person-complete-name'])) {    // Caracteres no permitidos en el nombre
                        array_push($errors, 113);
                        $countName++;
                    }
                    else if($countName == 0) {
                        $personName = filter_var($data['person-name'], FILTER_SANITIZE_STRING);
                        $personName = trim($personName);
                        $personSurname = filter_var($data['person-surname'], FILTER_SANITIZE_STRING);
                        $personSurname = trim($personSurname);
                        $personMaidenname = filter_var($data['person-maidenname'], FILTER_SANITIZE_STRING);
                        $personMaidenname = trim($personMaidenname);
                        $personCompleteName = filter_var($data['person-complete-name'], FILTER_SANITIZE_STRING);
                        $personCompleteName = trim($personCompleteName);
                    }
                }
                // Validación para nombre existente
                if(isset($personCompleteName) && $countName == 0) {	
                    if($this->checkperson($personCompleteName))
                        array_push($errors, 114);
                }
            }
            // Si es personaje
            else if($type == 2) {
                // Validación de campo nombre
                if(!isset($data['character-complete-name']))    // No existe el nombre del personaje
                    array_push($errors, 131);
                else {
                    if(empty($data['character-complete-name'])) {   // El nombre del personaje está vacío
                        array_push($errors, 132);
                        $countCharName++;
                    }
                    else if(preg_match('#[^\p{L}\s-]#u', $data['person-complete-name'])) {  // Caracteres no permitidos en el nombre
                        array_push($errors, 133);
                        $countCharName++;
                    }
                    else if(empty($data['character-person']) && empty($data['new-character-person-complete-name'])) {   // El nombre del intérprete está vacío
                        array_push($errors, 134);
                        $countCharName++;
                    }
                    else if($countCharName == 0) {
                        $personName = filter_var($data['character-name'], FILTER_SANITIZE_STRING);
                        $personName = trim($personName);
                        $personSurname = filter_var($data['character-surname'], FILTER_SANITIZE_STRING);
                        $personSurname = trim($personSurname);
                        $personMaidenname = filter_var($data['character-maidenname'], FILTER_SANITIZE_STRING);
                        $personMaidenname = trim($personMaidenname);
                        $personCompleteName = filter_var($data['character-complete-name'], FILTER_SANITIZE_STRING);
                        $personCompleteName = trim($personCompleteName);
                        if(!empty($data['character-person']))
                            $person = $data['character-person'];
                        else if(!empty($data['new-character-person-complete-name'])) {
                            $personNewName = filter_var($data['new-character-person-name'], FILTER_SANITIZE_STRING);
                            $personNewName = trim($personNewName);
                            $personNewSurname = filter_var($data['new-character-person-surname'], FILTER_SANITIZE_STRING);
                            $personNewSurname = trim($personNewSurname);
                            $personNewMaidenname = filter_var($data['new-character-person-maidenname'], FILTER_SANITIZE_STRING);
                            $personNewMaidenname = trim($personNewMaidenname);
                            $personNewCompleteName = filter_var($data['new-character-person-complete-name'], FILTER_SANITIZE_STRING);
                            $personNewCompleteName = $data['new-character-person-complete-name'];
                        }
                        $isActualCharacter = false;
                        if(isset($data['is-actual-character']) && !empty($data['is-actual-character'])) {
                            if($data['is-actual-character'] == 1)
                                $isActualCharacter = true;
                        }
                    }
                }
            }
            // Validación de campo imagen
            if(!isset($data['image-url']))  // No existe el campo de imagen
                array_push($errors, 115);
            else {
                if(empty($data['image-url'])) {	// No hay imagen
                    array_push($errors, 116);
                    $countThumb++;
                }
                else if(!preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?(?:jpe?g|gif|png)$_iuS', $data['image-url'])) {
                    array_push($errors, 117);
                }
                else if($countThumb == 0) {
                    $imageUrl = filter_var($data['image-url'], FILTER_SANITIZE_URL);
                    $imageUrl = trim($imageUrl);
                    if($type == 1)
                        $imageUrl = $this->renameFile($imageUrl, $personCompleteName);
                    else if($type == 2) {   // Se renombre la imagen con el nombre del interprete - personaje
                        if(!empty($data['character-person'])) { // Por Id de la persona
                            $artistInfo = json_decode($this->personaGetInfo($data['character-person']), true);
                            $artistName = $artistInfo[0]['personaNombre'];
                            $artistSurname = $artistInfo[0]['personaApaterno'];
                            $artistMaidenname = $artistInfo[0]['personaAmaterno'];
                            $artistCompleteName = trim($artistName . ' ' . $artistSurname . ' ' . $artistMaidenname);
                        }
                        if(!empty($data['new-character-person-complete-name'])) // La persona es nueva
                            $artistCompleteName = $personNewCompleteName;
                        $personCompleteName = explode('|', $personCompleteName);
                        $imageUrl = $this->renameFile($imageUrl, $artistCompleteName . ' ' . $personCompleteName[1]);
                    }
                    if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $imageUrl))
			             $imageUrl = $this->changeImageDomain($imageUrl);
                }
            }
            // Validación de campo keywords
            if(!isset($data['keywords']))   // No existe el campo de keywords
                array_push($errors, 118);
            else {
                if(empty($data['keywords'])) {	// No hay keywords
                    array_push($errors, 119);
                    $countKeywords++;
                }
                else if($countKeywords == 0) {
                    $keywords = filter_var($data['keywords'], FILTER_SANITIZE_STRING);
                    $keywords = trim($keywords);
                }
            }
            // Validación de campo de biografía
            if(!isset($data['bio-complete']))   // No existe el campo de biografía
                array_push($errors, 120);
            else {
                if(empty($data['bio-complete'])) {   // No hay biografía
                    array_push($errors, 121);
                    $countBio++;
                }
                else if($countBio == 0) {
                    $bioComplete = $data['bio-complete'];
                }
            }
            // Validación de campo de rama
            if(!isset($data['branches']))   // No existe el campo de ramas
                array_push($errors, 122);
            else {
                if(empty($data['branches'])) {   // No hay rama
                    array_push($errors, 123);
                    $countBranch++;
                }
                else if($countBranch == 0) {
                    $branch = filter_var($data['branches'], FILTER_SANITIZE_NUMBER_INT);
                    $branch = trim($branch);
                    $branch = intval($branch);
                }
            }
            // Validación de estado
            if(!isset($data['status'])) // No existe el estatus
                array_push($errors, 124);
            else {
                if(empty($data['status'])) {
                    array_push($errors, 125);
                    $countStatus++;
                }
                else if(!($data['status'] == 'save' || $data['status'] == 'publish')) {
                    array_push ($errors, 126);
                    $countStatus++;
                }
                else if($countStatus == 0) {
                    $s = filter_var($data['status'], FILTER_SANITIZE_STRING);
                    $s = trim($s);
                    if($s == 'publish')
                        $status = 0;
                    else if($s == 'save')
                        $status = 1;
                }
            }
            if(!empty($errors)) { // Respuesta con errores
                $json = json_encode(array('response'   => 'error',
                                          'serversays' => $this->composeErrors($errors),
                                          'values'     => $data));
                return $json;
            }
            else {
                if(Session::get('loggedIn')) {  // El usuario está loggeado
                    // Género
                    $gender = '';
                    if(isset($data['gender']) && ($data['gender'] == 1 || $data['gender'] == 2) && !empty($data['gender'])) {
                        $gender = filter_var($data['gender'], FILTER_SANITIZE_STRING);
                        $gender = trim($gender);
                    }
                    // Alias
                    $nicknames = '';
                    if(isset($data['nicknames']) && !empty($data['nicknames'])) {
                        $nicknames = filter_var($data['nicknames'], FILTER_SANITIZE_STRING);
                        $nicknames = trim($nicknames);
                    }
                    // Hobbies
                    $hobbies = '';
                    if(isset($data['hobbies']) && !empty($data['hobbies'])) {
                        $hobbies = filter_var($data['hobbies'], FILTER_SANITIZE_STRING);
                        $hobbies = trim($hobbies);
                    }
                    // Nacionalidad
                    $nacionality = '';
                    if(isset($data['nacionality']) && !empty($data['nacionality'])) {
                        $nacionality = filter_var($data['nacionality'], FILTER_SANITIZE_STRING);
                        $nacionality = trim($nacionality);
                    }
                    // Lugar de nacimiento
                    $birthplace = '';
                    if(isset($data['birthplace']) && !empty($data['birthplace'])) {
                        $birthplace = filter_var($data['birthplace'], FILTER_SANITIZE_STRING);
                        $birthplace = trim($birthplace);
                    }
                    // Fecha de nacimiento y signo zodiacal
                    $birthday = '0000-00-00';
                    $zodiacSign = '';
                    if(isset($data['birthday']) && !empty($data['birthday'])) {
                        $bs = explode('-', $data['birthday']);
                        if(checkdate($bs[1], $bs[0], $bs[2])) {
                            $birthday = $data['birthday'];
                            $birthday = date('Y-m-d', strtotime(str_replace('-', '-', $birthday)));
                            $zodiacSign = $this->zodiacSign($birthday);
                        }
                    }
                    // Fecha de fallecimiento
                    $deathday = '0000-00-00';
                    if(isset($data['deathday']) && !empty($data['deathday'])) {
                        $ds = explode('-', $data['deathday']);
                        if(checkdate($ds[1], $ds[0], $ds[2])) {
                            $deathday = $data['deathday'];
                            $deathday = date('Y-m-d', strtotime(str_replace('-', '-', $deathday)));
                        }
                    }
                    // Url de la biografía
                    $bioUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['bio-url'])) {
                        $bioUrl = filter_var($data['bio-url'], FILTER_SANITIZE_URL);
                        $bioUrl = trim($bioUrl);
                    }
                    // Resumen de la biografía
                    $bioResume = '';
                    if(isset($data['bio-resume']) && !empty($data['bio-resume'])) {
                        $bioResume = filter_var($data['bio-resume'], FILTER_SANITIZE_STRING);
                        $bioResume = trim($bioResume);
                    }
                    // Programas actuales existentes
                    $actualPrograms = '';
                    //if(isset($data['duallistbox_actualprogram']) && !empty(array_filter($data['duallistbox_actualprogram']))) {
                    if(isset($data['duallistbox_actualprogram']) && !empty($data['duallistbox_actualprogram'])) {
                        $ap = $data['duallistbox_actualprogram'];   // Array con los programas actuales
                        $ap_temp = array(); // Array temporal con los programas actuales 
                        // Verificación de los ids en la base de datos
                        foreach($ap as $key => $value) {
                            if($this->checkProgramId($value)) {
                                if(!in_array($value, $ap_temp))
                                    array_push($ap_temp, $value);
                            }
                        }
                        //if(!empty(array_filter($ap_temp))) // Filtra el resultado de la comprobación
                        if(!empty($ap_temp))
                            $actualPrograms = serialize($ap_temp);
                    }
                    // Programas relacionados existentes
                    $relatedPrograms = '';
                    //if(isset($data['duallistbox_relatedprogram']) && !empty(array_filter($data['duallistbox_relatedprogram']))) {
                    if(isset($data['duallistbox_relatedprogram']) && !empty($data['duallistbox_relatedprogram'])) {
                        $rp = $data['duallistbox_relatedprogram'];   // Array con los programas relacionados
                        $rp_temp = array(); // Array temporal con los programas relacionados 
                        // Verificación de los ids en la base de datos
                        foreach($rp as $key => $value) {
                            if($this->checkProgramId($value))
                                array_push($rp_temp, $value);
                        }
                        //if(!empty(array_filter($rp_temp))) // Filtra el resultado de la comprobación
                        if(!empty($rp_temp))
                            $relatedPrograms = serialize($rp_temp);
                    }
                    // Personajes actuales existentes
                    $actualCharacters = '';
                    //if(isset($data['duallistbox_actualcharacter']) && !empty(array_filter($data['duallistbox_actualcharacter']))) {
                    if(isset($data['duallistbox_actualcharacter']) && !empty($data['duallistbox_actualcharacter'])) {
                        $ac = $data['duallistbox_actualcharacter']; // Array con los personajes actuales
                        $ac_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($ac as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonaId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkProgramId($vp[1]);
                                    array_push($ac_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($ac_temp, $vp[0] . '|');
                            }
                        }
                        //if(!empty(array_filter($ac_temp)))  // Filtra el resultado de la comprobación
                        if(!empty($ac_temp)) 
                            $actualCharacters = serialize($ac_temp);
                    }
                    // Personajes relacionados existentes
                    $relatedCharacters = '';
                    //if(isset($data['duallistbox_relatedcharacter']) && !empty(array_filter($data['duallistbox_relatedcharacter']))) {
                    if(isset($data['duallistbox_relatedcharacter']) && !empty($data['duallistbox_relatedcharacter'])) {
                        $rc = $data['duallistbox_relatedcharacter']; // Array con los personajes relacionados
                        $rc_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($rc as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonaId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkProgramId($vp[1]);
                                    array_push($rc_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($rc_temp, $vp[0] . '|');
                            }
                        }
                        //if(!empty(array_filter($rc_temp)))  // Filtra el resultado de la comprobación
                        if(!empty($rc_temp))
                            $relatedCharacters = serialize ($rc_temp);
                    }
                    // Url de noticias
                    $newsUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['news'])) {
                        $newsUrl = filter_var($data['news'], FILTER_SANITIZE_URL);
                        $newsUrl = trim($newsUrl);
                    }
                    // Url de fotos
                    $photosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['photos'])) {
                        $photosUrl = filter_var($data['photos'], FILTER_SANITIZE_URL);
                        $photosUrl = trim($photosUrl);
                    }
                    // Url de videos
                    $videosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['videos'])) {
                        $videosUrl = filter_var($data['videos'], FILTER_SANITIZE_URL);
                        $videosUrl = trim($videosUrl);
                    }
                    // Url de Facebook
                    $facebookUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['facebook'])) {
                        $facebookUrl = filter_var($data['facebook'], FILTER_SANITIZE_URL);
                        $facebookUrl = trim($facebookUrl);
                    }
                    // Url de Twitter
                    $twitterUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['twitter'])) {
                        $twitterUrl = filter_var($data['twitter'], FILTER_SANITIZE_URL);
                        $twitterUrl = trim($twitterUrl);
                    }
                    // Url de Youtube
                    $youtubeUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['youtube'])) {
                        $youtubeUrl = filter_var($data['youtube'], FILTER_SANITIZE_URL);
                        $youtubeUrl = trim($youtubeUrl);
                    }
                    // Url de Instagram
                    $instagramUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['instagram'])) {
                        $instagramUrl = filter_var($data['instagram'], FILTER_SANITIZE_URL);
                        $instagramUrl = trim($instagramUrl);
                    }
                    // Url de Pinterest
                    $pinterestUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['pinterest'])) {
                        $pinterestUrl = filter_var($data['pinterest'], FILTER_SANITIZE_URL);
                        $pinterestUrl = trim($pinterestUrl);
                    }
                    // Url de Google Plus
                    $gplusUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['gplus'])) {
                        $gplusUrl = filter_var($data['gplus'], FILTER_SANITIZE_URL);
                        $gplusUrl = trim($gplusUrl);
                    }
                    // CEA para personas
                    $ceaType = '';
                    //if(isset($data['cea-type']) && !empty(array_filter($data['cea-type']))) {
                    if(isset($data['cea-type']) && !empty($data['cea-type'])) {
                        $ceaType = serialize($data['cea-type']);
                    }
                    // Hechos curiosos
                    $curiousFacts = '';
                    //if(isset($data['curious-fact']) && !empty(array_filter($data['curious-fact']))) {
                    if(isset($data['curious-fact']) && !empty($data['curious-fact'])) {
                        $curiousFacts = array_filter($data['curious-fact']);
                    }
                    // Premios para personas
                    $awards = '';
                   // if(isset($data['award-name']) && !empty(array_filter($data['award-name']))) {
                    if(isset($data['award-name']) && !empty($data['award-name'])) {
                        $awards = array_filter($data['award-name']);
                    }
                    // Nuevos programas actuales
                    $newActualPrograms = array();
                    //if(isset($data['new-actual-program-name']) && !empty(array_filter($data['new-actual-program-name']))) {
                    if(isset($data['new-actual-program-name']) && !empty($data['new-actual-program-name'])) {
                        $nap = array_filter($data['new-actual-program-name']);   // Array con los nuevos programas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nap as $key => $value) {
                            if(!$this->checkProgram($value))
                                array_push($newActualPrograms, $value);
                        }
                    }
                    // Nuevos programas relacionados
                    $newRelatedPrograms = array();
                    //if(isset($data['new-related-program-name']) && !empty(array_filter($data['new-related-program-name']))) {
                    if(isset($data['new-related-program-name']) && !empty($data['new-related-program-name'])) {
                        $nrp = array_filter($data['new-related-program-name']);   // Array con los nuevos programas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nrp as $key => $value) {
                            if(!$this->checkProgram($value))
                                array_push($newRelatedPrograms, $value);
                        }
                    }
                    $insertData = array('personaNombre'            => $personName,
                                        'personaApaterno'          => $personSurname,
                                        'personaAmaterno'          => $personMaidenname,
                                        'personaGenero'            => $gender,
                                        'personaAlias'             => $nicknames,
                                        'personaHobbies'           => $hobbies,
                                        'personaNacionalidad'      => $nacionality,
                                        'personaLugarNacimiento'   => $birthplace,
                                        'personaNacimiento'        => $birthday,
                                        'personaSigno'             => $zodiacSign,
                                        'personaDefuncion'         => $deathday,
                                        'personaUrlBio'            => $bioUrl,
                                        'personaResumenBio'        => $bioResume,
                                        'personaUrlThumb'          => $imageUrl,
                                        'personaKeywords'          => $keywords,
                                        'personaUrlNoticias'       => $newsUrl,
                                        'personaUrlFotos'          => $photosUrl,
                                        'personaUrlVideos'         => $videosUrl,
                                        'personaUrlFacebook'       => $facebookUrl,
                                        'personaUrlTwitter'        => $twitterUrl,
                                        'personaUrlYoutube'        => $youtubeUrl,
                                        'personaUrlInstagram'      => $instagramUrl,
                                        'personaUrlPinterest'      => $pinterestUrl,
                                        'personaUrlGplus'          => $gplusUrl,
                                        'personaContenidoBio'      => $bioComplete,
                                        'personaFechaIngreso'      => date('Y-m-d H:i:s'),
                                        'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                        'personaRama'              => $branch,
                                        'personaStatus'            => $status,
                                        'personaTipo'              => $type,
                                        'personaCEA'               => $ceaType);
                    try {
                        $this->db->beginTransaction();
                        $result = $this->db->insert('persona', $insertData);
                        $lastInsertId = $this->db->lastInsertId();
                        // Creación de relaciones
                        if($type == 1) {    // Para personas
                            $programs               = array();  // Array de programas actuales y relacionados
                            $arrayPrograms          = array();  // Array final de programas actuales y relacionados
                            $characters             = array();  // Array de personajes actuales y relacionados
                            $arrayCharacters        = array();  // Array final de personajes actuales y relacionados
                            $arrayActualPrograms    = array();  // Array para programas actuales
                            $arrayRelatedPrograms   = array();  // Array para programas relacionados
                            $arrayActualCharacters  = array();  // Array para personajes actuales
                            $arrayRelatedCharacters = array();  // Array para personajes relacionados
                            if(!empty($actualPrograms))
                                $arrayActualPrograms = unserialize($actualPrograms);
                            if(!empty($relatedPrograms))
                                $arrayRelatedPrograms = unserialize($relatedPrograms);
                            $programs = array_unique(array_merge($arrayActualPrograms, $arrayRelatedPrograms));    // Los dos arrays se unen, evitando ids repetidos
                            foreach ($programs as $program) {
                                $isActual = null;   // El programa es actual?
                                if(in_array($program, $arrayActualPrograms))   // Busca el Id del programa en el array deserializado de programas actuales
                                    $isActual = 1;
                                array_push($arrayPrograms, array('personaId'       => $lastInsertId,
                                                                 'programaId'      => $program,
                                                                 'personajeId'     => null,
                                                                 'programaActual'  => $isActual,
                                                                 'personajeActual' => null));
                            }
                            if(!empty($actualCharacters))
                                $arrayActualCharacters = unserialize($actualCharacters);
                            if(!empty($relatedCharacters))
                                $arrayRelatedCharacters = unserialize($relatedCharacters);
                            $characters = array_unique(array_merge($arrayActualCharacters, $arrayRelatedCharacters)); // Los dos arrays se unen, evitando ids repetidos
                            foreach($characters as $character) {
                                $isActual = null;   // El personaje y programa es actual?
                                if(in_array($character, $arrayActualCharacters))    // Busca los Ids del personaje y programa en el array deserializado de personajes actuales
                                    $isActual = 1;
                                $p = explode('|', $character);  // Separa los Ids del personaje y programa
                                $characterId = null;
                                $programId = null;
                                if(isset($p[0]))
                                    $characterId = intval($p[0]);
                                if(isset($p[1]))
                                    $programId = intval($p[1]);
                                array_push($arrayCharacters, array('personaId'       => $lastInsertId,
                                                                   'programaId'      => $programId,
                                                                   'personajeId'     => $characterId,
                                                                   'programaActual'  => $isActual,
                                                                   'personajeActual' => $isActual));
                            }
                            foreach ($arrayCharacters as $key => $value) {  // Quita del array de programas, los ids repetidos del array de la relación persona-programas-personajes
                                foreach($arrayPrograms as $k => $v) {
                                    if($value['programaId'] == $v['programaId'])
                                        unset($arrayPrograms[$k]);
                                }
                            }
                            $relations = array_merge($arrayPrograms, $arrayCharacters); // Une las relaciones de personas-programas y personas-programas-personajes sin repetición de Id
                            foreach($relations as $key => $value) { // Inserta las nuevas relaciones en la base de datos
                                $this->db->insert('personaprograma', $value);
                            }
                            // Nuevos programas actuales
                            if(!empty($newActualPrograms)) {
                                foreach($newActualPrograms as $key => $value) {
                                    $newActualProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newActualProgramName = trim($newActualProgramName);
                                    $newActualProgramUrl = $data['new-actual-program-url'][$key];
                                    $newActualProgramUrl = filter_var($newActualProgramUrl, FILTER_SANITIZE_URL);
                                    $newActualProgramUrl = trim($newActualProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newActualProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newActualProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newActualProgramId = $this->db->lastInsertId();
                                    $this->db->insert('personaprograma', array('personaId'       => $lastInsertId,
                                                                               'programaId'      => $newActualProgramId,
                                                                               'personajeId'     => null,
                                                                               'programaActual'  => 1,
                                                                               'personajeActual' => null));
                                }
                            }
                            // Nuevos programas relacionados
                            if(!empty($newRelatedPrograms)) {
                                foreach($newRelatedPrograms as $key => $value) {
                                    $newRelatedProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newRelatedProgramName = trim($newRelatedProgramName);
                                    $newRelatedProgramUrl = $data['new-related-program-url'][$key];
                                    $newRelatedProgramUrl = filter_var($newRelatedProgramUrl, FILTER_SANITIZE_URL);
                                    $newRelatedProgramUrl = trim($newRelatedProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newRelatedProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newRelatedProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newRelatedProgramId = $this->db->lastInsertId();
                                    $this->db->insert('personaprograma', array('personaId'       => $lastInsertId,
                                                                               'programaId'      => $newRelatedProgramId,
                                                                               'personajeId'     => null,
                                                                               'programaActual'  => null,
                                                                               'personajeActual' => null));
                                }
                            }
                            // Premios
                            if(!empty($awards)) {
                                $cat_temp = array_pop($data['award-category']);
                                $yea_temp = array_pop($data['award-year']);
                                foreach($awards as $key => $value) {
                                    $awardName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $awardName = trim($awardName);
                                    $awardCategory = filter_var($data['award-category'][$key]);
                                    $awardCategory = filter_var($awardCategory, FILTER_SANITIZE_STRING);
                                    $awardCategory = trim($awardCategory);
                                    $cy = filter_var($data['award-year'][$key], FILTER_SANITIZE_STRING);
                                    $cy = trim($cy);
                                    $cys = explode(', ', $cy);
                                    $year = array();
                                    foreach($cys as $k => $v) {
                                        if(intval($v) > 1000 && intval($v) < 2100)
                                            array_push($year, $v);
                                    }
                                    $awardYear = implode(", ", $year);
                                    $awardResult = filter_var($data['award-result'][$key]);
                                    $awardResult = filter_var($awardResult, FILTER_SANITIZE_STRING);
                                    $awardResult = trim($awardResult);
                                    $this->db->insert('personapremios', array('personaId'               => $lastInsertId,
                                                                              'personaPremiosNombre'    => $awardName,
                                                                              'personaPremiosCategoria' => $awardCategory,
                                                                              'personaPremiosResultado' => $awardResult,
                                                                              'personaPremiosAnio'      => $awardYear));
                                }
                            }
                        }
                        else if($type == 2) {   // Para personajes
                            if(!empty($data['character-person'])) { // Persona existene
                                $personId = filter_var($data['character-person'], FILTER_SANITIZE_NUMBER_INT);
                                $personId = trim($personId);
                            }
                            if(!empty($data['new-character-person-complete-name'])) {  // Persona nueva
                                $this->db->insert('persona', array('personaNombre'            => $personNewName,
                                                                   'personaApaterno'          => $personNewSurname,
                                                                   'personaAmaterno'          => $personNewMaidenname,
                                                                   'personaGenero'            => $gender,
                                                                   'personaUrlThumb'          => DUMMY_PERSON_URL_THUMB,
                                                                   'personaKeywords'          => $keywords,
                                                                   'personaRama'              => $branch,
                                                                   'personaContenidoBio'      => DUMMY_PERSON_BIO,
                                                                   'personaFechaIngreso'      => date('Y-m-d H:i:s'),
                                                                   'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                                                   'personaStatus'            => 1,
                                                                   'personaTipo'              => 1));
                                $personId = $this->db->lastInsertId();
                            }
                            $programs             = array();    // Array de programas actuales y relacionados
                            $arrayPrograms        = array();    // Array final de programas actuales y relacionados
                            $arrayActualPrograms  = array();    // Array para programas actuales
                            $arrayRelatedPrograms = array();    // Array para programas relacionados
                            if(!empty($actualPrograms))
                                $arrayActualPrograms = unserialize($actualPrograms);
                            if(!empty($relatedPrograms))
                                $arrayRelatedPrograms = unserialize($relatedPrograms);
                            // Nuevos programas actuales
                            if(!empty($newActualPrograms)) {
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                foreach($newActualPrograms as $key => $value) {
                                    $newActualProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newActualProgramName = trim($newActualProgramName);
                                    $newActualProgramUrl = $data['new-actual-program-url'][$key];
                                    $newActualProgramUrl = filter_var($newActualProgramUrl, FILTER_SANITIZE_URL);
                                    $newActualProgramUrl = trim($newActualProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newActualProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newActualProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newActualProgramId = $this->db->lastInsertId();
                                    array_push($arrayActualPrograms, $newActualProgramId);
                                }
                            }
                            // Nuevos programas relacionados
                            if(!empty($newRelatedPrograms)) {
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                foreach($newRelatedPrograms as $key => $value) {
                                    $newRelatedProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newRelatedProgramName = trim($newRelatedProgramName);
                                    $newRelatedProgramUrl = $data['new-related-program-url'][$key];
                                    $newRelatedProgramUrl = filter_var($newRelatedProgramUrl, FILTER_SANITIZE_URL);
                                    $newRelatedProgramUrl = trim($newRelatedProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newRelatedProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newRelatedProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newRelatedProgramId = $this->db->lastInsertId();
                                    array_push($arrayRelatedPrograms, $newRelatedProgramId);
                                }
                            }
                            $programs = array_unique(array_merge($arrayActualPrograms, $arrayRelatedPrograms));    // Los dos arrays se unen, evitando ids repetidos
                            if(empty($programs)) {  // Si no hay programas actuales o relacionados, sólo crea la relación persona-personaje
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                $this->db->insert('personaprograma', array('personaId'       => $personId,
                                                                           'programaId'      => null,
                                                                           'personajeId'     => $lastInsertId,
                                                                           'programaActual'  => null,
                                                                           'personajeActual' => $isActualC));
                            }
                            else {  // Crea la relación persona-programa-personaje
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                foreach ($programs as $program) {
                                    $isActualP = null;
                                    if(in_array($program, $arrayActualPrograms))   // Busca el Id del programa en el array deserializado de programas actuales
                                        $isActualP = 1;
                                    if($this->checkPersonaPrograma($personId, $program)) {  // Sólo actualiza la relación persona-programa con el personaje
                                        $this->update('personaprograma', array('personajeId'     => $lastInsertId,
                                                                               'programaActual'  => $isActualP,
                                                                               'personajeActual' => $isActualC), "`personaId` = {$personId} AND `programaId` = {$program} AND `personajeId` IS NULL");
                                    }
                                    else {  // Si no existe la relación persona-programa, la crea
                                        $this->db->insert('personaprograma', array('personaId'       => $personId,
                                                                                   'programaId'      => $program,
                                                                                   'personajeId'     => $lastInsertId,
                                                                                   'programaActual'  => $isActualP,
                                                                                   'personajeActual' => $isActualC));
                                    }
                                }
                            }
                        }
                        // Hechos curiosos
                        if(!empty($curiousFacts)) {
                            foreach($curiousFacts as $key => $value) {
                                $content = filter_var($value, FILTER_SANITIZE_STRING);
                                $content = trim($content);
                                $this->db->insert('personacuriosidades', array('personaId'                    => $lastInsertId,
                                                                               'personacuriosidadesContenido' => $content));
                            }
                        }
                        if($status == 0) {
                            $this->createJSON($lastInsertId);
                            $this->createSingleXML($lastInsertId);
                            //$this->updateDatosWP($lastInsertId);
                        }
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se creó con éxito.'));
                        return $json;   // Respuesta exitosa
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;	// Error en la base de datos
                    }
                }
                else {
                    $json = json_encode(array('response'   => 'error',
                                              'serversays' => $this->composeErrors(array_push($errors, 127)),
                                              'values'     => $data));
                    return $json;   // Sin permisos
                }
            }
        }
    }

    /**
     * Edit Save
     * 
     * Edita la información de persona o personaje nuevo dependiendo de su id
     * @param array data información cruda del formulario 
     * @return json con el resultado del proceso de creación
     * @version original: 2016/10/16 20:03 | modified:
     */
    public function editSave($data) {
        $errors = array();  // Array con todos los errores al momento de procesar los datos del formulario
        if(empty($data)) {  // Validación para información vacía
            array_push($errors, 100);
            return json_encode(array('response'   => 'error',
                                     'serversays' => $this->composeErrors($errors),
                                     'values'     => $data));
        }
        else {
            // Variables de error para los valores necesarios en la BD
            $countId = 0;       // Contador de error para la validación de id
            $countType = 0;     // Contador de error para la validación de tipo
            $countName = 0;     // Contador de error para la validación de nombre
            $countCharName = 0; // Contador de error para la validación de nombre de personaje
            $countThumb = 0;    // Contador de error para la validación de thumb
            $countKeywords = 0; // Contador de error para la validación de keywords
            $countBio = 0;      // Contador de error para la validación de biografía
            $countBranch = 0;   // Contador de error para la validación de rama
            $countStatus = 0;   // Contador de error para la validación de estado
            // Validación de id
            if(!isset($data['id'])) // Id no existe
                array_push($errors, 200);
            else {
                if(empty($data['id'])) {    // El Id está vacío
                    array_push($errors, 201);
                    $countId++;
                }
                else if(!ctype_digit($data['id'])) { // El Id no es un número
                    array_push($errors, 202);
                    $countId++;
                }
                else if(!$this->checkPersonaId($data['id'])) {  // El id no existe en la base de datos
                    array_push($data['id'], 203);
                    $countId++;
                }
                else if($countId == 0) {     // Asignación de Id a variable final
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                }
            }
            // Validación de campo tipo
            if(!isset($data['type']))   // No existe el tipo: persona o personaje
                array_push($errors, 101);
            else {
                if(empty($data['type'])) { // Tipo vacío
                    array_push($errors, 102);
                    $countType++;
                }
                else if(!($data['type'] != 'person' || $data['type'] != 'character')) {    // No existe el tipo seleccionado
                    array_push($errors, 103);
                    $countType++;
                }
                else if($countType == 0) {  // Asignación de tipo a variable final
                    $typeName = filter_var($data['type'], FILTER_SANITIZE_STRING);
                    $typeName = trim($typeName);
                    if($typeName == 'person')
                        $type = 1;
                    else if($typeName == 'character')
                        $type = 2;
                }
            }
            // Si es persona
            if($type == 1) {
                // Validación de campo nombre
                if(!isset($data['person-complete-name']))   // No existe el nombre
                    array_push($errors, 111);
                else {
                    if(empty($data['person-complete-name'])) { // Nombre vacío
                        array_push($errors, 112);
                        $countName++;
                    }
                    else if($countName == 0) {
                        $personName = filter_var($data['person-name'], FILTER_SANITIZE_STRING);
                        $personName = trim($personName);
                        $personSurname = filter_var($data['person-surname'], FILTER_SANITIZE_STRING);
                        $personSurname = trim($personSurname);
                        $personMaidenname = filter_var($data['person-maidenname'], FILTER_SANITIZE_STRING);
                        $personMaidenname = trim($personMaidenname);
                        $personCompleteName = filter_var($data['person-complete-name'], FILTER_SANITIZE_STRING);
                        $personCompleteName = trim($personCompleteName);
                    }
                }
            }
            // Si es personaje
            else if($type == 2) {
                // Validación de campo nombre
                if(!isset($data['character-complete-name']))    // No existe el nombre del personaje
                    array_push($errors, 131);
                else {
                    if(empty($data['character-complete-name'])) {   // El nombre del personaje está vacío
                        array_push($errors, 132);
                        $countCharName++;
                    }
                    else if($countCharName == 0) {
                        $personName = filter_var($data['character-name'], FILTER_SANITIZE_STRING);
                        $personName = trim($personName);
                        $personSurname = filter_var($data['character-surname'], FILTER_SANITIZE_STRING);
                        $personSurname = trim($personSurname);
                        $personMaidenname = filter_var($data['character-maidenname'], FILTER_SANITIZE_STRING);
                        $personMaidenname = trim($personMaidenname);
                        $personCompleteName = filter_var($data['character-complete-name'], FILTER_SANITIZE_STRING);
                        $personCompleteName = trim($personCompleteName);
                        $personId = filter_var($data['character-person'], FILTER_SANITIZE_NUMBER_INT);
                        $personId = trim($personId);
                        if(!empty($data['character-person']))
                            $person = $data['character-person'];
                        else if(!empty($data['new-character-person-complete-name'])) {
                            $personNewName = filter_var($data['new-character-person-name'], FILTER_SANITIZE_STRING);
                            $personNewName = trim($personNewName);
                            $personNewSurname = filter_var($data['new-character-person-surname'], FILTER_SANITIZE_STRING);
                            $personNewSurname = trim($personNewSurname);
                            $personNewMaidenname = filter_var($data['new-character-person-maidenname'], FILTER_SANITIZE_STRING);
                            $personNewMaidenname = trim($personNewMaidenname);
                            $personNewCompleteName = filter_var($data['new-character-person-complete-name'], FILTER_SANITIZE_STRING);
                            $personNewCompleteName = trim($personNewCompleteName);
                        }
                        $isActualCharacter = false;
                        if(isset($data['is-actual-character']) && !empty($data['is-actual-character'])) {
                            if($data['is-actual-character'] == 1)
                                $isActualCharacter = true;
                        }
                    }
                }
            }
            // Validación de campo imagen
            if(!isset($data['image-url']))  // No existe el campo de imagen
                array_push($errors, 115);
            else {
                if(empty($data['image-url'])) { // No hay imagen
                    array_push($errors, 116);
                    $countThumb++;
                }
                else if(!preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?(?:jpe?g|gif|png)$_iuS', $data['image-url'])) {
                    array_push($errors, 117);
                }
                else if($countThumb == 0) {
                    $imageUrl = filter_var($data['image-url'], FILTER_SANITIZE_URL);
                    $imageUrl = trim($imageUrl);
                    if($type == 1)
                        $imageUrl = $this->renameFile($imageUrl, $personCompleteName);
                    else if($type == 2) {
                        $artistInfo = json_decode($this->personaGetInfo($personId), true);
                        $artistName = $artistInfo[0]['personaNombre'];
                        $artistSurname = $artistInfo[0]['personaApaterno'];
                        $artistMaidenname = $artistInfo[0]['personaAmaterno'];
                        $artistCompleteName = trim($artistName . ' ' . $artistSurname . ' ' . $artistMaidenname);
                        $imageUrl = $this->renameFile($imageUrl, $artistCompleteName . ' ' . $personCompleteName);
                    }
                    if(preg_match("/" . INVALID_IMAGE_SUBDOMAIN . "/i", $imageUrl))
                        $imageUrl = $this->changeImageDomain($imageUrl);
                }
            }
            // Validación de campo keywords
            if(!isset($data['keywords']))   // No existe el campo de keywords
                array_push($errors, 118);
            else {
                if(empty($data['keywords'])) {  // No hay keywords
                    array_push($errors, 119);
                    $countKeywords++;
                }
                else if($countKeywords == 0) {
                    $keywords = filter_var($data['keywords'], FILTER_SANITIZE_STRING);
                    $keywords = trim($keywords);
                }
            }
            // Validación de campo de biografía
            if(!isset($data['bio-complete']))   // No existe el campo de biografía
                array_push($errors, 120);
            else {
                if(empty($data['bio-complete'])) {   // No hay biografía
                    array_push($errors, 121);
                    $countBio++;
                }
                else if($countBio == 0) {
                    $bioComplete = $data['bio-complete'];
                }
            }
            // Validación de campo de rama
            if(!isset($data['branches']))   // No existe el campo de ramas
                array_push($errors, 122);
            else {
                if(empty($data['branches'])) {   // No hay rama
                    array_push($errors, 123);
                    $countBranch++;
                }
                else if($countBranch == 0) {
                    $branch = filter_var($data['branches'], FILTER_SANITIZE_NUMBER_INT);
                    $branch = trim($branch);
                    $branch = intval($branch);
                }
            }
            // Validación de estado
            if(!isset($data['status'])) // No existe el estatus
                array_push($errors, 124);
            else {
                if(empty($data['status'])) {
                    array_push($errors, 125);
                    $countStatus++;
                }
                else if(!($data['status'] == 'save' || $data['status'] == 'publish')) {
                    array_push ($errors, 126);
                    $countStatus++;
                }
                else if($countStatus == 0) {
                    $s = filter_var($data['status'], FILTER_SANITIZE_STRING);
                    $s = trim($s);
                    if($s == 'publish')
                        $status = 0;
                    else if($s == 'save')
                        $status = 1;
                }
            }
            if(!empty($errors)) { // Respuesta con errores
                $json = json_encode(array('response'   => 'error',
                                          'serversays' => $this->composeErrors($errors),
                                          'values'     => $data));
                return $json;
            }
            else {
                if(Session::get('loggedIn')) {  // El usuario está loggeado
                    // Género
                    $gender = '';
                    if(isset($data['gender']) && ($data['gender'] == 1 || $data['gender'] == 2) && !empty($data['gender'])) {
                        $gender = filter_var($data['gender'], FILTER_SANITIZE_STRING);
                        $gender = trim($gender);
                    }
                    // Alias
                    $nicknames = '';
                    if(isset($data['nicknames']) && !empty($data['nicknames'])) {
                        $nicknames = filter_var($data['nicknames'], FILTER_SANITIZE_STRING);
                        $nicknames = trim($nicknames);
                    }
                    // Hobbies
                    $hobbies = '';
                    if(isset($data['hobbies']) && !empty($data['hobbies'])) {
                        $hobbies = filter_var($data['hobbies'], FILTER_SANITIZE_STRING);
                        $hobbies = trim($hobbies);
                    }
                    // Nacionalidad
                    $nacionality = '';
                    if(isset($data['nacionality']) && !empty($data['nacionality'])) {
                        $nacionality = filter_var($data['nacionality'], FILTER_SANITIZE_STRING);
                        $nacionality = trim($nacionality);
                    }
                    // Lugar de nacimiento
                    $birthplace = '';
                    if(isset($data['birthplace']) && !empty($data['birthplace'])) {
                        $birthplace = filter_var($data['birthplace'], FILTER_SANITIZE_STRING);
                        $birthplace = trim($birthplace);
                    }
                    // Fecha de nacimiento y signo zodiacal
                    $birthday = '0000-00-00';
                    $zodiacSign = '';
                    if(isset($data['birthday']) && !empty($data['birthday'])) {
                        $bs = explode('-', $data['birthday']);
                        if(checkdate($bs[1], $bs[0], $bs[2])) {
                            $birthday = $data['birthday'];
                            $birthday = date('Y-m-d', strtotime(str_replace('-', '-', $birthday)));
                            $zodiacSign = $this->zodiacSign($birthday);
                        }
                    }
                    // Fecha de fallecimiento
                    $deathday = '0000-00-00';
                    if(isset($data['deathday']) && !empty($data['deathday'])) {
                        $ds = explode('-', $data['deathday']);
                        if(checkdate($ds[1], $ds[0], $ds[2])) {
                            $deathday = $data['deathday'];
                            $deathday = date('Y-m-d', strtotime(str_replace('-', '-', $deathday)));
                        }
                    }
                    // Url de la biografía
                    $bioUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['bio-url'])) {
                        $bioUrl = filter_var($data['bio-url'], FILTER_SANITIZE_URL);
                        $bioUrl = trim($bioUrl);
                    }
                    // Resumen de la biografía
                    $bioResume = '';
                    if(isset($data['bio-resume']) && !empty($data['bio-resume'])) {
                        $bioResume = filter_var($data['bio-resume'], FILTER_SANITIZE_STRING);
                        $bioResume = trim($bioResume);
                    }
                    // Programas actuales existentes
                    $actualPrograms = '';
                    //if(isset($data['duallistbox_actualprogram']) && !empty(array_filter($data['duallistbox_actualprogram']))) {
                    if(isset($data['duallistbox_actualprogram']) && !empty($data['duallistbox_actualprogram'])) {
                        $ap = $data['duallistbox_actualprogram'];   // Array con los programas actuales
                        $ap_temp = array(); // Array temporal con los programas actuales 
                        // Verificación de los ids en la base de datos
                        foreach($ap as $key => $value) {
                            if($this->checkProgramId($value))
                                array_push($ap_temp, $value);
                        }
                        //if(!empty(array_filter($ap_temp))) // Filtra el resultado de la comprobación
                        if(!empty($ap_temp))
                            $actualPrograms = serialize($ap_temp);
                    }
                    // Programas relacionados existentes
                    $relatedPrograms = '';
                    //if(isset($data['duallistbox_relatedprogram']) && !empty(array_filter($data['duallistbox_relatedprogram']))) {
                    if(isset($data['duallistbox_relatedprogram']) && !empty($data['duallistbox_relatedprogram'])) {
                        $rp = $data['duallistbox_relatedprogram'];   // Array con los programas relacionados
                        $rp_temp = array(); // Array temporal con los programas relacionados 
                        // Verificación de los ids en la base de datos
                        foreach($rp as $key => $value) {
                            if($this->checkProgramId($value))
                                array_push($rp_temp, $value);
                        }
                        //if(!empty(array_filter($rp_temp))) // Filtra el resultado de la comprobación
                        if(!empty($rp_temp))
                            $relatedPrograms = serialize($rp_temp);
                    }
                    // Personajes actuales existentes
                    $actualCharacters = '';
                    //if(isset($data['duallistbox_actualcharacter']) && !empty(array_filter($data['duallistbox_actualcharacter']))) {
                    if(isset($data['duallistbox_actualcharacter']) && !empty($data['duallistbox_actualcharacter'])) {
                        $ac = $data['duallistbox_actualcharacter']; // Array con los personajes actuales
                        $ac_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($ac as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonaId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkProgramId($vp[1]);
                                    array_push($ac_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($ac_temp, $vp[0] . '|');
                            }
                        }
                        //if(!empty(array_filter($ac_temp)))  // Filtra el resultado de la comprobación
                        if(!empty($ac_temp)) 
                            $actualCharacters = serialize($ac_temp);
                    }
                    // Personajes relacionados existentes
                    $relatedCharacters = '';
                    //if(isset($data['duallistbox_relatedcharacter']) && !empty(array_filter($data['duallistbox_relatedcharacter']))) {
                    if(isset($data['duallistbox_relatedcharacter']) && !empty($data['duallistbox_relatedcharacter'])) {
                        $rc = $data['duallistbox_relatedcharacter']; // Array con los personajes relacionados
                        $rc_temp = array();
                        // Verificación de los ids en la base de datos
                        foreach($rc as $key => $value) {
                            $vp = explode('|', $value);
                            if($this->checkPersonaId($vp[0])) {
                                if(isset($vp[1])) {
                                    $this->checkProgramId($vp[1]);
                                    array_push($rc_temp, $vp[0] . '|' . $vp[1]);
                                }
                                else
                                    array_push($rc_temp, $vp[0] . '|');
                            }
                        }
                        //if(!empty(array_filter($rc_temp)))  // Filtra el resultado de la comprobación
                        if(!empty($rc_temp))
                            $relatedCharacters = serialize ($rc_temp);
                    }
                    // Url de noticias
                    $newsUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['news'])) {
                        $newsUrl = filter_var($data['news'], FILTER_SANITIZE_URL);
                        $newsUrl = trim($newsUrl);
                    }
                    // Url de fotos
                    $photosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['photos'])) {
                        $photosUrl = filter_var($data['photos'], FILTER_SANITIZE_URL);
                        $photosUrl = trim($photosUrl);
                    }
                    // Url de videos
                    $videosUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['videos'])) {
                        $videosUrl = filter_var($data['videos'], FILTER_SANITIZE_URL);
                        $videosUrl = trim($videosUrl);
                    }
                    // Url de Facebook
                    $facebookUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['facebook'])) {
                        $facebookUrl = filter_var($data['facebook'], FILTER_SANITIZE_URL);
                        $facebookUrl = trim($facebookUrl);
                    }
                    // Url de Twitter
                    $twitterUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['twitter'])) {
                        $twitterUrl = filter_var($data['twitter'], FILTER_SANITIZE_URL);
                        $twitterUrl = trim($twitterUrl);
                    }
                    // Url de Youtube
                    $youtubeUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['youtube'])) {
                        $youtubeUrl = filter_var($data['youtube'], FILTER_SANITIZE_URL);
                        $youtubeUrl = trim($youtubeUrl);
                    }
                    // Url de Instagram
                    $instagramUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['instagram'])) {
                        $instagramUrl = filter_var($data['instagram'], FILTER_SANITIZE_URL);
                        $instagramUrl = trim($instagramUrl);
                    }
                    // Url de Pinterest
                    $pinterestUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['pinterest'])) {
                        $pinterestUrl = filter_var($data['pinterest'], FILTER_SANITIZE_URL);
                        $pinterestUrl = trim($pinterestUrl);
                    }
                    // Url de Google Plus
                    $gplusUrl = '';
                    if(preg_match('_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]-*)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(?:[a-z\x{00a1}-\x{ffff}]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$_iuS', $data['gplus'])) {
                        $gplusUrl = filter_var($data['gplus'], FILTER_SANITIZE_URL);
                        $gplusUrl = trim($gplusUrl);
                    }
                    // CEA para personas
                    $ceaType = '';
                    //if(isset($data['cea-type']) && !empty(array_filter($data['cea-type']))) {
                    if(isset($data['cea-type']) && !empty($data['cea-type'])) {
                        $ceaType = serialize($data['cea-type']);
                    }
                    // Hechos curiosos
                    $curiousFacts = '';
                    //if(isset($data['curious-fact']) && !empty(array_filter($data['curious-fact']))) {
                    if(isset($data['curious-fact']) && !empty($data['curious-fact'])) {
                        $curiousFacts = array_filter($data['curious-fact']);
                    }
                    // Premios para personas
                    $awards = '';
                   // if(isset($data['award-name']) && !empty(array_filter($data['award-name']))) {
                    if(isset($data['award-name']) && !empty($data['award-name'])) {
                        $awards = array_filter($data['award-name']);
                    }
                    // Nuevos programas actuales
                    $newActualPrograms = array();
                    //if(isset($data['new-actual-program-name']) && !empty(array_filter($data['new-actual-program-name']))) {
                    if(isset($data['new-actual-program-name']) && !empty($data['new-actual-program-name'])) {
                        $nap = array_filter($data['new-actual-program-name']);   // Array con los nuevos programas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nap as $key => $value) {
                            if(!$this->checkProgram($value))
                                array_push($newActualPrograms, $value);
                        }
                    }
                    // Nuevos programas relacionados
                    $newRelatedPrograms = array();
                    //if(isset($data['new-related-program-name']) && !empty(array_filter($data['new-related-program-name']))) {
                    if(isset($data['new-related-program-name']) && !empty($data['new-related-program-name'])) {
                        $nrp = array_filter($data['new-related-program-name']);   // Array con los nuevos programas actuales
                        // Verificación del nombre en la base de datos
                        foreach($nrp as $key => $value) {
                            if(!$this->checkProgram($value))
                                array_push($newRelatedPrograms, $value);
                        }
                    }
                    $updateData = array('personaNombre'            => $personName,
                                        'personaApaterno'          => $personSurname,
                                        'personaAmaterno'          => $personMaidenname,
                                        'personaGenero'            => $gender,
                                        'personaAlias'             => $nicknames,
                                        'personaHobbies'           => $hobbies,
                                        'personaNacionalidad'      => $nacionality,
                                        'personaLugarNacimiento'   => $birthplace,
                                        'personaNacimiento'        => $birthday,
                                        'personaSigno'             => $zodiacSign,
                                        'personaDefuncion'         => $deathday,
                                        'personaUrlBio'            => $bioUrl,
                                        'personaResumenBio'        => $bioResume,
                                        'personaUrlThumb'          => $imageUrl,
                                        'personaKeywords'          => $keywords,
                                        'personaUrlNoticias'       => $newsUrl,
                                        'personaUrlFotos'          => $photosUrl,
                                        'personaUrlVideos'         => $videosUrl,
                                        'personaUrlFacebook'       => $facebookUrl,
                                        'personaUrlTwitter'        => $twitterUrl,
                                        'personaUrlYoutube'        => $youtubeUrl,
                                        'personaUrlInstagram'      => $instagramUrl,
                                        'personaUrlPinterest'      => $pinterestUrl,
                                        'personaUrlGplus'          => $gplusUrl,
                                        'personaContenidoBio'      => $bioComplete,
                                        'personaFechaModificacion' => date('Y-m-d H:i:s'),
                                        'personaRama'              => $branch,
                                        'personaStatus'            => $status,
                                        'personaTipo'              => $type,
                                        'personaCEA'               => $ceaType);
                    try {
                        $this->db->beginTransaction();
                        $result = $this->db->update('persona', $updateData, "`personaId` = {$id}");
                        // Actualización de relaciones
                        if($type == 1) {    // Para personas
                            $this->db->deleteAll('personaprograma', "personaId = '$id'"); // Elimina todas las relaciones existentes con ese Id
                            $programs               = array();  // Array de programas actuales y relacionados
                            $arrayPrograms          = array();  // Array final de programas actuales y relacionados
                            $characters             = array();  // Array de personajes actuales y relacionados
                            $arrayCharacters        = array();  // Array final de personajes actuales y relacionados
                            $arrayActualPrograms    = array();  // Array para programas actuales
                            $arrayRelatedPrograms   = array();  // Array para programas relacionados
                            $arrayActualCharacters  = array();  // Array para personajes actuales
                            $arrayRelatedCharacters = array();  // Array para personajes relacionados
                            if(!empty($actualPrograms))
                                $arrayActualPrograms = unserialize($actualPrograms);
                            if(!empty($relatedPrograms))
                                $arrayRelatedPrograms = unserialize($relatedPrograms);
                            $programs = array_unique(array_merge($arrayActualPrograms, $arrayRelatedPrograms));    // Los dos arrays se unen, evitando ids repetidos
                            foreach ($programs as $program) {
                                $isActual = null;   // El programa es actual?
                                if(in_array($program, $arrayActualPrograms))   // Busca el Id del programa en el array deserializado de programas actuales
                                    $isActual = 1;
                                array_push($arrayPrograms, array('personaId'       => $id,
                                                                 'programaId'      => $program,
                                                                 'personajeId'     => null,
                                                                 'programaActual'  => $isActual,
                                                                 'personajeActual' => null));
                            }
                            if(!empty($actualCharacters))
                                $arrayActualCharacters = unserialize($actualCharacters);
                            if(!empty($relatedCharacters))
                                $arrayRelatedCharacters = unserialize($relatedCharacters);
                            $characters = array_unique(array_merge($arrayActualCharacters, $arrayRelatedCharacters)); // Los dos arrays se unen, evitando ids repetidos
                            foreach($characters as $character) {
                                $isActual = null;   // El personaje y programa es actual?
                                if(in_array($character, $arrayActualCharacters))    // Busca los Ids del personaje y programa en el array deserializado de personajes actuales
                                    $isActual = 1;
                                $p = explode('|', $character);  // Separa los Ids del personaje y programa
                                $characterId = null;
                                $programId = null;
                                if(isset($p[0]))
                                    $characterId = intval($p[0]);
                                if(isset($p[1]))
                                    $programId = intval($p[1]);
                                array_push($arrayCharacters, array('personaId'       => $id,
                                                                   'programaId'      => $programId,
                                                                   'personajeId'     => $characterId,
                                                                   'programaActual'  => $isActual,
                                                                   'personajeActual' => $isActual));
                            }
                            foreach ($arrayCharacters as $key => $value) {  // Quita del array de programas, los ids repetidos del array de la relación persona-programas-personajes
                                foreach($arrayPrograms as $k => $v) {
                                    if($value['programaId'] == $v['programaId'])
                                        unset($arrayPrograms[$k]);
                                }
                            }
                            $relations = array_merge($arrayPrograms, $arrayCharacters); // Une las relaciones de personas-programas y personas-programas-personajes sin repetición de Id
                            foreach($relations as $key => $value) // Inserta las nuevas relaciones en la base de datos
                                $this->db->insert('personaprograma', $value);
                            //print_r($relations);
                            // Nuevos programas actuales
                            if(!empty($newActualPrograms)) {
                                foreach($newActualPrograms as $key => $value) {
                                    $newActualProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newActualProgramName = trim($newActualProgramName);
                                    $newActualProgramUrl = $data['new-actual-program-url'][$key];
                                    $newActualProgramUrl = filter_var($newActualProgramUrl, FILTER_SANITIZE_URL);
                                    $newActualProgramUrl = trim($newActualProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newActualProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newActualProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newActualProgramId = $this->db->lastInsertId();
                                    $this->db->insert('personaprograma', array('personaId'       => $id,
                                                                               'programaId'      => $newActualProgramId,
                                                                               'personajeId'     => null,
                                                                               'programaActual'  => 1,
                                                                               'personajeActual' => null));
                                }
                            }
                            // Nuevos programas relacionados
                            if(!empty($newRelatedPrograms)) {
                                foreach($newRelatedPrograms as $key => $value) {
                                    $newRelatedProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newRelatedProgramName = trim($newRelatedProgramName);
                                    $newRelatedProgramUrl = $data['new-related-program-url'][$key];
                                    $newRelatedProgramUrl = filter_var($newRelatedProgramUrl, FILTER_SANITIZE_URL);
                                    $newRelatedProgramUrl = trim($newRelatedProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newRelatedProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newRelatedProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newRelatedProgramId = $this->db->lastInsertId();
                                    $this->db->insert('personaprograma', array('personaId'       => $id,
                                                                               'programaId'      => $newRelatedProgramId,
                                                                               'personajeId'     => null,
                                                                               'programaActual'  => null,
                                                                               'personajeActual' => null));
                                }
                            }
                            // Premios
                            $awardsToUpdate = array();
                            if(!empty($awards)) {
                                $cat_temp = array_pop($data['award-category']);
                                $yea_temp = array_pop($data['award-year']);
                                foreach($awards as $key => $value) {
                                    $awardName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $awardName = trim($awardName);
                                    $awardCategory = filter_var($data['award-category'][$key]);
                                    $awardCategory = filter_var($awardCategory, FILTER_SANITIZE_STRING);
                                    $awardCategory = trim($awardCategory);
                                    $cy = filter_var($data['award-year'][$key], FILTER_SANITIZE_STRING);
                                    $cy = trim($cy);
                                    $cys = explode(', ', $cy);
                                    $year = array();
                                    foreach($cys as $k => $v) {
                                        if(intval($v) > 1000 && intval($v) < 2100)
                                            array_push($year, $v);
                                    }
                                    $awardYear = implode(", ", $year);
                                    $awardResult = filter_var($data['award-result'][$key]);
                                    $awardResult = filter_var($awardResult, FILTER_SANITIZE_STRING);
                                    $awardResult = trim($awardResult);
                                    array_push($awardsToUpdate, array('personaId'               => $id,
                                                                      'personaPremiosNombre'    => $awardName,
                                                                      'personaPremiosCategoria' => $awardCategory,
                                                                      'personaPremiosResultado' => $awardResult,
                                                                      'personaPremiosAnio'      => $awardYear));
                                }
                            }
                            $this->db->deleteAll('personapremios', "personaId = '$id'");   // Elimina todos los premios existentes con ese Id
                            foreach ($awardsToUpdate as $key => $value)
                                $this->db->insert('personapremios', $value);    // Inserta los nuevos premios
                        }
                        else if($type == 2) {
                            $this->db->deleteAll('personaprograma', "personajeId = '$id'"); // Elimina todas las relaciones existentes con ese Id
                            $programs             = array();    // Array de programas actuales y relacionados
                            $arrayPrograms        = array();    // Array final de programas actuales y relacionados
                            $arrayActualPrograms  = array();    // Array para programas actuales
                            $arrayRelatedPrograms = array();    // Array para programas relacionados
                            if(!empty($actualPrograms))
                                $arrayActualPrograms = unserialize($actualPrograms);
                            if(!empty($relatedPrograms))
                                $arrayRelatedPrograms = unserialize($relatedPrograms);
                            $programs = array_unique(array_merge($arrayActualPrograms, $arrayRelatedPrograms));    // Los dos arrays se unen, evitando ids repetidos
                            if(empty($programs)) {  // Si no hay programas actuales o relacionados, sólo crea la relación persona-personaje
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                $this->db->insert('personaprograma', array('personaId'       => $personId,
                                                                           'programaId'      => null,
                                                                           'personajeId'     => $id,
                                                                           'programaActual'  => null,
                                                                           'personajeActual' => $isActualC));
                            }
                            else {  // Crea la relación persona-programa-personaje
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                foreach ($programs as $program) {
                                    $isActualP = null;
                                    if(in_array($program, $arrayActualPrograms))   // Busca el Id del programa en el array deserializado de programas actuales
                                        $isActualP = 1;
                                    if($this->checkPersonaPrograma($personId, $program)) {  // Sólo actualiza la relación persona-programa con el personaje
                                        $this->update('personaprograma', array('personajeId'     => $id,
                                                                               'programaActual'  => $isActualP,
                                                                               'personajeActual' => $isActualC), "`personaId` = {$personId} AND `programaId` = {$program} AND `personajeId` IS NULL");
                                    }
                                    else {  // Si no existe la relación persona-programa, la crea
                                        $this->db->insert('personaprograma', array('personaId'       => $personId,
                                                                                   'programaId'      => $program,
                                                                                   'personajeId'     => $id,
                                                                                   'programaActual'  => $isActualP,
                                                                                   'personajeActual' => $isActualC));
                                    }
                                }
                            }
                            // Nuevos programas actuales
                            if(!empty($newActualPrograms)) {
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                foreach($newActualPrograms as $key => $value) {
                                    $newActualProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newActualProgramName = trim($newActualProgramName);
                                    $newActualProgramUrl = $data['new-actual-program-url'][$key];
                                    $newActualProgramUrl = filter_var($newActualProgramUrl, FILTER_SANITIZE_URL);
                                    $newActualProgramUrl = trim($newActualProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newActualProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newActualProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newActualProgramId = $this->db->lastInsertId();
                                    $this->db->insert('personaprograma', array('personaId'       => $personId,
                                                                               'programaId'      => $newActualProgramId,
                                                                               'personajeId'     => $id,
                                                                               'programaActual'  => 1,
                                                                               'personajeActual' => $isActualC));
                                }
                            }
                            // Nuevos programas relacionados
                            if(!empty($newRelatedPrograms)) {
                                $isActualC = null;
                                if($isActualCharacter)
                                    $isActualC = 1;
                                foreach($newRelatedPrograms as $key => $value) {
                                    $newRelatedProgramName = filter_var($value, FILTER_SANITIZE_STRING);
                                    $newRelatedProgramName = trim($newRelatedProgramName);
                                    $newRelatedProgramUrl = $data['new-related-program-url'][$key];
                                    $newRelatedProgramUrl = filter_var($newRelatedProgramUrl, FILTER_SANITIZE_URL);
                                    $newRelatedProgramUrl = trim($newRelatedProgramUrl);
                                    $this->db->insert('programa', array('programaTitulo'              => $newRelatedProgramName,
                                                                        'programaThumbnail'           => DUMMY_PROGRAM_URL_THUMB,
                                                                        'programaKeywords'            => $keywords,
                                                                        'programaDescripcionCompleta' => DUMMY_PROGRAM_DESCRIPTION,
                                                                        'programaRama'                => $branch,
                                                                        'programaUrl'                 => $newRelatedProgramUrl,
                                                                        'programaFechaIngreso'        => date('Y-m-d H:i:s'),
                                                                        'programaFechaModificacion'   => date('Y-m-d H:i:s'),
                                                                        'programaStatus'              => 1));
                                    $newRelatedProgramId = $this->db->lastInsertId();
                                    $this->db->insert('personaprograma', array('personaId'       => $personId,
                                                                               'programaId'      => $newRelatedProgramId,
                                                                               'personajeId'     => $id,
                                                                               'programaActual'  => null,
                                                                               'personajeActual' => null));
                                }
                            }
                        }
                        // Hechos curiosos
                        $factsToUpdate = array();
                        if(!empty($curiousFacts)) {
                            foreach($curiousFacts as $key => $value) {
                                $content = filter_var($value, FILTER_SANITIZE_STRING);
                                $content = trim($content);
                                array_push($factsToUpdate, array('personaId'                    => $id,
                                                                 'personacuriosidadesContenido' => $content));
                            }
                        }
                        $this->db->deleteAll('personacuriosidades', "personaId = '$id'");   // Elimina todos los hechos curiosos existentes con ese Id
                        foreach ($factsToUpdate as $key => $value)
                            $this->db->insert('personacuriosidades', $value);    // Inserta los nuevos hechos curiosos
                        if($status == 0) {
                            $this->createJSON($id);
                            $this->createSingleXML($id);
                        }
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se creó con éxito.'));
                        return $json;   // Respuesta exitosa
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;   // Error en la base de datos
                    }
                }
                else {
                    $json = json_encode(array('response'   => 'error',
                                              'serversays' => $this->composeErrors(array_push($errors, 127)),
                                              'values'     => $data));
                    return $json;   // Sin permisos
                }
            }
        }
    }

    /**
    * Error
    *
    * Función con la lista de errores y mensajes que se desplegaran
    * -De 0 a 99 errores de la base de datos
    * -De 100 a 1000 errores en los valores del inputs
    * --100s para errores en los procesos de creación
    * --200s para errores en los procesos de edición 
    * @return json contiene mensajes y textos
    * @version original: 2016/10/16 21:08 | modified:
    */
    public function error() {
        $messages = array(0 => 'Error en la base de datos.',
                        100 => 'No hay información enviada.',
                        101 => 'No existe el tipo.',
                        102 => 'No seleccionaste el tipo: persona o personaje.',
                        103 => 'No existe el tipo seleccionado.',
                        111 => 'No existe el nombre.',
                        112 => 'El nombre no puede estar vacío.',
                        113 => 'No se permiten esos caracteres en el nombre.',
                        114 => 'Ese persona ya está registrada.',
                        115 => 'No existe la imagen.',
                        116 => 'La imagen no puede estar vacía.',
                        117 => 'La imagen no tiene un formato correcto: sólo .JPG, .GIF o .PNG.',
                        118 => 'No existen los keywords.',
                        119 => 'Los keywords no pueden estar vacíos.',
                        120 => 'No existe la biografía.',
                        121 => 'La biografía no puede estar vacía.',
                        122 => 'No existe la rama,',
                        123 => 'La rama no puede estar vacía.',
                        124 => 'No existe el estatus.',
                        125 => 'El estatus no puede estar vacío.',
                        126 => 'El estatus no existe.',
                        127 => 'No estás loggeado.',
                        131 => 'No existe el nombre del personaje.',
                        132 => 'El nombre del personaje está vacío.',
                        133 => 'No se permiten esos caracteres en el nombre.',
                        134 => 'El nombre del intérprete del personaje está vacío.',
                        200 => 'No existe el Id.',
                        201 => 'El Id está vacío.',
                        202 => 'El Id no es un número.',
                        203 => 'El Id no existe en la base de datos.',
                        10  => 'Ese usuario no est&aacute; en la base de datos.',
                        201 => 'Ese usuario ya existe pero con otro id.');
        $texts = array('warning' => array('title' => 'El servidor encontró este error',
                                          'texts' => 'Pero no pasa nada, por favor cámbialo y vuelve a intentar.'),
                                          'dberror' => array('title' => 'El servidor encontró este grave error',
                                          'texts' => 'Por favor, ayúdanos a corregirlo reportándolo.'),
                                          'dbnull'  => array('title' => 'El servidor buscó y buscó pero',
                                          'texts' => 'Probablemente llegaste aquí por error pero si crees que es un error del servidor, ayúdanos a corregirlo reportándolo.</p><p align="center"><a href="' . URL .'persona/"><span class="btn btn-warning btn-sm ">Regresar</span></a>'));
        return json_encode(array('messages' => $messages, 'texts' => $texts));
    }
    
    /*
    * Compose Errors
    * 
    * Función que crea un array con los mensajes y textos así como de la lista errores de los procesos de creación o edición
    * @param array errorArray
    * @return array errorCompose
    * @version original: 2016/10/16 21:10 | modified:
    */
    public function composeErrors($errorArray) {
        $errorCompose = array();
        $error = array();
        $errors = array();
        $errorHandler = json_decode($this->error());
        $messages = $errorHandler->messages;
        $texts = $errorHandler->texts;
        $count = 0;
        if(in_array(0, $errorArray)) { // Error en base de datos
            $errorCompose['title'] = $texts->dberror->title;
            $errorCompose['texts'] = $texts->dberror->texts;
            $errorType = $errorArray[1];
            array_pop($errorArray);
        }
        else {
            $errorCompose['title'] = $texts->warning->title;
            $errorCompose['texts'] = $texts->warning->texts;
        }
        foreach ($errorArray as $key => $value) {
            switch($value) {
                case 0:
                    $errors[$count] = array('errorcode' => $value, 'errormessage' => $messages->{$value} . ' (' . $errorType .')');
                break;
                default:
                    $errors[$count] = array('errorcode' => $value, 'errormessage' => $messages->{$value});
            }
            $count++;
        } 
        $errorCompose['error'] = $errors;
        return $errorCompose;
    }
    
    /**
     * Permissions
     * 
     * Función que arma el query dependiendo de los permisos que tenga el usuario
     * @param string originalQuery
     * @param string column
     * @param string where
     * @param string extras
     * @return string query
     * @version orginal: | modified:
     */
    public function permissions($originalQuery, $column, $where, $extras) {
            $permissions = Session::get('permissions');
            if($permissions == 0)
                    $query = $originalQuery;
            else {
                    $pieces = explode("-", $permissions);
                    $piecesNum = count($pieces);
                    if($where)
                            $query = $originalQuery . ' AND (';
                    else
                            $query = $originalQuery . ' WHERE ';
                    if($piecesNum == 1)
                            $query .= $column .' = '.$pieces[0];
                    else {
                            $query .= $column .' = ' . $pieces[0];
                            for($i = 1; $i < $piecesNum; $i++)
                                    $query .= ' OR ' . $column . ' = ' . $pieces[$i];
                    }
                    if($where)
                            $query .= ')';
            }
            if($extras != "" || $extras != NULL)
                    $query .= $extras;

            return $query;
    }
    
    /**
     * Is Ajax
     *
     * Función que comprueba si la petición se realiza desde ajax 
     * @return request
     * @version original: 2014/02/27 16:18 | modified:
     */
    public function isAjax() {
       return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
    
    /**
     * Zodiac Sign
     * 
     * Función que obtiene el signo zodiacal dependiendo de la fecha de nacimiento
     * @param date birthday
     * @return string zodiac sign
     * @version original: 2016/10/17 11:40 | modified;
     */
    public function zodiacSign($birthday) {
        $bs = explode('-', $birthday);
        $zodiacSign = $bs[1] . '-' . $bs[2]; 
        $sign = '';
        if (($zodiacSign >= '03-21') && ($zodiacSign <= '04-20')) $sign = 'Aries';
        if (($zodiacSign >= '09-23') && ($zodiacSign <= '10-22')) $sign = 'Libra'; 
        if (($zodiacSign >= '04-21') && ($zodiacSign <= '05-21')) $sign = 'Tauro'; 
        if (($zodiacSign >= '10-23') && ($zodiacSign <= '11-22')) $sign = 'Escorpio'; 
        if (($zodiacSign >= '05-22') && ($zodiacSign <= '06-21')) $sign = 'Géminis'; 
        if (($zodiacSign >= '11-22') && ($zodiacSign <= '12-21')) $sign = 'Sagitario'; 
        if (($zodiacSign >= '06-22') && ($zodiacSign <= '07-22')) $sign = 'Cáncer'; 
        if (($zodiacSign >= '07-23') && ($zodiacSign <= '08-22')) $sign = 'Leo'; 
        if (($zodiacSign >= '01-21') && ($zodiacSign <= '02-18')) $sign = 'Acuario'; 
        if (($zodiacSign >= '08-23') && ($zodiacSign <= '09-22')) $sign = 'Virgo'; 
        if (($zodiacSign >= '02-19') && ($zodiacSign <= '03-20')) $sign = 'Piscis'; 
        if (empty($sign))                                         $sign = 'Capricornio'; 
        return $sign; 
    }
    
    /**
     * Check Program Id
     * 
     * Verifica si el id que se manda está en la base de datos
     * @param int $programaId
     * @return bool $result
     * @version original: 2016/10/17 12:27 | modified:
     */
    public function checkProgramId($programaId) {
        $programaId = filter_var($programaId, FILTER_SANITIZE_NUMBER_INT);
        $programaId = trim($programaId);
        try {
            $result = $this->db->select('SELECT
                                                programaId
                                           FROM 
                                                programa
                                          WHERE
                                                programaId
                                              =
                                                "' . $programaId . '"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }
    
    /**
     * Check Persona Id
     * 
     * Verifica si el id que se manda está en la base de datos
     * @param int $personaId
     * @return bool $result
     * @version original: 2016/10/17 17:44 | modified:
     */
    public function checkPersonaId($personaId) {
        $personaId = filter_var($personaId, FILTER_SANITIZE_NUMBER_INT);
        $personaId = trim($personaId);
        try {
            $result = $this->db->select('SELECT
                                                personaId
                                           FROM 
                                                persona
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }
    
    /**
     * Check Persona Programa
     * 
     * Verifica si existe la relación persona y programa
     * @param int $personaId
     * @param int $programaId
     * @return bool $result
     * @version original: 2016/10/17 16:57 | modified:
     */
    public function checkPersonaPrograma($personaId, $programaId) {
        $personaId = filter_var($personaId, FILTER_SANITIZE_NUMBER_INT);
        $personaId = trim($personaId);
        $programaId = filter_var($programaId, FILTER_SANITIZE_NUMBER_INT);
        $programaId = trim($programaId);
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM 
                                                personaprograma
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"
                                            AND
                                                programaId
                                              =
                                                "' . $programaId .'"
                                            AND 
                                                personajeId IS NULL');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }

    /**
     * Check Persona Personaje
     * 
     * Verifica si existe la relación persona y personaje
     * @param int $personaId
     * @param int $personajeId
     * @return bool $result
     * @version original: 2016/10/26 19:43 | modified:
     */
    public function checkPersonaPersonaje($personaId, $personajeId) {
        $personaId = filter_var($personaId, FILTER_SANITIZE_NUMBER_INT);
        $personaId = trim($personaId);
        $personajeId = filter_var($personajeId, FILTER_SANITIZE_NUMBER_INT);
        $personajeId = trim($personajeId);
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM 
                                                personaprograma
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"
                                            AND
                                                personajeId
                                              =
                                                "' . $personajeId .'"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }
    
    /**
     * Check Persona Programa Personaje
     * 
     * Verifica si existe la relación persona - programa - personaje
     * @param int $personaId
     * @param int $programaId
     * @param int $personajeId
     * @return bool $result
     * @version original: 2016/10/17 16:57 | modified:
     */
    public function checkPersonaProgramaPersonaje($personaId, $programaId, $personajeId) {
        $personaId = filter_var($personaId, FILTER_SANITIZE_NUMBER_INT);
        $personaId = trim($personaId);
        $programaId = filter_var($programaId, FILTER_SANITIZE_NUMBER_INT);
        $programaId = trim($programaId);
        $personajeId = filter_var($personajeId, FILTER_SANITIZE_NUMBER_INT);
        $personajeId = trim($personajeId);
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM 
                                                personaprograma
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"
                                            AND
                                                programaId
                                              =
                                                "' . $programaId .'"
                                            AND
                                                personajeId
                                              =
                                                "' . $personajeId .'"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }
    
    /**
    * Check Programa
    * 
    * Comprueba si ya existe la programa en la base de datos
    * @param string $name
    * @return bool
    * @version original: 2016/10/16 09:43 | modified: 2016/10/18 08:37
    */
    public function checkProgram($name) {
        $name = filter_var($name, FILTER_SANITIZE_STRING);
        $name = trim($name);
        try {
            $result = $this->db->select('SELECT
                                                programaId
                                           FROM 
                                                programa
                                          WHERE
                                                programaTitulo
                                              =
                                                "' . $name . '"
                                        COLLATE utf8_spanish_ci');
        }
        catch (RuntimeException $e) {
            return false;
        }
        return (bool) $result;
    }

    /**
    * Create Single XML
    * 
    * Crea el XML de la persona o personaje con base en su Id
    * @param int $personaId
    * @version original: 2016/10/18 11:36 | modified:
    */
    public function createSingleXML($personaId) {
        $imp     = new DOMImplementation;
        $dtd	 = $imp->createDocumentType("gsafeed", "-//Google//DTD GSA Feeds//EN", "");
        $dom 	 = $imp->createDocument("", "", $dtd);
        $dom    -> encoding = "UTF-8";
        $dom    -> formatOutput = true;
        $node    = $dom->createElement('gsafeed');
        $header  = $dom->createElement('header');
        $data    = $dom->createElement('datasource');
        $feed    = $dom->createElement('feedtype');
        $data   -> nodeValue = 'personas';
        $feed   -> nodeValue = 'metadata-and-url';
        $header -> appendChild($data);
        $header -> appendChild($feed);
        $node   -> appendChild($header);
        $group   = $dom->createElement('group');
        $node   -> appendChild($group);
        $dom    -> appendChild($node);
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM 
                                                persona
                                          WHERE
                                                personaId
                                              =
                                                "' . $personaId . '"');
        }
        catch (RuntimeException $e) {
            return false;
        }
        //if(!empty(array_filter($result))) {
        if(!empty($result)) {
            try {    // Obtiene el nombre de la rama
                $branchName = $this->db->select('SELECT
                                                        ramaNombre
                                                   FROM 
                                                        rama
                                                  WHERE
                                                        ramaId
                                                      =
                                                        "' . $result[0]['personaRama'] . '"');
            }
            catch (RuntimeException $e) {
                return false;
            }
            // Tipo de persona
            if($result[0]['personaTipo'] == 1) {
                $type = 1;
                $personaTipo = 'persona';
            }
            else if($result[0]['personaTipo'] == 2) {
                $type = 2;
                $personaTipo = 'personaje';
            }
            // Nombre completo
            $name = $result[0]['personaNombre'] . ' ' . $result[0]['personaApaterno'] . ' ' . $result[0]['personaAmaterno'];
            $name = trim($name);
            $record    = $dom->createElement('record');
            $group    -> appendChild($record);
            $metadata  = $dom->createElement('metadata');
            $content   = $dom->createElement('content');
            $record   -> appendChild($metadata);
            $record   -> appendChild($content);
            $content  -> appendChild($dom->createCDATASection(strip_tags(html_entity_decode($result[0]['personaContenidoBio']))));
            $record   -> setAttribute('url', INDEXED_URL . URLify::filter($branchName[0]['ramaNombre'], false) . '/' . $personaTipo . '/' . URLify::filter($name, false) . '/'); 
            $record   -> setAttribute('action','add');
            $record   -> setAttribute('mimetype','text/html');
            $record   -> setAttribute('lock','true');
            $record   -> setAttribute('last-modified', date('D, d M Y G:i:s') . ' CDT');
            $ignore = array('personaProgramaActual', 'personaUrlProgramaActual', 'personaPersonajeActual', 'personaConsultaRelacionada', 'personaRama', 'personaStatus', 'personaTipo', 'personaCEA'); // Ignora estos keys
            $data = $result[0]; // Copia para posteriormente usarla en las relaciones
            foreach($ignore as $k => $v)    // Remueve del array los keys ignorados
                unset($result[0][$v]);
            foreach($result[0] as $key => $value) {
                if(empty($value) || $value == NULL || $value == '0000-00-00') // Ignora valores nulos
                    continue;
                else {
                    switch($key) {
                        case 'personaId':
                            $metaName = 'id_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaNombre':
                            $metaName = 'nombre_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaApaterno':
                            $metaName = 'apellido_paterno_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaAmaterno':
                            $metaName = 'apellido_materno_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaGenero':
                            $metaName = 'genero_' . $personaTipo;
                            if($value == 1)
                                $metaContent = 'femenino';
                            else 
                                $metaContent = 'masculino';
                            break;
                        case 'personaAlias':
                            $metaName = 'alias_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaHobbies':
                            $metaName = 'hobbies_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaNacionalidad':
                            $metaName = 'nacionalidad_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaLugarNacimiento':
                            $metaName = 'lugar_nacimiento_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaNacimiento':
                            $metaName = 'fecha_nacimiento';
                            $metaContent = $value;
                            break;
                        case 'personaSigno':
                            $metaName = 'signo_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaDefuncion':
                            $metaName = 'fecha_fallecimiento_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaUrlBio':
                            $metaName = 'url_biografia';
                            $metaContent = $value;
                            break;
                        case 'personaResumenBio':
                            $metaName = 'resumen_biografia';
                            $metaContent = strip_tags(html_entity_decode($value));
                            break;
                        case 'personaUrlThumb':
                            $metaName = 'thumbnail_persona';
                            $metaContent = $value;
                            break;
                        case 'personaKeywords':
                            $metaName = 'keywords';
                            $metaContent = $value;
                            break;
                        case 'personaUrlNoticias':
                            $metaName = 'url_noticias_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaUrlFotos':
                            $metaName = 'url_fotos';
                            $metaContent = $value;
                            break;
                        case 'personaUrlVideos':
                            $metaName = 'url_videos';
                            $metaContent = $value;
                            break;
                        case 'personaUrlFacebook':
                            $metaName = 'url_facebook';
                            $metaContent = $value;
                            break;
                        case 'personaUrlTwitter':
                            $metaName = 'url_twitter';
                            $metaContent = $value;
                            break;
                        case 'personaUrlYoutube':
                            $metaName = 'url_youtube';
                            $metaContent = $value;
                            break;
                        case 'personaUrlInstagram':
                            $metaName = 'url_instagram';
                            $metaContent = $value;
                            break;
                        case 'personaUrlPinterest':
                            $metaName = 'url_pinterest';
                            $metaContent = $value;
                            break;
                        case 'personaUrlGplus':
                            $metaName = 'url_gplus';
                            $metaContent = $value;
                            break;
                        case 'personaUrlNoticias':
                            $metaName = 'url_noticias';
                            $metaContent = $value;
                            break;
                        case 'personaContenidoBio':
                            $metaName = 'content';
                            $metaContent = strip_tags(html_entity_decode($value));
                            break;
                        case 'personaFechaIngreso':
                            $metaName = 'fecha_creacion_' . $personaTipo;
                            $metaContent = $value;
                            break;
                        case 'personaFechaModificacion':
                            $metaName = 'fecha_modificacion_' . $personaTipo;
                            $metaContent = $value;
                            break;
                    }
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', $metaName);
                    $meta     -> setAttribute('content', $metaContent);
                }
            }
            // Seed Content
            $meta      = $dom->createElement('meta');
            $metadata -> appendChild($meta);
            $meta     -> setAttribute('name', 'seed_content');
            $meta     -> setAttribute('content', $personaTipo);
            // Relaciones
            $actualCharacters  = array();    // Array con el Id de los personajes actuales
            $relatedCharacters = array();    // Array con el Id de los personajes relacionados
            $actualPrograms    = array();    // Array con el Id de los programas actuales
            $relatedPrograms   = array();    // Array con el Id de los programas relacionados
            if($type == 1) {    // Relaciones de persona
                // Obtiene las relaciones de la persona
                $relations = json_decode($this->personaGetRelations($personaId), true);
                if(!empty($relations[0])) {
                    // Programas y personajes relacionados
                    foreach($relations as $key => $value) {
                        if(!empty($value['personajeId'])) { // Obtiene el Id del personaje actual y relacionado
                            if($value['personajeActual'] == 1)
                                array_push($actualCharacters, $value['personajeId']);
                            array_push($relatedCharacters, $value['personajeId']);
                        }
                        if(!empty($value['programaId'])) {  // Obtiene el Id del programa actual y relacionado
                            if($value['programaActual'] == 1)
                                array_push($actualPrograms, $value['programaId']);
                            array_push($relatedPrograms, $value['programaId']);
                        }
                    }
                    if(!empty($actualCharacters)) { // Muestra los personajes actuales en el XML
                        foreach ($actualCharacters as $key => $value) {
                            $character = json_decode($this->personaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_personaje_actual_' . $key);                          
                            $meta     -> setAttribute('content', $character[0]['personaNombre']);
                            if(!empty($character[0]['personaApaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_paterno_personaje_actual_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaApaterno']);
                            }
                            if(!empty($character[0]['personaAmaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_materno_personaje_actual_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaAmaterno']);
                            }
                            if(!empty($character[0]['personaUrlBio'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_bio_personaje_actual_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaUrlBio']);
                            }
                            if($character[0]['personaUrlThumb'] != NULL || !empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_thumb_personaje_actual_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaUrlThumb']);
                            }
                        }
                    }
                    if(!empty($relatedCharacters)) {    // Muestra los personajes relacionados en el XML
                        foreach ($relatedCharacters as $key => $value) {
                            $character = json_decode($this->personaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_personaje_relacionado_' . $key);                          
                            $meta     -> setAttribute('content', $character[0]['personaNombre']);
                            if(!empty($character[0]['personaApaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_paterno_personaje_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaApaterno']);
                            }
                            if(!empty($character[0]['personaAmaterno'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'apellido_materno_personaje_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaAmaterno']);
                            }
                            if(!empty($character[0]['personaUrlBio'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_bio_personaje_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaUrlBio']);
                            }
                            if($character[0]['personaUrlThumb'] != NULL || !empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_thumb_personaje_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $character[0]['personaUrlThumb']);
                            }
                        }
                    }
                    if(!empty($actualPrograms)) {   // Muestra los programas actuales en el XML
                        foreach ($actualPrograms as $key => $value) {
                            $index = '';
                            if($key > 0)
                                $index = '_' . $key;
                            $program = json_decode($this->programaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_programa_actual' . $index);                          
                            $meta     -> setAttribute('content', $program[0]['programaTitulo']);
                            if(!empty($program[0]['programaUrl'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_programa_actual' . $index);                          
                                $meta     -> setAttribute('content', $program[0]['programaUrl']);
                            }
                            if($program[0]['programaThumbnail'] != NULL || !empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) != basename(DUMMY_PROGRAM_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'thumb_programa_actual' . $index);                          
                                $meta     -> setAttribute('content', $program[0]['programaThumbnail']);
                            }
                        }
                    }
                    if(!empty($relatedPrograms)) {  // Muestra los programas relacionados en el XML
                        foreach ($relatedPrograms as $key => $value) {
                            $program = json_decode($this->programaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'programa_relacionado_' . $key);
                            $meta     -> setAttribute('content', $program[0]['programaTitulo']);
                            if(!empty($program[0]['programaUrl'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_programa_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $program[0]['programaUrl']);
                            }
                            if($program[0]['programaThumbnail'] != NULL || !empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) != basename(DUMMY_PROGRAM_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'thumb_programa_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $program[0]['programaThumbnail']);
                            }
                        }
                    }
                }
            }
            else if($type == 2) {
                // Obtiene las relaciones del personaje
                $relations = json_decode($this->personajeGetRelations($personaId), true);
                if(!empty($relations[0])) {
                    // Programas actuales y relacionados
                    $personId = '';
                    foreach($relations as $key => $value) {
                        if(!empty($value['programaId'])) {  // Obtiene el Id del programa actual y relacionado
                            if($value['programaActual'] == 1)
                                array_push($actualPrograms, $value['programaId']);
                            array_push($relatedPrograms, $value['programaId']);
                        }
                        $personId = $value['personaId'];
                    }
                    if(!empty($personId)) { // Muestra la persona relacionada
                        $person = json_decode($this->personaGetInfo($personId), true);
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'nombre_persona_relacionada');                          
                        $meta     -> setAttribute('content', $person[0]['personaNombre']);
                        if(!empty($person[0]['personaApaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_paterno_persona_relacionada');                          
                            $meta     -> setAttribute('content', $person[0]['personaApaterno']);
                        }
                        if(!empty($person[0]['personaAmaterno'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'apellido_materno_persona_relacionada');                          
                            $meta     -> setAttribute('content', $person[0]['personaAmaterno']);
                        }
                        if(!empty($person[0]['personaUrlBio'])) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_bio_persona_relacionada');                          
                            $meta     -> setAttribute('content', $person[0]['personaUrlBio']);
                        }
                        if($person[0]['personaUrlThumb'] != NULL || !empty($person[0]['personaUrlThumb']) || basename($person[0]['personaUrlThumb']) != basename(DUMMY_PERSON_URL_THUMB)) {
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'url_thumb_persona_relacionada');                          
                            $meta     -> setAttribute('content', $person[0]['personaUrlThumb']);
                        }
                    }
                    if(!empty($actualPrograms)) {   // Muestra los programas actuales en el XML
                        foreach ($actualPrograms as $key => $value) {
                            $program = json_decode($this->programaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'nombre_programa_actual_' . $key);                          
                            $meta     -> setAttribute('content', $program[0]['programaTitulo']);
                            if(!empty($program[0]['programaUrl'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_programa_actual_' . $key);                          
                                $meta     -> setAttribute('content', $program[0]['programaUrl']);
                            }
                            if($program[0]['programaThumbnail'] != NULL || !empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) != basename(DUMMY_PROGRAM_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'thumb_programa_actual_' . $key);                          
                                $meta     -> setAttribute('content', $program[0]['programaThumbnail']);
                            }
                        }
                    }
                    if(!empty($relatedPrograms)) {  // Muestra los programas relacionados en el XML
                        foreach ($relatedPrograms as $key => $value) {
                            $program = json_decode($this->programaGetInfo($value), true);
                            $meta      = $dom->createElement('meta');
                            $metadata -> appendChild($meta);
                            $meta     -> setAttribute('name', 'programa_relacionado_' . $key);
                            $meta     -> setAttribute('content', $program[0]['programaTitulo']);
                            if(!empty($program[0]['programaUrl'])) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'url_programa_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $program[0]['programaUrl']);
                            }
                            if($program[0]['programaThumbnail'] != NULL || !empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) != basename(DUMMY_PROGRAM_URL_THUMB)) {
                                $meta      = $dom->createElement('meta');
                                $metadata -> appendChild($meta);
                                $meta     -> setAttribute('name', 'thumb_programa_relacionado_' . $key);                          
                                $meta     -> setAttribute('content', $program[0]['programaThumbnail']);
                            }
                        }
                    }
                }
            }
            // Premios y reconocimientos
            $getAwards = json_decode($this->personaGetAwards($personaId), true);
            if(!empty($getAwards)) {
                foreach($getAwards as $key => $value) {
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', 'nombre_premio_' . $key . '_' . $personaTipo);
                    $meta     -> setAttribute('content', $value['personaPremiosNombre']);
                    if(!empty($value['personaPremiosCategoria'])) {
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'categoria_premio_' . $key . '_' . $personaTipo);
                        $meta     -> setAttribute('content', $value['personaPremiosCategoria']);
                    }
                    if(!empty($value['personaPremiosAnio'])) {
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'anio_premio_' . $key . '_' . $personaTipo);
                        $meta     -> setAttribute('content', $value['personaPremiosAnio']);
                    }
                    if(!empty($value['personaPremiosResultado'])) {
                        $meta      = $dom->createElement('meta');
                        $metadata -> appendChild($meta);
                        $meta     -> setAttribute('name', 'resultado_premio_' . $key . '_' . $personaTipo);
                        if($value['personaPremiosResultado'] == 1)
                            $meta     -> setAttribute('content', 'Ganador');
                        else if($value['personaPremiosResultado'] == 2)
                            $meta     ->setAttribute ('content', 'Nominado');
                    }
                }
            }
            // Persona Curiosidades
            $getCuriousFacts = json_decode($this->personaGetCuriousFacts($personaId), true);
            if(!empty($getCuriousFacts)) {
                foreach($getCuriousFacts as $key => $value) {
                    $meta      = $dom->createElement('meta');
                    $metadata -> appendChild($meta);
                    $meta     -> setAttribute('name', 'hecho_curioso_' . $key . '_' . $personaTipo);
                    $meta     -> setAttribute('content', $value['personacuriosidadesContenido']);
                }
            }
            // CEA
            $c = $data['personaCEA'];
            if(!empty($c)) {
                $cea = @unserialize($c);
                foreach ($cea as $key => $value) {
                    $meta       = $dom->createElement('meta');
                    $metadata  -> appendChild($meta);
                    $meta      -> setAttribute('name', 'cea_tipo_' . $key . '_' . $personaTipo);
                    $meta      -> setAttribute('content', $value);
                }
            }
            // JSON Path
            $meta      = $dom->createElement('meta');
            $metadata -> appendChild($meta);
            $meta     -> setAttribute('name', 'json_' . $personaTipo);
            if($type == 1) {
                $folder = 'personas';
                $meta     -> setAttribute('content', AWS_BUCKET_URL . 'json/personas/' . URLify::filter($name) . '.json');
            }
            else if($type == 2) {
                $folder = 'personajes';
                $meta     -> setAttribute('content', AWS_BUCKET_URL . 'json/personajes/' . URLify::filter($name) . '.json');
            }
            $xml = $dom->saveXML();
            $path = filexml . $folder;
            $file = $path . '/' . URLify::filter($name) . '.xml';
            if(!file_put_contents($file, $xml))
                throw new RuntimeException('No se pudo generar el archvio XML en la ruta indicada.');
            if(SEND_TO_GOOGLE) {
                $this->executePy($file, 'personas');
                if(SEND_TO_AWS)
                    $this->sendToAws($file, $folder, 'xml');
            }
        }
        else
            return false;
    }
    
    /**
     * Create JSON
     * 
     * Crea el archivo JSON de la persona o personaje con base en su Id
     * @param int $personaId
     * @version original: 2016/10/23 18:26 | modified:
     */
    public function createJson($personaId) {
        $json = array();
        try {
            $result = $this->db->select('SELECT
                                                *
                                           FROM
                                                persona
                                          WHERE
                                                personaId
                                              =
                                                :personaId',
                                        array('personaId' => $personaId));
        }
        catch(RuntimeException $e) {
            die('Error al obtener la información de la persona.');
        }
        // Nombre completo
        $name = $result[0]['personaNombre'] . ' ' . $result[0]['personaApaterno'] . ' ' . $result[0]['personaAmaterno'];
        $name = trim($name);
        $ignore = array('personaProgramaActual',
                        'personaUrlProgramaActual',
                        'personaPersonajeActual',
                        'personaConsultaRelacionada',
                        'personaStatus',
                        'personaCEA');  // Ignora estos keys especiales
        foreach($result[0] as $key => $value) {
            if(array_key_exists($key, $ignore))
                continue;
            else {
                switch($key) {
                    case 'personaId':
                        $k = 'id';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaNombre':
                        $k = 'nombre';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaApaterno':
                        $k = 'apellidoPaterno';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaAmaterno':
                        $k = 'apellidoMaterno';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaGenero':
                        $k = 'genero';
                        if($value == 1)
                            $v = 'femenino';
                        else 
                            $v = 'masculino';
                        $json[$k] = $v;
                        break;
                    case 'personaAlias':
                        $k = 'alias';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaHobbies':
                        $k = 'hobbies';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaNacionalidad':
                        $k = 'nacionalidad';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaLugarNacimiento':
                        $k = 'lugarNacimiento';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaNacimiento':
                        $k = 'fechaNacimiento';
                        if($value == '0000-00-00')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaSigno':
                        $k = 'signoZodiacal';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaDefuncion':
                        $k = 'fechaFallecimiento';
                        if($value == '0000-00-00')
                            $v = '';
                        else
                            $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlBio':
                        $k = 'urlBio';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaResumenBio':
                        $k = 'resumenBio';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlThumb':
                        $k = 'urlThumb';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaKeywords':
                        $k = 'keywords';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlNoticias':
                        $k = 'urlNoticias';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlFotos':
                        $k = 'urlFotos';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlVideos':
                        $k = 'urlVideos';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlFacebook':
                        $k = 'urlFacebook';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlTwitter':
                        $k = 'urlTwitter';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlYoutube':
                        $k = 'urlYoutube';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlInstagram':
                        $k = 'urlInstagram';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlPinterest':
                        $k = 'urlPinterest';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaUrlGplus':
                        $k = 'urlGplus';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaContenidoBio':
                        $k = 'contenidoBio';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaFechaIngreso':
                        $k = 'fechaCreacion';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaFechaModificacion':
                        $k = 'fechaModificacion';
                        $v = $value;
                        $json[$k] = $v;
                        break;
                    case 'personaTipo':
                        $k = 'seed_content';
                        if($value == 1)
                            $v = 'persona';
                        else if($value == 2)
                            $v = 'personaje';
                        $json[$k] = $v;
                        $type = $value;
                        break;
                }
            }
        }
        // CEA
        $c = $result[0]['personaCEA'];
        $cea = array();
        if(!empty($c)) {
            $c_temp = unserialize($c);
            foreach($c_temp as $key => $value)
                array_push($cea, array('tipo' => $value));
        }
        $json['cea'] = $cea;
        // Relaciones
        $aC                = array();
        $rC                = array();
        $aP                = array();
        $rP                = array();
        $relatedPerson     = array();
        $actualProgram     = array();
        $actualPrograms    = array();   
        $relatedProgram    = array();
        $relatedPrograms   = array();
        $actualCharacter   = array();
        $actualCharacters  = array();
        $relatedCharacter  = array();   // Array de personaje relacionado (nombre, apellido paterno, apellido materno, url de la biografía, url del thumb)
        $relatedCharacters = array();   // Array completo de personajes relacionados
        if($type == 1) {    // Relaciones de persona
            // Obtiene las relaciones de la persona
            $relations = json_decode($this->personaGetRelations($personaId), true);
            if(!empty($relations[0])) {
                // Programas y personajes relacionados
                foreach($relations as $key => $value) {
                    if(!empty($value['personajeId'])) { // Obtiene el Id del personaje actual y relacionado
                        if($value['personajeActual'] == 1)
                            array_push($aC, $value['personajeId']);
                        array_push($rC, $value['personajeId']);
                    }
                    if(!empty($value['programaId'])) {  // Obtiene el Id del programa actual y relacionado
                        if($value['programaActual'] == 1)
                            array_push($aP, $value['programaId']);
                        array_push($rP, $value['programaId']);
                    }
                }
                if(!empty($aC)) { // Inserta los personajes actuales en el JSON
                    $aC = array_unique($aC, SORT_REGULAR);
                    foreach ($aC as $key => $value) {
                        $character = json_decode($this->personaGetInfo($value), true);
                        $actualCharacter['nombre'] = $character[0]['personaNombre'];
                        $actualCharacter['apellidoPaterno'] = $character[0]['personaApaterno'];
                        $actualCharacter['apellidoMaterno'] = $character[0]['personaAmaterno'];
                        $actualCharacter['url'] = $character[0]['personaUrlBio'];
                        if($character[0]['personaUrlThumb'] == NULL || empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                            $actualCharacter['urlThumb'] = '';
                        else
                            $actualCharacter['urlThumb'] = $character[0]['personaUrlThumb'];
                        array_push($actualCharacters, $actualCharacter);
                    }
                }
                if(!empty($rC)) {   // Inserta los personajes relacionados en el JSON
                    $rC = array_unique($rC, SORT_REGULAR);
                    foreach ($rC as $key => $value) {
                        $character = json_decode($this->personaGetInfo($value), true);
                        $relatedCharacter['nombre'] = $character[0]['personaNombre'];
                        $relatedCharacter['apellidoPaterno'] = $character[0]['personaApaterno'];
                        $relatedCharacter['apellidoMaterno'] = $character[0]['personaAmaterno'];
                        $relatedCharacter['url'] = $character[0]['personaUrlBio'];
                        if($character[0]['personaUrlThumb'] == NULL || empty($character[0]['personaUrlThumb']) || basename($character[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                            $relatedCharacter['urlThumb'] = '';
                        else
                            $relatedCharacter['urlThumb'] = $character[0]['personaUrlThumb'];
                        array_push($relatedCharacters, $relatedCharacter);
                    }
                }
                if(!empty($aP)) {   // Inserta los programas actuales en el JSON
                    $aP = array_unique($aP, SORT_REGULAR);
                    foreach ($aP as $key => $value) {
                        $program = json_decode($this->programaGetInfo($value), true);    
                        $actualProgram['nombre'] = $program[0]['programaTitulo'];
                        $actualProgram['url'] = $program[0]['programaUrl'];
                        if($program[0]['programaThumbnail'] == NULL || empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) == basename(DUMMY_PROGRAM_URL_THUMB))
                            $actualProgram['urlThumb'] = '';
                        else
                            $actualProgram['urlThumb'] = $program[0]['programaThumbnail'];
                        array_push($actualPrograms, $actualProgram);
                    }
                }
                if(!empty($rP)) {   // Inserta los programas relacionados en el JSON
                    $rP = array_unique($rP, SORT_REGULAR);
                    foreach ($rP as $key => $value) {
                        $program = json_decode($this->programaGetInfo($value), true);
                        $relatedProgram['nombre'] = $program[0]['programaTitulo'];
                        $relatedProgram['url'] = $program[0]['programaUrl'];
                        if($program[0]['programaThumbnail'] == NULL || empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) == basename(DUMMY_PROGRAM_URL_THUMB))
                            $relatedProgram['urlThumb'] = '';
                        else
                            $relatedProgram['urlThumb'] = $program[0]['programaThumbnail'];
                        array_push($relatedPrograms, $relatedProgram);
                    }
                }
            }
            $emptyProgramArray = array('nombre'   => '',
                                       'url'      => '',
                                       'urlThumb' => '');
            $emptyPersonArray = array('nombre'          => '',
                                      'apellidoPaterno' => '',
                                      'apellidoMaterno' => '',
                                      'url'             => '',
                                      'urlThumb'        => '');
            if(empty($actualPrograms))
                $json['programasActuales'] = $emptyProgramArray;
            else
                $json['programasActuales'] = $actualPrograms;
            if(empty($relatedPrograms))
                $json['programasRelacionados'] = $emptyProgramArray;
            else
                $json['programasRelacionados'] = $relatedPrograms;
            if(empty($actualCharacters))
                $json['personajesActuales'] = $emptyPersonArray;
            else
                $json['personajesActuales'] = $actualCharacters;
            if(empty($relatedCharacters))
                $json['personajesRelacionados'] = $emptyPersonArray;
            else
                $json['personajesRelacionados'] = $relatedCharacters;
        }
        else if($type == 2) {
            // Obtiene las relaciones del personaje
            $relations = json_decode($this->personajeGetRelations($personaId), true);
            if(!empty($relations[0])) {
                // Programas y persona relacionados
                foreach($relations as $key => $value) {
                    if(!empty($value['programaId'])) {  // Obtiene el Id del programa actual y relacionado
                        if($value['programaActual'] == 1)
                            array_push($aP, $value['programaId']);
                        array_push($rP, $value['programaId']);
                    }
                }
                if(!empty($relations[0]['personaId'])) {    // Inserta a la persona relacionada en el JSON
                    $person = json_decode($this->personaGetInfo($relations[0]['personaId']), true);
                    $relatedPerson['nombre'] = $person[0]['personaNombre'];
                    $relatedPerson['apellidoPaterno'] = $person[0]['personaApaterno'];
                    $relatedPerson['apellidoMaterno'] = $person[0]['personaAmaterno'];
                    $relatedPerson['url'] = $person[0]['personaUrlBio'];
                    if($person[0]['personaUrlThumb'] == NULL || empty($person[0]['personaUrlThumb']) || basename($person[0]['personaUrlThumb']) == basename(DUMMY_PERSON_URL_THUMB))
                        $relatedPerson['urlThumb'] = '';
                    else
                        $relatedPerson['urlThumb'] = $person[0]['personaUrlThumb'];
                }
                if(!empty($aP)) {   // Inserta los programas actuales en el JSON
                    $aP = array_unique($aP, SORT_REGULAR);
                    foreach ($aP as $key => $value) {
                        $program = json_decode($this->programaGetInfo($value), true);    
                        $actualProgram['nombre'] = $program[0]['programaTitulo'];
                        $actualProgram['url'] = $program[0]['programaUrl'];
                        if($program[0]['programaThumbnail'] == NULL || empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) == basename(DUMMY_PROGRAM_URL_THUMB))
                            $actualProgram['urlThumb'] = '';
                        else
                            $actualProgram['urlThumb'] = $program[0]['programaThumbnail'];
                        array_push($actualPrograms, $actualProgram);
                    }
                }
                if(!empty($rP)) {   // Inserta los programas relacionados en el JSON
                    $rP = array_unique($rP, SORT_REGULAR);
                    foreach ($rP as $key => $value) {
                        $program = json_decode($this->programaGetInfo($value), true);
                        $relatedProgram['nombre'] = $program[0]['programaTitulo'];
                        $relatedProgram['url'] = $program[0]['programaUrl'];
                        if($program[0]['programaThumbnail'] == NULL || empty($program[0]['programaThumbnail']) || basename($program[0]['programaThumbnail']) == basename(DUMMY_PROGRAM_URL_THUMB))
                            $relatedProgram['urlThumb'] = '';
                        else
                            $relatedProgram['urlThumb'] = $program[0]['programaThumbnail'];
                        array_push($relatedPrograms, $relatedProgram);
                    }
                }
            }
            $emptyProgramArray = array('nombre'   => '',
                                           'url'      => '',
                                           'urlThumb' => '');
            $emptyPersonArray = array('nombre'          => '',
                                      'apellidoPaterno' => '',
                                      'apellidoMaterno' => '',
                                      'url'             => '',
                                      'urlThumb'        => '');
            if(empty($actualPrograms))
                $json['programasActuales'] = $emptyProgramArray;
            else
                $json['programasActuales'] = $actualPrograms;
            if(empty($relatedPrograms))
                $json['programasRelacionados'] = $emptyProgramArray;
            else
                $json['programasRelacionados'] = $relatedPrograms;
            if(empty($relatedPerson))
                $json['personaRelacionada'] = $emptyPersonArray;
            else
                $json['personaRelacionada'] = $relatedPerson;
        }
        // Premios y reconocimientos
        $getAwards = json_decode($this->personaGetAwards($personaId), true);
        $award = array();  // Array de premio (nombre, categoría, año y resultado)
        $awards = array();  // Array final de premios
        if(!empty($getAwards)) {
            foreach($getAwards as $key => $value) {
                $award['nombre'] = $value['personaPremiosNombre'];
                $award['categoria'] = $value['personaPremiosCategoria'];
                $award['anio'] = $value['personaPremiosAnio'];
                if($value['personaPremiosResultado'] == 1)
                    $award['resultado'] = 'Ganador';
                else if($value['personaPremiosResultado'] == 2)
                    $award['resultado'] = 'Nominado';
                else
                    $award['resultado'] = '';
                array_push($awards, $award);
            }
        }
        $json['premios'] = $awards;
        // Persona Curiosidades
        $getCuriousFacts = json_decode($this->personaGetCuriousFacts($personaId), true);
        $curiousFact = array(); // Array de hechos curiosos (nombre, url)
        $curiousFacts = array();    // Array final de hechos curiosos
        if(!empty($getCuriousFacts)) {
            foreach($getCuriousFacts as $key => $value) {
                $curiousFact['contenido'] = $value['personacuriosidadesContenido'];
                array_push($curiousFacts, $curiousFact);
            }
        }
        $json['curiosidades'] = $curiousFacts;
        if($type == 1) {
            $folder = 'personas';
            $jsonStart = 'persona(';   // JsonP
        }
        else if($type == 2) {
            $folder = 'personajes';
            $jsonStart = 'personaje(';
        }
        $jsonEnd = ');';
        $path = filejson . $folder;
        $file = $path . '/' . URLify::filter($name) . '.json';
        if(!file_put_contents($file, $jsonStart . json_encode($json) . $jsonEnd))
            throw new RuntimeException('No se pudo generar el archvio JSON en la ruta indicada.');
        if(SEND_TO_AWS)
            $this->sendToAws($file, $folder, 'json');
    }

    /**
     * Publish
     *
     * Publica a la persona o personaje desde el Data Table
     * @return json con el resultado del proceso de publicación
     * @version original: 2016/08/11 10:15 | modified: 
     */
    public function publish($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a publicar.',
                                     'values'     => ''));
        else {
            if(!isset($data['id']))
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'No existe el id.',
                                         'values'     => $data));
            else {
                if(empty($data['id']))
                    return json_encode(array('response'   => 'error',
                                             'serversays' => 'El id está vacío.',
                                             'values'     => $data));
                else {
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                    try {
                        $this->db->beginTransaction();
                        $this->db->update('persona', array('personaStatus'            => 0,
                                                           'personaFechaModificacion' => date('Y-m-d H:i:s')), "`personaId` = {$id}");
                        $this->createJSON($id);
                        $this->createSingleXML($id);
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se publicó con éxito.'));
                        return $json;
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
        }
    }

    /**
     * Unpublish
     *
     * Despublica a la persona o personaje desde el Data Table
     * @return json con el resultado del proceso de despublicación
     * @version original: 2016/08/11 18:10 | modified: 
     */
    public function unpublish($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a despublicar.',
                                     'values'     => ''));
        else {
            if(!isset($data['id']))
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'No existe el id.',
                                         'values'     => $data));
            else {
                if(empty($data['id']))
                    return json_encode(array('response'   => 'error',
                                             'serversays' => 'El id está vacío.',
                                             'values'     => $data));
                else {
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                    try {
                        $this->db->beginTransaction();
                        $this->db->update('persona', array('personaStatus'            => 1,
                                                           'personaFechaModificacion' => date('Y-m-d H:i:s')), "`personaId` = {$id}");
                        $this->createJSON($id);
                        $this->createSingleXML($id);
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se despublicó con éxito.'));
                        return $json;
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
        }
    }

    /**
     * Delete
     *
     * Borra a la persona o personaje desde el Data Table
     * @return json con el resultado del proceso de eliminación
     * @version original: 2016/08/11 18:56 | modified: 
     */
    public function delete($data) {
        if(empty($data))
            return json_encode(array('response'   => 'error',
                                     'serversays' => 'No hay datos a borrar.',
                                     'values'     => ''));
        else {
            if(!isset($data['id']))
                return json_encode(array('response'   => 'error',
                                         'serversays' => 'No existe el id.',
                                         'values'     => $data));
            else {
                if(empty($data['id']))
                    return json_encode(array('response'   => 'error',
                                             'serversays' => 'El id está vacío.',
                                             'values'     => $data));
                else {
                    $id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
                    $id = trim($id);
                    try {
                        $this->db->beginTransaction();
                        $this->createJSON($id);
                        $this->createSingleXML($id);
                        $this->db->delete('persona', "`personaId` = {$id}");
                        $this->db->commit();
                        $json = json_encode(array('response'   => 'success',
                                                  'serversays' => 'Se ha borrado con éxito.'));
                        return $json;
                    }
                    catch (RuntimeException $e) {
                        $this->db->rollBack();
                        $json = json_encode(array('response'   => 'error',
                                                  'serversays' => $this->composeErrors(array(0, $e->getMessage())),
                                                  'values'     => $data));
                        return $json;
                    }
                }
            }
        }
    }

    
    /**
     * Change Image Domain
     * 
     * Cambia el dominio de las imágenes de los thumbs para evitar el uso de:
     * -Origin
     * -Galaxypreview
     * -Dynamic
     * Y lo deja con el subdominio: i2
     * @param string url
     * @return string url url sin los subdominios
     * @version original: 2015/01/20 12:55 | modified:
     */
    public function changeImageDomain($url) {
        return preg_replace("/" . INVALID_IMAGE_SUBDOMAIN . "/i", ESMAS_IMAGE_SUBDOMAIN, $url);
    }
    
    /**
     * Rename File
     *
     * Modifica y renombra el archivo y regresa ese nombre para ser insertado en la base de datos, si no viene del bucket de AWS lo deja igual
     * @param string $fileName URL del archivo de imagen
     * @param string $name nombre con el que se sustituirá el nombre del archivo
     * @return string $newFileName nueva URL del archivo con el nombre
     * @version original: 2014/10/20 16:43 | modified:
     */
    public function renameFile($fileName, $name) {
        $serverHost = parse_url($fileName);
        $newFileName = "";
        if(preg_match("/".AWS_BUCKET."/", $serverHost["host"])) {
            $path = parse_url($fileName);
            $pathPieces = explode("/", $path["path"]);
            $newName = URLify::filter($name, false);
            $pathInfo = pathinfo($path["path"]);
            $extension = strtolower($pathInfo["extension"]);
            $arrayNewUrl = array($path["scheme"].":/", $path["host"], IMG_PERSON_UPLOAD_DIR.$newName.".".$extension);
            $newFileName = implode("/", $arrayNewUrl);
            $dir = escapeshellarg(IMG_PERSON_UPLOAD_DIR);
            $oldName = escapeshellarg($pathInfo["basename"]);
            $newNameWithExt = escapeshellarg($newName.".".$extension);
            $ext = escapeshellarg($extension);
            $resval = array();
            $command = PHP_PATH . ' "' . DIR_PATH . 'rename_file_from_aws.php" '. $dir .' '. $oldName .' '. $newNameWithExt .' '. $ext .'';
            exec($command, $resval);
        }
        else 
            $newFileName = $fileName;
        return $newFileName;
    }
    
    /**
     * Persona Get Info
     * 
     * Obtiene la información de la persona o personaje dependiendo de su id
     * @param type $personaId
     * @return 
     */
    public function personaGetInfo($personaId){
        return json_encode($this->db->select('SELECT * FROM persona WHERE personaId = :personaId', 
                                            array('personaId' => $personaId)));
    }

    /**
     * Programa Get Info
     * 
     * Obtiene la información del programa dependiendo de su id
     * @param int $programaId
     * @return json
     * @version original 2016/11/01 18:49 | modified: 
     */
    public function programaGetInfo($programaId){
        return json_encode($this->db->select('SELECT * FROM programa WHERE programaId = :programaId', 
                                            array('programaId' => $programaId)));
    }
    
    /**
     * Persona Get Relations
     * 
     * Obtiene la relación dependiendo del id de la persona
     * @param int $personaId
     * @return json
     * @version original 2016/10/19 08:48 | modified: 
     */
    public function personaGetRelations($personaId, $ob = null) {
        $orderBy = '';
        if($ob) 
            $orderBy = ' ' . $ob;
        return json_encode($this->db->select('SELECT * FROM personaprograma WHERE personaId = :personaId' . $orderBy,
                                            array('personaId' => $personaId)));
    }

    /**
     * Personaje Get Relations
     * 
     * Obtiene la relación dependiendo del id del personaje
     * @param int $personajeId
     * @return json
     * @version original 2016/10/27 17:22 | modified: 
     */
    public function personajeGetRelations($personajeId) {
        return json_encode($this->db->select('SELECT * FROM personaprograma WHERE personajeId = :personajeId',
                                            array('personajeId' => $personajeId)));
    }

    /**
     * Personaje Get Actual Relations
     *
     * Obtiene si los personajes son actuales a la persona
     * @param int $personajeId
     * @return json
     * @version original 2016/12/08 18:23 | modified: 
     */
    public function personajeGetActualRelations($personajeId) {
        return json_encode($this->db->select('SELECT p.personaPersonajeActual FROM persona p INNER JOIN personaprograma a ON p.personaId = a.personaId WHERE a.personajeId = :personajeId',
                                             array('personajeId' => $personajeId)));
    } 
    
    /**
     * Is Image
     *
     * Determina si una URL está disponible vía CURL
     * @param int $personaId
     * @return bool $ret booleano con el resultado de la verificación
     * @version orginal 2014/10/13 11:54 | modified: 
     */
    public function isImage($personaId) {
        $array = json_decode($this->personaGetInfo($personaId), true);
        $url = $array[0]['personaUrlThumb'];
        if($url != "") {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_NOBODY, true);
            $result = curl_exec($curl);
            $ret = false;
            $contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
            if($contentType != NULL) {
                preg_match('@([\w/+]+)(;\s+charset=(\S+))?@i', $contentType, $matches);
                $validTypes = array(
                    'jpg'  => 'image/jpeg',
                    'jpeg' => 'image/jpeg',
                    'png'  => 'image/png',
                    'gif'  => 'image/gif',
                );
                $imageStatus = array_search($matches[0], $validTypes);
                if ($result !== false) {
                    $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
                    if ($statusCode == 200 && $imageStatus)
                        $ret = true;   
                }
                curl_close($curl);
            }
            return $ret;
        }
    }
    
    /**
     * Get Image Info
     *
     * Obtiene el nombre, el peso y el tipo de una imagen por su URL
     * @param int $personaId
     * @return string json $imageInfo json con los datos de la imagen más un boolean validando que es una imagen (isimage, length, type, name)
     * @version original: 2014/10/13 11:58 | modified: 2014/11/10 10:03
     */
    public function getImageInfo($personaId) {
        $array = json_decode($this->personaGetInfo($personaId), true);
        $data = $array[0]['personaUrlThumb'];
        if($data != "" && $this->isImage($personaId) == true) {
            $imageUrl = $data;
            $fileName = basename($data);
            $contentLength = 0;
            $ch = curl_init($data);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $data = curl_exec($ch);
            curl_close($ch);
            if (preg_match('/Content-Length: (\d+)/', $data, $matches))
                $contentLength = (int)$matches[1]; // Contiene el tamaño del archivo en bytes
            if (preg_match('/Content-Type: (.*)/', $data, $matches))
                $contentType = $matches[1];
            if($contentLength === NULL || $contentLength === 0 || $contentLength === "") {
                ob_start();
                readfile($imageUrl);
                $imageData = ob_get_contents();
                $length = strlen($imageData);
                ob_end_clean();
                $contentLength = $length;
            }
            $contentType = trim(preg_replace('/\s+/', ' ', $contentType));
            $imageInfo = array('response' => 'isimage', 'length' => $contentLength, 'type' => $contentType, 'name' => $fileName);
            return json_encode($imageInfo);
        }
    }
    
    /**
     * Persona Get Awards
     *
     * Obtiene los premios y reconocimientos de la persona
     * @param int $personaId
     * @return string json
     * @version original: 2016/10/19 11:26 | modified: 
     */
    public function personaGetAwards($personaId) {
        return json_encode($this->db->select('SELECT * FROM personapremios WHERE personaId = :personaId', 
                                            array('personaId' => $personaId)));
    }

    /**
     * Persona Get Curiosidades
     *
     * Obtiene las curiosidades de la persona
     * @param int $personaId
     * @return string json
     * @version original: 2016/10/20 10:41 | modified: 
     */
     public function personaGetCuriousFacts($personaId) {
        return json_encode($this->db->select('SELECT * FROM personacuriosidades WHERE personaId = :personaId',
                                            array('personaId' => $personaId)));
    }
    
    /**
     * Execute Py 
     * 
     * Ejecuta el llamado a Python para mandar el archivo a Google
     * @param string $nameXml
     * @param string $channel
     * @version original: 2016/10/23 16:58 | modified: 
     */
    public function executePy($nameXml, $channel) {
        $log = 'logs/execute.log';  // Log de resultado de la ejecución
        $nameXml = dirname($_SERVER['SCRIPT_FILENAME']) . '/' . $nameXml;
        $serverGsa['google03'] = '216.250.129.15:19900';
        $serverGsa['google04'] = '216.250.129.16:19900';
        while(list($server, $ipServer) = each($serverGsa)) {
            $resVal = array();
            $command = PYTHON_PATH . ' ' . PUSHFEED_PATH . ' --datasource="' . $channel . '" --feedtype="incremental" --url="http://' . $ipServer . '/xmlfeed" --xmlfilename="' . $nameXml . '"';
            exec($command, $resVal);
            if(isset($resVal[0]) && $resVal[0] == 'Success')
                $message = '[' . date('D, d M Y G:i:s T') . '] Insertado en ' . $server . PHP_EOL;
            else
                $message = '[' . date('D, d M Y G:i:s T') . '] Fallo [' . $resVal[0] . ']: ' . $command . PHP_EOL;
            file_put_contents($log, $message, FILE_APPEND);
        }
    }
    
    /**
     * Send To AWS
     * 
     * Envía un archivo al bucket de AWS
     * @param string $file
     * @param string $category
     * @param string $fileType
     * @version original: 2016/10/23 17:14 | modified: 
     */
    public function sendToAws($file, $category, $fileType) {
        $log = 'logs/sendToAws.log';  // Log de resultado de la ejecución
        $file = escapeshellarg($file);
        $category = escapeshellarg($category);
        $fileType = escapeshellarg($fileType);
        $resVal = array();
        $command = PHP_PATH . ' "' . DIR_PATH . 'send_to_aws.php" ' . $file . ' ' . $category . ' ' . $fileType . '';
        exec($command, $resVal);
        if(isset($resVal[0]))
            $message = '[' . date('D, d M Y G:i:s T') . '] Falla [' . $resVal[0] . ']: ' . $command . PHP_EOL;
        else
            $message = '[' . date('D, d M Y G:i:s T') . ']: ' . $command . PHP_EOL;
        file_put_contents($log, $message, FILE_APPEND);
    }

    /**
     * Personaje Get List
     * 
     * Función que muestra la lista completa de personajes con su programa relacionado por id
     * @return json query
     * @version original: 2016/10/26 17:26 | modified:
     */
    public function personajeGetList($personaId) {
        return json_encode($this->db->select('SELECT
                                                    a.personaId AS personajeId, TRIM(CONCAT(a.personaNombre, \' \', a.personaApaterno, \' \', a.personaAmaterno)) AS personaNombre, c.programaId, c.programaTitulo
                                               FROM
                                                    persona a
                                         INNER JOIN
                                                    personaprograma b
                                                 ON
                                                    a.personaId = b.personajeId
                                          LEFT JOIN
                                                    programa c
                                                 ON
                                                    c.programaId = b.programaId
                                              WHERE
                                                    a.personaTipo = 2
                                                AND
                                                    b.personaId = ' . $personaId . '
                                           ORDER BY
                                                    a.personaNombre
                                                ASC'));
    }

    public function relacionaDatosWp($sirId,$sirTipo,$wpId,$instancia,$instanciaTipo,$imagenes){
        $queryExistPerson = $this->db->select("SELECT * FROM wordpress WHERE wordpressSirId=".$sirId." and wordpressWpId=".$wpId." and  wordpressInstancia like '".$instancia."'");
        
        $data = array('wordpressSirId'=>$sirId, 'wordpressSirTipo'=>$sirTipo,'wordpressWpId'=>$wpId,'wordpressInstancia'=>$instancia,"wordpressInstanciaTipo"=>$instanciaTipo,'wordpressPushImagenes'=>$imagenes);
        if($queryExistPerson){
            $queryWp  = $this->db->update('wordpress',$data,'wordpressSirId = '.$sirId.' and wordpressWpId='.$wpId.' and wordpressInstancia like"'.$instancia.'"');            
        }
        else{
            
            $queryWp  = $this->db->insert("wordpress",$data);
        }
        
        return $queryWp;
    }
    
    public function updateDatosWP($id){
         $queryContenido = $this->db->select("SELECT personaContenidoBio FROM persona WHERE personaId=".$id);
         $queryInstancias= $this->db->select("SELECT * FROM wordpress WHERE wordpressSirId=".$id);
         ini_set("soap.wsdl_cache_enabled", "0"); 
         define('SHARED_SECRET_WP','6bty789ww4tp1ksif34ape042assdadp0ps45');
         if($queryInstancias){
             $client = new SoapClient('http://192.168.1.104/deportes/wp-content/plugins/webservice/wbservice.wsdl',array("trace" => 1,"exceptions" => 0));
             try{
                $result = $client->__soapCall("actualizaDatosPost", array($id.SHARED_SECRET_WP,'contenido test sir'));
             }
             catch (SoapFault $exception) {
               echo 'error';
             }
         }
    }
}