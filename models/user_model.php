<?php
include 'branch_model.php';

class User_Model extends Model {
    public function __construct() {
        parent::__construct();
    }
	
	/**
	* Branch List
	*
	* Muestra la lista completa de ramas
	* @return array branchList
	* @version original: | modified:
	*/
	public function branchList() {
		$newB = new Branch_Model();
		return $newB->branchList();
	}
	
	/**
	* User List 
	* 
	* Muestra la lista completa de usuarios
	* @return array userList
	* @version original: | modified:
	*/
    public function userList() {
		try {
			$result = $this->db->select('SELECT * FROM usuariosadm');
			return array(true, $result);
		}
		catch (RuntimeException $e) {
			return array(false, array(0, $e->getMessage()));
		}
    }
    
	/**
	* User Single List
	*
	* Muestra la información de un usuario que exista en la base de datos
	* @param int userid
	* @return array userSingleList
	* @version original: | modified:
	*/
    public function userSingleList($userid) {
		$userid = filter_var($userid, FILTER_SANITIZE_NUMBER_INT);
		$userid = trim($userid);
		try {
			$result = $this->db->select('SELECT idusuariosAdm, login, rol, permissions FROM usuariosadm WHERE idusuariosAdm = :userid', array(':userid' => $userid));
			return array(true, $result);
		}
        catch (RuntimeException $e) {
			return array(false, array(0, $e->getMessage()));
		}
    }
    
	/**
	* Create
	* 
	* Crea un usuario nuevo en la base de datos
	* @param array data información cruda del formulario 
	* @return json con el resultado del proceso de creación
	* @version original: | modified:
	*/
    public function create($data) {
		$this->clearCreate();
		$errors = array();
		$path = 'user/';				// Path que se regresará
		$sessionName = 'createuser';	// Nombre de la sesión
		$contLogin = 0;					// Contador de error para validación de usuario
		$contPass = 0;					// Contador de error para validación de contraseña
		if(empty($data)) {	// Validación para inputs vacíos
			array_push($errors, 101);
			$json = json_encode(array(
									'response'	 => 'error',
									'serversays' => $errors,
									'values'	 => $data));
			return $this->composeJson($json, $path, $sessionName);
		}
		else {
			// Validación de campo de usuario
			if(!isset($data['login']))	// No hay usuario
				array_push($errors, 102);
			else {
				if($data['login'] == "") {	// Usuario vacío
					array_push($errors, 102);
					$contLogin++;
				}
				else if(preg_match('/[^a-zA-Z0-9_]/', $data['login'])) {	// Caracteres no permitidos en el usuario
					array_push($errors, 103);
					$contLogin++;
				}
				else if($contLogin == 0) {
					$login = filter_var($data['login'], FILTER_SANITIZE_STRING);
					$login = trim($login);
					$login = htmlentities($login, ENT_QUOTES, "ISO8859-1");
				}
			}
			// Validación para usuario existente
			if(isset($login) && $contLogin == 0) {	
				if($this->checkuser($login))
					array_push($errors, 104);
			}
			// Validación para campo de contraseña
			if(!isset($data['pass'])) // No hay contraseña
				array_push($errors, 105);
			else {
				if($data['pass'] == "")	{	// No hay contraseña
					array_push($errors, 105);
					$contPass++;
				}
				else if($contPass == 0) {
					$pass = filter_var($data['pass'], FILTER_SANITIZE_STRING);
					$pass = trim($pass);
				}
			}
			// Validación para campo de rama
			if(!isset($data["ramas"]))	// No hay rama
				array_push($errors, 106);
			else {
				$branch = $data["ramas"];
				$allPermissions = in_array(0, $branch);
				if($allPermissions === false)
					$permissions = implode("-", $branch);
				else // Todos los permisos
					$permissions = 0;
			}
			if(!empty($errors)) { // Respuesta con errores
				$json = json_encode(array(
									'response'	 => 'error',
									'serversays' => $this->composeErrors($errors),
									'values'	 => $data));
				return $this->composeJson($json, $path, $sessionName);
			}
			else {
				if(Session::get('rol') == 'admin') { // Validación de administrador
					try {
						$result = $this->db->insert('usuariosadm', array(
										'login' => $login,
										'pass' => Hash::create('sha256', $pass, HASH_PASSWORD_KEY),
										'rol' => $data['rol'],
										'permissions' => $permissions
									));
						$json = json_encode(array(
									'response'   => 'success',
									'serversays' => 'Se cre&oacute; con &eacute;xito.'));
						return $this->composeJson($json, $path);	// Respuesta exitosa
					}
					catch (RuntimeException $e) {
						$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors(array(0, $e->getMessage())),
									'values'	 => $data));
						return $this->composeJson($json, $path, $sessionName);	// Error en la base de datos
					}
				}
				else {
					$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors(array_push($errors, 107)),
									'values'	 => $data));
					return $this->composeJson($json, $path, $sessionName);	// Sin permisos
				}
			}
		}
    }
    
	/**
	* Edit Save
	* 
	* Edita un usuario
	* @param array data información del formulario 
	* @version original: | modified:
	*/
    public function editSave($data) {
		$this->clearEdit();
		$errors = array();
		$id = filter_var($data['id'], FILTER_SANITIZE_NUMBER_INT);
		$id = trim($id);			// Id del usuario con el que se trabajará
		$path = 'user/edit/' . $id;	// Path que se regresará
		$sessionName = 'edituser';	// Nombre de la sesión
		$contLogin = 0;				// Contador de error para validación de usuario
		if(empty($data)) {			// Validación para inputs vacíos
			array_push($errors, 101);
			$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $errors,
									'values'	 => $data));
			return $this->composeJson($json, $path, $sessionName);
		}
		else {
			// Validación de campo de usuario
			if(!isset($data['login']))	// No hay usuario
				array_push($errors, 102);
			else {
				if($data['login'] == "") { // Campo vacío
					array_push($errors, 102);
					$contLogin++;
				}
				else if(preg_match('/[^a-zA-Z0-9_]/', $data['login'])) {	// Caracteres no permitidos en el usuario
					array_push($errors, 103);
					$contLogin++;
				}
				else if($contLogin == 0) {
					$login = filter_var($data['login'], FILTER_SANITIZE_STRING);
					$login = trim($login);
					$login = htmlentities($login, ENT_QUOTES, "ISO8859-1");
				}
			}
			// Validación para usuario existente con diferente id
			if(isset($login) && $contLogin == 0) {
				$user = $this->userSingleList($data['id']);
				if(in_array(false, $user, true)) { // Error en la base de datos
					$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors($user[1]),
									'values'	 => $data));
					return $this->composeJson($json, $path, $sessionName);
				}
				else { // Si el usuario no coincide con el consultado
					if($login != $user[1][0]['login'])
						array_push($errors, 201);
				}
			}
			// Validación para campo de rama
			if(!isset($data["ramas"]))	// No hay rama
				array_push($errors, 106);
			else {
				$branch = $data["ramas"];
				$allPermissions = in_array(0, $branch);
				if($allPermissions === false)
					$permissions = implode("-", $branch);
				else // Todos los permisos
					$permissions = 0;
			}
			if(!empty($errors)) { // Respuesta con errores
				$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors($errors),
									'values'	 => $data));
				return $this->composeJson($json, $path, $sessionName);
			}
			else {
				if(Session::get('rol') == 'admin') { // Validación de administrador
					$postData = array(
									  'login' => $login,
									  'rol' => $data['rol'],
									  'permissions' => $permissions
								);
					if($data['pass'] != "") {	// Se crea nuevo password
						$pass = filter_var($data['pass'], FILTER_SANITIZE_STRING);
						$pass = trim($pass);
						$passData = array(
										   'pass' => Hash::create('sha256', $pass, HASH_PASSWORD_KEY));
						$postData = array_merge($postData, $passData);
					}
					try {
						$result = $this->db->update('usuariosadm', $postData, "`idusuariosAdm` = {$id}");
						$json = json_encode(array(
									'response'   => 'success',
									'serversays' => 'Se edit&oacute; con &eacute;xito.'));
						return $this->composeJson($json, $path);	// Respuesta exitosa
					}
					catch (RuntimeException $e) {
						$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors(array(0, $e->getMessage())),
									'values'	 => $data));
						return $this->composeJson($json, $path, $sessionName);	// Error en la base de datos
					}
				}
				else {
					$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors(array_push($errors, 107)),
									'values'	 => $data));
					return $this->composeJson($json, $path, $sessionName);	// Sin permisos
				}
			}
		}
    }
    
	/**
	* Delete
	* 
	* Borra un usuario
	* @param int userid 
	* @version original: | modified:
	*/
    public function delete($userid) {
		$path = 'user/';
		$userid = filter_var($userid, FILTER_SANITIZE_NUMBER_INT);
		$userid = trim($userid);
		try {
			//$result = $this->db->delete('usuariosadm', "idusuariosAdm = '$userid'");
			$json = json_encode(array(
									'response'   => 'success',
									'serversays' => 'Se borr&oacute; con &eacute;xito.'));
			return $this->composeJson($json, $path);	// Respuesta exitosa
		}
		catch (RuntimeException $e) {
			$json = json_encode(array(
									'response'   => 'error',
									'serversays' => $this->composeErrors(array(0, $e->getMessage()))));
			return $this->composeJson($json, $path, 'deleteuser'); // Error en la base de datos
		}
    }
	
	/**
	* Check User
	* 
	* Comprueba la existencia de un usuario
	* @param string name
	* @version original: 2015/02/12 18:00 | modified:
	*/
	public function checkuser($name) {
		$name = filter_var($name, FILTER_SANITIZE_STRING);
		$name = trim($name);
		$result = $this->db->select('SELECT
											idusuariosAdm
									   FROM 
											usuariosadm
									   WHERE
											LOWER(login)
										LIKE
											LOWER("%' . $name . '%")');
		if($result)
			return true;
		else
			return false;
	}
	
	/**
	* Clear Data
	*
	* Quita la variable de sesión para $_POST y redirecciona
	* @version original: 2014/02/16 16:06 | modified:
	*/
	public function clearData() {
		if(isset($_SESSION['createuser']))
			unset($_SESSION['createuser']);
		if(isset($_SESSION['edituser']))
			unset($_SESSION['edituser']);
		header('location: ' . URL . 'user/');
	}
	
	/**
	* Clear Create
	*
	* Quita la variable de sesión create para $_POST sin redirección
	* @version original: 2014/02/27 16:11 | modified:
	*/
	public function clearCreate() {
		if(isset($_SESSION['createuser']))
			unset($_SESSION['createuser']);
	}
	
	/**
	* Clear Edit
	*
	* Quita la variable de sesión edit para $_POST sin redirección
	* @version original: 2014/02/27 16:11 | modified:
	*/
	public function clearEdit() {
		if(isset($_SESSION['edituser']))
			unset($_SESSION['edituser']);
	}
	
	/**
	* Error
	*
	* Función con la lista de errores y mensajes que se desplegaran
	* -De 0 a 100 errores de la base de datos
	* -De 100 a 1000 errores en los valores del inputs
	* --100s para errores en los procesos de creación
	* --200s para errores en los procesos de edición 
	* @return json contiene mensajes y textos
	* @version original: 2014/02/27 16:18 | modified:
	*/
	public function error() {
		$messages = array(0   => 'Error en la base de datos',
						  101 => 'Todo est&aacute; vac&iacute;o.',
						  102 => 'El nombre de usuaro est&aacute; vac&iacute;o.',
						  103 => 'El nombre de usuaro tiene algunos caracteres raros. (Psst, psst... S&oacute;lo se permiten n&uacute;meros y letras)',
						  104 => 'Ya existe ese nombre de usuario.',
						  105 => 'No tiene contrase&ntilde;a.',
						  106 => 'No has seleccionado al menos una rama.',
						  107 => '&iexcl;Oye! Tu no tienes permiso de estar aqu&iacute;. Acus&aacute;ndote con tus pap&aacute;s... <script type="text/javascript">window.setTimeout(function(){window.location.href="' . URL . 'dashboard/";}, 5000);</script>',
						  10  => 'Ese usuario no est&aacute; en la base de datos.',
						  201 => 'Ese usuario ya existe pero con otro id.');
		$texts = array('warning' => array('title' => 'El servidor encontr&oacute; este error',
										  'texts' => 'Pero no pasa nada, por favor c&aacute;mbialo y vuelve a intentar.'),
					   'dberror' => array('title' => 'El servidor encontr&oacute; este grave error',
										  'texts' => 'Por favor, ay&uacute;danos a corregirlo report&aacute;ndolo.'),
					   'dbnull'  => array('title' => 'El servidor busc&oacute; y busc&oacute; pero',
										  'texts' => 'Probablemente llegaste aqu&iacute; por error pero si crees que es un error del servidor, ay&uacute;danos a corregirlo report&aacute;ndolo.</p><p align="center"><a href="' . URL .'user/"><span class="btn btn-warning btn-sm ">Regresar</span></a>'));
		return json_encode(array('messages' => $messages, 'texts' => $texts));
	}
	
	/**
	* Is Ajax
	*
	* Función que comprueba si la petición se realiza desde ajax 
	* @return request
	* @version original: 2014/02/27 16:18 | modified:
	*/
	public function isAjax() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}

	/**
	* Compose Json
	*
	* Función que regresa el json, si hay un error crea la variable de sesión con el Json y redirije
	* @return json
	* @version original: 2014/02/27 16:25 | modified:
	*/
	public function composeJson($json, $path, $session = NULL) {
		if($this->isAjax())
			return $json;
		else {
			if(isset($session))
				$_SESSION[$session] = $json;
			header('location: ' . URL . $path);
		}
	}
	
	/*
	* Compose Errors
	* 
	* Función que crea un array con los mensajes y textos así como de la lista errores de los procesos de creación o edición
	* @param array errorArray
	* @return array errorCompose
	* @version original: 2014/02/27 16:29 | modified:
	*/
	public function composeErrors($errorArray) {
		$errorCompose = array();
		$error = array();
		$errors = array();
		$errorHandler = json_decode($this->error());
		$messages = $errorHandler->messages;
		$texts = $errorHandler->texts;
		$count = 0;
		if(in_array(0, $errorArray)) { // Error en base de datos
			$errorCompose['title'] = $texts->dberror->title;
			$errorCompose['texts'] = $texts->dberror->texts;
			$errorType = $errorArray[1];
			array_pop($errorArray);
		}
		else {
			$errorCompose['title'] = $texts->warning->title;
			$errorCompose['texts'] = $texts->warning->texts;
		}
		foreach ($errorArray as $key => $value) {
			switch($value) {
				case 0:
					$errors[$count] = array('errorcode' => $value, 'errormessage' => $messages->{$value} . ' (' . $errorType .')');
				break;
				default:
					$errors[$count] = array('errorcode' => $value, 'errormessage' => $messages->{$value});
			}
			$count++;
		} 
		$errorCompose['error'] = $errors;
		return $errorCompose;
	}
}